﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Radiosonde_Final_Check
{
    //Delegate for update sonde data to screen.
    delegate void resetFrm(object one, object two);

    delegate void updateLocks(bool state);

    public partial class frmRelay : Form
    {
        //Interface pulls.
        ComboBox[] CRMin = new ComboBox[4];
        ComboBox[] CRLen = new ComboBox[4];

        private frmMainDisplay parent;

        public frmRelay()
        {
            InitializeComponent();
        }

        private void frmRelay_Load(object sender, EventArgs e)
        {
            parent = (frmMainDisplay)this.Owner;

            updateInterface();
           
            CRMin[0] = comboBoxCR1MIN;
            CRMin[1] = comboBoxCR2MIN;
            CRMin[2] = comboBoxCR3MIN;
            CRMin[3] = comboBoxCR4MIN;

            CRLen[0] = comboBoxCR1LEN;
            CRLen[1] = comboBoxCR2LEN;
            CRLen[2] = comboBoxCR3LEN;
            CRLen[3] = comboBoxCR4LEN;

            setupComboBoxs(); //Getthing the settings to the screen.

            
            parent.ValidSondeiMet1U[0].setGPSMode(true); //Collecting GPS Data

            if (parent.ValidSondeiMet1U != null)
            {
                parent.ValidSondeiMet1U[0].GPSDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(GPSDataUpdate_PropertyChange);
            }
              
            
        }

        void GPSDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            try
            {
                this.Invoke(new SendDataToScreen(this.updateGPSTime));
            }

            catch { }
        }

        private void updateGPSTime()
        {
            try
            {
                labelSondeSN.Text = parent.ValidSondeiMet1U[0].CalData.SerialNumber;
                labelCurrentGPSTime.Text = parent.ValidSondeiMet1U[0].GPSData.GPSTime;
            }
            catch { }
        }

        private void setupComboBoxs()
        {
            //Setting up the min.
            foreach (ComboBox min in CRMin)
            {
                min.Items.Clear();
                int counterMin = 0;
                while (counterMin != 60)
                {
                    min.Items.Add(counterMin.ToString("00"));
                    counterMin++;
                }
            }

            //Setting up length of time options.
            foreach (ComboBox len in CRLen)
            {
                len.Items.Clear();
                int countLen = 1;
                while (countLen != 10)
                {
                    len.Items.Add(countLen);
                    countLen++;
                }
            }

        }

        private void buttonProgram_Click(object sender, EventArgs e)
        {

            List<Universal_Radiosonde.eventTrigger> sondeEvent = new List<Universal_Radiosonde.eventTrigger>();

            for (int x = 0; x < 4; x++)
            {
                //create trigger time objects
                Universal_Radiosonde.eventTrigger tempEvent = new Universal_Radiosonde.eventTrigger();
                tempEvent.CRLoc = x + 1;

                //Collect data form the combo boxes.
                try //Check data for XX's
                {
                    tempEvent.triggerHR = 0;
                    tempEvent.triggerMIN = Convert.ToInt16(CRMin[x].Text);
                    tempEvent.triggerTime = Convert.ToInt16(CRLen[x].Text);


                    string test = "CR" + tempEvent.CRLoc.ToString() + "=ON" + tempEvent.triggerHR.ToString("00") + tempEvent.triggerMIN.ToString("00") + tempEvent.triggerTime.ToString() + "\r\n";
                }
                catch
                {
                    //Need to indicat that this CR needs to be set to off.
                    tempEvent.triggerHR = 99;
                    tempEvent.triggerMIN = 99;
                    tempEvent.triggerTime = 9;
                }

                sondeEvent.Add(tempEvent);
            }

            System.ComponentModel.BackgroundWorker backgroundWorkerRelyProgram = new BackgroundWorker();
            backgroundWorkerRelyProgram.DoWork += new DoWorkEventHandler(backgroundWorkerRelyProgram_DoWork);
            backgroundWorkerRelyProgram.RunWorkerAsync(sondeEvent);

  
        }

        void backgroundWorkerRelyProgram_DoWork(object sender, DoWorkEventArgs e)
        {

            List<Universal_Radiosonde.eventTrigger> sondeEvent = (List<Universal_Radiosonde.eventTrigger>)e.Argument;

            this.Invoke(new updateLocks(this.updateDisplay), new object[] { true });

            parent.ValidSondeiMet1U[0].setManufacturingMode(false);
            System.Threading.Thread.Sleep(500);
            parent.ValidSondeiMet1U[0].setGPSMode(false);



            //Check times for intersections.

            foreach (Universal_Radiosonde.eventTrigger ev in sondeEvent)
            {
                parent.UpdateMainStatusLabel("Writing Event " + ev.CRLoc.ToString() + " to the Sonde");
                parent.ValidSondeiMet1U[0].setCREvent(ev);

                System.Threading.Thread.Sleep(1000);
            }

            //Saving current settings.
            parent.ValidSondeiMet1U[0].saveConfiguration();


            parent.UpdateMainStatusLabel("Event Programming Complete.");
            parent.ValidSondeiMet1U[0].setManufacturingMode(true);

            parent.ValidSondeiMet1U[0].setManufacturingMode(true);
            System.Threading.Thread.Sleep(500);
            parent.ValidSondeiMet1U[0].setGPSMode(true);

            this.Invoke(new updateLocks(this.updateDisplay), new object[] { false });


        
        }

        private void updateDisplay(bool request)
        {
            if (request)
            {
                //Coloring in the block.
                parent.colorPOSCell(1, 0, "Testing");

                groupBoxEventProgramming.Enabled = false;
                groupBoxTest.Enabled = false;
            }
            else
            {
                groupBoxEventProgramming.Enabled = true;
                groupBoxTest.Enabled = true;


                //Coloring in the block.
                parent.colorPOSCell(1, 0, "Pass"); 
            }
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            if (buttonTest.Text == "Test")
            {
                parent.ValidSondeiMet1U[0].crEventTest(true);
                buttonTest.Text = "Stop";
            }
            else
            {
                parent.ValidSondeiMet1U[0].crEventTest(false);
                buttonTest.Text = "Test";
            }
        }

        public void updateInterface()
        {
            if (parent.ValidSondeiMet1U != null) //Disabling the interfaces if there are no radiosondes.
            {
                groupBoxEventProgramming.Enabled = true;
                groupBoxTest.Enabled = true;
            }
            else
            {
                groupBoxEventProgramming.Enabled = false;
                groupBoxTest.Enabled = false;
            }
        }

        public void outsideUpdate()
        {
            try
            {
                this.Invoke(new SendDataToScreen(this.updateInterface));
            }
            catch
            {
            }
        }

        public void outsideReset()
        {
           
            try
            {
                parent.ValidSondeiMet1U[0].GPSDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(GPSDataUpdate_PropertyChange);
            }
            catch
            {
            }
        }

        private void frmRelay_FormClosed(object sender, FormClosedEventArgs e)
        {
            parent.sondeRelaySettingsToolStripMenuItem.Enabled = true;
            this.Dispose();

        }

        #region bloat code for auto setting length to 1
        private void comboBoxCR1MIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxCR1LEN.Text = "1";
        }

        private void comboBoxCR2MIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxCR2LEN.Text = "1";
        }

        private void comboBoxCR3MIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxCR3LEN.Text = "1";
        }

        private void comboBoxCR4MIN_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxCR4LEN.Text = "1";
        }

        #endregion

    }




}
