﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Radiosonde;
using Bill;

namespace Radiosonde_Final_Check
{
    public partial class frmProgrammingDisplay : Form
    {
        //Public declares.
        public iMet1[] ValidSondeArryiMet1;
        public frmMainDisplay parent;
        public BackgroundWorker[] createRadiosondexCal;

        public frmProgrammingDisplay()
        {
            InitializeComponent();
        }

        static class programSettings
        {
            public static string TXMode = "Bell202";
            public static string hsp = "-5";
            public static string pturr = "1";
            public static string gpsrr = "1";
            public static string stat = "off";
            public static string ptu = "off";
            public static string gps = "off";
            public static string ptux = "on";
            public static string gpsx = "off";
            public static string txfreqSWP1 = "402.0";
            public static string txfreqSWP2 = "403.0";
            public static string txfreqSWP3 = "404.0";
            public static string txfreqSWP4 = "405.0";
            public static string programmingComPort = "COM999";

        }

        private void frmProgrammingDisplay_Load(object sender, EventArgs e)
        {
            parent = (frmMainDisplay)this.Owner;

            this.Left = (parent.Location.X + parent.Size.Width);
            this.Top = (parent.Location.Y);
            
            loadCurrentSettings(); //Loading current settings.
            updateDisplayxCalSettings(); //Updating current setting to screen.

            updateToolStripStatus();
            updateInterface();
        }

        public void updateToolStripStatus()
        {
            if (ValidSondeArryiMet1 != null)
            {
                toolStripStatusLabelProgramming.Text = "Ready....";
            }
            else
            {
                toolStripStatusLabelProgramming.Text = "No Radiosondes Found.";
            }
        }

        public void updateInterface()
        {
            if (ValidSondeArryiMet1 != null) //Disabling the interfaces if there are no radiosondes.
            {
                groupBoxOptions.Enabled = true;
                groupBoxControls.Enabled = true;
                //groupBoxProgrammerControls.Enabled = true;
            }
            else
            {
                groupBoxOptions.Enabled = false;
                groupBoxControls.Enabled = false;
                //groupBoxProgrammerControls.Enabled = false;
            }
        }

        #region This section load settings then displays current settings for creating xCal files.
        public void loadCurrentSettings()
        {
            string settingsFile = System.IO.File.ReadAllText("ProgrammingSettings.cfg");
            string[] settingsFileElements = settingsFile.Split('\n', '=');

            for (int i = 0; i < settingsFileElements.Length; i++)
            {
                switch (settingsFileElements[i])
                {
                    case "txmode":
                        programSettings.TXMode = settingsFileElements[i + 1];
                        break;

                    case "hsp":
                        programSettings.hsp = settingsFileElements[i + 1];
                        break;

                    case "pturr":
                        programSettings.pturr = settingsFileElements[i + 1];
                        break;

                    case "gpsrr":
                        programSettings.gpsrr = settingsFileElements[i + 1];
                        break;
                        
                    case "stat":
                        programSettings.stat = settingsFileElements[i + 1];
                        break;

                    case "ptu":
                        programSettings.ptu = settingsFileElements[i + 1];
                        break;

                    case "gps":
                        programSettings.gps = settingsFileElements[i + 1];
                        break;

                    case "ptux":
                        programSettings.ptux = settingsFileElements[i + 1];
                        break;

                    case "gpsx":
                        programSettings.gpsx = settingsFileElements[i + 1];
                        break;

                    case "txfreq":
                        string[] txFrequancies = settingsFileElements[i + 1].Split(',');
                        programSettings.txfreqSWP1 = txFrequancies[0];
                        programSettings.txfreqSWP2 = txFrequancies[1];
                        programSettings.txfreqSWP3 = txFrequancies[2];
                        programSettings.txfreqSWP4 = txFrequancies[3];
                        break;

                    case "programmerPort":
                        programSettings.programmingComPort = settingsFileElements[i + 1];
                        break;

                }
            }
        }

        public void updateDisplayxCalSettings()
        {
            textBoxTXMode.Text = programSettings.TXMode;
            textBoxHSP.Text = programSettings.hsp;
            textBoxPTURR.Text = programSettings.pturr;
            textBoxGPSRR.Text = programSettings.gpsrr;
            textBoxStat.Text = programSettings.stat;
            textBoxPTU.Text = programSettings.ptu;
            textBoxGPS.Text = programSettings.gps;
            textBoxPTUX.Text = programSettings.ptux;
            textBoxGPSX.Text = programSettings.gpsx;
            textBoxFreqP1.Text = programSettings.txfreqSWP1;
            textBoxFreqP2.Text = programSettings.txfreqSWP2;
            textBoxFreqP3.Text = programSettings.txfreqSWP3;
            textBoxFreqP4.Text = programSettings.txfreqSWP4;
        }

        #endregion



        private void frmProgrammingDisplay_FormClosed(object sender, FormClosedEventArgs e)
        {
            parent.sondeProgrammingToolStripMenuItem.Enabled = true;
            parent.UpdateMainStatusLabel("Radiosonde Programer Closed.");
        }

        private void buttonChangeSNLetter_Click(object sender, EventArgs e)
        {
            if (ValidSondeArryiMet1 != null)
            {
                string value = "";
                if (Bill.Interfaces.InputBox("New Serial Number Letter", "Input Letter A-Z", ref value) == DialogResult.OK)
                {
                    try
                    {
                        Convert.ToInt32(value);
                        MessageBox.Show("Incorrect input. Input char: A-Z","Input Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    catch { }

                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        parent.UpdateMainStatusLabel(ValidSondeArryiMet1[i].getSerialNum() + " changed to: " + value.ToUpper() + ValidSondeArryiMet1[i].getSerialNum().Substring(1, ValidSondeArryiMet1[i].getSerialNum().Length - 1));
                        ValidSondeArryiMet1[i].setSerialNum(value.ToUpper() + ValidSondeArryiMet1[i].getSerialNum().Substring(1, ValidSondeArryiMet1[i].getSerialNum().Length - 1));
                    }
                }
                else
                {
                    MessageBox.Show("SN Letter update aborted.", "SN Abouted.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("No Current Radiosondes.\nPlease search and try again.","No current radiosonde error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonWriteSNTNToAll_Click(object sender, EventArgs e)
        {
            if (ValidSondeArryiMet1 != null)
            {
                string value = "";
                Int32 inputNumber;

                int radiosondeRackLOC = 0;
                int radiosondeRackPOSLOC = 0;

                if (Bill.Interfaces.InputBox("Load SN/TN to all active radiosondes.", "Input the starting SN Number \"12345\"", ref value) == DialogResult.OK)
                {
                    try
                    {
                        try
                        {
                            inputNumber = Convert.ToInt32(value);
                        }
                        catch
                        {
                            inputNumber = Convert.ToInt32(value.Substring(1,value.Length-1));
                        }
                    }
                    catch(Exception error)
                    {
                        MessageBox.Show(error.Message, "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }


                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        parent.UpdateMainStatusLabel("Writing SN S" + inputNumber.ToString() + " to: " + ValidSondeArryiMet1[i].ComPort.PortName.ToString());
                        parent.determinSondeRackPos(ref radiosondeRackLOC, ref radiosondeRackPOSLOC, i);
                        parent.colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Testing");

                        bool status = ValidSondeArryiMet1[i].setSerialNum("S" + (inputNumber + i).ToString());
                        if (status)
                        {
                            parent.colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Pass");
                        }
                        else
                        {
                            parent.colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Fail");
                        }
                    }

                    System.Threading.Thread.Sleep(1200); //Taking a pause between SN and TN.

                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        parent.UpdateMainStatusLabel("Writing TN S" + inputNumber.ToString() + " to: " + ValidSondeArryiMet1[i].ComPort.PortName.ToString());
                        parent.determinSondeRackPos(ref radiosondeRackLOC, ref radiosondeRackPOSLOC, i);
                        parent.colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Testing");
                        
                        bool status = ValidSondeArryiMet1[i].setTrackingID("S" + (inputNumber + i).ToString());
                        if (status)
                        {
                            parent.colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Pass");
                        }
                        else
                        {
                            parent.colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Fail");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("SN/TN write aborted.", "SN/TN Abouted.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("No Current Radiosondes.\nPlease search and try again.", "No current radiosonde error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void buttonWritexCalFiles_Click(object sender, EventArgs e)
        {
            int amountOfBackgroundWorkers = parent.howManyBackgroundWorkers(ValidSondeArryiMet1.Length)/2;

            //Serial Number(s)
            string[] trackingNumberArray = new string[ValidSondeArryiMet1.Length];
            for (int i = 0; i < trackingNumberArray.Length; i++)
            {
                trackingNumberArray[i] = ValidSondeArryiMet1[i].getTrackingID();
            }

            //Creating the background workers.
            createRadiosondexCal = new BackgroundWorker[amountOfBackgroundWorkers];
            for (int i = 0; i < amountOfBackgroundWorkers; i++)
            {
                createRadiosondexCal[i] = new BackgroundWorker();
                createRadiosondexCal[i].WorkerSupportsCancellation = true;
                createRadiosondexCal[i].DoWork += new DoWorkEventHandler(createRadiosondexCal_DoWork);
                createRadiosondexCal[i].RunWorkerCompleted += new RunWorkerCompletedEventHandler(createRadiosondexCal_RunWorkerCompleted);
            }

            //Stopping Radiosonde data
            parent.setRadiosondeStatusProcessRadiosondeHex(false);

            startCreateRadiosondexCalBackgroundWorkersArray(amountOfBackgroundWorkers, trackingNumberArray, 0);

        }

        private void startCreateRadiosondexCalBackgroundWorkersArray(int amountOfBackgroundWorkers, string[] trackingNumberArray, int coefficentLoadType)
        {
            //Spliting the work up between the background workers.
            //As well as setting the array up that will be sent to the background worker
            int dataDividedUp = trackingNumberArray.Length / amountOfBackgroundWorkers;
            int dataToBeSentOutCounter = 0;
            int arrayIndex = 0;
            string[,] dataToBeSentOut = new string[amountOfBackgroundWorkers, 200];
            int counterTrackingNumberArrayIndex = 0;

            //The following code breaks down the incoming array to a 2d array. It also includes the "mode" the 
            //provided list of serial number will be processed in. IE load AT, UT and so on.
            //index 0 of each array is that "mode" type.
            for (int i = 0; i < trackingNumberArray.Length + amountOfBackgroundWorkers; i++)
            {
                if (dataToBeSentOutCounter == 0 && arrayIndex < amountOfBackgroundWorkers)
                {
                    dataToBeSentOut[arrayIndex, 0] = coefficentLoadType.ToString();
                }

                if (dataToBeSentOutCounter > 0)
                {
                    dataToBeSentOut[arrayIndex, dataToBeSentOutCounter] = trackingNumberArray[counterTrackingNumberArrayIndex];
                    counterTrackingNumberArrayIndex++;
                }

                if (arrayIndex == amountOfBackgroundWorkers - 1 && dataToBeSentOutCounter >= dataDividedUp && amountOfBackgroundWorkers > 1)
                {
                    for (int m = 1; m < (trackingNumberArray.Length - counterTrackingNumberArrayIndex) + 1; m++)
                    {
                        dataToBeSentOut[arrayIndex, m + dataToBeSentOutCounter] = trackingNumberArray[(m + counterTrackingNumberArrayIndex - 1)];
                    }
                    break;
                }

                if (dataToBeSentOutCounter == dataDividedUp)
                {
                    dataToBeSentOutCounter = 0;
                    arrayIndex++;
                }
                else
                {
                    dataToBeSentOutCounter++;
                }
            }

            //Starting the Background Works and send them there commands and serial number to handle.
            for (int i = 0; i < amountOfBackgroundWorkers; i++)
            {
                //breaking down the 2d array into a singal array that can be sent to the background worker.
                string[] dataToBackgroundWorker = new string[dataToBeSentOut.Length / amountOfBackgroundWorkers];
                for (int m = 0; m < dataToBeSentOut.Length / amountOfBackgroundWorkers; m++)
                {
                    dataToBackgroundWorker[m] = dataToBeSentOut[i, m];
                }

                //starting the backgound worker and sending it its work load.
                if (!createRadiosondexCal[i].IsBusy)
                {
                    createRadiosondexCal[i].RunWorkerAsync(dataToBackgroundWorker);
                }
                dataToBackgroundWorker = null;
            }
        }

        private void createRadiosondexCal_DoWork(object sender, DoWorkEventArgs e)
        {
            int loopCounter = 0;
            int radiosondeRackLOC = 0;
            int radiosondeRackPOSLOC = 0;
            BackgroundWorker worker = sender as BackgroundWorker;

            //breaking recived event data into a string array.
            string[] trackingNumbers = e.Argument as string[];

            //Compiling coeffient int request array
            int[] requestedCoeffients = new int[44];
            for(int x = 0; x<requestedCoeffients.Length; x++)
            {
                requestedCoeffients[x] = x;
            }

            for (int i = 1; i < trackingNumbers.Length; i++)
            {
                for (int j = 0; j < ValidSondeArryiMet1.Length; j++)
                {
                    if (trackingNumbers[i] == ValidSondeArryiMet1[j].getTrackingID())
                    {
                        parent.UpdateMainStatusLabel("Creating xCal File for: " + ValidSondeArryiMet1[j].getTrackingID());
                    retryGetCoefficents: //A retry goto.
                        parent.determinSondeRackPos(ref radiosondeRackLOC, ref radiosondeRackPOSLOC, j);
                        parent.colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Testing");
                        string[] radiosondeCoefficents = ValidSondeArryiMet1[j].getCoefficentArray(requestedCoeffients);


                        if (radiosondeCoefficents == null || radiosondeCoefficents.Length < 44 && loopCounter < 4)
                        {
                            loopCounter++;
                            parent.UpdateMainStatusLabel("Error in creating xCal for: " + ValidSondeArryiMet1[j].getTrackingID() + "(Retry #" + loopCounter.ToString() + ")");
                            goto retryGetCoefficents;
                        }


                        if (radiosondeCoefficents[43] != null)
                        {
                            //If the coeffients are collected properly I will collect the rest of the data needed and write the xcal file.

                            // create a writer and open the file
                            System.IO.FileInfo t = new System.IO.FileInfo(parent.getCurrentxCalLOC() + ValidSondeArryiMet1[j].getTrackingID() + ".cal");
                            System.IO.StreamWriter xCalCoefficentFile = t.CreateText();


                            // write a line of text to the file
                            //programSettingFile.WriteLine("RackAStart=COM" + ProgramSettings.StartRackARadiosondeComms.ToString());
                            for (int w = 0; w < radiosondeCoefficents.Length; w++)
                            {
                                string[] split = radiosondeCoefficents[w].Split('=');
                                xCalCoefficentFile.WriteLine("cal[" + w + "]=" + split[1]);
                            }
      
                            xCalCoefficentFile.WriteLine("sn=" + ValidSondeArryiMet1[j].getSerialNum());
                            xCalCoefficentFile.WriteLine("sid=" + ValidSondeArryiMet1[j].getTrackingID());
                            xCalCoefficentFile.WriteLine("pid=" + ValidSondeArryiMet1[j].getTUProbeID());
                            xCalCoefficentFile.WriteLine("txmode=" + programSettings.TXMode);
                            xCalCoefficentFile.WriteLine("hsp=" + programSettings.hsp);
                            xCalCoefficentFile.WriteLine("pturr=" + programSettings.pturr);
                            xCalCoefficentFile.WriteLine("gpsrr=" + programSettings.gpsrr);
                            xCalCoefficentFile.WriteLine("stat=" + programSettings.stat);
                            xCalCoefficentFile.WriteLine("ptu=" + programSettings.ptu);
                            xCalCoefficentFile.WriteLine("gps=" + programSettings.gps);
                            xCalCoefficentFile.WriteLine("ptux=" + programSettings.ptux);
                            xCalCoefficentFile.WriteLine("gpsx=" + programSettings.gpsx);
                            xCalCoefficentFile.WriteLine("txfreq=" + programSettings.txfreqSWP1 + "," + programSettings.txfreqSWP2 + "," + programSettings.txfreqSWP3 + "," + programSettings.txfreqSWP4);
                            // close the stream
                            xCalCoefficentFile.Close();

                            parent.colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Pass");
                            parent.UpdateMainStatusLabel("xCal File for: " + ValidSondeArryiMet1[j].getTrackingID() + " Completed."); 
                        }
                        else
                        {
                            parent.colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Fail");
                            parent.UpdateMainStatusLabel("xCal File for: " + ValidSondeArryiMet1[j].getTrackingID() + " Aborted.");
                            parent.UpdateMainStatusLabel("Error Message: " + radiosondeCoefficents[0]);
                        }

                    }
                }
            }
        }

        private void createRadiosondexCal_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Resetting global coefficent background worker array.
            bool[] allBackgroundWorkersComplete = new bool[createRadiosondexCal.Length];
            int completeWorkersCounter = 0;
            for (int i = 0; i < createRadiosondexCal.Length; i++)
            {
                if (createRadiosondexCal[i].IsBusy == false)
                {
                    allBackgroundWorkersComplete[i] = true;
                }
            }

            for (int i = 0; i < allBackgroundWorkersComplete.Length; i++)
            {
                completeWorkersCounter = completeWorkersCounter + Convert.ToInt16(allBackgroundWorkersComplete[i]);
            }

            if (completeWorkersCounter == createRadiosondexCal.Length)
            {
                createRadiosondexCal = null; //Resetting background worker array.
                parent.UpdateMainStatusLabel("Clearing all radiosonde buffers. Please stand by.");

                //Clearing all radiosonde buffers.
                for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                {
                    ValidSondeArryiMet1[i].ComPort.DiscardInBuffer();
                }

                parent.setRadiosondeStatusProcessRadiosondeHex(true); //Restarting data collection.
                parent.UpdateMainStatusLabel("All xCal Files written.");

                buttonWritexCalFiles.Enabled = true;
            }
        }

        private void buttonSaveSettings_Click(object sender, EventArgs e)
        {
            // create a writer and open the file
            System.IO.FileInfo t = new System.IO.FileInfo("ProgrammingSettings.cfg");
            System.IO.StreamWriter programmingConfigFile = t.CreateText();

            // writing a lines of text to the file
            programmingConfigFile.WriteLine("txmode=" + textBoxTXMode.Text);
            programmingConfigFile.WriteLine("hsp=" + textBoxHSP.Text);
            programmingConfigFile.WriteLine("pturr=" + textBoxPTURR.Text);
            programmingConfigFile.WriteLine("gpsrr=" + textBoxGPSRR.Text);
            programmingConfigFile.WriteLine("stat=" + textBoxStat.Text);
            programmingConfigFile.WriteLine("ptu=" + textBoxPTU.Text);
            programmingConfigFile.WriteLine("gps=" + textBoxGPS.Text);
            programmingConfigFile.WriteLine("ptux=" + textBoxPTUX.Text);
            programmingConfigFile.WriteLine("gpsx=" + textBoxGPSX.Text);
            programmingConfigFile.WriteLine("txfreq=" + textBoxFreqP1.Text + "," + textBoxFreqP2.Text + "," + textBoxFreqP3.Text + "," + textBoxFreqP4.Text);
            programmingConfigFile.WriteLine("programmerPort=COM99");

            // close the stream
            programmingConfigFile.Close();
        }

    }
}
