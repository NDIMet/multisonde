﻿namespace Radiosonde_Final_Check
{
    partial class frmMainDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainDisplay));
            this.menuStripMainDisplay = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSnapshotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intervalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.minToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.logFormatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multiSondeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iMet1TestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.startLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pauseLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondeProgrammingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondeAuxSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sondeRelaySettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripMainDisplay = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelMainDisplay = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelLogging = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBoxStatus = new System.Windows.Forms.GroupBox();
            this.groupBoxControlSondes = new System.Windows.Forms.GroupBox();
            this.buttonControlSondeSearch = new System.Windows.Forms.Button();
            this.comboBoxSondeType = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonControlSondeReconnect = new System.Windows.Forms.Button();
            this.buttonControlSondeStop = new System.Windows.Forms.Button();
            this.groupBoxTransmitterStatus = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelCurrentTXSondeID = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBoxCurrentGPS = new System.Windows.Forms.GroupBox();
            this.labelCurrentSatCount = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labelCurrentLat = new System.Windows.Forms.Label();
            this.labelCurrentLong = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelCurrentAlt = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBoxCurrentPTU = new System.Windows.Forms.GroupBox();
            this.checkBoxCorrection = new System.Windows.Forms.CheckBox();
            this.comboBoxSensorPressure = new System.Windows.Forms.ComboBox();
            this.comboBoxSensorTempHumidity = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelChamberTemperature = new System.Windows.Forms.Label();
            this.labelChamberHumidity = new System.Windows.Forms.Label();
            this.labelSensorPressure = new System.Windows.Forms.Label();
            this.labelSensorTemperature = new System.Windows.Forms.Label();
            this.labelSensorHumidity = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBoxControls = new System.Windows.Forms.GroupBox();
            this.groupBoxControlsEnv = new System.Windows.Forms.GroupBox();
            this.buttonStopConditions = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxDesiredPressure = new System.Windows.Forms.TextBox();
            this.buttonHold = new System.Windows.Forms.Button();
            this.textBoxDesiredTemperature = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonRunConditions = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxDesiredHumidity = new System.Windows.Forms.TextBox();
            this.tabControlRackDisplay = new System.Windows.Forms.TabControl();
            this.tabPageRackA = new System.Windows.Forms.TabPage();
            this.statusStripRackA = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelRackA = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridViewRackA = new System.Windows.Forms.DataGridView();
            this.ColumnPOS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSondeSN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSondeTN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSondePN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSondeCom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPressureTemp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPressure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPDiff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTemperature = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTDiff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnHumidity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnUDiff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFirmware = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTXMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageRackB = new System.Windows.Forms.TabPage();
            this.statusStripRackB = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelRackB = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridViewRackB = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageRackC = new System.Windows.Forms.TabPage();
            this.dataGridViewRackC = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStripRackC = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelRackC = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabPageRackD = new System.Windows.Forms.TabPage();
            this.dataGridViewRackD = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStripRackD = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelRackD = new System.Windows.Forms.ToolStripStatusLabel();
            this.backgroundWorkerUpdateTempHumidity = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerUpdatePressure = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerRadiosondeDataCollect = new System.ComponentModel.BackgroundWorker();
            this.tabControlCoefficent = new System.Windows.Forms.TabControl();
            this.tabPageCoefficentAll = new System.Windows.Forms.TabPage();
            this.buttonLoadCancel = new System.Windows.Forms.Button();
            this.buttonLoadAllCoefficentAll = new System.Windows.Forms.Button();
            this.buttonLoadAllCoefficentPressure = new System.Windows.Forms.Button();
            this.buttonLoadAllCoefficentHumidity = new System.Windows.Forms.Button();
            this.buttonLoadAllCoefficentHumidityTemp = new System.Windows.Forms.Button();
            this.buttonLoadAllCoefficentAirTemp = new System.Windows.Forms.Button();
            this.tabPageCoefficentSingal = new System.Windows.Forms.TabPage();
            this.buttonClearReport = new System.Windows.Forms.Button();
            this.buttonLoadSingleCoefficentAll = new System.Windows.Forms.Button();
            this.buttonLoadSingleCoefficentPressure = new System.Windows.Forms.Button();
            this.buttonLoadSingleCoefficentHumidity = new System.Windows.Forms.Button();
            this.buttonLoadSingleCoefficentHumidityTemp = new System.Windows.Forms.Button();
            this.buttonLoadSingleCoefficentAirTemp = new System.Windows.Forms.Button();
            this.groupBoxCoefficent = new System.Windows.Forms.GroupBox();
            this.pressureManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripMainDisplay.SuspendLayout();
            this.statusStripMainDisplay.SuspendLayout();
            this.groupBoxStatus.SuspendLayout();
            this.groupBoxControlSondes.SuspendLayout();
            this.groupBoxTransmitterStatus.SuspendLayout();
            this.groupBoxCurrentGPS.SuspendLayout();
            this.groupBoxCurrentPTU.SuspendLayout();
            this.groupBoxControls.SuspendLayout();
            this.groupBoxControlsEnv.SuspendLayout();
            this.tabControlRackDisplay.SuspendLayout();
            this.tabPageRackA.SuspendLayout();
            this.statusStripRackA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRackA)).BeginInit();
            this.tabPageRackB.SuspendLayout();
            this.statusStripRackB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRackB)).BeginInit();
            this.tabPageRackC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRackC)).BeginInit();
            this.statusStripRackC.SuspendLayout();
            this.tabPageRackD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRackD)).BeginInit();
            this.statusStripRackD.SuspendLayout();
            this.tabControlCoefficent.SuspendLayout();
            this.tabPageCoefficentAll.SuspendLayout();
            this.tabPageCoefficentSingal.SuspendLayout();
            this.groupBoxCoefficent.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripMainDisplay
            // 
            this.menuStripMainDisplay.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.loggingToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStripMainDisplay.Location = new System.Drawing.Point(0, 0);
            this.menuStripMainDisplay.Name = "menuStripMainDisplay";
            this.menuStripMainDisplay.Size = new System.Drawing.Size(849, 24);
            this.menuStripMainDisplay.TabIndex = 0;
            this.menuStripMainDisplay.Text = "menuStripMainDisplay";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveSnapshotToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveSnapshotToolStripMenuItem
            // 
            this.saveSnapshotToolStripMenuItem.Name = "saveSnapshotToolStripMenuItem";
            this.saveSnapshotToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.saveSnapshotToolStripMenuItem.Text = "Save Snapshot";
            this.saveSnapshotToolStripMenuItem.Click += new System.EventHandler(this.saveSnapshotToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.changeUserToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // changeUserToolStripMenuItem
            // 
            this.changeUserToolStripMenuItem.Name = "changeUserToolStripMenuItem";
            this.changeUserToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.changeUserToolStripMenuItem.Text = "Change User";
            this.changeUserToolStripMenuItem.Click += new System.EventHandler(this.changeUserToolStripMenuItem_Click);
            // 
            // loggingToolStripMenuItem
            // 
            this.loggingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newLogToolStripMenuItem,
            this.openLogToolStripMenuItem,
            this.intervalsToolStripMenuItem,
            this.toolStripSeparator2,
            this.logFormatToolStripMenuItem,
            this.toolStripSeparator1,
            this.startLoggingToolStripMenuItem,
            this.pauseLoggingToolStripMenuItem,
            this.stopLoggingToolStripMenuItem});
            this.loggingToolStripMenuItem.Name = "loggingToolStripMenuItem";
            this.loggingToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.loggingToolStripMenuItem.Text = "Logging";
            // 
            // newLogToolStripMenuItem
            // 
            this.newLogToolStripMenuItem.Name = "newLogToolStripMenuItem";
            this.newLogToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newLogToolStripMenuItem.Text = "New Log";
            this.newLogToolStripMenuItem.Click += new System.EventHandler(this.newLogToolStripMenuItem_Click);
            // 
            // openLogToolStripMenuItem
            // 
            this.openLogToolStripMenuItem.Name = "openLogToolStripMenuItem";
            this.openLogToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.openLogToolStripMenuItem.Text = "Open Log";
            this.openLogToolStripMenuItem.Click += new System.EventHandler(this.openLogToolStripMenuItem_Click);
            // 
            // intervalsToolStripMenuItem
            // 
            this.intervalsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.minToolStripMenuItem,
            this.minToolStripMenuItem1});
            this.intervalsToolStripMenuItem.Name = "intervalsToolStripMenuItem";
            this.intervalsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.intervalsToolStripMenuItem.Text = "Intervals";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItem2.Text = "1/1 Sec";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItem3.Text = "1/10 Sec";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(121, 22);
            this.toolStripMenuItem4.Text = "1/60 Sec";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // minToolStripMenuItem
            // 
            this.minToolStripMenuItem.Name = "minToolStripMenuItem";
            this.minToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.minToolStripMenuItem.Text = "1/5 Min";
            this.minToolStripMenuItem.Click += new System.EventHandler(this.minToolStripMenuItem_Click);
            // 
            // minToolStripMenuItem1
            // 
            this.minToolStripMenuItem1.Name = "minToolStripMenuItem1";
            this.minToolStripMenuItem1.Size = new System.Drawing.Size(121, 22);
            this.minToolStripMenuItem1.Text = "1/15 Min";
            this.minToolStripMenuItem1.Click += new System.EventHandler(this.minToolStripMenuItem1_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(149, 6);
            // 
            // logFormatToolStripMenuItem
            // 
            this.logFormatToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.multiSondeToolStripMenuItem,
            this.iMet1TestToolStripMenuItem});
            this.logFormatToolStripMenuItem.Enabled = false;
            this.logFormatToolStripMenuItem.Name = "logFormatToolStripMenuItem";
            this.logFormatToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.logFormatToolStripMenuItem.Text = "Log Format";
            // 
            // multiSondeToolStripMenuItem
            // 
            this.multiSondeToolStripMenuItem.CheckOnClick = true;
            this.multiSondeToolStripMenuItem.Name = "multiSondeToolStripMenuItem";
            this.multiSondeToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.multiSondeToolStripMenuItem.Text = "Multi Sonde";
            // 
            // iMet1TestToolStripMenuItem
            // 
            this.iMet1TestToolStripMenuItem.CheckOnClick = true;
            this.iMet1TestToolStripMenuItem.Name = "iMet1TestToolStripMenuItem";
            this.iMet1TestToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.iMet1TestToolStripMenuItem.Text = "iMet-1 Test";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // startLoggingToolStripMenuItem
            // 
            this.startLoggingToolStripMenuItem.Enabled = false;
            this.startLoggingToolStripMenuItem.Name = "startLoggingToolStripMenuItem";
            this.startLoggingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.startLoggingToolStripMenuItem.Text = "Start Logging";
            this.startLoggingToolStripMenuItem.Click += new System.EventHandler(this.startLoggingToolStripMenuItem_Click);
            // 
            // pauseLoggingToolStripMenuItem
            // 
            this.pauseLoggingToolStripMenuItem.Enabled = false;
            this.pauseLoggingToolStripMenuItem.Name = "pauseLoggingToolStripMenuItem";
            this.pauseLoggingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.pauseLoggingToolStripMenuItem.Text = "Pause Logging";
            this.pauseLoggingToolStripMenuItem.Click += new System.EventHandler(this.pauseLoggingToolStripMenuItem_Click);
            // 
            // stopLoggingToolStripMenuItem
            // 
            this.stopLoggingToolStripMenuItem.Enabled = false;
            this.stopLoggingToolStripMenuItem.Name = "stopLoggingToolStripMenuItem";
            this.stopLoggingToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.stopLoggingToolStripMenuItem.Text = "Stop Logging";
            this.stopLoggingToolStripMenuItem.Click += new System.EventHandler(this.stopLoggingToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sondeProgrammingToolStripMenuItem,
            this.sondeAuxSettingsToolStripMenuItem,
            this.testToolStripMenuItem,
            this.sondeRelaySettingsToolStripMenuItem,
            this.pressureManagerToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // sondeProgrammingToolStripMenuItem
            // 
            this.sondeProgrammingToolStripMenuItem.Name = "sondeProgrammingToolStripMenuItem";
            this.sondeProgrammingToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.sondeProgrammingToolStripMenuItem.Text = "Sonde Programming";
            this.sondeProgrammingToolStripMenuItem.Click += new System.EventHandler(this.sondeProgrammingToolStripMenuItem_Click);
            // 
            // sondeAuxSettingsToolStripMenuItem
            // 
            this.sondeAuxSettingsToolStripMenuItem.Name = "sondeAuxSettingsToolStripMenuItem";
            this.sondeAuxSettingsToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.sondeAuxSettingsToolStripMenuItem.Text = "Sonde TX Settings";
            this.sondeAuxSettingsToolStripMenuItem.Click += new System.EventHandler(this.sondeAuxSettingsToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.testToolStripMenuItem.Text = "test";
            this.testToolStripMenuItem.Visible = false;
            this.testToolStripMenuItem.Click += new System.EventHandler(this.testToolStripMenuItem_Click);
            // 
            // sondeRelaySettingsToolStripMenuItem
            // 
            this.sondeRelaySettingsToolStripMenuItem.Enabled = false;
            this.sondeRelaySettingsToolStripMenuItem.Name = "sondeRelaySettingsToolStripMenuItem";
            this.sondeRelaySettingsToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.sondeRelaySettingsToolStripMenuItem.Text = "Sonde Relay Settings";
            this.sondeRelaySettingsToolStripMenuItem.Click += new System.EventHandler(this.sondeRelaySettingsToolStripMenuItem_Click);
            // 
            // statusStripMainDisplay
            // 
            this.statusStripMainDisplay.AutoSize = false;
            this.statusStripMainDisplay.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelMainDisplay,
            this.toolStripStatusLabelLogging});
            this.statusStripMainDisplay.Location = new System.Drawing.Point(0, 752);
            this.statusStripMainDisplay.Name = "statusStripMainDisplay";
            this.statusStripMainDisplay.Size = new System.Drawing.Size(849, 46);
            this.statusStripMainDisplay.Stretch = false;
            this.statusStripMainDisplay.TabIndex = 1;
            this.statusStripMainDisplay.Text = "statusStripMainDisplay";
            this.statusStripMainDisplay.DoubleClick += new System.EventHandler(this.statusStripMainDisplay_DoubleClick);
            // 
            // toolStripStatusLabelMainDisplay
            // 
            this.toolStripStatusLabelMainDisplay.AutoSize = false;
            this.toolStripStatusLabelMainDisplay.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.toolStripStatusLabelMainDisplay.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabelMainDisplay.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.toolStripStatusLabelMainDisplay.Name = "toolStripStatusLabelMainDisplay";
            this.toolStripStatusLabelMainDisplay.Size = new System.Drawing.Size(638, 41);
            this.toolStripStatusLabelMainDisplay.Text = "Loaded.";
            this.toolStripStatusLabelMainDisplay.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // toolStripStatusLabelLogging
            // 
            this.toolStripStatusLabelLogging.AutoSize = false;
            this.toolStripStatusLabelLogging.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabelLogging.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.toolStripStatusLabelLogging.Name = "toolStripStatusLabelLogging";
            this.toolStripStatusLabelLogging.Size = new System.Drawing.Size(180, 41);
            this.toolStripStatusLabelLogging.Text = "No current logging.";
            // 
            // groupBoxStatus
            // 
            this.groupBoxStatus.Controls.Add(this.groupBoxControlSondes);
            this.groupBoxStatus.Controls.Add(this.groupBoxTransmitterStatus);
            this.groupBoxStatus.Controls.Add(this.groupBoxCurrentGPS);
            this.groupBoxStatus.Controls.Add(this.groupBoxCurrentPTU);
            this.groupBoxStatus.Location = new System.Drawing.Point(646, 27);
            this.groupBoxStatus.Name = "groupBoxStatus";
            this.groupBoxStatus.Size = new System.Drawing.Size(200, 233);
            this.groupBoxStatus.TabIndex = 3;
            this.groupBoxStatus.TabStop = false;
            this.groupBoxStatus.Text = "System Status";
            // 
            // groupBoxControlSondes
            // 
            this.groupBoxControlSondes.Controls.Add(this.buttonControlSondeSearch);
            this.groupBoxControlSondes.Controls.Add(this.comboBoxSondeType);
            this.groupBoxControlSondes.Controls.Add(this.label12);
            this.groupBoxControlSondes.Controls.Add(this.buttonControlSondeReconnect);
            this.groupBoxControlSondes.Controls.Add(this.buttonControlSondeStop);
            this.groupBoxControlSondes.Location = new System.Drawing.Point(7, 19);
            this.groupBoxControlSondes.Name = "groupBoxControlSondes";
            this.groupBoxControlSondes.Size = new System.Drawing.Size(187, 92);
            this.groupBoxControlSondes.TabIndex = 1;
            this.groupBoxControlSondes.TabStop = false;
            this.groupBoxControlSondes.Text = "Sondes";
            // 
            // buttonControlSondeSearch
            // 
            this.buttonControlSondeSearch.Location = new System.Drawing.Point(13, 38);
            this.buttonControlSondeSearch.Name = "buttonControlSondeSearch";
            this.buttonControlSondeSearch.Size = new System.Drawing.Size(75, 23);
            this.buttonControlSondeSearch.TabIndex = 1;
            this.buttonControlSondeSearch.Text = "Search";
            this.buttonControlSondeSearch.UseVisualStyleBackColor = true;
            this.buttonControlSondeSearch.Click += new System.EventHandler(this.buttonControlSondeSearch_Click);
            // 
            // comboBoxSondeType
            // 
            this.comboBoxSondeType.FormattingEnabled = true;
            this.comboBoxSondeType.Items.AddRange(new object[] {
            "iMet-TX-HUM",
            "iMet-TX-E+E",
            "iMet Universal",
            "iMet 2.3x"});
            this.comboBoxSondeType.Location = new System.Drawing.Point(73, 13);
            this.comboBoxSondeType.Name = "comboBoxSondeType";
            this.comboBoxSondeType.Size = new System.Drawing.Size(108, 21);
            this.comboBoxSondeType.TabIndex = 4;
            this.comboBoxSondeType.Text = "iMet Universal";
            this.comboBoxSondeType.SelectedIndexChanged += new System.EventHandler(this.comboBoxSondeType_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Sonde Type:";
            this.label12.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.label12_MouseDoubleClick);
            // 
            // buttonControlSondeReconnect
            // 
            this.buttonControlSondeReconnect.Enabled = false;
            this.buttonControlSondeReconnect.Location = new System.Drawing.Point(56, 64);
            this.buttonControlSondeReconnect.Name = "buttonControlSondeReconnect";
            this.buttonControlSondeReconnect.Size = new System.Drawing.Size(75, 23);
            this.buttonControlSondeReconnect.TabIndex = 2;
            this.buttonControlSondeReconnect.Text = "Reconnect";
            this.buttonControlSondeReconnect.UseVisualStyleBackColor = true;
            this.buttonControlSondeReconnect.Click += new System.EventHandler(this.buttonControlSondeReconnect_Click);
            // 
            // buttonControlSondeStop
            // 
            this.buttonControlSondeStop.Enabled = false;
            this.buttonControlSondeStop.Location = new System.Drawing.Point(98, 38);
            this.buttonControlSondeStop.Name = "buttonControlSondeStop";
            this.buttonControlSondeStop.Size = new System.Drawing.Size(75, 23);
            this.buttonControlSondeStop.TabIndex = 1;
            this.buttonControlSondeStop.Text = "All Stop";
            this.buttonControlSondeStop.UseVisualStyleBackColor = true;
            this.buttonControlSondeStop.Click += new System.EventHandler(this.buttonControlSondeStop_Click);
            // 
            // groupBoxTransmitterStatus
            // 
            this.groupBoxTransmitterStatus.Controls.Add(this.label17);
            this.groupBoxTransmitterStatus.Controls.Add(this.label18);
            this.groupBoxTransmitterStatus.Controls.Add(this.label15);
            this.groupBoxTransmitterStatus.Controls.Add(this.label16);
            this.groupBoxTransmitterStatus.Controls.Add(this.labelCurrentTXSondeID);
            this.groupBoxTransmitterStatus.Controls.Add(this.label14);
            this.groupBoxTransmitterStatus.Enabled = false;
            this.groupBoxTransmitterStatus.Location = new System.Drawing.Point(6, 305);
            this.groupBoxTransmitterStatus.Name = "groupBoxTransmitterStatus";
            this.groupBoxTransmitterStatus.Size = new System.Drawing.Size(188, 57);
            this.groupBoxTransmitterStatus.TabIndex = 2;
            this.groupBoxTransmitterStatus.TabStop = false;
            this.groupBoxTransmitterStatus.Text = "TX Status";
            this.groupBoxTransmitterStatus.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(103, 39);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "XXXXXXXX";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(64, 39);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Span:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(103, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "XXXXXXXX";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(60, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Power:";
            // 
            // labelCurrentTXSondeID
            // 
            this.labelCurrentTXSondeID.AutoSize = true;
            this.labelCurrentTXSondeID.Location = new System.Drawing.Point(103, 13);
            this.labelCurrentTXSondeID.Name = "labelCurrentTXSondeID";
            this.labelCurrentTXSondeID.Size = new System.Drawing.Size(63, 13);
            this.labelCurrentTXSondeID.TabIndex = 1;
            this.labelCurrentTXSondeID.Text = "SXXXXXXX";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Current Sonde:";
            // 
            // groupBoxCurrentGPS
            // 
            this.groupBoxCurrentGPS.Controls.Add(this.labelCurrentSatCount);
            this.groupBoxCurrentGPS.Controls.Add(this.label13);
            this.groupBoxCurrentGPS.Controls.Add(this.labelCurrentLat);
            this.groupBoxCurrentGPS.Controls.Add(this.labelCurrentLong);
            this.groupBoxCurrentGPS.Controls.Add(this.label4);
            this.groupBoxCurrentGPS.Controls.Add(this.labelCurrentAlt);
            this.groupBoxCurrentGPS.Controls.Add(this.label6);
            this.groupBoxCurrentGPS.Controls.Add(this.label5);
            this.groupBoxCurrentGPS.Enabled = false;
            this.groupBoxCurrentGPS.Location = new System.Drawing.Point(6, 231);
            this.groupBoxCurrentGPS.Name = "groupBoxCurrentGPS";
            this.groupBoxCurrentGPS.Size = new System.Drawing.Size(188, 71);
            this.groupBoxCurrentGPS.TabIndex = 1;
            this.groupBoxCurrentGPS.TabStop = false;
            this.groupBoxCurrentGPS.Text = "GPS Status";
            this.groupBoxCurrentGPS.Visible = false;
            // 
            // labelCurrentSatCount
            // 
            this.labelCurrentSatCount.AutoSize = true;
            this.labelCurrentSatCount.Location = new System.Drawing.Point(132, 53);
            this.labelCurrentSatCount.Name = "labelCurrentSatCount";
            this.labelCurrentSatCount.Size = new System.Drawing.Size(21, 13);
            this.labelCurrentSatCount.TabIndex = 15;
            this.labelCurrentSatCount.Text = "XX";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(110, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Sat:";
            // 
            // labelCurrentLat
            // 
            this.labelCurrentLat.AutoSize = true;
            this.labelCurrentLat.Location = new System.Drawing.Point(60, 17);
            this.labelCurrentLat.Name = "labelCurrentLat";
            this.labelCurrentLat.Size = new System.Drawing.Size(87, 13);
            this.labelCurrentLat.TabIndex = 11;
            this.labelCurrentLat.Text = "XX.XXXXXXXXX";
            // 
            // labelCurrentLong
            // 
            this.labelCurrentLong.AutoSize = true;
            this.labelCurrentLong.Location = new System.Drawing.Point(60, 35);
            this.labelCurrentLong.Name = "labelCurrentLong";
            this.labelCurrentLong.Size = new System.Drawing.Size(87, 13);
            this.labelCurrentLong.TabIndex = 12;
            this.labelCurrentLong.Text = "XX.XXXXXXXXX";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Lat:";
            // 
            // labelCurrentAlt
            // 
            this.labelCurrentAlt.AutoSize = true;
            this.labelCurrentAlt.Location = new System.Drawing.Point(60, 53);
            this.labelCurrentAlt.Name = "labelCurrentAlt";
            this.labelCurrentAlt.Size = new System.Drawing.Size(52, 13);
            this.labelCurrentAlt.TabIndex = 13;
            this.labelCurrentAlt.Text = "XXX.XXX";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Alt:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Long:";
            // 
            // groupBoxCurrentPTU
            // 
            this.groupBoxCurrentPTU.Controls.Add(this.checkBoxCorrection);
            this.groupBoxCurrentPTU.Controls.Add(this.comboBoxSensorPressure);
            this.groupBoxCurrentPTU.Controls.Add(this.comboBoxSensorTempHumidity);
            this.groupBoxCurrentPTU.Controls.Add(this.label11);
            this.groupBoxCurrentPTU.Controls.Add(this.label10);
            this.groupBoxCurrentPTU.Controls.Add(this.labelChamberTemperature);
            this.groupBoxCurrentPTU.Controls.Add(this.labelChamberHumidity);
            this.groupBoxCurrentPTU.Controls.Add(this.labelSensorPressure);
            this.groupBoxCurrentPTU.Controls.Add(this.labelSensorTemperature);
            this.groupBoxCurrentPTU.Controls.Add(this.labelSensorHumidity);
            this.groupBoxCurrentPTU.Controls.Add(this.label1);
            this.groupBoxCurrentPTU.Controls.Add(this.label2);
            this.groupBoxCurrentPTU.Controls.Add(this.label3);
            this.groupBoxCurrentPTU.Location = new System.Drawing.Point(7, 114);
            this.groupBoxCurrentPTU.Name = "groupBoxCurrentPTU";
            this.groupBoxCurrentPTU.Size = new System.Drawing.Size(187, 116);
            this.groupBoxCurrentPTU.TabIndex = 0;
            this.groupBoxCurrentPTU.TabStop = false;
            this.groupBoxCurrentPTU.Text = "Env PTU";
            // 
            // checkBoxCorrection
            // 
            this.checkBoxCorrection.AutoSize = true;
            this.checkBoxCorrection.Checked = true;
            this.checkBoxCorrection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCorrection.Location = new System.Drawing.Point(120, 75);
            this.checkBoxCorrection.Name = "checkBoxCorrection";
            this.checkBoxCorrection.Size = new System.Drawing.Size(60, 17);
            this.checkBoxCorrection.TabIndex = 13;
            this.checkBoxCorrection.Text = "Correct";
            this.checkBoxCorrection.UseVisualStyleBackColor = true;
            // 
            // comboBoxSensorPressure
            // 
            this.comboBoxSensorPressure.FormattingEnabled = true;
            this.comboBoxSensorPressure.Items.AddRange(new object[] {
            "Paroscientific"});
            this.comboBoxSensorPressure.Location = new System.Drawing.Point(7, 32);
            this.comboBoxSensorPressure.Name = "comboBoxSensorPressure";
            this.comboBoxSensorPressure.Size = new System.Drawing.Size(106, 21);
            this.comboBoxSensorPressure.TabIndex = 12;
            this.comboBoxSensorPressure.Text = "P Sensor";
            this.comboBoxSensorPressure.SelectedValueChanged += new System.EventHandler(this.comboBoxSensorPressure_SelectedValueChanged);
            this.comboBoxSensorPressure.Click += new System.EventHandler(this.comboBoxSensorPressure_Click);
            // 
            // comboBoxSensorTempHumidity
            // 
            this.comboBoxSensorTempHumidity.FormattingEnabled = true;
            this.comboBoxSensorTempHumidity.Items.AddRange(new object[] {
            "GE Hygro",
            "HMP234"});
            this.comboBoxSensorTempHumidity.Location = new System.Drawing.Point(9, 73);
            this.comboBoxSensorTempHumidity.Name = "comboBoxSensorTempHumidity";
            this.comboBoxSensorTempHumidity.Size = new System.Drawing.Size(104, 21);
            this.comboBoxSensorTempHumidity.TabIndex = 11;
            this.comboBoxSensorTempHumidity.Text = "TU Sensor";
            this.comboBoxSensorTempHumidity.SelectedValueChanged += new System.EventHandler(this.comboBoxSensorTempHumidity_SelectedValueChanged);
            this.comboBoxSensorTempHumidity.Click += new System.EventHandler(this.comboBoxSensorTempHumidity_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(124, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Chamber";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(29, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Sensors";
            // 
            // labelChamberTemperature
            // 
            this.labelChamberTemperature.AutoSize = true;
            this.labelChamberTemperature.Location = new System.Drawing.Point(126, 35);
            this.labelChamberTemperature.Name = "labelChamberTemperature";
            this.labelChamberTemperature.Size = new System.Drawing.Size(52, 13);
            this.labelChamberTemperature.TabIndex = 7;
            this.labelChamberTemperature.Text = "XXX.XXC";
            // 
            // labelChamberHumidity
            // 
            this.labelChamberHumidity.AutoSize = true;
            this.labelChamberHumidity.Location = new System.Drawing.Point(126, 53);
            this.labelChamberHumidity.Name = "labelChamberHumidity";
            this.labelChamberHumidity.Size = new System.Drawing.Size(53, 13);
            this.labelChamberHumidity.TabIndex = 8;
            this.labelChamberHumidity.Text = "XXX.XX%";
            // 
            // labelSensorPressure
            // 
            this.labelSensorPressure.AutoSize = true;
            this.labelSensorPressure.Location = new System.Drawing.Point(28, 56);
            this.labelSensorPressure.Name = "labelSensorPressure";
            this.labelSensorPressure.Size = new System.Drawing.Size(46, 13);
            this.labelSensorPressure.TabIndex = 3;
            this.labelSensorPressure.Text = "000.000";
            this.labelSensorPressure.DoubleClick += new System.EventHandler(this.labelSensorPressure_DoubleClick);
            // 
            // labelSensorTemperature
            // 
            this.labelSensorTemperature.AutoSize = true;
            this.labelSensorTemperature.Location = new System.Drawing.Point(28, 97);
            this.labelSensorTemperature.Name = "labelSensorTemperature";
            this.labelSensorTemperature.Size = new System.Drawing.Size(46, 13);
            this.labelSensorTemperature.TabIndex = 4;
            this.labelSensorTemperature.Text = "000.000";
            this.labelSensorTemperature.DoubleClick += new System.EventHandler(this.labelSensorTemperature_DoubleClick);
            // 
            // labelSensorHumidity
            // 
            this.labelSensorHumidity.AutoSize = true;
            this.labelSensorHumidity.Location = new System.Drawing.Point(103, 97);
            this.labelSensorHumidity.Name = "labelSensorHumidity";
            this.labelSensorHumidity.Size = new System.Drawing.Size(46, 13);
            this.labelSensorHumidity.TabIndex = 5;
            this.labelSensorHumidity.Text = "000.000";
            this.labelSensorHumidity.DoubleClick += new System.EventHandler(this.labelSensorHumidity_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "P:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "T:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(80, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "U:";
            // 
            // groupBoxControls
            // 
            this.groupBoxControls.Controls.Add(this.groupBoxControlsEnv);
            this.groupBoxControls.Enabled = false;
            this.groupBoxControls.Location = new System.Drawing.Point(646, 401);
            this.groupBoxControls.Name = "groupBoxControls";
            this.groupBoxControls.Size = new System.Drawing.Size(200, 133);
            this.groupBoxControls.TabIndex = 4;
            this.groupBoxControls.TabStop = false;
            this.groupBoxControls.Text = "System Controls";
            this.groupBoxControls.Visible = false;
            // 
            // groupBoxControlsEnv
            // 
            this.groupBoxControlsEnv.Controls.Add(this.buttonStopConditions);
            this.groupBoxControlsEnv.Controls.Add(this.label7);
            this.groupBoxControlsEnv.Controls.Add(this.textBoxDesiredPressure);
            this.groupBoxControlsEnv.Controls.Add(this.buttonHold);
            this.groupBoxControlsEnv.Controls.Add(this.textBoxDesiredTemperature);
            this.groupBoxControlsEnv.Controls.Add(this.label8);
            this.groupBoxControlsEnv.Controls.Add(this.buttonRunConditions);
            this.groupBoxControlsEnv.Controls.Add(this.label9);
            this.groupBoxControlsEnv.Controls.Add(this.textBoxDesiredHumidity);
            this.groupBoxControlsEnv.Enabled = false;
            this.groupBoxControlsEnv.Location = new System.Drawing.Point(7, 11);
            this.groupBoxControlsEnv.Name = "groupBoxControlsEnv";
            this.groupBoxControlsEnv.Size = new System.Drawing.Size(187, 114);
            this.groupBoxControlsEnv.TabIndex = 0;
            this.groupBoxControlsEnv.TabStop = false;
            this.groupBoxControlsEnv.Text = "Env";
            this.groupBoxControlsEnv.Visible = false;
            // 
            // buttonStopConditions
            // 
            this.buttonStopConditions.Enabled = false;
            this.buttonStopConditions.Location = new System.Drawing.Point(124, 87);
            this.buttonStopConditions.Name = "buttonStopConditions";
            this.buttonStopConditions.Size = new System.Drawing.Size(45, 21);
            this.buttonStopConditions.TabIndex = 14;
            this.buttonStopConditions.Text = "Stop";
            this.buttonStopConditions.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Pressure:";
            // 
            // textBoxDesiredPressure
            // 
            this.textBoxDesiredPressure.Location = new System.Drawing.Point(89, 13);
            this.textBoxDesiredPressure.Name = "textBoxDesiredPressure";
            this.textBoxDesiredPressure.Size = new System.Drawing.Size(66, 20);
            this.textBoxDesiredPressure.TabIndex = 9;
            this.textBoxDesiredPressure.Text = "XXXX.XXX";
            // 
            // buttonHold
            // 
            this.buttonHold.Enabled = false;
            this.buttonHold.Location = new System.Drawing.Point(73, 87);
            this.buttonHold.Name = "buttonHold";
            this.buttonHold.Size = new System.Drawing.Size(45, 21);
            this.buttonHold.TabIndex = 13;
            this.buttonHold.Text = "Hold";
            this.buttonHold.UseVisualStyleBackColor = true;
            // 
            // textBoxDesiredTemperature
            // 
            this.textBoxDesiredTemperature.Location = new System.Drawing.Point(89, 39);
            this.textBoxDesiredTemperature.Name = "textBoxDesiredTemperature";
            this.textBoxDesiredTemperature.Size = new System.Drawing.Size(66, 20);
            this.textBoxDesiredTemperature.TabIndex = 10;
            this.textBoxDesiredTemperature.Text = "XXX.XXC";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Temperature:";
            // 
            // buttonRunConditions
            // 
            this.buttonRunConditions.Location = new System.Drawing.Point(22, 87);
            this.buttonRunConditions.Name = "buttonRunConditions";
            this.buttonRunConditions.Size = new System.Drawing.Size(45, 21);
            this.buttonRunConditions.TabIndex = 12;
            this.buttonRunConditions.Text = "Run";
            this.buttonRunConditions.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Humidity:";
            // 
            // textBoxDesiredHumidity
            // 
            this.textBoxDesiredHumidity.Location = new System.Drawing.Point(89, 65);
            this.textBoxDesiredHumidity.Name = "textBoxDesiredHumidity";
            this.textBoxDesiredHumidity.Size = new System.Drawing.Size(66, 20);
            this.textBoxDesiredHumidity.TabIndex = 11;
            this.textBoxDesiredHumidity.Text = "XXX.XX%";
            // 
            // tabControlRackDisplay
            // 
            this.tabControlRackDisplay.Controls.Add(this.tabPageRackA);
            this.tabControlRackDisplay.Controls.Add(this.tabPageRackB);
            this.tabControlRackDisplay.Controls.Add(this.tabPageRackC);
            this.tabControlRackDisplay.Controls.Add(this.tabPageRackD);
            this.tabControlRackDisplay.Location = new System.Drawing.Point(0, 28);
            this.tabControlRackDisplay.Name = "tabControlRackDisplay";
            this.tabControlRackDisplay.SelectedIndex = 0;
            this.tabControlRackDisplay.Size = new System.Drawing.Size(640, 722);
            this.tabControlRackDisplay.TabIndex = 5;
            // 
            // tabPageRackA
            // 
            this.tabPageRackA.Controls.Add(this.statusStripRackA);
            this.tabPageRackA.Controls.Add(this.dataGridViewRackA);
            this.tabPageRackA.Location = new System.Drawing.Point(4, 22);
            this.tabPageRackA.Name = "tabPageRackA";
            this.tabPageRackA.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRackA.Size = new System.Drawing.Size(632, 696);
            this.tabPageRackA.TabIndex = 0;
            this.tabPageRackA.Text = "Rack A";
            this.tabPageRackA.UseVisualStyleBackColor = true;
            // 
            // statusStripRackA
            // 
            this.statusStripRackA.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelRackA});
            this.statusStripRackA.Location = new System.Drawing.Point(3, 671);
            this.statusStripRackA.Name = "statusStripRackA";
            this.statusStripRackA.Size = new System.Drawing.Size(626, 22);
            this.statusStripRackA.TabIndex = 1;
            this.statusStripRackA.Text = "statusStrip1";
            // 
            // toolStripStatusLabelRackA
            // 
            this.toolStripStatusLabelRackA.Name = "toolStripStatusLabelRackA";
            this.toolStripStatusLabelRackA.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabelRackA.Text = "Loaded.";
            // 
            // dataGridViewRackA
            // 
            this.dataGridViewRackA.AllowUserToDeleteRows = false;
            this.dataGridViewRackA.AllowUserToResizeRows = false;
            this.dataGridViewRackA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRackA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnPOS,
            this.ColumnSondeSN,
            this.ColumnSondeTN,
            this.ColumnSondePN,
            this.ColumnSondeCom,
            this.ColumnPressureTemp,
            this.ColumnPressure,
            this.ColumnPDiff,
            this.ColumnTemperature,
            this.ColumnTDiff,
            this.ColumnHumidity,
            this.ColumnUDiff,
            this.ColumnFirmware,
            this.ColumnTXMode});
            this.dataGridViewRackA.Location = new System.Drawing.Point(8, 6);
            this.dataGridViewRackA.Name = "dataGridViewRackA";
            this.dataGridViewRackA.RowHeadersVisible = false;
            this.dataGridViewRackA.Size = new System.Drawing.Size(621, 662);
            this.dataGridViewRackA.TabIndex = 0;
            this.dataGridViewRackA.TabStop = false;
            this.dataGridViewRackA.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRackA_CellDoubleClick);
            // 
            // ColumnPOS
            // 
            this.ColumnPOS.HeaderText = "POS";
            this.ColumnPOS.Name = "ColumnPOS";
            this.ColumnPOS.Width = 35;
            // 
            // ColumnSondeSN
            // 
            this.ColumnSondeSN.HeaderText = "SondeSN";
            this.ColumnSondeSN.Name = "ColumnSondeSN";
            this.ColumnSondeSN.Width = 55;
            // 
            // ColumnSondeTN
            // 
            this.ColumnSondeTN.HeaderText = "SondeTN";
            this.ColumnSondeTN.Name = "ColumnSondeTN";
            this.ColumnSondeTN.Width = 55;
            // 
            // ColumnSondePN
            // 
            this.ColumnSondePN.HeaderText = "SondePN";
            this.ColumnSondePN.Name = "ColumnSondePN";
            this.ColumnSondePN.Width = 55;
            // 
            // ColumnSondeCom
            // 
            this.ColumnSondeCom.HeaderText = "Com";
            this.ColumnSondeCom.Name = "ColumnSondeCom";
            this.ColumnSondeCom.Width = 48;
            // 
            // ColumnPressureTemp
            // 
            this.ColumnPressureTemp.HeaderText = "P_Temp";
            this.ColumnPressureTemp.Name = "ColumnPressureTemp";
            this.ColumnPressureTemp.Width = 50;
            // 
            // ColumnPressure
            // 
            this.ColumnPressure.HeaderText = "Pressure";
            this.ColumnPressure.Name = "ColumnPressure";
            this.ColumnPressure.Width = 55;
            // 
            // ColumnPDiff
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.ColumnPDiff.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColumnPDiff.HeaderText = "P_Diff";
            this.ColumnPDiff.Name = "ColumnPDiff";
            this.ColumnPDiff.Width = 55;
            // 
            // ColumnTemperature
            // 
            this.ColumnTemperature.HeaderText = "Temp";
            this.ColumnTemperature.Name = "ColumnTemperature";
            this.ColumnTemperature.Width = 45;
            // 
            // ColumnTDiff
            // 
            this.ColumnTDiff.HeaderText = "T_Diff";
            this.ColumnTDiff.Name = "ColumnTDiff";
            this.ColumnTDiff.Width = 45;
            // 
            // ColumnHumidity
            // 
            this.ColumnHumidity.HeaderText = "Humidity";
            this.ColumnHumidity.Name = "ColumnHumidity";
            this.ColumnHumidity.Width = 50;
            // 
            // ColumnUDiff
            // 
            this.ColumnUDiff.HeaderText = "U_Diff";
            this.ColumnUDiff.Name = "ColumnUDiff";
            this.ColumnUDiff.Width = 50;
            // 
            // ColumnFirmware
            // 
            this.ColumnFirmware.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnFirmware.HeaderText = "Firmware";
            this.ColumnFirmware.Name = "ColumnFirmware";
            this.ColumnFirmware.Width = 60;
            // 
            // ColumnTXMode
            // 
            this.ColumnTXMode.HeaderText = "TXMode";
            this.ColumnTXMode.Name = "ColumnTXMode";
            this.ColumnTXMode.Width = 60;
            // 
            // tabPageRackB
            // 
            this.tabPageRackB.Controls.Add(this.statusStripRackB);
            this.tabPageRackB.Controls.Add(this.dataGridViewRackB);
            this.tabPageRackB.Location = new System.Drawing.Point(4, 22);
            this.tabPageRackB.Name = "tabPageRackB";
            this.tabPageRackB.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRackB.Size = new System.Drawing.Size(632, 696);
            this.tabPageRackB.TabIndex = 1;
            this.tabPageRackB.Text = "Rack B";
            this.tabPageRackB.UseVisualStyleBackColor = true;
            // 
            // statusStripRackB
            // 
            this.statusStripRackB.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelRackB});
            this.statusStripRackB.Location = new System.Drawing.Point(3, 671);
            this.statusStripRackB.Name = "statusStripRackB";
            this.statusStripRackB.Size = new System.Drawing.Size(626, 22);
            this.statusStripRackB.TabIndex = 6;
            this.statusStripRackB.Text = "statusStrip1";
            // 
            // toolStripStatusLabelRackB
            // 
            this.toolStripStatusLabelRackB.Name = "toolStripStatusLabelRackB";
            this.toolStripStatusLabelRackB.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabelRackB.Text = "Loaded.";
            // 
            // dataGridViewRackB
            // 
            this.dataGridViewRackB.AllowUserToDeleteRows = false;
            this.dataGridViewRackB.AllowUserToResizeRows = false;
            this.dataGridViewRackB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRackB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13});
            this.dataGridViewRackB.Location = new System.Drawing.Point(8, 6);
            this.dataGridViewRackB.Name = "dataGridViewRackB";
            this.dataGridViewRackB.RowHeadersVisible = false;
            this.dataGridViewRackB.Size = new System.Drawing.Size(618, 662);
            this.dataGridViewRackB.TabIndex = 4;
            this.dataGridViewRackB.TabStop = false;
            this.dataGridViewRackB.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRackB_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "POS";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 35;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "SondeSN";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 55;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "SondeTN";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 55;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "SondePN";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 55;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Com";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 50;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "P_Temp";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 50;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Pressure";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 50;
            // 
            // dataGridViewTextBoxColumn8
            // 
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn8.HeaderText = "P_Diff";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 50;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Temp";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 50;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "T_Diff";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.Width = 50;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Humidity";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 50;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "U_Diff";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.Width = 50;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn13.HeaderText = "Firmware";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.Width = 60;
            // 
            // tabPageRackC
            // 
            this.tabPageRackC.Controls.Add(this.dataGridViewRackC);
            this.tabPageRackC.Controls.Add(this.statusStripRackC);
            this.tabPageRackC.Location = new System.Drawing.Point(4, 22);
            this.tabPageRackC.Name = "tabPageRackC";
            this.tabPageRackC.Size = new System.Drawing.Size(632, 696);
            this.tabPageRackC.TabIndex = 2;
            this.tabPageRackC.Text = "Rack C";
            this.tabPageRackC.UseVisualStyleBackColor = true;
            // 
            // dataGridViewRackC
            // 
            this.dataGridViewRackC.AllowUserToDeleteRows = false;
            this.dataGridViewRackC.AllowUserToResizeRows = false;
            this.dataGridViewRackC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRackC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26});
            this.dataGridViewRackC.Location = new System.Drawing.Point(8, 6);
            this.dataGridViewRackC.Name = "dataGridViewRackC";
            this.dataGridViewRackC.RowHeadersVisible = false;
            this.dataGridViewRackC.Size = new System.Drawing.Size(618, 662);
            this.dataGridViewRackC.TabIndex = 6;
            this.dataGridViewRackC.TabStop = false;
            this.dataGridViewRackC.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRackC_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "POS";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.Width = 35;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "SondeSN";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.Width = 55;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "SondeTN";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 55;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "SondePN";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Width = 55;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "Com";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.Width = 50;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "P_Temp";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.Width = 50;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "Pressure";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.Width = 50;
            // 
            // dataGridViewTextBoxColumn21
            // 
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn21.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn21.HeaderText = "P_Diff";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.Width = 50;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.HeaderText = "Temp";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.Width = 50;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.HeaderText = "T_Diff";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Width = 50;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.HeaderText = "Humidity";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.Width = 50;
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.HeaderText = "U_Diff";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.Width = 50;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn26.HeaderText = "Firmware";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.Width = 60;
            // 
            // statusStripRackC
            // 
            this.statusStripRackC.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelRackC});
            this.statusStripRackC.Location = new System.Drawing.Point(0, 674);
            this.statusStripRackC.Name = "statusStripRackC";
            this.statusStripRackC.Size = new System.Drawing.Size(632, 22);
            this.statusStripRackC.TabIndex = 5;
            this.statusStripRackC.Text = "statusStrip1";
            // 
            // toolStripStatusLabelRackC
            // 
            this.toolStripStatusLabelRackC.Name = "toolStripStatusLabelRackC";
            this.toolStripStatusLabelRackC.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabelRackC.Text = "Loaded.";
            // 
            // tabPageRackD
            // 
            this.tabPageRackD.Controls.Add(this.dataGridViewRackD);
            this.tabPageRackD.Controls.Add(this.statusStripRackD);
            this.tabPageRackD.Location = new System.Drawing.Point(4, 22);
            this.tabPageRackD.Name = "tabPageRackD";
            this.tabPageRackD.Size = new System.Drawing.Size(632, 696);
            this.tabPageRackD.TabIndex = 3;
            this.tabPageRackD.Text = "Rack D";
            this.tabPageRackD.UseVisualStyleBackColor = true;
            // 
            // dataGridViewRackD
            // 
            this.dataGridViewRackD.AllowUserToDeleteRows = false;
            this.dataGridViewRackD.AllowUserToResizeRows = false;
            this.dataGridViewRackD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewRackD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39});
            this.dataGridViewRackD.Location = new System.Drawing.Point(8, 6);
            this.dataGridViewRackD.Name = "dataGridViewRackD";
            this.dataGridViewRackD.RowHeadersVisible = false;
            this.dataGridViewRackD.Size = new System.Drawing.Size(618, 662);
            this.dataGridViewRackD.TabIndex = 6;
            this.dataGridViewRackD.TabStop = false;
            this.dataGridViewRackD.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRackD_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.HeaderText = "POS";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.Width = 35;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.HeaderText = "SondeSN";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.Width = 55;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.HeaderText = "SondeTN";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.Width = 55;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.HeaderText = "SondePN";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.Width = 55;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.HeaderText = "Com";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.Width = 50;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.HeaderText = "P_Temp";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.Width = 50;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.HeaderText = "Pressure";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.Width = 50;
            // 
            // dataGridViewTextBoxColumn34
            // 
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dataGridViewTextBoxColumn34.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn34.HeaderText = "P_Diff";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.Width = 50;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.HeaderText = "Temp";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.Width = 50;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.HeaderText = "T_Diff";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.Width = 50;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.HeaderText = "Humidity";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.Width = 50;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.HeaderText = "U_Diff";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.Width = 50;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn39.HeaderText = "Firmware";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.Width = 60;
            // 
            // statusStripRackD
            // 
            this.statusStripRackD.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelRackD});
            this.statusStripRackD.Location = new System.Drawing.Point(0, 674);
            this.statusStripRackD.Name = "statusStripRackD";
            this.statusStripRackD.Size = new System.Drawing.Size(632, 22);
            this.statusStripRackD.TabIndex = 5;
            this.statusStripRackD.Text = "statusStrip1";
            // 
            // toolStripStatusLabelRackD
            // 
            this.toolStripStatusLabelRackD.Name = "toolStripStatusLabelRackD";
            this.toolStripStatusLabelRackD.Size = new System.Drawing.Size(49, 17);
            this.toolStripStatusLabelRackD.Text = "Loaded.";
            // 
            // backgroundWorkerUpdateTempHumidity
            // 
            this.backgroundWorkerUpdateTempHumidity.WorkerSupportsCancellation = true;
            this.backgroundWorkerUpdateTempHumidity.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerUpdateTempHumidity_DoWork);
            // 
            // backgroundWorkerUpdatePressure
            // 
            this.backgroundWorkerUpdatePressure.WorkerSupportsCancellation = true;
            this.backgroundWorkerUpdatePressure.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerUpdatePressure_DoWork);
            // 
            // backgroundWorkerRadiosondeDataCollect
            // 
            this.backgroundWorkerRadiosondeDataCollect.WorkerSupportsCancellation = true;
            this.backgroundWorkerRadiosondeDataCollect.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerRadiosondeDataCollect_DoWork);
            this.backgroundWorkerRadiosondeDataCollect.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerRadiosondeDataCollect_RunWorkerCompleted);
            // 
            // tabControlCoefficent
            // 
            this.tabControlCoefficent.Controls.Add(this.tabPageCoefficentAll);
            this.tabControlCoefficent.Controls.Add(this.tabPageCoefficentSingal);
            this.tabControlCoefficent.Location = new System.Drawing.Point(2, 19);
            this.tabControlCoefficent.Name = "tabControlCoefficent";
            this.tabControlCoefficent.SelectedIndex = 0;
            this.tabControlCoefficent.Size = new System.Drawing.Size(196, 188);
            this.tabControlCoefficent.TabIndex = 6;
            // 
            // tabPageCoefficentAll
            // 
            this.tabPageCoefficentAll.Controls.Add(this.buttonLoadCancel);
            this.tabPageCoefficentAll.Controls.Add(this.buttonLoadAllCoefficentAll);
            this.tabPageCoefficentAll.Controls.Add(this.buttonLoadAllCoefficentPressure);
            this.tabPageCoefficentAll.Controls.Add(this.buttonLoadAllCoefficentHumidity);
            this.tabPageCoefficentAll.Controls.Add(this.buttonLoadAllCoefficentHumidityTemp);
            this.tabPageCoefficentAll.Controls.Add(this.buttonLoadAllCoefficentAirTemp);
            this.tabPageCoefficentAll.Location = new System.Drawing.Point(4, 22);
            this.tabPageCoefficentAll.Name = "tabPageCoefficentAll";
            this.tabPageCoefficentAll.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCoefficentAll.Size = new System.Drawing.Size(188, 162);
            this.tabPageCoefficentAll.TabIndex = 1;
            this.tabPageCoefficentAll.Text = "All Radiosonde";
            this.tabPageCoefficentAll.UseVisualStyleBackColor = true;
            // 
            // buttonLoadCancel
            // 
            this.buttonLoadCancel.Enabled = false;
            this.buttonLoadCancel.Location = new System.Drawing.Point(5, 138);
            this.buttonLoadCancel.Name = "buttonLoadCancel";
            this.buttonLoadCancel.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadCancel.TabIndex = 10;
            this.buttonLoadCancel.Text = "Cancel Load";
            this.buttonLoadCancel.UseVisualStyleBackColor = true;
            this.buttonLoadCancel.Click += new System.EventHandler(this.buttonLoadCancel_Click);
            // 
            // buttonLoadAllCoefficentAll
            // 
            this.buttonLoadAllCoefficentAll.Location = new System.Drawing.Point(5, 112);
            this.buttonLoadAllCoefficentAll.Name = "buttonLoadAllCoefficentAll";
            this.buttonLoadAllCoefficentAll.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadAllCoefficentAll.TabIndex = 9;
            this.buttonLoadAllCoefficentAll.Text = "Load All Coefficents";
            this.buttonLoadAllCoefficentAll.UseVisualStyleBackColor = true;
            this.buttonLoadAllCoefficentAll.Click += new System.EventHandler(this.buttonLoadAllCoefficentAll_Click);
            // 
            // buttonLoadAllCoefficentPressure
            // 
            this.buttonLoadAllCoefficentPressure.Location = new System.Drawing.Point(5, 5);
            this.buttonLoadAllCoefficentPressure.Name = "buttonLoadAllCoefficentPressure";
            this.buttonLoadAllCoefficentPressure.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadAllCoefficentPressure.TabIndex = 8;
            this.buttonLoadAllCoefficentPressure.Text = "Load All Pressure";
            this.buttonLoadAllCoefficentPressure.UseVisualStyleBackColor = true;
            this.buttonLoadAllCoefficentPressure.Click += new System.EventHandler(this.buttonLoadAllCoefficentPressure_Click);
            // 
            // buttonLoadAllCoefficentHumidity
            // 
            this.buttonLoadAllCoefficentHumidity.Location = new System.Drawing.Point(5, 85);
            this.buttonLoadAllCoefficentHumidity.Name = "buttonLoadAllCoefficentHumidity";
            this.buttonLoadAllCoefficentHumidity.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadAllCoefficentHumidity.TabIndex = 7;
            this.buttonLoadAllCoefficentHumidity.Text = "Load All Humidity";
            this.buttonLoadAllCoefficentHumidity.UseVisualStyleBackColor = true;
            this.buttonLoadAllCoefficentHumidity.Click += new System.EventHandler(this.buttonLoadAllCoefficentHumidity_Click);
            // 
            // buttonLoadAllCoefficentHumidityTemp
            // 
            this.buttonLoadAllCoefficentHumidityTemp.Enabled = false;
            this.buttonLoadAllCoefficentHumidityTemp.Location = new System.Drawing.Point(5, 58);
            this.buttonLoadAllCoefficentHumidityTemp.Name = "buttonLoadAllCoefficentHumidityTemp";
            this.buttonLoadAllCoefficentHumidityTemp.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadAllCoefficentHumidityTemp.TabIndex = 6;
            this.buttonLoadAllCoefficentHumidityTemp.Text = "Load All Humidity Temp";
            this.buttonLoadAllCoefficentHumidityTemp.UseVisualStyleBackColor = true;
            this.buttonLoadAllCoefficentHumidityTemp.Click += new System.EventHandler(this.buttonLoadAllCoefficentHumidityTemp_Click);
            // 
            // buttonLoadAllCoefficentAirTemp
            // 
            this.buttonLoadAllCoefficentAirTemp.Location = new System.Drawing.Point(5, 31);
            this.buttonLoadAllCoefficentAirTemp.Name = "buttonLoadAllCoefficentAirTemp";
            this.buttonLoadAllCoefficentAirTemp.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadAllCoefficentAirTemp.TabIndex = 5;
            this.buttonLoadAllCoefficentAirTemp.Text = "Load All Air Temp";
            this.buttonLoadAllCoefficentAirTemp.UseVisualStyleBackColor = true;
            this.buttonLoadAllCoefficentAirTemp.Click += new System.EventHandler(this.buttonLoadAllCoefficentAirTemp_Click);
            // 
            // tabPageCoefficentSingal
            // 
            this.tabPageCoefficentSingal.Controls.Add(this.buttonClearReport);
            this.tabPageCoefficentSingal.Controls.Add(this.buttonLoadSingleCoefficentAll);
            this.tabPageCoefficentSingal.Controls.Add(this.buttonLoadSingleCoefficentPressure);
            this.tabPageCoefficentSingal.Controls.Add(this.buttonLoadSingleCoefficentHumidity);
            this.tabPageCoefficentSingal.Controls.Add(this.buttonLoadSingleCoefficentHumidityTemp);
            this.tabPageCoefficentSingal.Controls.Add(this.buttonLoadSingleCoefficentAirTemp);
            this.tabPageCoefficentSingal.Location = new System.Drawing.Point(4, 22);
            this.tabPageCoefficentSingal.Name = "tabPageCoefficentSingal";
            this.tabPageCoefficentSingal.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCoefficentSingal.Size = new System.Drawing.Size(188, 162);
            this.tabPageCoefficentSingal.TabIndex = 0;
            this.tabPageCoefficentSingal.Text = "Single Radiosonde";
            this.tabPageCoefficentSingal.UseVisualStyleBackColor = true;
            // 
            // buttonClearReport
            // 
            this.buttonClearReport.Location = new System.Drawing.Point(6, 140);
            this.buttonClearReport.Name = "buttonClearReport";
            this.buttonClearReport.Size = new System.Drawing.Size(179, 21);
            this.buttonClearReport.TabIndex = 5;
            this.buttonClearReport.Text = "Clear Report";
            this.buttonClearReport.UseVisualStyleBackColor = true;
            this.buttonClearReport.Click += new System.EventHandler(this.buttonClearReport_Click);
            // 
            // buttonLoadSingleCoefficentAll
            // 
            this.buttonLoadSingleCoefficentAll.Location = new System.Drawing.Point(6, 113);
            this.buttonLoadSingleCoefficentAll.Name = "buttonLoadSingleCoefficentAll";
            this.buttonLoadSingleCoefficentAll.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadSingleCoefficentAll.TabIndex = 4;
            this.buttonLoadSingleCoefficentAll.Text = "Load All";
            this.buttonLoadSingleCoefficentAll.UseVisualStyleBackColor = true;
            this.buttonLoadSingleCoefficentAll.Click += new System.EventHandler(this.buttonLoadSingleCoefficentAll_Click);
            // 
            // buttonLoadSingleCoefficentPressure
            // 
            this.buttonLoadSingleCoefficentPressure.Location = new System.Drawing.Point(6, 5);
            this.buttonLoadSingleCoefficentPressure.Name = "buttonLoadSingleCoefficentPressure";
            this.buttonLoadSingleCoefficentPressure.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadSingleCoefficentPressure.TabIndex = 3;
            this.buttonLoadSingleCoefficentPressure.Text = "Load Pressure";
            this.buttonLoadSingleCoefficentPressure.UseVisualStyleBackColor = true;
            this.buttonLoadSingleCoefficentPressure.Click += new System.EventHandler(this.buttonLoadSingleCoefficentPressure_Click);
            // 
            // buttonLoadSingleCoefficentHumidity
            // 
            this.buttonLoadSingleCoefficentHumidity.Location = new System.Drawing.Point(6, 86);
            this.buttonLoadSingleCoefficentHumidity.Name = "buttonLoadSingleCoefficentHumidity";
            this.buttonLoadSingleCoefficentHumidity.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadSingleCoefficentHumidity.TabIndex = 2;
            this.buttonLoadSingleCoefficentHumidity.Text = "Load Humidity";
            this.buttonLoadSingleCoefficentHumidity.UseVisualStyleBackColor = true;
            this.buttonLoadSingleCoefficentHumidity.Click += new System.EventHandler(this.buttonLoadSingleCoefficentHumidity_Click);
            // 
            // buttonLoadSingleCoefficentHumidityTemp
            // 
            this.buttonLoadSingleCoefficentHumidityTemp.Enabled = false;
            this.buttonLoadSingleCoefficentHumidityTemp.Location = new System.Drawing.Point(6, 59);
            this.buttonLoadSingleCoefficentHumidityTemp.Name = "buttonLoadSingleCoefficentHumidityTemp";
            this.buttonLoadSingleCoefficentHumidityTemp.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadSingleCoefficentHumidityTemp.TabIndex = 1;
            this.buttonLoadSingleCoefficentHumidityTemp.Text = "Load Humidity Temp";
            this.buttonLoadSingleCoefficentHumidityTemp.UseVisualStyleBackColor = true;
            this.buttonLoadSingleCoefficentHumidityTemp.Click += new System.EventHandler(this.buttonLoadSingleCoefficentHumidityTemp_Click);
            // 
            // buttonLoadSingleCoefficentAirTemp
            // 
            this.buttonLoadSingleCoefficentAirTemp.Location = new System.Drawing.Point(6, 32);
            this.buttonLoadSingleCoefficentAirTemp.Name = "buttonLoadSingleCoefficentAirTemp";
            this.buttonLoadSingleCoefficentAirTemp.Size = new System.Drawing.Size(179, 21);
            this.buttonLoadSingleCoefficentAirTemp.TabIndex = 0;
            this.buttonLoadSingleCoefficentAirTemp.Text = "Load Air Temp";
            this.buttonLoadSingleCoefficentAirTemp.UseVisualStyleBackColor = true;
            this.buttonLoadSingleCoefficentAirTemp.Click += new System.EventHandler(this.buttonLoadSingleCoefficentAirTemp_Click);
            // 
            // groupBoxCoefficent
            // 
            this.groupBoxCoefficent.Controls.Add(this.tabControlCoefficent);
            this.groupBoxCoefficent.Enabled = false;
            this.groupBoxCoefficent.Location = new System.Drawing.Point(646, 540);
            this.groupBoxCoefficent.Name = "groupBoxCoefficent";
            this.groupBoxCoefficent.Size = new System.Drawing.Size(200, 211);
            this.groupBoxCoefficent.TabIndex = 6;
            this.groupBoxCoefficent.TabStop = false;
            this.groupBoxCoefficent.Text = "Load Coefficents";
            // 
            // pressureManagerToolStripMenuItem
            // 
            this.pressureManagerToolStripMenuItem.Name = "pressureManagerToolStripMenuItem";
            this.pressureManagerToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.pressureManagerToolStripMenuItem.Text = "Pressure Manager";
            this.pressureManagerToolStripMenuItem.Click += new System.EventHandler(this.pressureManagerToolStripMenuItem_Click);
            // 
            // frmMainDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 798);
            this.Controls.Add(this.groupBoxCoefficent);
            this.Controls.Add(this.tabControlRackDisplay);
            this.Controls.Add(this.groupBoxControls);
            this.Controls.Add(this.groupBoxStatus);
            this.Controls.Add(this.statusStripMainDisplay);
            this.Controls.Add(this.menuStripMainDisplay);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMainDisplay;
            this.Name = "frmMainDisplay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Radiosonde MultiSonde";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMainDisplay_FormClosing);
            this.Load += new System.EventHandler(this.frmMainDisplay_Load);
            this.Resize += new System.EventHandler(this.frmMainDisplay_Resize);
            this.menuStripMainDisplay.ResumeLayout(false);
            this.menuStripMainDisplay.PerformLayout();
            this.statusStripMainDisplay.ResumeLayout(false);
            this.statusStripMainDisplay.PerformLayout();
            this.groupBoxStatus.ResumeLayout(false);
            this.groupBoxControlSondes.ResumeLayout(false);
            this.groupBoxControlSondes.PerformLayout();
            this.groupBoxTransmitterStatus.ResumeLayout(false);
            this.groupBoxTransmitterStatus.PerformLayout();
            this.groupBoxCurrentGPS.ResumeLayout(false);
            this.groupBoxCurrentGPS.PerformLayout();
            this.groupBoxCurrentPTU.ResumeLayout(false);
            this.groupBoxCurrentPTU.PerformLayout();
            this.groupBoxControls.ResumeLayout(false);
            this.groupBoxControlsEnv.ResumeLayout(false);
            this.groupBoxControlsEnv.PerformLayout();
            this.tabControlRackDisplay.ResumeLayout(false);
            this.tabPageRackA.ResumeLayout(false);
            this.tabPageRackA.PerformLayout();
            this.statusStripRackA.ResumeLayout(false);
            this.statusStripRackA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRackA)).EndInit();
            this.tabPageRackB.ResumeLayout(false);
            this.tabPageRackB.PerformLayout();
            this.statusStripRackB.ResumeLayout(false);
            this.statusStripRackB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRackB)).EndInit();
            this.tabPageRackC.ResumeLayout(false);
            this.tabPageRackC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRackC)).EndInit();
            this.statusStripRackC.ResumeLayout(false);
            this.statusStripRackC.PerformLayout();
            this.tabPageRackD.ResumeLayout(false);
            this.tabPageRackD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRackD)).EndInit();
            this.statusStripRackD.ResumeLayout(false);
            this.statusStripRackD.PerformLayout();
            this.tabControlCoefficent.ResumeLayout(false);
            this.tabPageCoefficentAll.ResumeLayout(false);
            this.tabPageCoefficentSingal.ResumeLayout(false);
            this.groupBoxCoefficent.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMainDisplay;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStripMainDisplay;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelMainDisplay;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxStatus;
        private System.Windows.Forms.GroupBox groupBoxControls;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBoxCurrentGPS;
        private System.Windows.Forms.GroupBox groupBoxCurrentPTU;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl tabControlRackDisplay;
        private System.Windows.Forms.TabPage tabPageRackA;
        private System.Windows.Forms.TabPage tabPageRackB;
        private System.Windows.Forms.TabPage tabPageRackC;
        private System.Windows.Forms.TabPage tabPageRackD;
        private System.Windows.Forms.ToolStripMenuItem loggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startLoggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pauseLoggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopLoggingToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxTransmitterStatus;
        private System.Windows.Forms.Button buttonRunConditions;
        private System.Windows.Forms.TextBox textBoxDesiredHumidity;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxDesiredPressure;
        private System.Windows.Forms.TextBox textBoxDesiredTemperature;
        private System.Windows.Forms.Button buttonStopConditions;
        private System.Windows.Forms.Button buttonHold;
        private System.Windows.Forms.Label labelSensorPressure;
        private System.Windows.Forms.Label labelSensorTemperature;
        private System.Windows.Forms.Label labelSensorHumidity;
        private System.Windows.Forms.Label labelChamberTemperature;
        private System.Windows.Forms.Label labelChamberHumidity;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelCurrentSatCount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelCurrentLat;
        private System.Windows.Forms.Label labelCurrentLong;
        private System.Windows.Forms.Label labelCurrentAlt;
        private System.Windows.Forms.GroupBox groupBoxControlSondes;

        private System.Windows.Forms.GroupBox groupBoxControlsEnv;
        private System.Windows.Forms.Button buttonControlSondeReconnect;
        private System.Windows.Forms.Button buttonControlSondeStop;
        private System.Windows.Forms.ComboBox comboBoxSondeType;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelCurrentTXSondeID;
        private System.Windows.Forms.ComboBox comboBoxSensorTempHumidity;
        private System.Windows.Forms.ComboBox comboBoxSensorPressure;
        private System.ComponentModel.BackgroundWorker backgroundWorkerUpdateTempHumidity;
        private System.ComponentModel.BackgroundWorker backgroundWorkerUpdatePressure;
        private System.Windows.Forms.ToolStripMenuItem openLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intervalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem minToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Button buttonControlSondeSearch;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelLogging;
        private System.Windows.Forms.StatusStrip statusStripRackA;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRackA;
        private System.ComponentModel.BackgroundWorker backgroundWorkerRadiosondeDataCollect;
        private System.Windows.Forms.StatusStrip statusStripRackC;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRackC;
        private System.Windows.Forms.StatusStrip statusStripRackD;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRackD;
        private System.Windows.Forms.TabControl tabControlCoefficent;
        private System.Windows.Forms.TabPage tabPageCoefficentSingal;
        private System.Windows.Forms.TabPage tabPageCoefficentAll;
        private System.Windows.Forms.GroupBox groupBoxCoefficent;
        private System.Windows.Forms.Button buttonLoadSingleCoefficentHumidityTemp;
        private System.Windows.Forms.Button buttonLoadSingleCoefficentAirTemp;
        private System.Windows.Forms.Button buttonLoadSingleCoefficentAll;
        private System.Windows.Forms.Button buttonLoadSingleCoefficentPressure;
        private System.Windows.Forms.Button buttonLoadSingleCoefficentHumidity;
        private System.Windows.Forms.Button buttonLoadAllCoefficentAll;
        private System.Windows.Forms.Button buttonLoadAllCoefficentPressure;
        private System.Windows.Forms.Button buttonLoadAllCoefficentHumidity;
        private System.Windows.Forms.Button buttonLoadAllCoefficentHumidityTemp;
        private System.Windows.Forms.Button buttonLoadAllCoefficentAirTemp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem logFormatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multiSondeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iMet1TestToolStripMenuItem;
        private System.Windows.Forms.Button buttonLoadCancel;
        private System.Windows.Forms.Button buttonClearReport;
        private System.Windows.Forms.DataGridView dataGridViewRackA;
        private System.Windows.Forms.DataGridView dataGridViewRackB;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridView dataGridViewRackC;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridView dataGridViewRackD;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.StatusStrip statusStripRackB;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRackB;
        private System.Windows.Forms.ToolStripMenuItem changeUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem sondeProgrammingToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxCorrection;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveSnapshotToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem sondeAuxSettingsToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPOS;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSondeSN;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSondeTN;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSondePN;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSondeCom;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPressureTemp;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPressure;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPDiff;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTemperature;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTDiff;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnHumidity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUDiff;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFirmware;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTXMode;
        public System.Windows.Forms.ToolStripMenuItem sondeRelaySettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pressureManagerToolStripMenuItem;
    }
}

