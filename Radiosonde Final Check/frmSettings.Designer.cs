﻿namespace Radiosonde_Final_Check
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxRackComPorts = new System.Windows.Forms.GroupBox();
            this.comboBoxRackDEndingCom = new System.Windows.Forms.ComboBox();
            this.comboBoxRackDStartingCom = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxRackCEndingCom = new System.Windows.Forms.ComboBox();
            this.comboBoxRackCStartingCom = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxRackBEndingCom = new System.Windows.Forms.ComboBox();
            this.comboBoxRackBStartingCom = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxRackAEndingCom = new System.Windows.Forms.ComboBox();
            this.comboBoxRackAStartingCom = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxSensorsSettings = new System.Windows.Forms.GroupBox();
            this.comboBoxEE31ComPort = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.comboBoxSpectrumComPort = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBoxGPSComPort = new System.Windows.Forms.ComboBox();
            this.comboBoxHMP234ComPort = new System.Windows.Forms.ComboBox();
            this.comboBoxGEHygroComPort = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxChamberComPort = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxColorSettings = new System.Windows.Forms.GroupBox();
            this.panelColorRepeat = new System.Windows.Forms.Panel();
            this.panelColorEnSciSN = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.panelColoriMet1SN = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.panelColorFail = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.panelColorPass = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBoxAcceptableDiff = new System.Windows.Forms.GroupBox();
            this.textBoxAcceptableHumidityDiff = new System.Windows.Forms.TextBox();
            this.textBoxAcceptableTempDiff = new System.Windows.Forms.TextBox();
            this.textBoxAcceptablePressureDiff = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBoxFileLOC = new System.Windows.Forms.GroupBox();
            this.buttonSelectxCoefficentLOC = new System.Windows.Forms.Button();
            this.buttonSelectCoefficentLOC = new System.Windows.Forms.Button();
            this.textBoxxCalCoefficentStorageLoc = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.textBoxCoefficentStorageLoc = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tabControlSettings = new System.Windows.Forms.TabControl();
            this.tabPageRack = new System.Windows.Forms.TabPage();
            this.tabPageSensors = new System.Windows.Forms.TabPage();
            this.groupBoxChamberSettings = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tabPageColors = new System.Windows.Forms.TabPage();
            this.tabPageFiles = new System.Windows.Forms.TabPage();
            this.textBoxParoConnection = new System.Windows.Forms.TextBox();
            this.groupBoxRackComPorts.SuspendLayout();
            this.groupBoxSensorsSettings.SuspendLayout();
            this.groupBoxColorSettings.SuspendLayout();
            this.groupBoxAcceptableDiff.SuspendLayout();
            this.groupBoxFileLOC.SuspendLayout();
            this.tabControlSettings.SuspendLayout();
            this.tabPageRack.SuspendLayout();
            this.tabPageSensors.SuspendLayout();
            this.groupBoxChamberSettings.SuspendLayout();
            this.tabPageColors.SuspendLayout();
            this.tabPageFiles.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxRackComPorts
            // 
            this.groupBoxRackComPorts.Controls.Add(this.comboBoxRackDEndingCom);
            this.groupBoxRackComPorts.Controls.Add(this.comboBoxRackDStartingCom);
            this.groupBoxRackComPorts.Controls.Add(this.label9);
            this.groupBoxRackComPorts.Controls.Add(this.comboBoxRackCEndingCom);
            this.groupBoxRackComPorts.Controls.Add(this.comboBoxRackCStartingCom);
            this.groupBoxRackComPorts.Controls.Add(this.label7);
            this.groupBoxRackComPorts.Controls.Add(this.comboBoxRackBEndingCom);
            this.groupBoxRackComPorts.Controls.Add(this.comboBoxRackBStartingCom);
            this.groupBoxRackComPorts.Controls.Add(this.label5);
            this.groupBoxRackComPorts.Controls.Add(this.comboBoxRackAEndingCom);
            this.groupBoxRackComPorts.Controls.Add(this.comboBoxRackAStartingCom);
            this.groupBoxRackComPorts.Controls.Add(this.label10);
            this.groupBoxRackComPorts.Controls.Add(this.label8);
            this.groupBoxRackComPorts.Controls.Add(this.label6);
            this.groupBoxRackComPorts.Controls.Add(this.label4);
            this.groupBoxRackComPorts.Controls.Add(this.label3);
            this.groupBoxRackComPorts.Controls.Add(this.label2);
            this.groupBoxRackComPorts.Controls.Add(this.label1);
            this.groupBoxRackComPorts.Location = new System.Drawing.Point(3, 3);
            this.groupBoxRackComPorts.Name = "groupBoxRackComPorts";
            this.groupBoxRackComPorts.Size = new System.Drawing.Size(231, 142);
            this.groupBoxRackComPorts.TabIndex = 0;
            this.groupBoxRackComPorts.TabStop = false;
            this.groupBoxRackComPorts.Text = "Rack Com Ports";
            // 
            // comboBoxRackDEndingCom
            // 
            this.comboBoxRackDEndingCom.FormattingEnabled = true;
            this.comboBoxRackDEndingCom.Location = new System.Drawing.Point(152, 109);
            this.comboBoxRackDEndingCom.Name = "comboBoxRackDEndingCom";
            this.comboBoxRackDEndingCom.Size = new System.Drawing.Size(69, 21);
            this.comboBoxRackDEndingCom.TabIndex = 28;
            // 
            // comboBoxRackDStartingCom
            // 
            this.comboBoxRackDStartingCom.FormattingEnabled = true;
            this.comboBoxRackDStartingCom.Location = new System.Drawing.Point(58, 109);
            this.comboBoxRackDStartingCom.Name = "comboBoxRackDStartingCom";
            this.comboBoxRackDStartingCom.Size = new System.Drawing.Size(69, 21);
            this.comboBoxRackDStartingCom.TabIndex = 27;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(133, 112);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "--";
            // 
            // comboBoxRackCEndingCom
            // 
            this.comboBoxRackCEndingCom.FormattingEnabled = true;
            this.comboBoxRackCEndingCom.Location = new System.Drawing.Point(152, 83);
            this.comboBoxRackCEndingCom.Name = "comboBoxRackCEndingCom";
            this.comboBoxRackCEndingCom.Size = new System.Drawing.Size(69, 21);
            this.comboBoxRackCEndingCom.TabIndex = 25;
            // 
            // comboBoxRackCStartingCom
            // 
            this.comboBoxRackCStartingCom.FormattingEnabled = true;
            this.comboBoxRackCStartingCom.Location = new System.Drawing.Point(58, 83);
            this.comboBoxRackCStartingCom.Name = "comboBoxRackCStartingCom";
            this.comboBoxRackCStartingCom.Size = new System.Drawing.Size(69, 21);
            this.comboBoxRackCStartingCom.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(133, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "--";
            // 
            // comboBoxRackBEndingCom
            // 
            this.comboBoxRackBEndingCom.FormattingEnabled = true;
            this.comboBoxRackBEndingCom.Location = new System.Drawing.Point(152, 57);
            this.comboBoxRackBEndingCom.Name = "comboBoxRackBEndingCom";
            this.comboBoxRackBEndingCom.Size = new System.Drawing.Size(69, 21);
            this.comboBoxRackBEndingCom.TabIndex = 22;
            // 
            // comboBoxRackBStartingCom
            // 
            this.comboBoxRackBStartingCom.FormattingEnabled = true;
            this.comboBoxRackBStartingCom.Location = new System.Drawing.Point(58, 57);
            this.comboBoxRackBStartingCom.Name = "comboBoxRackBStartingCom";
            this.comboBoxRackBStartingCom.Size = new System.Drawing.Size(69, 21);
            this.comboBoxRackBStartingCom.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(133, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "--";
            // 
            // comboBoxRackAEndingCom
            // 
            this.comboBoxRackAEndingCom.FormattingEnabled = true;
            this.comboBoxRackAEndingCom.Location = new System.Drawing.Point(152, 31);
            this.comboBoxRackAEndingCom.Name = "comboBoxRackAEndingCom";
            this.comboBoxRackAEndingCom.Size = new System.Drawing.Size(69, 21);
            this.comboBoxRackAEndingCom.TabIndex = 19;
            // 
            // comboBoxRackAStartingCom
            // 
            this.comboBoxRackAStartingCom.FormattingEnabled = true;
            this.comboBoxRackAStartingCom.Location = new System.Drawing.Point(58, 31);
            this.comboBoxRackAStartingCom.Name = "comboBoxRackAStartingCom";
            this.comboBoxRackAStartingCom.Size = new System.Drawing.Size(69, 21);
            this.comboBoxRackAStartingCom.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 112);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Rack D:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Rack C:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Rack B:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(133, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "--";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(123, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ending Com";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Starting Com";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rack A:";
            // 
            // groupBoxSensorsSettings
            // 
            this.groupBoxSensorsSettings.Controls.Add(this.textBoxParoConnection);
            this.groupBoxSensorsSettings.Controls.Add(this.comboBoxEE31ComPort);
            this.groupBoxSensorsSettings.Controls.Add(this.label27);
            this.groupBoxSensorsSettings.Controls.Add(this.comboBoxSpectrumComPort);
            this.groupBoxSensorsSettings.Controls.Add(this.label16);
            this.groupBoxSensorsSettings.Controls.Add(this.comboBoxGPSComPort);
            this.groupBoxSensorsSettings.Controls.Add(this.comboBoxHMP234ComPort);
            this.groupBoxSensorsSettings.Controls.Add(this.comboBoxGEHygroComPort);
            this.groupBoxSensorsSettings.Controls.Add(this.label14);
            this.groupBoxSensorsSettings.Controls.Add(this.label13);
            this.groupBoxSensorsSettings.Controls.Add(this.label12);
            this.groupBoxSensorsSettings.Controls.Add(this.label11);
            this.groupBoxSensorsSettings.Location = new System.Drawing.Point(3, 3);
            this.groupBoxSensorsSettings.Name = "groupBoxSensorsSettings";
            this.groupBoxSensorsSettings.Size = new System.Drawing.Size(231, 205);
            this.groupBoxSensorsSettings.TabIndex = 2;
            this.groupBoxSensorsSettings.TabStop = false;
            this.groupBoxSensorsSettings.Text = "Sensor Settings";
            // 
            // comboBoxEE31ComPort
            // 
            this.comboBoxEE31ComPort.FormattingEnabled = true;
            this.comboBoxEE31ComPort.Location = new System.Drawing.Point(134, 70);
            this.comboBoxEE31ComPort.Name = "comboBoxEE31ComPort";
            this.comboBoxEE31ComPort.Size = new System.Drawing.Size(69, 21);
            this.comboBoxEE31ComPort.TabIndex = 28;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(46, 73);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 13);
            this.label27.TabIndex = 27;
            this.label27.Text = "EE31 Com Port:";
            // 
            // comboBoxSpectrumComPort
            // 
            this.comboBoxSpectrumComPort.Enabled = false;
            this.comboBoxSpectrumComPort.FormattingEnabled = true;
            this.comboBoxSpectrumComPort.Location = new System.Drawing.Point(134, 124);
            this.comboBoxSpectrumComPort.Name = "comboBoxSpectrumComPort";
            this.comboBoxSpectrumComPort.Size = new System.Drawing.Size(69, 21);
            this.comboBoxSpectrumComPort.TabIndex = 26;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(74, 127);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "Spectrum:";
            // 
            // comboBoxGPSComPort
            // 
            this.comboBoxGPSComPort.Enabled = false;
            this.comboBoxGPSComPort.FormattingEnabled = true;
            this.comboBoxGPSComPort.Location = new System.Drawing.Point(134, 151);
            this.comboBoxGPSComPort.Name = "comboBoxGPSComPort";
            this.comboBoxGPSComPort.Size = new System.Drawing.Size(69, 21);
            this.comboBoxGPSComPort.TabIndex = 22;
            // 
            // comboBoxHMP234ComPort
            // 
            this.comboBoxHMP234ComPort.FormattingEnabled = true;
            this.comboBoxHMP234ComPort.Location = new System.Drawing.Point(134, 43);
            this.comboBoxHMP234ComPort.Name = "comboBoxHMP234ComPort";
            this.comboBoxHMP234ComPort.Size = new System.Drawing.Size(69, 21);
            this.comboBoxHMP234ComPort.TabIndex = 20;
            // 
            // comboBoxGEHygroComPort
            // 
            this.comboBoxGEHygroComPort.FormattingEnabled = true;
            this.comboBoxGEHygroComPort.Location = new System.Drawing.Point(134, 17);
            this.comboBoxGEHygroComPort.Name = "comboBoxGEHygroComPort";
            this.comboBoxGEHygroComPort.Size = new System.Drawing.Size(69, 21);
            this.comboBoxGEHygroComPort.TabIndex = 19;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(51, 154);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "GPS Com Port:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(2, 100);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Paro Com Port:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(31, 46);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(98, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "HMP234 Com Port:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "GE Hygro Com Port:";
            // 
            // comboBoxChamberComPort
            // 
            this.comboBoxChamberComPort.FormattingEnabled = true;
            this.comboBoxChamberComPort.Location = new System.Drawing.Point(116, 56);
            this.comboBoxChamberComPort.Name = "comboBoxChamberComPort";
            this.comboBoxChamberComPort.Size = new System.Drawing.Size(69, 21);
            this.comboBoxChamberComPort.TabIndex = 24;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "Chamber Com Port:";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(330, 256);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Ok";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(411, 256);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // groupBoxColorSettings
            // 
            this.groupBoxColorSettings.Controls.Add(this.panelColorRepeat);
            this.groupBoxColorSettings.Controls.Add(this.panelColorEnSciSN);
            this.groupBoxColorSettings.Controls.Add(this.label25);
            this.groupBoxColorSettings.Controls.Add(this.panelColoriMet1SN);
            this.groupBoxColorSettings.Controls.Add(this.label24);
            this.groupBoxColorSettings.Controls.Add(this.panelColorFail);
            this.groupBoxColorSettings.Controls.Add(this.label23);
            this.groupBoxColorSettings.Controls.Add(this.panelColorPass);
            this.groupBoxColorSettings.Controls.Add(this.label22);
            this.groupBoxColorSettings.Controls.Add(this.label21);
            this.groupBoxColorSettings.Controls.Add(this.label17);
            this.groupBoxColorSettings.Location = new System.Drawing.Point(3, 3);
            this.groupBoxColorSettings.Name = "groupBoxColorSettings";
            this.groupBoxColorSettings.Size = new System.Drawing.Size(231, 160);
            this.groupBoxColorSettings.TabIndex = 5;
            this.groupBoxColorSettings.TabStop = false;
            this.groupBoxColorSettings.Text = "Color Settings";
            // 
            // panelColorRepeat
            // 
            this.panelColorRepeat.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColorRepeat.Location = new System.Drawing.Point(105, 130);
            this.panelColorRepeat.Name = "panelColorRepeat";
            this.panelColorRepeat.Size = new System.Drawing.Size(21, 21);
            this.panelColorRepeat.TabIndex = 15;
            this.panelColorRepeat.DoubleClick += new System.EventHandler(this.panelColorRepeat_DoubleClick);
            // 
            // panelColorEnSciSN
            // 
            this.panelColorEnSciSN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColorEnSciSN.Location = new System.Drawing.Point(105, 103);
            this.panelColorEnSciSN.Name = "panelColorEnSciSN";
            this.panelColorEnSciSN.Size = new System.Drawing.Size(21, 21);
            this.panelColorEnSciSN.TabIndex = 12;
            this.panelColorEnSciSN.DoubleClick += new System.EventHandler(this.panelColorEnSciSN_DoubleClick);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(9, 132);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 13);
            this.label25.TabIndex = 13;
            this.label25.Text = "Repeat Element:";
            // 
            // panelColoriMet1SN
            // 
            this.panelColoriMet1SN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColoriMet1SN.Location = new System.Drawing.Point(105, 76);
            this.panelColoriMet1SN.Name = "panelColoriMet1SN";
            this.panelColoriMet1SN.Size = new System.Drawing.Size(21, 21);
            this.panelColoriMet1SN.TabIndex = 9;
            this.panelColoriMet1SN.DoubleClick += new System.EventHandler(this.panelColoriMet1SN_DoubleClick);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(38, 105);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "EnSci SN:";
            // 
            // panelColorFail
            // 
            this.panelColorFail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColorFail.Location = new System.Drawing.Point(105, 49);
            this.panelColorFail.Name = "panelColorFail";
            this.panelColorFail.Size = new System.Drawing.Size(21, 21);
            this.panelColorFail.TabIndex = 6;
            this.panelColorFail.DoubleClick += new System.EventHandler(this.panelColorFail_DoubleClick);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(38, 78);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 13);
            this.label23.TabIndex = 7;
            this.label23.Text = "iMet-1 SN:";
            // 
            // panelColorPass
            // 
            this.panelColorPass.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelColorPass.Location = new System.Drawing.Point(105, 22);
            this.panelColorPass.Name = "panelColorPass";
            this.panelColorPass.Size = new System.Drawing.Size(21, 21);
            this.panelColorPass.TabIndex = 3;
            this.panelColorPass.DoubleClick += new System.EventHandler(this.panelColorPass_DoubleClick);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(69, 51);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(26, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Fail:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(62, 24);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Pass:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(0, 13);
            this.label17.TabIndex = 0;
            // 
            // groupBoxAcceptableDiff
            // 
            this.groupBoxAcceptableDiff.Controls.Add(this.textBoxAcceptableHumidityDiff);
            this.groupBoxAcceptableDiff.Controls.Add(this.textBoxAcceptableTempDiff);
            this.groupBoxAcceptableDiff.Controls.Add(this.textBoxAcceptablePressureDiff);
            this.groupBoxAcceptableDiff.Controls.Add(this.label19);
            this.groupBoxAcceptableDiff.Controls.Add(this.label20);
            this.groupBoxAcceptableDiff.Controls.Add(this.label18);
            this.groupBoxAcceptableDiff.Location = new System.Drawing.Point(240, 3);
            this.groupBoxAcceptableDiff.Name = "groupBoxAcceptableDiff";
            this.groupBoxAcceptableDiff.Size = new System.Drawing.Size(228, 100);
            this.groupBoxAcceptableDiff.TabIndex = 6;
            this.groupBoxAcceptableDiff.TabStop = false;
            this.groupBoxAcceptableDiff.Text = "Acceptable Diff\'s";
            // 
            // textBoxAcceptableHumidityDiff
            // 
            this.textBoxAcceptableHumidityDiff.Location = new System.Drawing.Point(151, 69);
            this.textBoxAcceptableHumidityDiff.Name = "textBoxAcceptableHumidityDiff";
            this.textBoxAcceptableHumidityDiff.Size = new System.Drawing.Size(60, 20);
            this.textBoxAcceptableHumidityDiff.TabIndex = 5;
            // 
            // textBoxAcceptableTempDiff
            // 
            this.textBoxAcceptableTempDiff.Location = new System.Drawing.Point(151, 43);
            this.textBoxAcceptableTempDiff.Name = "textBoxAcceptableTempDiff";
            this.textBoxAcceptableTempDiff.Size = new System.Drawing.Size(60, 20);
            this.textBoxAcceptableTempDiff.TabIndex = 4;
            // 
            // textBoxAcceptablePressureDiff
            // 
            this.textBoxAcceptablePressureDiff.Location = new System.Drawing.Point(151, 17);
            this.textBoxAcceptablePressureDiff.Name = "textBoxAcceptablePressureDiff";
            this.textBoxAcceptablePressureDiff.Size = new System.Drawing.Size(60, 20);
            this.textBoxAcceptablePressureDiff.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(32, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(113, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Acceptable Temp Diff:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(19, 72);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(126, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Acceptable Humidity Diff:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(18, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(127, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Acceptable Pressure Diff:";
            // 
            // groupBoxFileLOC
            // 
            this.groupBoxFileLOC.Controls.Add(this.buttonSelectxCoefficentLOC);
            this.groupBoxFileLOC.Controls.Add(this.buttonSelectCoefficentLOC);
            this.groupBoxFileLOC.Controls.Add(this.textBoxxCalCoefficentStorageLoc);
            this.groupBoxFileLOC.Controls.Add(this.label28);
            this.groupBoxFileLOC.Controls.Add(this.textBoxCoefficentStorageLoc);
            this.groupBoxFileLOC.Controls.Add(this.label26);
            this.groupBoxFileLOC.Location = new System.Drawing.Point(3, 3);
            this.groupBoxFileLOC.Name = "groupBoxFileLOC";
            this.groupBoxFileLOC.Size = new System.Drawing.Size(468, 109);
            this.groupBoxFileLOC.TabIndex = 7;
            this.groupBoxFileLOC.TabStop = false;
            this.groupBoxFileLOC.Text = "File Options";
            // 
            // buttonSelectxCoefficentLOC
            // 
            this.buttonSelectxCoefficentLOC.Location = new System.Drawing.Point(433, 77);
            this.buttonSelectxCoefficentLOC.Name = "buttonSelectxCoefficentLOC";
            this.buttonSelectxCoefficentLOC.Size = new System.Drawing.Size(29, 23);
            this.buttonSelectxCoefficentLOC.TabIndex = 5;
            this.buttonSelectxCoefficentLOC.Text = "<-";
            this.buttonSelectxCoefficentLOC.UseVisualStyleBackColor = true;
            this.buttonSelectxCoefficentLOC.Click += new System.EventHandler(this.buttonSelectxCoefficentLOC_Click);
            // 
            // buttonSelectCoefficentLOC
            // 
            this.buttonSelectCoefficentLOC.Location = new System.Drawing.Point(433, 35);
            this.buttonSelectCoefficentLOC.Name = "buttonSelectCoefficentLOC";
            this.buttonSelectCoefficentLOC.Size = new System.Drawing.Size(29, 23);
            this.buttonSelectCoefficentLOC.TabIndex = 4;
            this.buttonSelectCoefficentLOC.Text = "<-";
            this.buttonSelectCoefficentLOC.UseVisualStyleBackColor = true;
            this.buttonSelectCoefficentLOC.Click += new System.EventHandler(this.buttonSelectCoefficentLOC_Click);
            // 
            // textBoxxCalCoefficentStorageLoc
            // 
            this.textBoxxCalCoefficentStorageLoc.Location = new System.Drawing.Point(13, 79);
            this.textBoxxCalCoefficentStorageLoc.Name = "textBoxxCalCoefficentStorageLoc";
            this.textBoxxCalCoefficentStorageLoc.Size = new System.Drawing.Size(414, 20);
            this.textBoxxCalCoefficentStorageLoc.TabIndex = 3;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(9, 62);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(165, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "xCal Coefficent Storage Location:";
            // 
            // textBoxCoefficentStorageLoc
            // 
            this.textBoxCoefficentStorageLoc.Location = new System.Drawing.Point(13, 37);
            this.textBoxCoefficentStorageLoc.Name = "textBoxCoefficentStorageLoc";
            this.textBoxCoefficentStorageLoc.Size = new System.Drawing.Size(414, 20);
            this.textBoxCoefficentStorageLoc.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(9, 20);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(142, 13);
            this.label26.TabIndex = 0;
            this.label26.Text = "Coefficent Storage Location:";
            // 
            // tabControlSettings
            // 
            this.tabControlSettings.Controls.Add(this.tabPageRack);
            this.tabControlSettings.Controls.Add(this.tabPageSensors);
            this.tabControlSettings.Controls.Add(this.tabPageColors);
            this.tabControlSettings.Controls.Add(this.tabPageFiles);
            this.tabControlSettings.Location = new System.Drawing.Point(7, 13);
            this.tabControlSettings.Name = "tabControlSettings";
            this.tabControlSettings.SelectedIndex = 0;
            this.tabControlSettings.Size = new System.Drawing.Size(483, 237);
            this.tabControlSettings.TabIndex = 8;
            // 
            // tabPageRack
            // 
            this.tabPageRack.Controls.Add(this.groupBoxRackComPorts);
            this.tabPageRack.Location = new System.Drawing.Point(4, 22);
            this.tabPageRack.Name = "tabPageRack";
            this.tabPageRack.Size = new System.Drawing.Size(475, 211);
            this.tabPageRack.TabIndex = 0;
            this.tabPageRack.Text = "Rack";
            this.tabPageRack.UseVisualStyleBackColor = true;
            // 
            // tabPageSensors
            // 
            this.tabPageSensors.Controls.Add(this.groupBoxChamberSettings);
            this.tabPageSensors.Controls.Add(this.groupBoxSensorsSettings);
            this.tabPageSensors.Controls.Add(this.groupBoxAcceptableDiff);
            this.tabPageSensors.Location = new System.Drawing.Point(4, 22);
            this.tabPageSensors.Name = "tabPageSensors";
            this.tabPageSensors.Size = new System.Drawing.Size(475, 211);
            this.tabPageSensors.TabIndex = 1;
            this.tabPageSensors.Text = "Sensors";
            this.tabPageSensors.UseVisualStyleBackColor = true;
            // 
            // groupBoxChamberSettings
            // 
            this.groupBoxChamberSettings.Controls.Add(this.comboBox1);
            this.groupBoxChamberSettings.Controls.Add(this.label29);
            this.groupBoxChamberSettings.Controls.Add(this.label15);
            this.groupBoxChamberSettings.Controls.Add(this.comboBoxChamberComPort);
            this.groupBoxChamberSettings.Location = new System.Drawing.Point(240, 109);
            this.groupBoxChamberSettings.Name = "groupBoxChamberSettings";
            this.groupBoxChamberSettings.Size = new System.Drawing.Size(228, 99);
            this.groupBoxChamberSettings.TabIndex = 7;
            this.groupBoxChamberSettings.TabStop = false;
            this.groupBoxChamberSettings.Text = "Chamber Settings";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Watlow",
            "Thermotron"});
            this.comboBox1.Location = new System.Drawing.Point(116, 29);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(99, 21);
            this.comboBox1.TabIndex = 26;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(11, 32);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(99, 13);
            this.label29.TabIndex = 25;
            this.label29.Text = "Chamber Controller:";
            // 
            // tabPageColors
            // 
            this.tabPageColors.Controls.Add(this.groupBoxColorSettings);
            this.tabPageColors.Location = new System.Drawing.Point(4, 22);
            this.tabPageColors.Name = "tabPageColors";
            this.tabPageColors.Size = new System.Drawing.Size(475, 211);
            this.tabPageColors.TabIndex = 2;
            this.tabPageColors.Text = "Colors";
            this.tabPageColors.UseVisualStyleBackColor = true;
            // 
            // tabPageFiles
            // 
            this.tabPageFiles.Controls.Add(this.groupBoxFileLOC);
            this.tabPageFiles.Location = new System.Drawing.Point(4, 22);
            this.tabPageFiles.Name = "tabPageFiles";
            this.tabPageFiles.Size = new System.Drawing.Size(475, 211);
            this.tabPageFiles.TabIndex = 3;
            this.tabPageFiles.Text = "Files";
            this.tabPageFiles.UseVisualStyleBackColor = true;
            // 
            // textBoxParoConnection
            // 
            this.textBoxParoConnection.Location = new System.Drawing.Point(87, 97);
            this.textBoxParoConnection.Name = "textBoxParoConnection";
            this.textBoxParoConnection.Size = new System.Drawing.Size(138, 20);
            this.textBoxParoConnection.TabIndex = 29;
            // 
            // frmSettings
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(501, 285);
            this.Controls.Add(this.tabControlSettings);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSettings";
            this.ShowInTaskbar = false;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.groupBoxRackComPorts.ResumeLayout(false);
            this.groupBoxRackComPorts.PerformLayout();
            this.groupBoxSensorsSettings.ResumeLayout(false);
            this.groupBoxSensorsSettings.PerformLayout();
            this.groupBoxColorSettings.ResumeLayout(false);
            this.groupBoxColorSettings.PerformLayout();
            this.groupBoxAcceptableDiff.ResumeLayout(false);
            this.groupBoxAcceptableDiff.PerformLayout();
            this.groupBoxFileLOC.ResumeLayout(false);
            this.groupBoxFileLOC.PerformLayout();
            this.tabControlSettings.ResumeLayout(false);
            this.tabPageRack.ResumeLayout(false);
            this.tabPageSensors.ResumeLayout(false);
            this.groupBoxChamberSettings.ResumeLayout(false);
            this.groupBoxChamberSettings.PerformLayout();
            this.tabPageColors.ResumeLayout(false);
            this.tabPageFiles.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxRackComPorts;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBoxSensorsSettings;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ComboBox comboBoxRackAStartingCom;
        private System.Windows.Forms.ComboBox comboBoxRackAEndingCom;
        private System.Windows.Forms.ComboBox comboBoxRackDEndingCom;
        private System.Windows.Forms.ComboBox comboBoxRackDStartingCom;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxRackCEndingCom;
        private System.Windows.Forms.ComboBox comboBoxRackCStartingCom;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxRackBEndingCom;
        private System.Windows.Forms.ComboBox comboBoxRackBStartingCom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxGPSComPort;
        private System.Windows.Forms.ComboBox comboBoxHMP234ComPort;
        private System.Windows.Forms.ComboBox comboBoxGEHygroComPort;
        private System.Windows.Forms.ComboBox comboBoxChamberComPort;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBoxSpectrumComPort;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBoxColorSettings;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBoxAcceptableDiff;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxAcceptableHumidityDiff;
        private System.Windows.Forms.TextBox textBoxAcceptableTempDiff;
        private System.Windows.Forms.TextBox textBoxAcceptablePressureDiff;
        private System.Windows.Forms.Panel panelColorPass;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panelColorFail;
        private System.Windows.Forms.Panel panelColoriMet1SN;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panelColorRepeat;
        private System.Windows.Forms.Panel panelColorEnSciSN;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBoxFileLOC;
        private System.Windows.Forms.TextBox textBoxCoefficentStorageLoc;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabControl tabControlSettings;
        private System.Windows.Forms.TabPage tabPageRack;
        private System.Windows.Forms.TabPage tabPageSensors;
        private System.Windows.Forms.TabPage tabPageColors;
        private System.Windows.Forms.TabPage tabPageFiles;
        private System.Windows.Forms.ComboBox comboBoxEE31ComPort;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox textBoxxCalCoefficentStorageLoc;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBoxChamberSettings;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button buttonSelectxCoefficentLOC;
        private System.Windows.Forms.Button buttonSelectCoefficentLOC;
        private System.Windows.Forms.TextBox textBoxParoConnection;
    }
}