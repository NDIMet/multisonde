﻿namespace Radiosonde_Final_Check
{
    partial class frmProgrammingDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProgrammingDisplay));
            this.statusStripProgramming = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelProgramming = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBoxControls = new System.Windows.Forms.GroupBox();
            this.buttonWritexCalFiles = new System.Windows.Forms.Button();
            this.buttonWriteSNTNToAll = new System.Windows.Forms.Button();
            this.buttonChangeSNLetter = new System.Windows.Forms.Button();
            this.groupBoxOptions = new System.Windows.Forms.GroupBox();
            this.buttonSaveSettings = new System.Windows.Forms.Button();
            this.textBoxFreqP4 = new System.Windows.Forms.TextBox();
            this.textBoxFreqP3 = new System.Windows.Forms.TextBox();
            this.textBoxFreqP2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxFreqP1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxGPSX = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxPTUX = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxGPS = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPTU = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxStat = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxGPSRR = new System.Windows.Forms.TextBox();
            this.textBoxPTURR = new System.Windows.Forms.TextBox();
            this.textBoxHSP = new System.Windows.Forms.TextBox();
            this.textBoxTXMode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxProgrammerControls = new System.Windows.Forms.GroupBox();
            this.labelProgrammingStatus = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.buttoniMet12Bell202 = new System.Windows.Forms.Button();
            this.buttonProgramSondes = new System.Windows.Forms.Button();
            this.buttonLoadFirmware = new System.Windows.Forms.Button();
            this.statusStripProgramming.SuspendLayout();
            this.groupBoxControls.SuspendLayout();
            this.groupBoxOptions.SuspendLayout();
            this.groupBoxProgrammerControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStripProgramming
            // 
            this.statusStripProgramming.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelProgramming});
            this.statusStripProgramming.Location = new System.Drawing.Point(0, 412);
            this.statusStripProgramming.Name = "statusStripProgramming";
            this.statusStripProgramming.Size = new System.Drawing.Size(238, 22);
            this.statusStripProgramming.TabIndex = 1;
            this.statusStripProgramming.Text = "statusStrip1";
            // 
            // toolStripStatusLabelProgramming
            // 
            this.toolStripStatusLabelProgramming.Name = "toolStripStatusLabelProgramming";
            this.toolStripStatusLabelProgramming.Size = new System.Drawing.Size(43, 17);
            this.toolStripStatusLabelProgramming.Text = "XXXXXX";
            // 
            // groupBoxControls
            // 
            this.groupBoxControls.Controls.Add(this.buttonWritexCalFiles);
            this.groupBoxControls.Controls.Add(this.buttonWriteSNTNToAll);
            this.groupBoxControls.Controls.Add(this.buttonChangeSNLetter);
            this.groupBoxControls.Location = new System.Drawing.Point(12, 199);
            this.groupBoxControls.Name = "groupBoxControls";
            this.groupBoxControls.Size = new System.Drawing.Size(215, 79);
            this.groupBoxControls.TabIndex = 2;
            this.groupBoxControls.TabStop = false;
            this.groupBoxControls.Text = "Radiosonde Controls";
            // 
            // buttonWritexCalFiles
            // 
            this.buttonWritexCalFiles.Location = new System.Drawing.Point(11, 48);
            this.buttonWritexCalFiles.Name = "buttonWritexCalFiles";
            this.buttonWritexCalFiles.Size = new System.Drawing.Size(92, 23);
            this.buttonWritexCalFiles.TabIndex = 2;
            this.buttonWritexCalFiles.Text = "Write All xCals";
            this.buttonWritexCalFiles.UseVisualStyleBackColor = true;
            this.buttonWritexCalFiles.Click += new System.EventHandler(this.buttonWritexCalFiles_Click);
            // 
            // buttonWriteSNTNToAll
            // 
            this.buttonWriteSNTNToAll.Location = new System.Drawing.Point(11, 19);
            this.buttonWriteSNTNToAll.Name = "buttonWriteSNTNToAll";
            this.buttonWriteSNTNToAll.Size = new System.Drawing.Size(92, 23);
            this.buttonWriteSNTNToAll.TabIndex = 1;
            this.buttonWriteSNTNToAll.Text = "Write All SN/TN";
            this.buttonWriteSNTNToAll.UseVisualStyleBackColor = true;
            this.buttonWriteSNTNToAll.Click += new System.EventHandler(this.buttonWriteSNTNToAll_Click);
            // 
            // buttonChangeSNLetter
            // 
            this.buttonChangeSNLetter.Location = new System.Drawing.Point(109, 19);
            this.buttonChangeSNLetter.Name = "buttonChangeSNLetter";
            this.buttonChangeSNLetter.Size = new System.Drawing.Size(92, 23);
            this.buttonChangeSNLetter.TabIndex = 0;
            this.buttonChangeSNLetter.Text = "Change S Letter";
            this.buttonChangeSNLetter.UseVisualStyleBackColor = true;
            this.buttonChangeSNLetter.Click += new System.EventHandler(this.buttonChangeSNLetter_Click);
            // 
            // groupBoxOptions
            // 
            this.groupBoxOptions.Controls.Add(this.buttonSaveSettings);
            this.groupBoxOptions.Controls.Add(this.textBoxFreqP4);
            this.groupBoxOptions.Controls.Add(this.textBoxFreqP3);
            this.groupBoxOptions.Controls.Add(this.textBoxFreqP2);
            this.groupBoxOptions.Controls.Add(this.label14);
            this.groupBoxOptions.Controls.Add(this.label13);
            this.groupBoxOptions.Controls.Add(this.label12);
            this.groupBoxOptions.Controls.Add(this.textBoxFreqP1);
            this.groupBoxOptions.Controls.Add(this.label11);
            this.groupBoxOptions.Controls.Add(this.label10);
            this.groupBoxOptions.Controls.Add(this.textBoxGPSX);
            this.groupBoxOptions.Controls.Add(this.label9);
            this.groupBoxOptions.Controls.Add(this.textBoxPTUX);
            this.groupBoxOptions.Controls.Add(this.label7);
            this.groupBoxOptions.Controls.Add(this.textBoxGPS);
            this.groupBoxOptions.Controls.Add(this.label6);
            this.groupBoxOptions.Controls.Add(this.textBoxPTU);
            this.groupBoxOptions.Controls.Add(this.label5);
            this.groupBoxOptions.Controls.Add(this.textBoxStat);
            this.groupBoxOptions.Controls.Add(this.label8);
            this.groupBoxOptions.Controls.Add(this.textBoxGPSRR);
            this.groupBoxOptions.Controls.Add(this.textBoxPTURR);
            this.groupBoxOptions.Controls.Add(this.textBoxHSP);
            this.groupBoxOptions.Controls.Add(this.textBoxTXMode);
            this.groupBoxOptions.Controls.Add(this.label4);
            this.groupBoxOptions.Controls.Add(this.label3);
            this.groupBoxOptions.Controls.Add(this.label2);
            this.groupBoxOptions.Controls.Add(this.label1);
            this.groupBoxOptions.Location = new System.Drawing.Point(12, 12);
            this.groupBoxOptions.Name = "groupBoxOptions";
            this.groupBoxOptions.Size = new System.Drawing.Size(215, 181);
            this.groupBoxOptions.TabIndex = 3;
            this.groupBoxOptions.TabStop = false;
            this.groupBoxOptions.Text = "xCal File Options";
            // 
            // buttonSaveSettings
            // 
            this.buttonSaveSettings.Location = new System.Drawing.Point(68, 152);
            this.buttonSaveSettings.Name = "buttonSaveSettings";
            this.buttonSaveSettings.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveSettings.TabIndex = 33;
            this.buttonSaveSettings.Text = "Save";
            this.buttonSaveSettings.UseVisualStyleBackColor = true;
            this.buttonSaveSettings.Click += new System.EventHandler(this.buttonSaveSettings_Click);
            // 
            // textBoxFreqP4
            // 
            this.textBoxFreqP4.Location = new System.Drawing.Point(158, 126);
            this.textBoxFreqP4.Name = "textBoxFreqP4";
            this.textBoxFreqP4.Size = new System.Drawing.Size(41, 20);
            this.textBoxFreqP4.TabIndex = 32;
            this.textBoxFreqP4.Text = "402.0";
            // 
            // textBoxFreqP3
            // 
            this.textBoxFreqP3.Location = new System.Drawing.Point(111, 126);
            this.textBoxFreqP3.Name = "textBoxFreqP3";
            this.textBoxFreqP3.Size = new System.Drawing.Size(41, 20);
            this.textBoxFreqP3.TabIndex = 31;
            this.textBoxFreqP3.Text = "402.0";
            // 
            // textBoxFreqP2
            // 
            this.textBoxFreqP2.Location = new System.Drawing.Point(64, 126);
            this.textBoxFreqP2.Name = "textBoxFreqP2";
            this.textBoxFreqP2.Size = new System.Drawing.Size(41, 20);
            this.textBoxFreqP2.TabIndex = 30;
            this.textBoxFreqP2.Text = "402.0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(160, 110);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "SWP4";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(112, 110);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "SWP3";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(65, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "SWP2";
            // 
            // textBoxFreqP1
            // 
            this.textBoxFreqP1.Location = new System.Drawing.Point(17, 126);
            this.textBoxFreqP1.Name = "textBoxFreqP1";
            this.textBoxFreqP1.Size = new System.Drawing.Size(41, 20);
            this.textBoxFreqP1.TabIndex = 26;
            this.textBoxFreqP1.Text = "402.0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "SWP1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(84, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "TX Freq\'s";
            // 
            // textBoxGPSX
            // 
            this.textBoxGPSX.Location = new System.Drawing.Point(165, 71);
            this.textBoxGPSX.Name = "textBoxGPSX";
            this.textBoxGPSX.Size = new System.Drawing.Size(29, 20);
            this.textBoxGPSX.TabIndex = 20;
            this.textBoxGPSX.Text = "off";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(161, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "GPSX";
            // 
            // textBoxPTUX
            // 
            this.textBoxPTUX.Location = new System.Drawing.Point(128, 71);
            this.textBoxPTUX.Name = "textBoxPTUX";
            this.textBoxPTUX.Size = new System.Drawing.Size(29, 20);
            this.textBoxPTUX.TabIndex = 18;
            this.textBoxPTUX.Text = "off";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(124, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "PTUX";
            // 
            // textBoxGPS
            // 
            this.textBoxGPS.Location = new System.Drawing.Point(91, 71);
            this.textBoxGPS.Name = "textBoxGPS";
            this.textBoxGPS.Size = new System.Drawing.Size(29, 20);
            this.textBoxGPS.TabIndex = 16;
            this.textBoxGPS.Text = "off";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(91, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "GPS";
            // 
            // textBoxPTU
            // 
            this.textBoxPTU.Location = new System.Drawing.Point(56, 71);
            this.textBoxPTU.Name = "textBoxPTU";
            this.textBoxPTU.Size = new System.Drawing.Size(29, 20);
            this.textBoxPTU.TabIndex = 14;
            this.textBoxPTU.Text = "off";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(56, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "PTU";
            // 
            // textBoxStat
            // 
            this.textBoxStat.Location = new System.Drawing.Point(21, 71);
            this.textBoxStat.Name = "textBoxStat";
            this.textBoxStat.Size = new System.Drawing.Size(29, 20);
            this.textBoxStat.TabIndex = 12;
            this.textBoxStat.Text = "off";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Stat";
            // 
            // textBoxGPSRR
            // 
            this.textBoxGPSRR.Location = new System.Drawing.Point(167, 32);
            this.textBoxGPSRR.Name = "textBoxGPSRR";
            this.textBoxGPSRR.Size = new System.Drawing.Size(37, 20);
            this.textBoxGPSRR.TabIndex = 7;
            // 
            // textBoxPTURR
            // 
            this.textBoxPTURR.Location = new System.Drawing.Point(121, 32);
            this.textBoxPTURR.Name = "textBoxPTURR";
            this.textBoxPTURR.Size = new System.Drawing.Size(37, 20);
            this.textBoxPTURR.TabIndex = 6;
            // 
            // textBoxHSP
            // 
            this.textBoxHSP.Location = new System.Drawing.Point(78, 32);
            this.textBoxHSP.Name = "textBoxHSP";
            this.textBoxHSP.Size = new System.Drawing.Size(37, 20);
            this.textBoxHSP.TabIndex = 5;
            // 
            // textBoxTXMode
            // 
            this.textBoxTXMode.Location = new System.Drawing.Point(12, 32);
            this.textBoxTXMode.Name = "textBoxTXMode";
            this.textBoxTXMode.Size = new System.Drawing.Size(60, 20);
            this.textBoxTXMode.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(163, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "GPSRR";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(118, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "PTURR";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(83, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "HSP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "TXMode";
            // 
            // groupBoxProgrammerControls
            // 
            this.groupBoxProgrammerControls.Controls.Add(this.labelProgrammingStatus);
            this.groupBoxProgrammerControls.Controls.Add(this.label15);
            this.groupBoxProgrammerControls.Controls.Add(this.buttoniMet12Bell202);
            this.groupBoxProgrammerControls.Controls.Add(this.buttonProgramSondes);
            this.groupBoxProgrammerControls.Controls.Add(this.buttonLoadFirmware);
            this.groupBoxProgrammerControls.Enabled = false;
            this.groupBoxProgrammerControls.Location = new System.Drawing.Point(12, 284);
            this.groupBoxProgrammerControls.Name = "groupBoxProgrammerControls";
            this.groupBoxProgrammerControls.Size = new System.Drawing.Size(215, 124);
            this.groupBoxProgrammerControls.TabIndex = 4;
            this.groupBoxProgrammerControls.TabStop = false;
            this.groupBoxProgrammerControls.Text = "Programmer Control";
            // 
            // labelProgrammingStatus
            // 
            this.labelProgrammingStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelProgrammingStatus.Location = new System.Drawing.Point(6, 98);
            this.labelProgrammingStatus.Name = "labelProgrammingStatus";
            this.labelProgrammingStatus.Size = new System.Drawing.Size(202, 18);
            this.labelProgrammingStatus.TabIndex = 4;
            this.labelProgrammingStatus.Text = "..........";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 79);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Programming Status:";
            // 
            // buttoniMet12Bell202
            // 
            this.buttoniMet12Bell202.Location = new System.Drawing.Point(109, 49);
            this.buttoniMet12Bell202.Name = "buttoniMet12Bell202";
            this.buttoniMet12Bell202.Size = new System.Drawing.Size(92, 23);
            this.buttoniMet12Bell202.TabIndex = 2;
            this.buttoniMet12Bell202.Text = "IMS => Bell202";
            this.buttoniMet12Bell202.UseVisualStyleBackColor = true;
            // 
            // buttonProgramSondes
            // 
            this.buttonProgramSondes.Location = new System.Drawing.Point(10, 49);
            this.buttonProgramSondes.Name = "buttonProgramSondes";
            this.buttonProgramSondes.Size = new System.Drawing.Size(92, 23);
            this.buttonProgramSondes.TabIndex = 1;
            this.buttonProgramSondes.Text = "Prog. Sondes";
            this.buttonProgramSondes.UseVisualStyleBackColor = true;
            // 
            // buttonLoadFirmware
            // 
            this.buttonLoadFirmware.Location = new System.Drawing.Point(11, 20);
            this.buttonLoadFirmware.Name = "buttonLoadFirmware";
            this.buttonLoadFirmware.Size = new System.Drawing.Size(92, 23);
            this.buttonLoadFirmware.TabIndex = 0;
            this.buttonLoadFirmware.Text = "Load Firmware";
            this.buttonLoadFirmware.UseVisualStyleBackColor = true;
            // 
            // frmProgrammingDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(238, 434);
            this.Controls.Add(this.groupBoxProgrammerControls);
            this.Controls.Add(this.groupBoxOptions);
            this.Controls.Add(this.groupBoxControls);
            this.Controls.Add(this.statusStripProgramming);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmProgrammingDisplay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Sonde Programming Tool";
            this.Load += new System.EventHandler(this.frmProgrammingDisplay_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmProgrammingDisplay_FormClosed);
            this.statusStripProgramming.ResumeLayout(false);
            this.statusStripProgramming.PerformLayout();
            this.groupBoxControls.ResumeLayout(false);
            this.groupBoxOptions.ResumeLayout(false);
            this.groupBoxOptions.PerformLayout();
            this.groupBoxProgrammerControls.ResumeLayout(false);
            this.groupBoxProgrammerControls.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripProgramming;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelProgramming;
        private System.Windows.Forms.GroupBox groupBoxControls;
        private System.Windows.Forms.GroupBox groupBoxOptions;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxStat;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxGPSRR;
        private System.Windows.Forms.TextBox textBoxPTURR;
        private System.Windows.Forms.TextBox textBoxHSP;
        private System.Windows.Forms.TextBox textBoxTXMode;
        private System.Windows.Forms.TextBox textBoxGPSX;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxPTUX;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxGPS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPTU;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxFreqP1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxFreqP4;
        private System.Windows.Forms.TextBox textBoxFreqP3;
        private System.Windows.Forms.TextBox textBoxFreqP2;
        private System.Windows.Forms.Button buttonChangeSNLetter;
        private System.Windows.Forms.Button buttonWriteSNTNToAll;
        private System.Windows.Forms.Button buttonWritexCalFiles;
        private System.Windows.Forms.GroupBox groupBoxProgrammerControls;
        private System.Windows.Forms.Button buttonProgramSondes;
        private System.Windows.Forms.Button buttonLoadFirmware;
        private System.Windows.Forms.Button buttoniMet12Bell202;
        private System.Windows.Forms.Label labelProgrammingStatus;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button buttonSaveSettings;
    }
}