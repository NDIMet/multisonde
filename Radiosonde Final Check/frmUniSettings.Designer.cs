﻿namespace Radiosonde_Final_Check
{
    partial class frmUniSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUniSettings));
            this.groupBoxTXMode = new System.Windows.Forms.GroupBox();
            this.radioButtonBell202Mode = new System.Windows.Forms.RadioButton();
            this.radioButtonIMSMode = new System.Windows.Forms.RadioButton();
            this.groupBoxFreqSettings = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxSP4 = new System.Windows.Forms.ComboBox();
            this.comboBoxSP3 = new System.Windows.Forms.ComboBox();
            this.comboBoxSP2 = new System.Windows.Forms.ComboBox();
            this.comboBoxSP1 = new System.Windows.Forms.ComboBox();
            this.panelSwitchDetail = new System.Windows.Forms.Panel();
            this.buttonSet = new System.Windows.Forms.Button();
            this.groupBoxTXMode.SuspendLayout();
            this.groupBoxFreqSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxTXMode
            // 
            this.groupBoxTXMode.Controls.Add(this.radioButtonBell202Mode);
            this.groupBoxTXMode.Controls.Add(this.radioButtonIMSMode);
            this.groupBoxTXMode.Location = new System.Drawing.Point(12, 264);
            this.groupBoxTXMode.Name = "groupBoxTXMode";
            this.groupBoxTXMode.Size = new System.Drawing.Size(174, 44);
            this.groupBoxTXMode.TabIndex = 0;
            this.groupBoxTXMode.TabStop = false;
            this.groupBoxTXMode.Text = "Transmitter Mode";
            // 
            // radioButtonBell202Mode
            // 
            this.radioButtonBell202Mode.AutoSize = true;
            this.radioButtonBell202Mode.Location = new System.Drawing.Point(78, 20);
            this.radioButtonBell202Mode.Name = "radioButtonBell202Mode";
            this.radioButtonBell202Mode.Size = new System.Drawing.Size(90, 17);
            this.radioButtonBell202Mode.TabIndex = 1;
            this.radioButtonBell202Mode.TabStop = true;
            this.radioButtonBell202Mode.Text = "Bell202 Mode";
            this.radioButtonBell202Mode.UseVisualStyleBackColor = true;
            // 
            // radioButtonIMSMode
            // 
            this.radioButtonIMSMode.AutoSize = true;
            this.radioButtonIMSMode.Checked = true;
            this.radioButtonIMSMode.Location = new System.Drawing.Point(7, 20);
            this.radioButtonIMSMode.Name = "radioButtonIMSMode";
            this.radioButtonIMSMode.Size = new System.Drawing.Size(58, 17);
            this.radioButtonIMSMode.TabIndex = 0;
            this.radioButtonIMSMode.TabStop = true;
            this.radioButtonIMSMode.Text = "IMSTX";
            this.radioButtonIMSMode.UseVisualStyleBackColor = true;
            // 
            // groupBoxFreqSettings
            // 
            this.groupBoxFreqSettings.Controls.Add(this.label1);
            this.groupBoxFreqSettings.Controls.Add(this.comboBoxSP4);
            this.groupBoxFreqSettings.Controls.Add(this.comboBoxSP3);
            this.groupBoxFreqSettings.Controls.Add(this.comboBoxSP2);
            this.groupBoxFreqSettings.Controls.Add(this.comboBoxSP1);
            this.groupBoxFreqSettings.Controls.Add(this.panelSwitchDetail);
            this.groupBoxFreqSettings.Location = new System.Drawing.Point(12, 13);
            this.groupBoxFreqSettings.Name = "groupBoxFreqSettings";
            this.groupBoxFreqSettings.Size = new System.Drawing.Size(174, 245);
            this.groupBoxFreqSettings.TabIndex = 1;
            this.groupBoxFreqSettings.TabStop = false;
            this.groupBoxFreqSettings.Text = "TX Freq Settings";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(108, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "OFF";
            // 
            // comboBoxSP4
            // 
            this.comboBoxSP4.FormattingEnabled = true;
            this.comboBoxSP4.Location = new System.Drawing.Point(82, 166);
            this.comboBoxSP4.Name = "comboBoxSP4";
            this.comboBoxSP4.Size = new System.Drawing.Size(78, 21);
            this.comboBoxSP4.TabIndex = 4;
            this.comboBoxSP4.Enter += new System.EventHandler(this.comboBoxSP_Enter);
            this.comboBoxSP4.MouseEnter += new System.EventHandler(this.comboBoxSP4_MouseEnter);
            // 
            // comboBoxSP3
            // 
            this.comboBoxSP3.FormattingEnabled = true;
            this.comboBoxSP3.Location = new System.Drawing.Point(82, 139);
            this.comboBoxSP3.Name = "comboBoxSP3";
            this.comboBoxSP3.Size = new System.Drawing.Size(78, 21);
            this.comboBoxSP3.TabIndex = 3;
            this.comboBoxSP3.Enter += new System.EventHandler(this.comboBoxSP_Enter);
            this.comboBoxSP3.MouseEnter += new System.EventHandler(this.comboBoxSP3_MouseEnter);
            // 
            // comboBoxSP2
            // 
            this.comboBoxSP2.FormattingEnabled = true;
            this.comboBoxSP2.Location = new System.Drawing.Point(82, 112);
            this.comboBoxSP2.Name = "comboBoxSP2";
            this.comboBoxSP2.Size = new System.Drawing.Size(78, 21);
            this.comboBoxSP2.TabIndex = 2;
            this.comboBoxSP2.Enter += new System.EventHandler(this.comboBoxSP_Enter);
            this.comboBoxSP2.MouseEnter += new System.EventHandler(this.comboBoxSP2_MouseEnter);
            // 
            // comboBoxSP1
            // 
            this.comboBoxSP1.FormattingEnabled = true;
            this.comboBoxSP1.Location = new System.Drawing.Point(82, 85);
            this.comboBoxSP1.Name = "comboBoxSP1";
            this.comboBoxSP1.Size = new System.Drawing.Size(78, 21);
            this.comboBoxSP1.TabIndex = 0;
            this.comboBoxSP1.Enter += new System.EventHandler(this.comboBoxSP_Enter);
            this.comboBoxSP1.MouseEnter += new System.EventHandler(this.comboBoxSP1_MouseEnter);
            // 
            // panelSwitchDetail
            // 
            this.panelSwitchDetail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelSwitchDetail.BackgroundImage")));
            this.panelSwitchDetail.Location = new System.Drawing.Point(9, 19);
            this.panelSwitchDetail.Name = "panelSwitchDetail";
            this.panelSwitchDetail.Size = new System.Drawing.Size(86, 204);
            this.panelSwitchDetail.TabIndex = 1;
            // 
            // buttonSet
            // 
            this.buttonSet.Location = new System.Drawing.Point(41, 314);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(112, 23);
            this.buttonSet.TabIndex = 2;
            this.buttonSet.Text = "Set Radiosondes";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // frmUniSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(194, 343);
            this.Controls.Add(this.buttonSet);
            this.Controls.Add(this.groupBoxFreqSettings);
            this.Controls.Add(this.groupBoxTXMode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUniSettings";
            this.ShowInTaskbar = false;
            this.Text = "Universal TX Settings";
            this.Load += new System.EventHandler(this.frmUniSettings_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUniSettings_FormClosing);
            this.groupBoxTXMode.ResumeLayout(false);
            this.groupBoxTXMode.PerformLayout();
            this.groupBoxFreqSettings.ResumeLayout(false);
            this.groupBoxFreqSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxTXMode;
        private System.Windows.Forms.RadioButton radioButtonBell202Mode;
        private System.Windows.Forms.RadioButton radioButtonIMSMode;
        private System.Windows.Forms.GroupBox groupBoxFreqSettings;
        private System.Windows.Forms.ComboBox comboBoxSP1;
        private System.Windows.Forms.Panel panelSwitchDetail;
        private System.Windows.Forms.ComboBox comboBoxSP3;
        private System.Windows.Forms.ComboBox comboBoxSP2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxSP4;
        private System.Windows.Forms.Button buttonSet;
    }
}