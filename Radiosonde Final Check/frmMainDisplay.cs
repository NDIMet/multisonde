﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.IO;

using Radiosonde;
using TestEquipment;
using Universal_Radiosonde;


namespace Radiosonde_Final_Check
{
    #region Delegates for invoking back into the main thread

    //Data to be displayed from sensor thread to main thread
    delegate void UpdateTempHumidity(string currentTemp, string currentHumidity);

    //Data to be displayed from pressure sensor thread to main thread.
    delegate void UpdatePressure(string currentPressure);

    //Status to be displayed from the Background Worker
    delegate void UpdateMainStatusLabelData(string Message);

    //Status to be displated from the background Worker to the rack status labels.
    delegate void UpdateRackStatusLabelData(int Rack, string Message);

    //Data needed for setting up the racks data grids.
    delegate void UpdateRacksDataGridsData(int counterRackA, int counterRackB, int counterRackC, int counterRackD);

    //Delegate for update sonde data to screen.
    delegate void SendDataToScreen();

    //Delegate for updating the elements
    delegate void UpdateLine(Universal_Radiosonde.CalData calData);

    #endregion

    public partial class frmMainDisplay : Form
    {
        public frmMainDisplay()
        {
            InitializeComponent();
        }

        //Public declares.
        public iMet1[] ValidSondeArryiMet1;
        public iMet1U[] ValidSondeiMet1U;
        public BackgroundWorker[] loadRadiosondeCoefficent; //Background Workers for loading coefficents to a radiosonde.
        public BackgroundWorker backgroundWorkerLogRadiosondeData; //Background worker for loading radiosonde data.
        public BackgroundWorker backgroundLoadID;
        public BackgroundWorker backgroundUpdatePressureData;

        //objects needed for item updates.
        public BackgroundWorker[] backgroundRadiosondeDisplay;// Worker updated the displaying of one radiosonde.

        public List<string> commandsRun = new List<string>();
        public List<string> commandRunTime = new List<string>();

        public int numberOfSondes = 0;
        public GEHygro referanceGE = null;
        public HMP234 referanceHMP234 = null;
        public epluseEE31 referanceEE31 = null;
        //public Paroscientific referanceParo = null;   //Replaced with a global Program object.

        //public object for forms
        public frmProgrammingDisplay programmingDisplay;
        public frmUniSettings auxSettings;
        frmRelay programRelay;

        #region Local Public Static Classes

        public static class LoggingSettings //Settings of the logging conditions.
        {
            public static int loggingIntervalTime = 0;
            public static string loggingCurrentLogFile = "";
            public static bool loggingFileEntered = false;
            public static bool loggingIntervalEntered = false;
            public static int counterLoggingInterval = 0;
            public static bool loggingEnabled = false;
            public static int loggingCounterTotal = 0;
        }

        public static class ProgramSettings  
        {
            public static string CurrentUser = "";

            //Rack "A" com ports
            public static int StartRackARadiosondeComms = 90; //24;
            public static int EndRackARadiosondeComms = 92; //72;
            
            //Rack "B" com ports
            public static int StartRackBRadiosondeComms = 0;
            public static int EndRackBRadiosondeComms = 0;
            //Rack "C" com ports
            public static int StartRackCRadiosondeComms = 0;
            public static int EndRackCRadiosondeComms = 0;

            //Rack "D" com ports
            public static int StartRackDRadiosondeComms = 0;
            public static int EndRackDRadiosondeComms = 0;

            //Acceptable differances
            public static double AcceptableTempDiff = 0.5;
            public static double AcceptablePressureDiff = 1.8;
            public static double AcceptableHumidityDiff = 8;


            //Referance Sensors Settings.
            public static string SensorComPortGE = "COM3";
            public static string SensorComPortHMP234 = "COM8";
            public static string SensorParoComPort = "COM11";
            public static string SensorThermotronComPort = "COM13";
            public static string SensorComPortEE31 = "COM14";

            //Current sensors in use.
            public static string CurrentTUSensor = "";
            public static string CurrentPressureSensor = "";

            //Error log location
            public static string logErrorLocation = DateTime.Now.ToString("yyyyMMdd") + "-ErrorLog.csv";

            //Coefficent storage loaction
            public static string coefficentStorageLocation = "";
            public static string xCalCoefficentStorageLocation = "";

            //Max amount of load coefficent background workers
            public static int maxNumberOfBackgroundWorkers = 8;

            //Colors for pass, fails, radiosonde types, and other things like that.
            public static Color colorPass = System.Drawing.Color.Green;
            public static Color colorFail = System.Drawing.Color.Red;
            public static Color colorRepeat = System.Drawing.Color.LightSeaGreen;
            public static Color colorRadiosondeiMet1 = System.Drawing.Color.White;
            public static Color colorRadiosondeEnSci = System.Drawing.Color.LightGray;
            public static Color colorRadiosondeElementPass = System.Drawing.Color.White;

            public static bool logOn = true;
            
        }
        public static class RadiosondeStatus
        {
            public static bool activeRadiosondeData = false;
            public static bool processRadiosondeHex = false;
            public static string currentRadiosondeType = "";
        }

        #endregion

        #region Methods for allow child forms access to parent internal call items.

        public bool setRadiosondeStatusProcessRadiosondeHex(bool currentSet)
        {
            try
            {
                RadiosondeStatus.processRadiosondeHex = currentSet;
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
            
        }

        private void frmMainDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ProgramSettings.CurrentUser != "")
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2);

                if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    //Shutting down radiosondes.
                    if (ValidSondeArryiMet1 != null)
                    {
                        //Let people know something is happening
                        UpdateMainStatusLabel("Closing all COM ports and clearing memory. Please stand by.");
                        Update();

                        buttonControlSondeStop_Click(null, null);
                    }
                    if (ValidSondeiMet1U != null)
                    {
                        UpdateMainStatusLabel("Closing all COM ports and clearing memory. Please stand by.");
                        Update();

                        buttonControlSondeStop_Click(null, null);
                    }

                    //Shutting down referances
                    if (backgroundWorkerUpdateTempHumidity.IsBusy == true)
                    {
                        ProgramSettings.CurrentTUSensor = "Manual";
                    }

                    if (backgroundWorkerUpdatePressure.IsBusy == true)
                    {
                        ProgramSettings.CurrentPressureSensor = "Manual";
                    }
                }
            }
        }

        private void frmMainDisplay_Load(object sender, EventArgs e)
        {
            //Setting the program to process class high.
            //Process thisProc = Process.GetCurrentProcess();
            //thisProc.PriorityClass = ProcessPriorityClass.High;


            //Getting version number
            string buildVersion = System.Reflection.Assembly.GetExecutingAssembly().FullName;

            //Load settings from a file.
            loadSettingFromFile();

            //Current User doing the work form.
            string value = "";

            //Allowing for logOn to be disabled.
            if (ProgramSettings.logOn)
            {

                while (value == "")
                {
                    if (InputBox("User Name", "Your Name:", ref value) == DialogResult.OK)
                    {
                        ProgramSettings.CurrentUser = value;
                        
                    }
                    else
                    {
                        value = "Close";
                        Application.Exit();
                    }
                }
            }
            else
            {
                value = "unknownUser";
                ProgramSettings.CurrentUser = value;
            }

            //Updating display
            UpdateMainStatusLabel("Loaded... " + "Current User: " + value + " ,Current Program: " + buildVersion);

            backgroundLoadID = new BackgroundWorker();
            backgroundLoadID.DoWork +=new DoWorkEventHandler(backgroundLoadID_DoWork);

            RadiosondeStatus.currentRadiosondeType = "iMet Universal";
            comboBoxSondeType_SelectedIndexChanged(null, null);
            
        }

        public void loadSettingFromFile()
        {
            string configLine = "";
            TextReader ProgramSettingsReader = new StreamReader("Settings.cfg");
            // read lines of text

            configLine = ProgramSettingsReader.ReadToEnd();
            configLine = configLine.Replace("\r", "");
            string[] configElement = configLine.Split('\n', '=');

            // close the stream
            ProgramSettingsReader.Close();

            //Setting up the global stuff.
            //rack com ports.


            if (configElement[1].Length == 4)
            { ProgramSettings.StartRackARadiosondeComms = Convert.ToInt16(configElement[1].Substring(3, 1)); }
            if (configElement[1].Length == 5)
            { ProgramSettings.StartRackARadiosondeComms = Convert.ToInt16(configElement[1].Substring(3, 2)); }
            if (configElement[1].Length == 6)
            { ProgramSettings.StartRackARadiosondeComms = Convert.ToInt16(configElement[1].Substring(3, 3)); }

            if (configElement[3].Length == 4)
            { ProgramSettings.EndRackARadiosondeComms = Convert.ToInt16(configElement[3].Substring(3, 1)); }
            if (configElement[3].Length == 5)
            { ProgramSettings.EndRackARadiosondeComms = Convert.ToInt16(configElement[3].Substring(3, 2)); }
            if (configElement[3].Length == 6)
            { ProgramSettings.EndRackARadiosondeComms = Convert.ToInt16(configElement[3].Substring(3, 3)); }

            if (configElement[5].Length == 4)
            { ProgramSettings.StartRackBRadiosondeComms = Convert.ToInt16(configElement[5].Substring(3, 1)); }
            if (configElement[5].Length == 5)
            { ProgramSettings.StartRackBRadiosondeComms = Convert.ToInt16(configElement[5].Substring(3, 2)); }
            if (configElement[5].Length == 6)
            { ProgramSettings.StartRackBRadiosondeComms = Convert.ToInt16(configElement[5].Substring(3, 3)); }

            if (configElement[7].Length == 4)
            { ProgramSettings.EndRackBRadiosondeComms = Convert.ToInt16(configElement[7].Substring(3, 1)); }
            if (configElement[7].Length == 5)
            { ProgramSettings.EndRackBRadiosondeComms = Convert.ToInt16(configElement[7].Substring(3, 2)); }
            if (configElement[7].Length == 6)
            { ProgramSettings.EndRackBRadiosondeComms = Convert.ToInt16(configElement[7].Substring(3, 3)); }

            if (configElement[9].Length == 4)
            { ProgramSettings.StartRackCRadiosondeComms = Convert.ToInt16(configElement[9].Substring(3, 1)); }
            if (configElement[9].Length == 5)
            { ProgramSettings.StartRackCRadiosondeComms = Convert.ToInt16(configElement[9].Substring(3, 2)); }
            if (configElement[9].Length == 6)
            { ProgramSettings.StartRackCRadiosondeComms = Convert.ToInt16(configElement[9].Substring(3, 3)); }

            if (configElement[11].Length == 4)
            { ProgramSettings.EndRackCRadiosondeComms = Convert.ToInt16(configElement[11].Substring(3, 1)); }
            if (configElement[11].Length == 5)
            { ProgramSettings.EndRackCRadiosondeComms = Convert.ToInt16(configElement[11].Substring(3, 2)); }
            if (configElement[11].Length == 6)
            { ProgramSettings.EndRackCRadiosondeComms = Convert.ToInt16(configElement[11].Substring(3, 3)); }

            if (configElement[13].Length == 4)
            { ProgramSettings.StartRackDRadiosondeComms = Convert.ToInt16(configElement[13].Substring(3, 1)); }
            if (configElement[13].Length == 5)
            { ProgramSettings.StartRackDRadiosondeComms = Convert.ToInt16(configElement[13].Substring(3, 2)); }
            if (configElement[13].Length == 6)
            { ProgramSettings.StartRackDRadiosondeComms = Convert.ToInt16(configElement[13].Substring(3, 3)); }

            if (configElement[15].Length == 4)
            { ProgramSettings.EndRackDRadiosondeComms = Convert.ToInt16(configElement[15].Substring(3, 1)); }
            if (configElement[15].Length == 5)
            { ProgramSettings.EndRackDRadiosondeComms = Convert.ToInt16(configElement[15].Substring(3, 2)); }
            if (configElement[15].Length == 6)
            { ProgramSettings.EndRackDRadiosondeComms = Convert.ToInt16(configElement[15].Substring(3, 3)); }

            //Correcting count to correct number of comports.
            if (ProgramSettings.EndRackARadiosondeComms > 0)
            { ProgramSettings.EndRackARadiosondeComms++; }

            if (ProgramSettings.EndRackBRadiosondeComms > 0)
            { ProgramSettings.EndRackBRadiosondeComms++; }

            if (ProgramSettings.EndRackCRadiosondeComms > 0)
            { ProgramSettings.EndRackCRadiosondeComms++; }

            if (ProgramSettings.EndRackDRadiosondeComms > 0)
            { ProgramSettings.EndRackDRadiosondeComms++; }
             
            //Setting sensor coms.
            ProgramSettings.SensorComPortGE = configElement[17];

            ProgramSettings.SensorComPortHMP234 = configElement[19];
            ProgramSettings.SensorParoComPort = configElement[21];
            ProgramSettings.SensorThermotronComPort = configElement[23];

            //Setting acceptable diffs.
            ProgramSettings.AcceptablePressureDiff = Convert.ToDouble(configElement[25]);
            ProgramSettings.AcceptableTempDiff = Convert.ToDouble(configElement[27]);
            ProgramSettings.AcceptableHumidityDiff = Convert.ToDouble(configElement[29]);

            //Setting coefficent storage location
            ProgramSettings.coefficentStorageLocation = configElement[31];

            //Loading colors used in the program
            ProgramSettings.colorPass = Color.FromArgb(Convert.ToInt32(configElement[35]));
            ProgramSettings.colorFail = Color.FromArgb(Convert.ToInt32(configElement[37]));
            ProgramSettings.colorRepeat = Color.FromArgb(Convert.ToInt32(configElement[39]));
            ProgramSettings.colorRadiosondeiMet1 = Color.FromArgb(Convert.ToInt32(configElement[41]));
            ProgramSettings.colorRadiosondeEnSci = Color.FromArgb(Convert.ToInt32(configElement[43]));
            ProgramSettings.colorRadiosondeElementPass = Color.FromArgb(Convert.ToInt32(configElement[45]));

            //Loading the xCal storage location
            ProgramSettings.xCalCoefficentStorageLocation = configElement[47];

            //Loading EE31 sensor Com Port settings.
            ProgramSettings.SensorComPortEE31 = configElement[49];

            //Loading logOn Settings
            ProgramSettings.logOn = Convert.ToBoolean(configElement[51]);

        }

        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.ShowInTaskbar = false;
            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        public void configUpdate(string sensor)
        {
            loadSettingFromFile();
            UpdateMainStatusLabel("Settings Updated");
        }

        public string getCurrentxCalLOC()
        {
            return ProgramSettings.xCalCoefficentStorageLocation;
        }

        #region Main controls button, combo boxes, labels, and more.
        private void buttonControlSondeStop_Click(object sender, EventArgs e)
        {
            if (LoggingSettings.loggingEnabled == true)
            {
                DialogResult loggingCurrentSondes = MessageBox.Show("Currently logging. Do you wish to cancel logging?", "Stopping Radiosonde", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (loggingCurrentSondes == DialogResult.Yes)
                {
                    //Enable pause and stop logging.
                    pauseLoggingToolStripMenuItem.Enabled = false;
                    stopLoggingToolStripMenuItem.Enabled = false;

                    //Disableing the start of logging.
                    startLoggingToolStripMenuItem.Enabled = false;

                    //Disabling logging
                    LoggingSettings.loggingEnabled = false;

                    //Disabling event programming
                    sondeRelaySettingsToolStripMenuItem.Enabled = false;

                    //Clearing current file and logging intervals
                    LoggingSettings.loggingCurrentLogFile = "";
                    LoggingSettings.loggingFileEntered = false;
                    LoggingSettings.loggingIntervalEntered = false;
                    resetCheckTimeInterval();

                    RadiosondeStatus.activeRadiosondeData = false;
                    UpdateMainStatusLabel("Logging Stopped.");
                }
                if (loggingCurrentSondes == DialogResult.No)
                {
                    return;
                }
            }

            //Stopping background worker that get the radiosonde data.
            backgroundWorkerRadiosondeDataCollect.CancelAsync();
            //timerGetRackASondeData.Enabled =false; //stopping the timer that collects the data.

            //Stopping the TU background worker.
            backgroundWorkerUpdateTempHumidity.Dispose();

            //Stopping the pressure sensor update timer.
            backgroundWorkerUpdatePressure.Dispose();

            Thread.Sleep(100); //taking a little break to wait for any timer code to end.

            UpdateMainStatusLabel("Clearing memory, please stand by.");
            toolStripStatusLabelRackA.Text = "Clearing memory, please stand by.";
            toolStripStatusLabelRackB.Text = "Clearing memory, please stand by.";
            toolStripStatusLabelRackC.Text = "Clearing memory, please stand by.";
            toolStripStatusLabelRackD.Text = "Clearing memory, please stand by.";
            Update();

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":


                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        ValidSondeArryiMet1[i].ComPort.Dispose();
                    }

                    Thread.Sleep(1000);

                    ValidSondeArryiMet1 = null;

                    break;

                case "iMet Un":

                    /*
                    for (int bw = 0; bw < backgroundRadiosondeDisplay.Length; bw++)
                    {
                        backgroundRadiosondeDisplay[bw].CancelAsync();
                    }
                     */ 


                    for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                    {
                        //Closing radiosonde port
                        ValidSondeiMet1U[i].closePort();

                        
                    }

                    //Disableing the txsettings controls.
                    if (auxSettings != null && auxSettings.IsDisposed == false)
                    {
                        auxSettings.controls_Off();
                        if (auxSettings.backgroundWorkerSetSettings.IsBusy)
                        {
                            auxSettings.backgroundWorkerSetSettings.CancelAsync();
                        }
                    }

                    //Clearing all objects from them array and making the system ready to scan again.
                    ValidSondeiMet1U = null;

                    if (programRelay != null)
                    {
                        programRelay.outsideUpdate();
                    }

                    break;

                case "iMet 2.":
                    /*
                    for (int bw = 0; bw < backgroundRadiosondeDisplay.Length; bw++)
                    {
                        backgroundRadiosondeDisplay[bw].CancelAsync();
                    }
                     */


                    for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                    {
                        //Closing radiosonde port
                        ValidSondeiMet1U[i].closePort();


                    }

                    //Disableing the txsettings controls.
                    if (auxSettings != null && auxSettings.IsDisposed == false)
                    {
                        auxSettings.controls_Off();
                        if (auxSettings.backgroundWorkerSetSettings.IsBusy)
                        {
                            auxSettings.backgroundWorkerSetSettings.CancelAsync();
                        }
                    }

                    //Clearing all objects from them array and making the system ready to scan again.
                    ValidSondeiMet1U = null;

                    if (programRelay != null)
                    {
                        programRelay.outsideUpdate();
                    }

                    break;
            }

            //Resetting global items.
            numberOfSondes = 0; //resetting sonde counter to it starting condition.

            //Resetting/Disableing the programming options.
            updateProgrammingWindow();
            

            //Resetting the data grids.
            dataGridViewRackA.Rows.Clear();
            dataGridViewRackB.Rows.Clear();
            dataGridViewRackC.Rows.Clear();
            dataGridViewRackD.Rows.Clear();

            //Resetting the buttons.
            buttonControlSondeSearch.Enabled = true;
            buttonControlSondeStop.Enabled = false;
            comboBoxSondeType.Enabled = true;

            UpdateMainStatusLabel("Ready...");
            toolStripStatusLabelRackA.Text = "Ready...";
            toolStripStatusLabelRackB.Text = "Ready...";
            toolStripStatusLabelRackC.Text = "Ready...";
            toolStripStatusLabelRackD.Text = "Ready...";
            System.GC.Collect(); //Clearing anything left in memory that might be hanging around.

            //disabling processing Radio.
            RadiosondeStatus.processRadiosondeHex = false;

            //Disabling Reconnect button
            buttonControlSondeReconnect.Enabled = false;

            //Disableing the coefficent loading
            groupBoxCoefficent.Enabled = false;
        }

        private void buttonControlSondeReconnect_Click(object sender, EventArgs e)
        {
            RadiosondeStatus.processRadiosondeHex = false;
            UpdateMainStatusLabel("Reconnecting to all current Radiosondes.");
            Update();
            Thread.Sleep(1000);

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":

                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        ValidSondeArryiMet1[i].Initialize();
                        UpdateMainStatusLabel("Radiosonde on " + ValidSondeArryiMet1[i].ComPort.PortName + " restarted.");
                        Update();
                    }
                    break;

                case "iMet Un":
                    //Not sure anything is needed hear,,, the new class should startup on its own.
                    break;
            }

            UpdateMainStatusLabel("All Radiosondes restarted.");

            RadiosondeStatus.processRadiosondeHex = true;
        }

        private void buttonControlSondeSearch_Click(object sender, EventArgs e)
        {
            //Moving the data gride pointer.
            dataGridViewRackA.CurrentCell = dataGridViewRackA.Rows[0].Cells[4];

            //Checking to make sure a sonde type has been selected.
            if (comboBoxSondeType.Text == "Sonde Type")
            {
                DialogResult results = MessageBox.Show("Please select a sonde type.", "Select a sonde", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }

            buttonControlSondeSearch.Enabled = false; //Disableing the search button.
            comboBoxSondeType.Enabled = false; //Disable the ability to change types of sonde until all stopped.

            //Checking to see if Rack A is disabled
            if (ProgramSettings.StartRackARadiosondeComms == 0)
            {
                UpdateMainStatusLabel("Rack A Currently Disabled.");
                toolStripStatusLabelRackA.Text = "Rack A Currently Disabled.";
            }
            if (ProgramSettings.StartRackBRadiosondeComms == 0)
            {
                UpdateMainStatusLabel("Rack B Currently Disabled.");
                toolStripStatusLabelRackB.Text = "Rack B Currently Disabled.";
            }

            if (ProgramSettings.StartRackCRadiosondeComms == 0)
            {
                UpdateMainStatusLabel("Rack C Currently Disabled.");
                toolStripStatusLabelRackC.Text = "Rack C Currently Disabled.";
            }

            if (ProgramSettings.StartRackDRadiosondeComms == 0)
            {
                UpdateMainStatusLabel("Rack D Currently Disabled.");
                toolStripStatusLabelRackD.Text = "Rack D Currently Disabled.";
            }

            
            //Enabling the collection of radiosonde data.
            RadiosondeStatus.processRadiosondeHex = true;

            //Starting Background worker.
            backgroundWorkerRadiosondeDataCollect.RunWorkerAsync();

        }

        private void statusStripMainDisplay_DoubleClick(object sender, EventArgs e)
        {
            string commandReport = "";
            if (commandsRun.Count <= 20)
            {
                for (int i = 0; i < commandsRun.Count; i++)
                {
                    commandReport = commandReport + i.ToString() + " - " + commandsRun[i] + "\n";
                }
            }

            if (commandsRun.Count > 20)
            {
                for (int i = commandsRun.Count-20; i < commandsRun.Count; i++)
                {
                    commandReport = commandReport + i.ToString() + " - " + commandsRun[i] + "\n";
                }
            }

            DialogResult pastCommandReport = MessageBox.Show(commandReport + "Would you like to generate a report?","Past command report.",MessageBoxButtons.YesNo,MessageBoxIcon.Information,MessageBoxDefaultButton.Button2);
            
            if (pastCommandReport == DialogResult.Yes)
            {
                // create a writer and open the file
                string currentDate = DateTime.Now.ToString("yyyyMMdd");
                string currentTime = DateTime.Now.ToString("HHmmss");
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" +currentDate + " " + currentTime + " Multisonde Command Report.csv";


                TextWriter commandLog = File.AppendText(path);

                // write a line of text to the file
                for (int i = 0; i < commandsRun.Count; i++)
                {
                    commandLog.WriteLine(i.ToString() + "," + commandRunTime[i] + "," + commandsRun[i]);
                }
                // close the stream
                commandLog.Close();
                //Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            }

        }

        private void label12_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            DialogResult enableSondeTypeComboBox = MessageBox.Show("Enabling the sonde type combo box could cause problem.\nEnable your your own risk.", "Enable Sonde Type Select.", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (enableSondeTypeComboBox == DialogResult.Yes)
            {
                comboBoxSondeType.Enabled = true;
            }
        }

        #endregion

        #region Program interfaces. This includes labels and combo boxes.

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateMainStatusLabel("Settings Window Opened.");
            frmSettings OverallSettings = new frmSettings();
            OverallSettings.Show(this);
        }

        private void labelSensorTemperature_DoubleClick(object sender, EventArgs e)
        {
            string value = "";
            if (InputBox("Input Temperature", "Input Temperature:", ref value) == DialogResult.OK)
            {
                if (value == "") { value = "0.0"; }
                labelSensorTemperature.Text = Convert.ToDouble(value).ToString("0.00");
            }
        }

        private void labelSensorPressure_DoubleClick(object sender, EventArgs e)
        {
            string value = "";
            if (InputBox("Input Pressure", "Input Pressure:", ref value) == DialogResult.OK)
            {
                if (value == "") { value = "0.0"; }
                labelSensorPressure.Text = Convert.ToDouble(value).ToString("0.00");
            }
        }

        private void labelSensorHumidity_DoubleClick(object sender, EventArgs e)
        {
            string value = "";
            if (InputBox("Input Humidity", "Input Humidity:", ref value) == DialogResult.OK)
            {
                if (value == "") { value = "0.0"; }
                labelSensorHumidity.Text = value.ToString();
            }
        }

        private void comboBoxSensorTempHumidity_Click(object sender, EventArgs e)
        {
            //Clearing all contents
            comboBoxSensorTempHumidity.Items.Clear();

            //Adding known TU sensors
            comboBoxSensorTempHumidity.Items.Add("Manual");
            comboBoxSensorTempHumidity.Items.Add("GE Hygro");
            comboBoxSensorTempHumidity.Items.Add("HMP234");
            comboBoxSensorTempHumidity.Items.Add("EE31");

            //Building the sonde list of valid iMet1 radiosondes.
            if (ValidSondeArryiMet1 != null)
            {
                for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                {
                    comboBoxSensorTempHumidity.Items.Add(ValidSondeArryiMet1[i].getTrackingID());
                }
            }

            //Building the sonde list of valid bell202 radiosonde.
            if (ValidSondeiMet1U != null)
            {
                for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                {
                    comboBoxSensorTempHumidity.Items.Add(ValidSondeiMet1U[i].CalData.SerialNumber);
                }
            }

        }

        private void comboBoxSensorPressure_Click(object sender, EventArgs e)
        {
            //Clearing all contents
            comboBoxSensorPressure.Items.Clear();

            //Adding known pressure sensors
            comboBoxSensorPressure.Items.Add("Manual");
            comboBoxSensorPressure.Items.Add("Paroscientific");
        }

        private void comboBoxSensorTempHumidity_SelectedValueChanged(object sender, EventArgs e)
        {
            //Resetting the reasing to the inital state to make sure that during a switch it doesn't cross over the data.
            labelSensorTemperature.Text = "000.000";
            labelSensorHumidity.Text = "000.000";

            //Manual
            if (comboBoxSensorTempHumidity.Text.Substring(0, 2) == "Ma")
            {
                ProgramSettings.CurrentTUSensor = "Ma";
                backgroundWorkerUpdateTempHumidity.CancelAsync();
                
                //closing com ports of sensors to allow them to be used by other programs.
                if (referanceGE !=null && referanceGE.ComPort.IsOpen == true)
                {
                    referanceGE.ComPort.Dispose();
                }

                if (referanceHMP234!=null && referanceHMP234.ComPort.IsOpen == true)
                {
                    referanceHMP234.ComPort.Dispose();
                }

                if (referanceEE31 != null && referanceEE31.ComPort.IsOpen == true)
                {
                    referanceEE31.ComPort.Dispose();
                }
                                

            }

            //GE Sellected.
            if (comboBoxSensorTempHumidity.Text.Substring(0, 2) == "GE")
            {
                //Closing the HMP234 if it has been used to allow it to be use else where.
                if (referanceHMP234 != null && referanceHMP234.ComPort.IsOpen == true)
                {
                    referanceHMP234.ComPort.Dispose();
                }

                //Closing EE31 if it has been used to allow it to be used else where.
                if (referanceEE31 != null && referanceEE31.ComPort.IsOpen == true)
                {
                    referanceEE31.ComPort.Dispose();
                }

                ProgramSettings.CurrentTUSensor = "GE";
                if (referanceGE == null)
                {
                    referanceGE = new GEHygro(ProgramSettings.SensorComPortGE);
                }

                if (referanceGE.ComPort.IsOpen != true)
                {
                    try
                    {
                        referanceGE.ComPort.Open();
                    } 
                    catch (Exception) 
                    {
                        MessageBox.Show("Com Port " + ProgramSettings.SensorComPortGE + " not available. Check settings and try again.", "Com Port Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }

            //HMP234 sellected.
            if (comboBoxSensorTempHumidity.Text.Substring(0, 3) == "HMP")
            {
                //Closing the GE if it has been used to allow it to be use else where.
                if (referanceGE != null && referanceGE.ComPort.IsOpen == true)
                {
                    referanceGE.ComPort.Dispose();
                }

                //Closing EE31 if it has been used to allow it to be used else where.
                if (referanceEE31 != null && referanceEE31.ComPort.IsOpen == true)
                {
                    referanceEE31.ComPort.Dispose();
                }

                ProgramSettings.CurrentTUSensor = "HMP";
                if (referanceHMP234 == null)
                {
                    referanceHMP234 = new HMP234(ProgramSettings.SensorComPortHMP234);
                }
                
                if (referanceHMP234.ComPort.IsOpen != true)
                {
                    try
                    {
                        referanceHMP234.ComPort.Open();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Com Port " + ProgramSettings.SensorComPortHMP234 + " not available. Check settings and try again.", "Com Port Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    referanceHMP234.startHMP234Sensor();
                }
            }

            //If you have selected the EE31 referance sensor.
            if (comboBoxSensorTempHumidity.Text.Substring(0, 2) == "EE")
            {
                //Closing the GE if it has been used to allow it to be use else where.
                if (referanceGE != null && referanceGE.ComPort.IsOpen == true)
                {
                    referanceGE.ComPort.Dispose();
                }

                //Closing the HMP234 if it has been used to allow it to be use else where.
                if (referanceHMP234 != null && referanceHMP234.ComPort.IsOpen == true)
                {
                    referanceHMP234.ComPort.Dispose();
                }

                ProgramSettings.CurrentTUSensor = "EE";
                if (referanceEE31 == null)
                {
                    referanceEE31 = new epluseEE31(ProgramSettings.SensorComPortEE31);
                }

                if (referanceEE31.ComPort.IsOpen != true)
                {
                    try
                    {
                        referanceEE31.ComPort.Open();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Com Port " + ProgramSettings.SensorComPortEE31 + " not available. Check settings and try again.", "Com Port Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                startBackgroundWorkerTU();
                return;
            }

            if (ValidSondeArryiMet1 != null)
            {
                //if we want to use a iMet1 sonde as a referance.
                if (comboBoxSensorTempHumidity.Text.Substring(0, 1) == "S")
                {
                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        if (ValidSondeArryiMet1[i].getTrackingID() == comboBoxSensorTempHumidity.Text)
                        {
                            if (i < 10)
                            { ProgramSettings.CurrentTUSensor = "S0" + i.ToString(); }
                            else
                            { ProgramSettings.CurrentTUSensor = "S" + i.ToString(); }
                        }
                    }
                }

                if (comboBoxSensorTempHumidity.Text.Substring(0, 1) == "E")
                {
                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        if (ValidSondeArryiMet1[i].getTrackingID() == comboBoxSensorTempHumidity.Text)
                        {
                            if (i < 10)
                            { ProgramSettings.CurrentTUSensor = "E0" + i.ToString(); }
                            else
                            { ProgramSettings.CurrentTUSensor = "E" + i.ToString(); }
                        }
                    }
                }
            }

            if (ValidSondeiMet1U != null)
            {

            }

            startBackgroundWorkerTU();
        }

        private void startBackgroundWorkerTU()
        {
            //Starting Temp Humidity Background worker.
            if (backgroundWorkerUpdateTempHumidity.IsBusy != true)
            {
                backgroundWorkerUpdateTempHumidity.RunWorkerAsync();
            } 
        }

        private void comboBoxSensorPressure_SelectedValueChanged(object sender, EventArgs e)
        {
            //Resetting the reasing to the inital state to make sure that during a switch it doesn't cross over the data.
            labelSensorPressure.Text = "000.000";

            //Manual
            if (comboBoxSensorPressure.Text.Substring(0, 2) == "Ma")
            {
                ProgramSettings.CurrentPressureSensor = "Ma";
                if (backgroundWorkerUpdatePressure.IsBusy) { backgroundWorkerUpdatePressure.CancelAsync(); } //Stopping pressure collection if running.
                 
                //closing com ports of sensors to allow them to be used by other programs.
                if (Program.sensorPressure != null)
                {
                    Program.sensorPressure.disconnect();
                    Program.sensorPressure = null;
                }

                return;

            }
            
            //Paro Sellected
            if (comboBoxSensorPressure.Text.Substring(0, 1) == "P")
            {
                ProgramSettings.CurrentPressureSensor = "PARO";
                //Setting up the pressure sensor.
                string[] paroSettings = ProgramSettings.SensorParoComPort.Split(',');
                Program.sensorPressure = new Paroscientific(paroSettings[0], Convert.ToInt16(paroSettings[1]));

                //Init the pressure cell
                Program.sensorPressure.Initiate();

            }


            //Starting Pressure Background worker.
            if (backgroundWorkerUpdatePressure.IsBusy != true)
            {
                backgroundWorkerUpdatePressure.RunWorkerAsync();
            }
        }


        #endregion

        #region Background Workers for handeling Pressure and Humidity

        private void backgroundWorkerUpdateTempHumidity_DoWork(object sender, DoWorkEventArgs e)
        {
            string currentTemp = "";
            string currentHumidity = "";

            while (backgroundWorkerUpdateTempHumidity.CancellationPending == false)
            {
                try
                {

                    if (backgroundWorkerUpdateTempHumidity.CancellationPending) /// Something to stop the process.
                    {
                        e.Cancel = true;
                        return;
                    }

                    //Manual process.
                    if (ProgramSettings.CurrentTUSensor == "Ma")
                    {
                        e.Cancel = true;
                        return;
                    }

                    //GE Hygro
                    if (ProgramSettings.CurrentTUSensor == "GE")
                    {
                        referanceGE.getGEHygroData();
                        currentTemp = referanceGE.CurrentTemp.ToString();
                        currentHumidity = referanceGE.CurrentHumidity.ToString();
                    }

                    //HMP234
                    if (ProgramSettings.CurrentTUSensor == "HMP")
                    {
                        sensorCorrection.sensorCorHMP234Blk correctHMP = new sensorCorrection.sensorCorHMP234Blk();

                        referanceHMP234.getHMP234Data();
                        currentTemp = referanceHMP234.CurrentTemp.ToString();

                        if (checkBoxCorrection.Checked)
                        {
                            currentHumidity = correctHMP.getCorrectedHumidity(referanceHMP234.CurrentHumidity).ToString("0.00");
                        }
                        else
                        {
                            currentHumidity = referanceHMP234.CurrentHumidity.ToString();
                        }
                    }

                    //EE31
                    if (ProgramSettings.CurrentTUSensor == "EE")
                    {
                        sensorCorrection.sensorCorEE31 correctEE31 = new sensorCorrection.sensorCorEE31();

                        string reportTemp = "";
                        string reportHumidty = "";
                        currentTemp = referanceEE31.getAirTemp(ref reportTemp).ToString("0.000");

                        if (checkBoxCorrection.Checked)
                        {
                            currentHumidity = correctEE31.getCorrectedHumidity(referanceEE31.getHumidity(ref reportHumidty)).ToString("0.000");
                        }
                        else
                        {
                            currentHumidity = referanceEE31.getHumidity(ref reportHumidty).ToString("0.000");
                        }

                        //Reporting errors.
                        if (reportTemp != ""){UpdateMainStatusLabel(reportTemp);}
                        if (reportHumidty != "") { UpdateMainStatusLabel(reportHumidty); }

                        Thread.Sleep(500);

                    }

                    //Sonde reports
                    if (ProgramSettings.CurrentTUSensor.Substring(0,1) == "S")
                    {
                        //protecting the program form crashing if all the sondes are stopped.
                        if (ValidSondeArryiMet1 == null)
                        {
                            ProgramSettings.CurrentTUSensor = "Ma";
                            return;
                        }
                        Thread.Sleep(1000); //Slow down the request for updated sonde temp and humidity.
                        if (ProgramSettings.CurrentTUSensor != "Ma")
                        {
                            currentTemp = ValidSondeArryiMet1[Convert.ToInt16(ProgramSettings.CurrentTUSensor.Substring(1, 2))].getAirTemp();
                            currentHumidity = ValidSondeArryiMet1[Convert.ToInt16(ProgramSettings.CurrentTUSensor.Substring(1, 2))].getRH();
                        }
                    }
                }
                catch
                {

                }
                try
                {
                    this.Invoke(new UpdateTempHumidity(this.UpdateTempHumidityDisplay), new object[] { currentTemp, currentHumidity });
                }
                catch
                {
                }
            }
        }

        public void UpdateTempHumidityDisplay(string currentTemp, string currentHumidity)
        {
            labelSensorTemperature.Text = currentTemp;
            labelSensorHumidity.Text = currentHumidity;
        }

        private void backgroundWorkerUpdatePressure_DoWork(object sender, DoWorkEventArgs e)
        {
            string currentPressure = "";

            //Paro
            if (ProgramSettings.CurrentPressureSensor == "PARO")
            {
                if (!Program.sensorPressure.Initiate())
                {
                    MessageBox.Show("Error connecting to Paro.", "Pressure Ref Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Program.sensorPressure = null;
                    return;
                }
            }


            while (backgroundWorkerUpdatePressure.CancellationPending == false)
            {

                if (backgroundWorkerUpdatePressure.CancellationPending) /// Something to stop the process.
                {
                    e.Cancel = true;
                    return;
                }

                //Manual process.
                if (ProgramSettings.CurrentPressureSensor == "Ma")
                {
                    e.Cancel = true;
                    return;
                }

                //Paro
                if (ProgramSettings.CurrentPressureSensor == "PARO")
                {
                    Program.sensorPressure.Read();
                    Program.sensorPressure.waitForSocketData.WaitOne(4000);

                    if (Program.sensorPressure != null)
                    {
                        currentPressure = Program.sensorPressure.getCurrentPressure().ToString("0.000");
                    }
                }

                try
                {
                    this.Invoke(new UpdatePressure(this.UpdatePressureDisplay), new object[] { currentPressure });
                }
                catch
                {
                }
            }
        }

        public void UpdatePressureDisplay(string currentPressure)
        {
            labelSensorPressure.Text = currentPressure;
        }
        #endregion
        
        #region Logging data controls section. This area includes creating files setting options and running the logging.
        #region Save and Open
        private void newLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string DateCode = DateTime.Now.ToString("yyyyMMdd-HHmm");

            // Displays a SaveFileDialog so the user can save.
            SaveFileDialog saveFileDialogWOLog = new SaveFileDialog();
            saveFileDialogWOLog.Filter = "Logfile|*.csv";
            saveFileDialogWOLog.Title = "Save Log File As:";
            saveFileDialogWOLog.FileName = DateCode + "-"+".csv";
            DialogResult saveResults = saveFileDialogWOLog.ShowDialog();

            if (saveResults == DialogResult.Cancel)
            {
                saveFileDialogWOLog.Dispose();
                return;
            }

            // If the file name is not an empty string open it for saving.
            if (saveFileDialogWOLog.FileName != "")
            {
                //Defining the new log file.
                LoggingSettings.loggingCurrentLogFile = saveFileDialogWOLog.FileName;

                if (File.Exists(LoggingSettings.loggingCurrentLogFile) == false)
                {
                    // create a writer and open the file
                    TextWriter currentLogging = File.AppendText(LoggingSettings.loggingCurrentLogFile);


                    // write a line of text to the file
                    currentLogging.WriteLine("TimeStamp(sec),SerialNum,TrackingNum,TUProbeNum,FirmwareVer,Pressure(hPa),PressCnt,PressRefCnt,PressTemp(degC),PressTempCnt,PressTempRefCnt,AirTemp(degC),AirTempCnt,AirTempRefCnt,InternTemp(degC),InternTempCnt,InternTempRefCnt,Humidity(RH),HumCnt,TransHumidity(RH),HumTemp(degC),HumTempCnt,HumTempRefCnt,BattVolt(V),BattVoltCnt,BattVoltRefCnt,AmbPressure,AmbTemperature,AmbHumidity,PressureDiff,TemperatureDiff,HumidityDiff,");

                    // close the stream
                    currentLogging.Close();

                }

                LoggingSettings.loggingFileEntered = true;
                enableLoggingCheck();
            }
        }

        private void openLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string DateCode = DateTime.Now.ToString("yyyyMMdd-HHmm");

            // Displays a SaveFileDialog so the user can save.
            OpenFileDialog saveFileDialogWOLog = new OpenFileDialog();
            saveFileDialogWOLog.Filter = "Logfile|*.csv";
            saveFileDialogWOLog.Title = "Save Log File As:";
            //saveFileDialogWOLog.FileName = DateCode + "-" + ProgramSettings.currentUser + "-Enter You Note Here" + ".csv";
            DialogResult saveResults = saveFileDialogWOLog.ShowDialog();

            if (saveResults == DialogResult.Cancel)
            {
                saveFileDialogWOLog.Dispose();
                return;
            }

            // If the file name is not an empty string open it for saving.
            if (saveFileDialogWOLog.FileName != "")
            {
                //Defining the new log file.
                LoggingSettings.loggingCurrentLogFile = saveFileDialogWOLog.FileName;

                if (File.Exists(LoggingSettings.loggingCurrentLogFile) == false)
                {
                    // create a writer and open the file
                    TextWriter currentLogging = File.AppendText(LoggingSettings.loggingCurrentLogFile);


                    // write a line of text to the file
                    currentLogging.WriteLine("TimeStamp(sec),SerialNum,TrackingNum,TUProbeNum,FirmwareVer,Pressure(hPa),PressCnt,PressRefCnt,PressTemp(degC),PressTempCnt,PressTempRefCnt,AirTemp(degC),AirTempCnt,AirTempRefCnt,InternTemp(degC),InternTempCnt,InternTempRefCnt,Humidity(RH),HumCnt,TransHumidity(RH),HumTemp(degC),HumTempCnt,HumTempRefCnt,BattVolt(V),BattVoltCnt,BattVoltRefCnt,AmbPressure,AmbTemperature,AmbHumidity,PressureDiff,TemperatureDiff,HumidityDiff,");

                    // close the stream
                    currentLogging.Close();

                }
                LoggingSettings.loggingFileEntered = true;
                enableLoggingCheck();

                //Setting total counter to the total line contained in the current selected file.
                string[] countingLines = File.ReadAllLines(LoggingSettings.loggingCurrentLogFile);
                LoggingSettings.loggingCounterTotal = Convert.ToInt16(countingLines.Length)-1;
            }
        }
        #endregion

        #region Contorl for interval selection.
        private void resetCheckTimeInterval()
        {
            toolStripMenuItem2.Checked = false;
            toolStripMenuItem3.Checked = false;
            toolStripMenuItem4.Checked = false;
            minToolStripMenuItem.Checked = false;
            minToolStripMenuItem1.Checked = false;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            resetCheckTimeInterval();
            toolStripMenuItem2.Checked = true;
            LoggingSettings.loggingIntervalTime = 1;
            LoggingSettings.loggingIntervalEntered = true;
            enableLoggingCheck();
            
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            resetCheckTimeInterval();
            toolStripMenuItem3.Checked = true;
            LoggingSettings.loggingIntervalTime = 10;
            LoggingSettings.loggingIntervalEntered = true;
            enableLoggingCheck();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            resetCheckTimeInterval();
            toolStripMenuItem4.Checked = true;
            LoggingSettings.loggingIntervalTime = 60;
            LoggingSettings.loggingIntervalEntered = true;
            enableLoggingCheck();
        }

        private void minToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetCheckTimeInterval();
            minToolStripMenuItem.Checked = true;
            LoggingSettings.loggingIntervalTime = 60 * 5;
            LoggingSettings.loggingIntervalEntered = true;
            enableLoggingCheck();
        }

        private void minToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            resetCheckTimeInterval();
            minToolStripMenuItem1.Checked = true;
            LoggingSettings.loggingIntervalTime = 60 * 15;
            LoggingSettings.loggingIntervalEntered = true;
            enableLoggingCheck();
        }
        #endregion

        private void enableLoggingCheck()
        {
            if (LoggingSettings.loggingFileEntered == true && LoggingSettings.loggingIntervalEntered == true)
            {
                //Checking to see if logging is already going. if so jump over this.
                if (LoggingSettings.loggingEnabled == false)
                {
                    startLoggingToolStripMenuItem.Enabled = true;
                }
            }
        }

        private void startLoggingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Check to make sure that active data is there.
            if (RadiosondeStatus.activeRadiosondeData != true)
            {
                MessageBox.Show("No Current Radiosondes. Search first and try again.", "Logging start error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            //Enable pause and stop logging.
            pauseLoggingToolStripMenuItem.Enabled = true;
            stopLoggingToolStripMenuItem.Enabled = true;

            //Disableing the start of logging.
            startLoggingToolStripMenuItem.Enabled = false;

            //Enable logging
            LoggingSettings.loggingEnabled = true;
            UpdateMainStatusLabel("Logging Started. File: " + LoggingSettings.loggingCurrentLogFile + " | " + LoggingSettings.loggingIntervalTime + "sec/Packet");
            toolStripStatusLabelLogging.Text = "Logging Started.";
            backgroundWorkerLogRadiosondeData = new BackgroundWorker();
            backgroundWorkerLogRadiosondeData.WorkerSupportsCancellation = true;
            backgroundWorkerLogRadiosondeData.DoWork += new DoWorkEventHandler(backgroundWorkerLogRadiosondeData_DoWork);
            backgroundWorkerLogRadiosondeData.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerLogRadiosondeData_RunWorkerCompleted);
            backgroundWorkerLogRadiosondeData.RunWorkerAsync();
            
        }

        private void pauseLoggingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pauseLoggingToolStripMenuItem.Text == "Pause Logging")
            {
                //Stop logging, but retain file settings and intervals.
                LoggingSettings.loggingEnabled = false;

                //Resetting text to all the logging to be resumed.
                pauseLoggingToolStripMenuItem.Text = "Resume Logging";
                toolStripStatusLabelLogging.Text = "Logging Paused.";
                UpdateMainStatusLabel("Logging Paused.");
                return;
            }

            if (pauseLoggingToolStripMenuItem.Text == "Resume Logging")
            {
                //Restart logging.
                LoggingSettings.loggingEnabled = true;

                //Resetting test to all the logging to be paused.
                pauseLoggingToolStripMenuItem.Text = "Pause Logging";
                toolStripStatusLabelLogging.Text = "Logging Resumed.";
                UpdateMainStatusLabel("Logging Resumed.");
                return;
            }

        }

        private void stopLoggingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult stopLoggingRequest = MessageBox.Show("Are you sure you want to stop logging?", "Stop Logging", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (stopLoggingRequest == DialogResult.Yes)
            {
                //Enable pause and stop logging.
                pauseLoggingToolStripMenuItem.Enabled = false;
                stopLoggingToolStripMenuItem.Enabled = false;

                //Disableing the start of logging.
                startLoggingToolStripMenuItem.Enabled = false;

                //Disabling logging
                LoggingSettings.loggingEnabled = false;
                backgroundWorkerLogRadiosondeData.CancelAsync();

                //Clearing current file and logging intervals
                LoggingSettings.loggingCurrentLogFile = "";
                LoggingSettings.loggingFileEntered = false;
                LoggingSettings.loggingIntervalEntered = false;
                LoggingSettings.loggingCounterTotal = 0;
                resetCheckTimeInterval();
                RadiosondeStatus.activeRadiosondeData = false;
                toolStripStatusLabelLogging.Text = "Logging Stopped.";
                UpdateMainStatusLabel("Logging Stopped.");

            }
            if (stopLoggingRequest == DialogResult.No)
            {
                return;
            }
        }

        private void backgroundWorkerLogRadiosondeData_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            //If logging is enabled log data.
            while (worker.CancellationPending == false)
            {
                if (LoggingSettings.loggingEnabled == true)
                {
                    LoggingSettings.counterLoggingInterval++;

                    if (LoggingSettings.counterLoggingInterval >= LoggingSettings.loggingIntervalTime)
                    {
                        logValidRadiosondeData();
                        LoggingSettings.counterLoggingInterval = 0;
                    }
                }

            Thread.Sleep(1000);
            }
        }

        private void backgroundWorkerLogRadiosondeData_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            backgroundWorkerLogRadiosondeData = null;
        }

        #endregion

        #region Area provided for loging data to a file.

        private void logValidRadiosondeData()
        {
            string fullDataAndTime = DateTime.Now.ToString("ddd MMM dd HH:mm:ss yyyy");
            string humidityCount = "";

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0,7))
            {
                case "iMet-TX":
                    #region Logging for older iMet-1 NWS sondes.
                    // create a writer and open the file
                    TextWriter currentLogging = File.AppendText(LoggingSettings.loggingCurrentLogFile);

                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {

                        //Selecting the proper humidity process for the humidity count. This is bases on the type of radiosonde selected.
                        if (RadiosondeStatus.currentRadiosondeType == "iMet-TX-HUM")
                        {
                            humidityCount = ValidSondeArryiMet1[i].getRHFreq();
                        }
                        if (RadiosondeStatus.currentRadiosondeType == "iMet-TX-E+E")
                        {
                            humidityCount = ValidSondeArryiMet1[i].getRHFreqEPlusE();
                        }

                        // write a line of text to the file
                        currentLogging.WriteLine(fullDataAndTime + "," + ValidSondeArryiMet1[i].getSerialNum() + "," + ValidSondeArryiMet1[i].getTrackingID() + "," +
                            ValidSondeArryiMet1[i].getTUProbeID() + "," + ValidSondeArryiMet1[i].getFirmwareVersion() + "," +
                            ValidSondeArryiMet1[i].getPressure() + "," + ValidSondeArryiMet1[i].getPressureCount() + "," +
                            ValidSondeArryiMet1[i].getPressureRefCount() + "," + ValidSondeArryiMet1[i].getPressureTemp() + "," +
                            ValidSondeArryiMet1[i].getPressureTempCount() + "," + ValidSondeArryiMet1[i].getPressureTempRefCount() + "," +
                            ValidSondeArryiMet1[i].getAirTemp() + "," + ValidSondeArryiMet1[i].getAirTempCount() + "," +
                            ValidSondeArryiMet1[i].getAirTempRefCount() + "," + ValidSondeArryiMet1[i].getInternalTemp() + "," +
                            ValidSondeArryiMet1[i].getInternalTempCount() + "," + ValidSondeArryiMet1[i].getInternalTempRefCount() + "," +
                            ValidSondeArryiMet1[i].getRH() + "," + humidityCount + "," +
                            ValidSondeArryiMet1[i].getRHTranslated() + "," + ValidSondeArryiMet1[i].getHumidityTemp() + "," +
                            ValidSondeArryiMet1[i].getHumidityTempCount() + "," + ValidSondeArryiMet1[i].getHumidityTempRefCount() + "," +
                            ValidSondeArryiMet1[i].getBatteryVoltage() + "," + ValidSondeArryiMet1[i].getBatteryVoltageCount() + "," +
                            ValidSondeArryiMet1[i].getBatteryVoltageRefCount() + "," + labelSensorPressure.Text + "," +
                            labelSensorTemperature.Text + "," + labelSensorHumidity.Text + "," +
                            (Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - Convert.ToDouble(labelSensorPressure.Text)).ToString() + "," +
                            (Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - Convert.ToDouble(labelSensorTemperature.Text)).ToString() + "," +
                            (Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - Convert.ToDouble(labelSensorHumidity.Text)).ToString() + ","
                            );

                    }
                    // close the stream
                    currentLogging.Close();
                    LoggingSettings.loggingCounterTotal++;
                    toolStripStatusLabelLogging.Text = LoggingSettings.loggingCounterTotal.ToString() + " Total Packets Logged.";
                    break;

                    #endregion

                case "iMet Un":
                    #region Logging for universal radiosondes.
                    double currentAirTemp, currentHumidity, currentPressure;
                    currentAirTemp = Convert.ToDouble(labelSensorTemperature.Text);
                    currentHumidity = Convert.ToDouble(labelSensorHumidity.Text);
                    currentPressure = Convert.ToDouble(labelSensorPressure.Text);

                    if (File.Exists(LoggingSettings.loggingCurrentLogFile) == false)
                    {
                        // create a writer and open the file
                        TextWriter currentLoggingCheck = File.AppendText(LoggingSettings.loggingCurrentLogFile);


                        // write a line of text to the file
                        currentLoggingCheck.WriteLine("TimeStamp(sec),SerialNum,TrackingNum,TUProbeNum,FirmwareVer,Pressure(hPa),PressCnt,PressRefCnt,PressTemp(degC),PressTempCnt,PressTempRefCnt,AirTemp(degC),AirTempCnt,AirTempRefCnt,InternTemp(degC),InternTempCnt,InternTempRefCnt,Humidity(RH),HumCnt,TransHumidity(RH),HumTemp(degC),HumTempCnt,HumTempRefCnt,BattVolt(V),BattVoltCnt,BattVoltRefCnt,AmbPressure,AmbTemperature,AmbHumidity,PressureDiff,TemperatureDiff,HumidityDiff,");

                        // close the stream
                        currentLoggingCheck.Close();

                    }

                    // create a writer and open the file
                    TextWriter currentBell202Logging = File.AppendText(LoggingSettings.loggingCurrentLogFile);

                    for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                    {
                        //Note i am not writing pressure data..... might want to think about that..... This code will need to be edited later if the two need to be run at the same time.
                        // write a line of text to the file
                        currentBell202Logging.WriteLine(fullDataAndTime + "," + ValidSondeiMet1U[i].CalData.SondeID + "," + ValidSondeiMet1U[i].CalData.SerialNumber + "," +
                            ValidSondeiMet1U[i].CalData.ProbeID + "," + "Bell202" + "," +
                            ValidSondeiMet1U[i].CalData.Pressure + "," + ValidSondeiMet1U[i].CalData.PressureCounts + "," +
                            ValidSondeiMet1U[i].CalData.PressureRefCounts + "," + ValidSondeiMet1U[i].CalData.PressureTemperature + "," +
                            ValidSondeiMet1U[i].CalData.PressureTempCounts + "," + ValidSondeiMet1U[i].CalData.PressureTempRefCounts + "," +
                            ValidSondeiMet1U[i].CalData.Temperature + "," + ValidSondeiMet1U[i].CalData.TemperatureCounts + "," +
                            ValidSondeiMet1U[i].CalData.TemperatureRefCounts + "," + ValidSondeiMet1U[i].CalData.InternalTemp + "," +
                            "0" + "," + "0" + "," + //Internal air temp count + Internal Air Temp Count Ref
                            ValidSondeiMet1U[i].CalData.Humidity + "," + ValidSondeiMet1U[i].CalData.HumidityFrequency + "," +
                            "0" + "," + "0" + "," + // ValidSondeArryiMet1[i].getRHTranslated() + "," + ValidSondeArryiMet1[i].getHumidityTemp() + "," +
                            "0" + "," + "0" + "," + // ValidSondeArryiMet1[i].getHumidityTempCount() + "," + ValidSondeArryiMet1[i].getHumidityTempRefCount() + "," +
                            "5" + "," + "0" + "," + // ValidSondeArryiMet1[i].getBatteryVoltage() + "," + ValidSondeArryiMet1[i].getBatteryVoltageCount() + "," +
                            "0" + "," + labelSensorPressure.Text + "," + // ValidSondeArryiMet1[i].getBatteryVoltageRefCount() + "," + "0" + "," +
                            currentAirTemp.ToString() + "," + currentHumidity.ToString() + "," +
                            (Convert.ToDouble(ValidSondeiMet1U[i].CalData.Pressure) - currentPressure).ToString() + "," +
                            (Convert.ToDouble(ValidSondeiMet1U[i].CalData.Temperature) - currentAirTemp).ToString() + "," +
                            (Convert.ToDouble(ValidSondeiMet1U[i].CalData.Humidity) - currentHumidity).ToString() + ","
                            );

                    }
                    // close the stream
                    currentBell202Logging.Close();

                    break;
                    #endregion

                case "iMet 2.":
                    #region Logging for older iMet radiosondes. Firmware version 2.3x
                    double currentAirTempOld, currentHumidityOld, currentPressureOld;
                    currentAirTempOld = Convert.ToDouble(labelSensorTemperature.Text);
                    currentHumidityOld = Convert.ToDouble(labelSensorHumidity.Text);
                    currentPressureOld = Convert.ToDouble(labelSensorPressure.Text);

                    if (File.Exists(LoggingSettings.loggingCurrentLogFile) == false)
                    {
                        // create a writer and open the file
                        TextWriter currentLoggingCheck = File.AppendText(LoggingSettings.loggingCurrentLogFile);


                        // write a line of text to the file
                        currentLoggingCheck.WriteLine("TimeStamp(sec),SerialNum,TrackingNum,TUProbeNum,FirmwareVer,Pressure(hPa),PressCnt,PressRefCnt,PressTemp(degC),PressTempCnt,PressTempRefCnt,AirTemp(degC),AirTempCnt,AirTempRefCnt,InternTemp(degC),InternTempCnt,InternTempRefCnt,Humidity(RH),HumCnt,TransHumidity(RH),HumTemp(degC),HumTempCnt,HumTempRefCnt,BattVolt(V),BattVoltCnt,BattVoltRefCnt,AmbPressure,AmbTemperature,AmbHumidity,PressureDiff,TemperatureDiff,HumidityDiff,");

                        // close the stream
                        currentLoggingCheck.Close();

                    }

                    // create a writer and open the file
                    TextWriter currentBell202LoggingOld = File.AppendText(LoggingSettings.loggingCurrentLogFile);

                    for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                    {
                        //Note i am not writing pressure data..... might want to think about that..... This code will need to be edited later if the two need to be run at the same time.
                        // write a line of text to the file
                        currentBell202LoggingOld.WriteLine(fullDataAndTime + "," + ValidSondeiMet1U[i].CalData.SondeID + "," + ValidSondeiMet1U[i].CalData.SerialNumber + "," +
                            ValidSondeiMet1U[i].CalData.ProbeID + "," + "2.3x" + "," +
                            ValidSondeiMet1U[i].PTUData.Pressure + "," + ValidSondeiMet1U[i].CalData.PressureCounts + "," +
                            ValidSondeiMet1U[i].CalData.PressureRefCounts + "," + ValidSondeiMet1U[i].CalData.PressureTemperature + "," +
                            ValidSondeiMet1U[i].CalData.PressureTempCounts + "," + ValidSondeiMet1U[i].CalData.PressureTempRefCounts + "," +
                            ValidSondeiMet1U[i].PTUData.AirTemperature + "," + ValidSondeiMet1U[i].CalData.TemperatureCounts + "," +
                            ValidSondeiMet1U[i].CalData.TemperatureRefCounts + "," + ValidSondeiMet1U[i].CalData.InternalTemp + "," +
                            "0" + "," + "0" + "," + //Internal air temp count + Internal Air Temp Count Ref
                            ValidSondeiMet1U[i].PTUData.RelativeHumidity + "," + ValidSondeiMet1U[i].CalData.HumidityFrequency + "," +
                            "0" + "," + "0" + "," + // ValidSondeArryiMet1[i].getRHTranslated() + "," + ValidSondeArryiMet1[i].getHumidityTemp() + "," +
                            "0" + "," + "0" + "," + // ValidSondeArryiMet1[i].getHumidityTempCount() + "," + ValidSondeArryiMet1[i].getHumidityTempRefCount() + "," +
                            "5" + "," + "0" + "," + // ValidSondeArryiMet1[i].getBatteryVoltage() + "," + ValidSondeArryiMet1[i].getBatteryVoltageCount() + "," +
                            "0" + "," + labelSensorPressure.Text + "," + // ValidSondeArryiMet1[i].getBatteryVoltageRefCount() + "," + "0" + "," +
                            currentAirTempOld.ToString() + "," + currentHumidityOld.ToString() + "," +
                            (Convert.ToDouble(ValidSondeiMet1U[i].PTUData.Pressure) - currentPressureOld).ToString() + "," +
                            (Convert.ToDouble(ValidSondeiMet1U[i].PTUData.AirTemperature) - currentAirTempOld).ToString() + "," +
                            (Convert.ToDouble(ValidSondeiMet1U[i].PTUData.RelativeHumidity) - currentHumidityOld).ToString() + ","
                            );

                    }
                    // close the stream
                    currentBell202LoggingOld.Close();

                    break;
                    #endregion
            }
        }

        #endregion

        #region Background worker that searchs for radiosonde and collects data from valid radiosondes once per sec.

        private void backgroundWorkerRadiosondeDataCollect_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":
                    #region Background worker for iMet-1 legacy radiosondes.

                    if (backgroundWorkerRadiosondeDataCollect.CancellationPending) /// Something to stop the process.
                    {
                        e.Cancel = true;
                        return;
                    }

                    //Searching for valid radiosondes.
                    //Adding up all the com ports.
                    int totalNumberOfComPorts = (ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                        (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                        (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms) +
                        (ProgramSettings.EndRackDRadiosondeComms - ProgramSettings.StartRackDRadiosondeComms);



                    string[] possibleSondeNames = new string[totalNumberOfComPorts];

                    iMet1[] PossibleSondeArry = new iMet1[possibleSondeNames.Length];

                    //Counters for each rack.
                    int counterRackA = 0;
                    int counterRackB = 0;
                    int counterRackC = 0;
                    int counterRackD = 0;

                    for (int i = 0; i < possibleSondeNames.Length; i++)
                    {
                        if (i <= ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms)
                        {
                            possibleSondeNames[i] = "COM" + (counterRackA + ProgramSettings.StartRackARadiosondeComms).ToString();
                            counterRackA++;
                        }

                        if (i > ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms &&
                            i <= ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms)))
                        {
                            possibleSondeNames[i] = "COM" + (counterRackB + ProgramSettings.StartRackBRadiosondeComms).ToString();
                            counterRackB++;
                        }

                        if (i > ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms)) &&
                            i <= ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                                    (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms)))
                        {
                            possibleSondeNames[i] = "COM" + (counterRackC + ProgramSettings.StartRackCRadiosondeComms).ToString();
                            counterRackC++;
                        }

                        if (i > ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                                    (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms)) &&
                            i <= ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                                    (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms) +
                                    (ProgramSettings.EndRackDRadiosondeComms - ProgramSettings.StartRackDRadiosondeComms)))
                        {
                            possibleSondeNames[i] = "COM" + (counterRackD + ProgramSettings.StartRackDRadiosondeComms).ToString();
                            counterRackD++;
                        }
                    }

                    for (int i = 0; i < possibleSondeNames.Length; i++)
                    {
                        PossibleSondeArry[i] = new iMet1(possibleSondeNames[i]);  //setting com ports to radiosondes.   
                        PossibleSondeArry[i].Initialize(); //opens com port and send data on command. Also get first data packet.

                        try
                        {
                            this.Invoke(new UpdateMainStatusLabelData(this.UpdateMainStatusLabel), new object[] { ("Searching " + possibleSondeNames[i]) });
                        }
                        catch
                        {
                            writeToErrorLog("Unable to report searching for radiosonde on " + possibleSondeNames[i]);
                        }


                    }

                    try
                    {
                        this.Invoke(new UpdateMainStatusLabelData(this.UpdateMainStatusLabel), new object[] { ("Clearing buffers") });
                    }
                    catch
                    {
                        writeToErrorLog("Unable to report Clearing buffers to screen.");
                    }

                    foreach (iMet1 sonde in PossibleSondeArry)
                    {
                        if (sonde.ComPort.IsOpen == true)
                        {
                            sonde.ComPort.DiscardInBuffer();
                        }
                    }

                    Thread.Sleep(4000); //Giving the hardware time to catch up to the software.


                    for (int i = 0; i < PossibleSondeArry.Length; i++)
                    {
                        bool isSonde = PossibleSondeArry[i].detectSonde();
                        if (isSonde)
                        {
                            numberOfSondes++;

                            try
                            {
                                this.Invoke(new UpdateMainStatusLabelData(this.UpdateMainStatusLabel), new object[] { ("Radiosonde found on: " + possibleSondeNames[i]) });
                            }
                            catch
                            {
                                writeToErrorLog("Unable to report Radiosonde found on " + possibleSondeNames[i]);
                            }
                        }
                        else
                        {
                            this.Invoke(new UpdateMainStatusLabelData(this.UpdateMainStatusLabel), new object[] { ("Radiosonde not found on: " + possibleSondeNames[i]) });
                            PossibleSondeArry[i] = null; //Setting object to null,,, to stop looking at it.
                        }
                    }
                    ValidSondeArryiMet1 = new iMet1[numberOfSondes]; //Moving all the valid sonde to the global sonde arry.

                    int j = 0;
                    int comNumber = 0;
                    //resetting rack couters.
                    counterRackA = 0;
                    counterRackB = 0;
                    counterRackC = 0;
                    counterRackD = 0;

                    for (int i = 0; i < PossibleSondeArry.Length; i++)
                    {
                        if (PossibleSondeArry[i] != null)
                        {
                            ValidSondeArryiMet1[j] = PossibleSondeArry[i];
                            //creating Rack arrays.
                            if (ValidSondeArryiMet1[j].ComPort.PortName.Length == 4)
                            { comNumber = Convert.ToInt16(ValidSondeArryiMet1[j].ComPort.PortName.Substring(3, 1)); }

                            if (ValidSondeArryiMet1[j].ComPort.PortName.Length == 5)
                            { comNumber = Convert.ToInt16(ValidSondeArryiMet1[j].ComPort.PortName.Substring(3, 2)); }

                            if (ValidSondeArryiMet1[j].ComPort.PortName.Length == 6)
                            { comNumber = Convert.ToInt16(ValidSondeArryiMet1[j].ComPort.PortName.Substring(3, 3)); }

                            if (comNumber >= ProgramSettings.StartRackARadiosondeComms && comNumber < ProgramSettings.EndRackARadiosondeComms)
                            { counterRackA++; }

                            if (comNumber >= ProgramSettings.StartRackBRadiosondeComms && comNumber < ProgramSettings.EndRackBRadiosondeComms)
                            { counterRackB++; }

                            if (comNumber >= ProgramSettings.StartRackCRadiosondeComms && comNumber < ProgramSettings.EndRackCRadiosondeComms)
                            { counterRackC++; }

                            if (comNumber >= ProgramSettings.StartRackDRadiosondeComms && comNumber < ProgramSettings.EndRackDRadiosondeComms)
                            { counterRackD++; }

                            j++;
                        }
                    }
                    //Setup the data grids for each racks.
                    try
                    {
                        this.Invoke(new UpdateRacksDataGridsData(this.UpdateRackDataGrid), new object[] { counterRackA, counterRackB, counterRackC, counterRackD });
                    }
                    catch { writeToErrorLog("Unable to update one of the rack data grids."); }

                    UpdateMainStatusLabel(ValidSondeArryiMet1.Length.ToString() + " Radiosonde(s) found.");

                    try
                    {
                        this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 1, counterRackA.ToString() + " Radiosonde(s) found." });
                        this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 2, counterRackB.ToString() + " Radiosonde(s) found." });
                        this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 3, counterRackC.ToString() + " Radiosonde(s) found." });
                        this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 4, counterRackD.ToString() + " Radiosonde(s) found." });
                    }
                    catch
                    {
                        writeToErrorLog("Unable to report Clearing buffers to screen.");
                    }


                    //In cas no sondes are found.
                    if (numberOfSondes == 0)
                    {
                        DialogResult search0results = MessageBox.Show("No Radiosonde detected. Is the power on?", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        RadiosondeStatus.activeRadiosondeData = false;
                        e.Cancel = true;
                        return;
                    }
                    else
                    {
                        RadiosondeStatus.activeRadiosondeData = true;
                    }

                    //Clear out any and all data from the radiosonde serial ports. This is needed do to the large timeout now allowed.
                    for (int ab = 0; ab < ValidSondeArryiMet1.Length; ab++)
                    {
                        ValidSondeArryiMet1[ab].ComPort.ReadExisting();
                    }

                    //Loop for updating data from valid radiosondes.
                    while (backgroundWorkerRadiosondeDataCollect.CancellationPending == false)
                    {
                        if (ValidSondeArryiMet1.Length > 0)
                        {
                            RadiosondeStatus.activeRadiosondeData = true;
                        }

                        //Allows data collection to be paused. This allows the command to be sent.
                        if (RadiosondeStatus.processRadiosondeHex == true)
                        {

                            for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                            {
                                try
                                {
                                    ValidSondeArryiMet1[i].getLastHexData();
                                }
                                catch
                                {
                                    //Some kind of error log write should go here.
                                    writeToErrorLog("Unable to getLastHexData form " + ValidSondeArryiMet1[i].ComPort.PortName);
                                    //need to jump to wait. I think.
                                }

                            }
                            //Getting the time that data was collected.
                        }

                        Thread.Sleep(100);
                        //Update data to data grids.
                        try
                        {
                            this.Invoke(new SendDataToScreen(this.UpdateDataGridsDisplay));
                        }
                        catch
                        {
                            if (backgroundWorkerRadiosondeDataCollect.CancellationPending == false)
                            {
                                writeToErrorLog("Unable to start update to screen");
                            }
                        }

                        //Taking a little pause so that this thread just doesn't run off.
                        try
                        {
                            if (ValidSondeArryiMet1.Length > 8)
                            {
                                Thread.Sleep(300);
                            }
                            else
                            {
                                Thread.Sleep(800);
                            }
                        }
                        catch { }
                    }
                    #endregion

                    break;

                case "iMet Un":

                    #region Background Worker for the Universal Radiosondes.
                    if (backgroundWorkerRadiosondeDataCollect.CancellationPending) /// Something to stop the process.
                    {
                        e.Cancel = true;
                        return;
                    }

                    //Searching for valid radiosondes.
                    //Adding up all the com ports.
                    int totalNumberOfComPortsU = (ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                        (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                        (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms) +
                        (ProgramSettings.EndRackDRadiosondeComms - ProgramSettings.StartRackDRadiosondeComms);



                    string[] possibleSondeNamesU = new string[totalNumberOfComPortsU];

                    iMet1U[] PossibleSondeArryU = new iMet1U[possibleSondeNamesU.Length];

                    //Counters for each rack.
                    int counterRackAU = 0;
                    int counterRackBU = 0;
                    int counterRackCU = 0;
                    int counterRackDU = 0;

                    for (int i = 0; i < possibleSondeNamesU.Length; i++)
                    {
                        if (i <= ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms)
                        {
                            possibleSondeNamesU[i] = "COM" + (counterRackAU + ProgramSettings.StartRackARadiosondeComms).ToString();
                            counterRackAU++;
                        }

                        if (i > ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms &&
                            i <= ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms)))
                        {
                            possibleSondeNamesU[i] = "COM" + (counterRackBU + ProgramSettings.StartRackBRadiosondeComms).ToString();
                            counterRackBU++;
                        }

                        if (i > ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms)) &&
                            i <= ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                                    (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms)))
                        {
                            possibleSondeNamesU[i] = "COM" + (counterRackCU + ProgramSettings.StartRackCRadiosondeComms).ToString();
                            counterRackCU++;
                        }

                        if (i > ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                                    (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms)) &&
                            i <= ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                                    (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms) +
                                    (ProgramSettings.EndRackDRadiosondeComms - ProgramSettings.StartRackDRadiosondeComms)))
                        {
                            possibleSondeNamesU[i] = "COM" + (counterRackDU + ProgramSettings.StartRackDRadiosondeComms).ToString();
                            counterRackDU++;
                        }
                    }

                    int numberOfValidRadiosondes = 0; //Count of valid radiosonde.

                    for (int i = 0; i < possibleSondeNamesU.Length; i++)
                    {
                        try
                        {
                            this.Invoke(new UpdateMainStatusLabelData(this.UpdateMainStatusLabel), new object[] { ("Searching " + possibleSondeNamesU[i]) });
                        }
                        catch
                        {
                            writeToErrorLog("Unable to report searching for radiosonde on " + possibleSondeNamesU[i]);
                        }


                        PossibleSondeArryU[i] = new iMet1U(possibleSondeNamesU[i]);  //setting com ports to radiosondes. 

                        if (PossibleSondeArryU[i].getPortName() == "COM146")
                        {
                            string test = "test";
                        }

                        if (PossibleSondeArryU[i].checkForRadiosonde()) //Checks to see if a radiosonde is attached.
                        {
                            
                            this.Invoke(new UpdateMainStatusLabelData(this.UpdateMainStatusLabel), new object[] { ("Radiosonde found on " + possibleSondeNamesU[i]) });
                            numberOfValidRadiosondes++;
                        }
                        else
                        {
                            PossibleSondeArryU[i] = null;
                        }
                    }

                    
                        int comNumberU = 0;
                        //resetting rack couters.
                        counterRackAU = 0;
                        counterRackBU = 0;
                        counterRackCU = 0;
                        counterRackDU = 0;

                        //Setting up the Valid array of universal radiosondes
                        ValidSondeiMet1U = new iMet1U[numberOfValidRadiosondes];


                        //Compiling the Valid radiosondes into the global.
                        int ij = 0;
                        for (int w = 0, x = 0; x < ValidSondeiMet1U.Length && ij < PossibleSondeArryU.Length; ij++)
                        {

                            if (PossibleSondeArryU[ij] != null)
                            {
                                ValidSondeiMet1U[x] = PossibleSondeArryU[ij];
                                //ValidSondeiMet1U[x].Reset();
                                ValidSondeiMet1U[x].OpenPort();

                                ValidSondeiMet1U[x].setPollResponseMode();
                                System.Threading.Thread.Sleep(100);
                                ValidSondeiMet1U[x].setManufacturingMode(true);

                                //Counting up sondes and sorting them to the correct rack.
                                if (ValidSondeiMet1U[x].getPortName().Length == 4)
                                { comNumberU = Convert.ToInt16(ValidSondeiMet1U[x].getPortName().Substring(3, 1)); }

                                if (ValidSondeiMet1U[x].getPortName().Length == 5)
                                { comNumberU = Convert.ToInt16(ValidSondeiMet1U[x].getPortName().Substring(3, 2)); }

                                if (ValidSondeiMet1U[x].getPortName().Length == 6)
                                { comNumberU = Convert.ToInt16(ValidSondeiMet1U[x].getPortName().Substring(3, 3)); }

                                if (comNumberU >= ProgramSettings.StartRackARadiosondeComms && comNumberU < ProgramSettings.EndRackARadiosondeComms)
                                { counterRackAU++; }

                                if (comNumberU >= ProgramSettings.StartRackBRadiosondeComms && comNumberU < ProgramSettings.EndRackBRadiosondeComms)
                                { counterRackBU++; }

                                if (comNumberU >= ProgramSettings.StartRackCRadiosondeComms && comNumberU < ProgramSettings.EndRackCRadiosondeComms)
                                { counterRackCU++; }

                                if (comNumberU >= ProgramSettings.StartRackDRadiosondeComms && comNumberU < ProgramSettings.EndRackDRadiosondeComms)
                                { counterRackDU++; }

                                x++;
                            }

                        }

                        //Setting up the data grids.
                        try
                        {
                            this.Invoke(new UpdateRacksDataGridsData(this.UpdateRackDataGrid), new object[] { counterRackAU, counterRackBU, counterRackCU, counterRackDU });
                        }
                        catch { writeToErrorLog("Unable to update one of the rack data grids."); }

                        UpdateMainStatusLabel(ValidSondeiMet1U.Length.ToString() + " Radiosonde(s) found.");
                        

                        try
                        {
                            this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 1, counterRackAU.ToString() + " Radiosonde(s) found." });
                            this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 2, counterRackBU.ToString() + " Radiosonde(s) found." });
                            this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 3, counterRackCU.ToString() + " Radiosonde(s) found." });
                            this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 4, counterRackDU.ToString() + " Radiosonde(s) found." });
                        }
                        catch
                        {
                            writeToErrorLog("Unable to report Clearing buffers to screen.");
                        }


                        //In case no sondes are found.
                        if (numberOfValidRadiosondes == 0)
                        {
                            DialogResult search0results = MessageBox.Show("No Radiosonde detected. Is the power on?", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            RadiosondeStatus.activeRadiosondeData = false;

                            if (programRelay != null)
                            {
                                programRelay.outsideUpdate();
                            }

                            return;
                        }
                        else
                        {
                            RadiosondeStatus.activeRadiosondeData = true;
                            if (auxSettings != null)
                            {
                                auxSettings.controls_On();
                            }

                            if (programRelay != null)
                            {
                                programRelay.outsideUpdate();
                                programRelay.outsideReset();
                                
                            }
                        }

                        //Starting backgroundworkers that will update the uni radiosonde display
                    /*
                        backgroundRadiosondeDisplay = new BackgroundWorker[numberOfValidRadiosondes];
                        for(int back = 0; back<backgroundRadiosondeDisplay.Length; back++)
                        {
                            backgroundRadiosondeDisplay[back] = new BackgroundWorker();
                            backgroundRadiosondeDisplay[back].WorkerSupportsCancellation = true;
                            backgroundRadiosondeDisplay[back].DoWork += new DoWorkEventHandler(worker_DoWork);
                            object[] tempInformation = new object[] { ValidSondeiMet1U[back], back };
                            backgroundRadiosondeDisplay[back].RunWorkerAsync(tempInformation);
                        }
                    */
                        //Loop for updating data from valid radiosondes.
                        while (backgroundWorkerRadiosondeDataCollect.CancellationPending == false)
                        {
                            if (ValidSondeiMet1U.Length > 0)
                            {
                                RadiosondeStatus.activeRadiosondeData = true;
                            }


                            Thread.Sleep(100);
                            //Update data to data grids.
                            try
                            {
                                //Old way of updating the data screen
                                this.Invoke(new SendDataToScreen(this.UpdateDataGridsDisplay));
                                if (!sondeRelaySettingsToolStripMenuItem.Enabled)
                                {
                                    sondeRelaySettingsToolStripMenuItem.Enabled = true;
                                }
                            }
                            catch
                            {
                                if (backgroundWorkerRadiosondeDataCollect.CancellationPending == false)
                                {
                                    writeToErrorLog("Unable to start update to screen");
                                }
                            }

                            //Taking a little pause so that this thread just doesn't run off.
                            try
                            {
                                if (ValidSondeArryiMet1.Length > 8)
                                {
                                    Thread.Sleep(300);
                                }
                                else
                                {
                                    Thread.Sleep(800);
                                }
                            }
                            catch { }


                        }
                    
                    break;
                    #endregion

                case "iMet 2.":
                    #region Background Worker for radiosondes with firmware 2.3x
                    if (backgroundWorkerRadiosondeDataCollect.CancellationPending) /// Something to stop the process.
                    {
                        e.Cancel = true;
                        return;
                    }

                    //Searching for valid radiosondes.
                    //Adding up all the com ports.
                    int totalNumberOfComPortsUold = (ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                        (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                        (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms) +
                        (ProgramSettings.EndRackDRadiosondeComms - ProgramSettings.StartRackDRadiosondeComms);



                    string[] possibleSondeNamesUold = new string[totalNumberOfComPortsUold];

                    iMet1U[] PossibleSondeArryUold = new iMet1U[possibleSondeNamesUold.Length];

                    //Counters for each rack.
                    int counterRackAUold = 0;
                    int counterRackBUold = 0;
                    int counterRackCUold = 0;
                    int counterRackDUold = 0;

                    for (int i = 0; i < possibleSondeNamesUold.Length; i++)
                    {
                        if (i <= ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms)
                        {
                            possibleSondeNamesUold[i] = "COM" + (counterRackAUold + ProgramSettings.StartRackARadiosondeComms).ToString();
                            counterRackAUold++;
                        }

                        if (i > ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms &&
                            i <= ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms)))
                        {
                            possibleSondeNamesUold[i] = "COM" + (counterRackBUold + ProgramSettings.StartRackBRadiosondeComms).ToString();
                            counterRackBUold++;
                        }

                        if (i > ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms)) &&
                            i <= ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                                    (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms)))
                        {
                            possibleSondeNamesUold[i] = "COM" + (counterRackCUold + ProgramSettings.StartRackCRadiosondeComms).ToString();
                            counterRackCUold++;
                        }

                        if (i > ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                                    (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms)) &&
                            i <= ((ProgramSettings.EndRackARadiosondeComms - ProgramSettings.StartRackARadiosondeComms) +
                                    (ProgramSettings.EndRackBRadiosondeComms - ProgramSettings.StartRackBRadiosondeComms) +
                                    (ProgramSettings.EndRackCRadiosondeComms - ProgramSettings.StartRackCRadiosondeComms) +
                                    (ProgramSettings.EndRackDRadiosondeComms - ProgramSettings.StartRackDRadiosondeComms)))
                        {
                            possibleSondeNamesUold[i] = "COM" + (counterRackDUold + ProgramSettings.StartRackDRadiosondeComms).ToString();
                            counterRackDUold++;
                        }
                    }

                    int numberOfValidRadiosondesold = 0; //Count of valid radiosonde.

                    for (int i = 0; i < possibleSondeNamesUold.Length; i++)
                    {
                        try
                        {
                            this.Invoke(new UpdateMainStatusLabelData(this.UpdateMainStatusLabel), new object[] { ("Searching " + possibleSondeNamesUold[i]) });
                        }
                        catch
                        {
                            writeToErrorLog("Unable to report searching for radiosonde on " + possibleSondeNamesUold[i]);
                        }


                        PossibleSondeArryUold[i] = new iMet1U(possibleSondeNamesUold[i]);  //setting com ports to radiosondes. 

                        if (PossibleSondeArryUold[i].getPortName() == "COM146")
                        {
                            string test = "test";
                        }

                        if (PossibleSondeArryUold[i].checkForRadiosonde()) //Checks to see if a radiosonde is attached.
                        {

                            this.Invoke(new UpdateMainStatusLabelData(this.UpdateMainStatusLabel), new object[] { ("Radiosonde found on " + possibleSondeNamesUold[i]) });
                            numberOfValidRadiosondesold++;
                        }
                        else
                        {
                            PossibleSondeArryUold[i] = null;
                        }
                    }


                    int comNumberUold = 0;
                    //resetting rack couters.
                    counterRackAUold = 0;
                    counterRackBUold = 0;
                    counterRackCUold = 0;
                    counterRackDUold = 0;

                    //Setting up the Valid array of universal radiosondes
                    ValidSondeiMet1U = new iMet1U[numberOfValidRadiosondesold];


                    //Compiling the Valid radiosondes into the global.
                    int ijold = 0;
                    for (int w = 0, x = 0; x < ValidSondeiMet1U.Length && ijold < PossibleSondeArryUold.Length; ijold++)
                    {

                        if (PossibleSondeArryUold[ijold] != null)
                        {
                            ValidSondeiMet1U[x] = PossibleSondeArryUold[ijold];
                            //ValidSondeiMet1U[x].Reset();
                            ValidSondeiMet1U[x].OpenPort();

                            //ValidSondeiMet1U[x].setPollResponseMode(); //This could cause an error.
                            System.Threading.Thread.Sleep(100);
                            //ValidSondeiMet1U[x].setManufacturingMode(true); //This doesn't exsist in the old sonde.
                            ValidSondeiMet1U[x].setPTUMode(true); //This should give us some data.

                            //Counting up sondes and sorting them to the correct rack.
                            if (ValidSondeiMet1U[x].getPortName().Length == 4)
                            { comNumberUold = Convert.ToInt16(ValidSondeiMet1U[x].getPortName().Substring(3, 1)); }

                            if (ValidSondeiMet1U[x].getPortName().Length == 5)
                            { comNumberUold = Convert.ToInt16(ValidSondeiMet1U[x].getPortName().Substring(3, 2)); }

                            if (ValidSondeiMet1U[x].getPortName().Length == 6)
                            { comNumberUold = Convert.ToInt16(ValidSondeiMet1U[x].getPortName().Substring(3, 3)); }

                            if (comNumberUold >= ProgramSettings.StartRackARadiosondeComms && comNumberUold < ProgramSettings.EndRackARadiosondeComms)
                            { counterRackAUold++; }

                            if (comNumberUold >= ProgramSettings.StartRackBRadiosondeComms && comNumberUold < ProgramSettings.EndRackBRadiosondeComms)
                            { counterRackBUold++; }

                            if (comNumberUold >= ProgramSettings.StartRackCRadiosondeComms && comNumberUold < ProgramSettings.EndRackCRadiosondeComms)
                            { counterRackCUold++; }

                            if (comNumberUold >= ProgramSettings.StartRackDRadiosondeComms && comNumberUold < ProgramSettings.EndRackDRadiosondeComms)
                            { counterRackDUold++; }

                            x++;
                        }

                    }

                    //Setting up the data grids.
                    try
                    {
                        this.Invoke(new UpdateRacksDataGridsData(this.UpdateRackDataGrid), new object[] { counterRackAUold, counterRackBUold, counterRackCUold, counterRackDUold });
                    }
                    catch { writeToErrorLog("Unable to update one of the rack data grids."); }

                    UpdateMainStatusLabel(ValidSondeiMet1U.Length.ToString() + " Radiosonde(s) found.");


                    try
                    {
                        this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 1, counterRackAUold.ToString() + " Radiosonde(s) found." });
                        this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 2, counterRackBUold.ToString() + " Radiosonde(s) found." });
                        this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 3, counterRackCUold.ToString() + " Radiosonde(s) found." });
                        this.Invoke(new UpdateRackStatusLabelData(this.UpdateRackStatusLabels), new object[] { 4, counterRackDUold.ToString() + " Radiosonde(s) found." });
                    }
                    catch
                    {
                        writeToErrorLog("Unable to report Clearing buffers to screen.");
                    }


                    //In case no sondes are found.
                    if (numberOfValidRadiosondesold == 0)
                    {
                        DialogResult search0results = MessageBox.Show("No Radiosonde detected. Is the power on?", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        RadiosondeStatus.activeRadiosondeData = false;

                        if (programRelay != null)
                        {
                            programRelay.outsideUpdate();
                        }

                        return;
                    }
                    else
                    {
                        RadiosondeStatus.activeRadiosondeData = true;
                        /*
                        if (auxSettings != null)
                        {
                            auxSettings.controls_On();
                        }

                        if (programRelay != null)
                        {
                            programRelay.outsideUpdate();
                            programRelay.outsideReset();

                        }
                        */
                    }

                    //Starting backgroundworkers that will update the uni radiosonde display
                    /*
                        backgroundRadiosondeDisplay = new BackgroundWorker[numberOfValidRadiosondesold];
                        for(int back = 0; back<backgroundRadiosondeDisplay.Length; back++)
                        {
                            backgroundRadiosondeDisplay[back] = new BackgroundWorker();
                            backgroundRadiosondeDisplay[back].WorkerSupportsCancellation = true;
                            backgroundRadiosondeDisplay[back].DoWork += new DoWorkEventHandler(worker_DoWork);
                            object[] tempInformation = new object[] { ValidSondeiMet1U[back], back };
                            backgroundRadiosondeDisplay[back].RunWorkerAsync(tempInformation);
                        }
                    */
                    //Loop for updating data from valid radiosondes.
                    while (backgroundWorkerRadiosondeDataCollect.CancellationPending == false)
                    {
                        if (ValidSondeiMet1U.Length > 0)
                        {
                            RadiosondeStatus.activeRadiosondeData = true;
                        }


                        Thread.Sleep(100);
                        //Update data to data grids.
                        try
                        {
                            //Old way of updating the data screen
                            this.Invoke(new SendDataToScreen(this.UpdateDataGridsDisplay));
                            /*
                            if (!sondeRelaySettingsToolStripMenuItem.Enabled)
                            {
                                sondeRelaySettingsToolStripMenuItem.Enabled = true;
                            }
                            */ 
                        }
                        catch
                        {
                            if (backgroundWorkerRadiosondeDataCollect.CancellationPending == false)
                            {
                                writeToErrorLog("Unable to start update to screen");
                            }
                        }

                        //Taking a little pause so that this thread just doesn't run off.
                        try
                        {
                            if (ValidSondeArryiMet1.Length > 8)
                            {
                                Thread.Sleep(300);
                            }
                            else
                            {
                                Thread.Sleep(800);
                            }
                        }
                        catch { }


                    }



                    break;
                    #endregion
            }

        }


        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Breaking out data from the event call
            object[] incomingInformation = (object[])e.Argument;
            Universal_Radiosonde.iMet1U radiosonde = (Universal_Radiosonde.iMet1U)incomingInformation[0]; //This thread radiosonde
            int radiosondeIndex = (int)incomingInformation[1];

            //Starting process loop to fill in data.

            while (!backgroundRadiosondeDisplay[radiosondeIndex].CancellationPending)
            {
                try
                {
                    //Current Sensor Readings.
                    double currentHumidity = Convert.ToDouble(labelSensorHumidity.Text);
                    double currentTemp = Convert.ToDouble(labelSensorTemperature.Text);
                    double currentPressure = Convert.ToDouble(labelSensorPressure.Text);

                    //Counters for dataGide position.
                    int[] rackPosCounter = new int[4] { -1, -1, -1, -1 };
                    //Data View Grids
                    System.Windows.Forms.DataGridView[] dataGrids = new DataGridView[4];
                    dataGrids[0] = dataGridViewRackA;
                    dataGrids[1] = dataGridViewRackB;
                    dataGrids[2] = dataGridViewRackC;
                    dataGrids[3] = dataGridViewRackD;

                    int tempComNumber = 0;
                    int rackToBeUpdated = 0;
                    for (int testingName = 0; testingName < 2; testingName++)
                    {
                        if (ValidSondeiMet1U[radiosondeIndex].getPortName().Length == (4 + testingName))
                        { tempComNumber = Convert.ToInt16(ValidSondeiMet1U[radiosondeIndex].getPortName().Substring(3, 1 + testingName)); }
                    }

                    //Determining the rack that will be updated.
                    if (tempComNumber >= ProgramSettings.StartRackARadiosondeComms && tempComNumber < ProgramSettings.EndRackARadiosondeComms)
                    { rackToBeUpdated = 0; }
                    if (tempComNumber >= ProgramSettings.StartRackBRadiosondeComms && tempComNumber < ProgramSettings.EndRackBRadiosondeComms)
                    { rackToBeUpdated = 1; }
                    if (tempComNumber >= ProgramSettings.StartRackCRadiosondeComms && tempComNumber < ProgramSettings.EndRackCRadiosondeComms)
                    { rackToBeUpdated = 2; }
                    if (tempComNumber >= ProgramSettings.StartRackDRadiosondeComms && tempComNumber < ProgramSettings.EndRackDRadiosondeComms)
                    { rackToBeUpdated = 3; }

                    //Counter to keep track of what row in the rack to upgrade
                    rackPosCounter[rackToBeUpdated]++;


                    //Loading data into the gridview
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[0].Value = (radiosondeIndex + 1).ToString();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Value = ValidSondeiMet1U[radiosondeIndex].CalData.SondeID;
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Value = ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber;
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Value = ValidSondeiMet1U[radiosondeIndex].CalData.ProbeID;
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[4].Value = ValidSondeiMet1U[radiosondeIndex].getPortName();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[5].Value = ValidSondeiMet1U[radiosondeIndex].CalData.PressureTemperature.ToString();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[6].Value = ValidSondeiMet1U[radiosondeIndex].CalData.Pressure.ToString();

                    //Displaying differances Also AT and Humidity
                    //Pressure Differance
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[7].Value = (ValidSondeiMet1U[radiosondeIndex].CalData.Pressure - currentPressure).ToString("0.000");

                    //AT
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[8].Value = ValidSondeiMet1U[radiosondeIndex].CalData.Temperature.ToString();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[9].Value = (ValidSondeiMet1U[radiosondeIndex].CalData.Temperature - currentTemp).ToString("0.000");

                    //Humidity
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[10].Value = ValidSondeiMet1U[radiosondeIndex].CalData.Humidity.ToString();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[11].Value = (ValidSondeiMet1U[radiosondeIndex].CalData.Humidity - currentHumidity).ToString("0.000");

                    //Firmware
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[12].Value = "Universal";

                    //Pass/Fail Coloring of GrideView
                    //Checking sonde id
                    if (checkForRepeat(ValidSondeiMet1U[radiosondeIndex].CalData.SondeID, "Serial Number") == false)
                    {
                        dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Style.BackColor = Color.White;

                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SondeID.Substring(0, 1).ToUpper() == "S")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                        }
                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SondeID.Substring(0, 1).ToUpper() == "E")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                        }

                        /*if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                        }
                         */
                    }
                    else
                    {
                        //dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = repeatSondeElements;
                    }

                    //Checking the tracking number
                    if (checkForRepeat(ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber, "Tracking Number") == false)
                    {
                        dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = Color.White;

                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber.Substring(0, 1).ToUpper() == "S")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                        }
                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber.Substring(0, 1).ToUpper() == "E")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                        }
                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber.Substring(0, 1).ToUpper() == "U")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = System.Drawing.Color.LightBlue;
                        }
                        /*
                        if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                        }
                         */
                    }
                    else
                    {
                        //dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[2].Style.BackColor = repeatSondeElements;
                    }

                    //Checking to make sure the PN starts with a "P"
                    if (checkForRepeat(ValidSondeiMet1U[radiosondeIndex].CalData.ProbeID, "Probe Number") == false)
                    {
                        dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = Color.White;

                        if (ValidSondeiMet1U[radiosondeIndex].CalData.ProbeID.Substring(0, 1).ToUpper() == "P")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                        }
                        else
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = ProgramSettings.colorFail;

                        }
                    }
                    else
                    {
                        //dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = repeatSondeElements;
                    }

                    //Pressure Differance
                    if (Math.Abs(currentPressure - ValidSondeiMet1U[radiosondeIndex].CalData.Pressure) > ProgramSettings.AcceptablePressureDiff)
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[7].Style.BackColor = ProgramSettings.colorFail; }
                    else
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[7].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

                    //Temp Differance
                    if (Math.Abs(currentTemp - ValidSondeiMet1U[radiosondeIndex].CalData.Temperature) > ProgramSettings.AcceptableTempDiff)
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[9].Style.BackColor = ProgramSettings.colorFail; }
                    else
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[9].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

                    //Humidity Differance
                    if (Math.Abs(currentHumidity - ValidSondeiMet1U[radiosondeIndex].CalData.Humidity) > ProgramSettings.AcceptableHumidityDiff)
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[11].Style.BackColor = ProgramSettings.colorFail; }
                    else
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[11].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }


                    //Pausing for a little while
                    Thread.Sleep(2000);
                }
                catch (Exception displayError)
                {
                    UpdateMainStatusLabel(displayError.Message);
                }
            }
            

            //Letting the operatior know that the radiosonde update has ended.
            UpdateMainStatusLabel("Radiosonde " + radiosonde.CalData.ProbeID + " has shutdown.");

        }

        /*
        void CalDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            System.ComponentModel.BackgroundWorker updateData = new BackgroundWorker();
            updateData.DoWork +=new DoWorkEventHandler(updateData_DoWork);
            updateData.RunWorkerAsync(data);
        }
        */

        
        private void updateRadiosondeData(Universal_Radiosonde.CalData calData)
        {
            //Determining what radiosonde is being updated.
            Universal_Radiosonde.CalData updatingSonde = calData;
            int radiosondeIndex=0;
            for (int x = 0; x < ValidSondeiMet1U.Length; x++)
            {
                if (ValidSondeiMet1U[x].CalData.SerialNumber == updatingSonde.SerialNumber &&
                    ValidSondeiMet1U[x].CalData.SondeID == updatingSonde.SondeID &&
                    ValidSondeiMet1U[x].CalData.ProbeID == updatingSonde.ProbeID &&
                    ValidSondeiMet1U[x].CalData.HumidityFrequency == updatingSonde.HumidityFrequency &&
                    ValidSondeiMet1U[x].CalData.TemperatureCounts == updatingSonde.TemperatureCounts)
                {
                    radiosondeIndex = x;
                }
            }

            //Current Sensor Readings.
            double currentHumidity = Convert.ToDouble(labelSensorHumidity.Text);
            double currentTemp = Convert.ToDouble(labelSensorTemperature.Text);
            double currentPressure = Convert.ToDouble(labelSensorPressure.Text);

            //Counters for dataGide position.
            int[] rackPosCounter = new int[4] { -1, -1, -1, -1 };
            //Data View Grids
            System.Windows.Forms.DataGridView[] dataGrids = new DataGridView[4];
            dataGrids[0] = dataGridViewRackA;
            dataGrids[1] = dataGridViewRackB;
            dataGrids[2] = dataGridViewRackC;
            dataGrids[3] = dataGridViewRackD;

            int tempComNumber = 0;
            int rackToBeUpdated = 0;
            for (int testingName = 0; testingName < 2; testingName++)
            {
                if (ValidSondeiMet1U[radiosondeIndex].getPortName().Length == (4 + testingName))
                { tempComNumber = Convert.ToInt16(ValidSondeiMet1U[radiosondeIndex].getPortName().Substring(3, 1 + testingName)); }
            }

            //Determining the rack that will be updated.
            if (tempComNumber >= ProgramSettings.StartRackARadiosondeComms && tempComNumber < ProgramSettings.EndRackARadiosondeComms)
            { rackToBeUpdated = 0; }
            if (tempComNumber >= ProgramSettings.StartRackBRadiosondeComms && tempComNumber < ProgramSettings.EndRackBRadiosondeComms)
            { rackToBeUpdated = 1; }
            if (tempComNumber >= ProgramSettings.StartRackCRadiosondeComms && tempComNumber < ProgramSettings.EndRackCRadiosondeComms)
            { rackToBeUpdated = 2; }
            if (tempComNumber >= ProgramSettings.StartRackDRadiosondeComms && tempComNumber < ProgramSettings.EndRackDRadiosondeComms)
            { rackToBeUpdated = 3; }

            //Counter to keep track of what row in the rack to upgrade
            rackPosCounter[rackToBeUpdated]++;

            
            //Loading data into the gridview
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[0].Value = (radiosondeIndex + 1).ToString();
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Value = ValidSondeiMet1U[radiosondeIndex].CalData.SondeID;
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Value = ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber;
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Value = ValidSondeiMet1U[radiosondeIndex].CalData.ProbeID;
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[4].Value = ValidSondeiMet1U[radiosondeIndex].getPortName();
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[5].Value = ValidSondeiMet1U[radiosondeIndex].CalData.PressureTemperature.ToString();
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[6].Value = ValidSondeiMet1U[radiosondeIndex].CalData.Pressure.ToString();

            //Displaying differances Also AT and Humidity
            //Pressure Differance
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[7].Value = (ValidSondeiMet1U[radiosondeIndex].CalData.Pressure - currentPressure).ToString("0.000");

            //AT
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[8].Value = ValidSondeiMet1U[radiosondeIndex].CalData.Temperature.ToString();
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[9].Value = (ValidSondeiMet1U[radiosondeIndex].CalData.Temperature - currentTemp).ToString("0.000");

            //Humidity
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[10].Value = ValidSondeiMet1U[radiosondeIndex].CalData.Humidity.ToString();
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[11].Value = (ValidSondeiMet1U[radiosondeIndex].CalData.Humidity - currentHumidity).ToString("0.000");

            //Firmware
            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[12].Value = "Universal";

            //Pass/Fail Coloring of GrideView
            //Checking sonde id
            if (checkForRepeat(ValidSondeiMet1U[radiosondeIndex].CalData.SondeID, "Serial Number") == false)
            {
                dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Style.BackColor = Color.White;

                if (ValidSondeiMet1U[radiosondeIndex].CalData.SondeID.Substring(0, 1).ToUpper() == "S")
                {
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                }
                if (ValidSondeiMet1U[radiosondeIndex].CalData.SondeID.Substring(0, 1).ToUpper() == "E")
                {
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                }

                //if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                //{
                //    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                //}
                 
            }
            else
            {
                //dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = repeatSondeElements;
            }

            //Checking the tracking number
            if (checkForRepeat(ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber, "Tracking Number") == false)
            {
                dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = Color.White;

                if (ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber.Substring(0, 1).ToUpper() == "S")
                {
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                }
                if (ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber.Substring(0, 1).ToUpper() == "E")
                {
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                }
                if (ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber.Substring(0, 1).ToUpper() == "U")
                {
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = System.Drawing.Color.LightBlue;
                }
                /*
                if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                {
                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                }
                 */
            }
            else
            {
                //dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[2].Style.BackColor = repeatSondeElements;
            }

            //Checking to make sure the PN starts with a "P"
            if (checkForRepeat(ValidSondeiMet1U[radiosondeIndex].CalData.ProbeID, "Probe Number") == false)
            {
                dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = Color.White;

                if (ValidSondeiMet1U[radiosondeIndex].CalData.ProbeID.Substring(0, 1).ToUpper() == "P")
                {
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                }
                else
                {
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = ProgramSettings.colorFail;

                }
            }
            else
            {
                //dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = repeatSondeElements;
            }

            //Pressure Differance
            if (Math.Abs(currentPressure - ValidSondeiMet1U[radiosondeIndex].CalData.Pressure) > ProgramSettings.AcceptablePressureDiff)
            { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[7].Style.BackColor = ProgramSettings.colorFail; }
            else
            { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[7].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

            //Temp Differance
            if (Math.Abs(currentTemp - ValidSondeiMet1U[radiosondeIndex].CalData.Temperature) > ProgramSettings.AcceptableTempDiff)
            { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[9].Style.BackColor = ProgramSettings.colorFail; }
            else
            { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[9].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

            //Humidity Differance
            if (Math.Abs(currentHumidity - ValidSondeiMet1U[radiosondeIndex].CalData.Humidity) > ProgramSettings.AcceptableHumidityDiff)
            { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[11].Style.BackColor = ProgramSettings.colorFail; }
            else
            { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[11].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }
                    
        }

        void updateData_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                updateCreater.PropertyChangeEventArgs radiosondeEvent = (updateCreater.PropertyChangeEventArgs)e.Argument;
                Universal_Radiosonde.CalData radiosondeData = (Universal_Radiosonde.CalData)radiosondeEvent.NewValue;
                //this.Invoke(new SendDataToScreen(this.UpdateDataGridsDisplay));
                //BeginInvoke(new UpdateLine(this.updateRadiosondeData), new object[] { radiosondeData });

                //UpdateMainStatusLabel(radiosondeData.ProbeID + "," + radiosondeData.HumidityFrequency + "," + radiosondeData.TemperatureCounts);


                //Determining what radiosonde is being updated.
                Universal_Radiosonde.CalData updatingSonde = radiosondeData;
                int radiosondeIndex = 0;
                for (int x = 0; x < ValidSondeiMet1U.Length; x++)
                {
                    if (ValidSondeiMet1U[x] != null)
                    {
                        if (ValidSondeiMet1U[x].CalData.SerialNumber == updatingSonde.SerialNumber &&
                            ValidSondeiMet1U[x].CalData.SondeID == updatingSonde.SondeID &&
                            ValidSondeiMet1U[x].CalData.ProbeID == updatingSonde.ProbeID &&
                            ValidSondeiMet1U[x].CalData.HumidityFrequency == updatingSonde.HumidityFrequency &&
                            ValidSondeiMet1U[x].CalData.TemperatureCounts == updatingSonde.TemperatureCounts)
                        {
                            radiosondeIndex = x;
                        }
                    }
                }

                //Current Sensor Readings.
                double currentHumidity = Convert.ToDouble(labelSensorHumidity.Text);
                double currentTemp = Convert.ToDouble(labelSensorTemperature.Text);
                double currentPressure = Convert.ToDouble(labelSensorPressure.Text);

                //Counters for dataGide position.
                int[] rackPosCounter = new int[4] { -1, -1, -1, -1 };
                //Data View Grids
                System.Windows.Forms.DataGridView[] dataGrids = new DataGridView[4];
                dataGrids[0] = dataGridViewRackA;
                dataGrids[1] = dataGridViewRackB;
                dataGrids[2] = dataGridViewRackC;
                dataGrids[3] = dataGridViewRackD;

                int tempComNumber = 0;
                int rackToBeUpdated = 0;
                for (int testingName = 0; testingName < 2; testingName++)
                {
                    if (ValidSondeiMet1U[radiosondeIndex].getPortName().Length == (4 + testingName))
                    { tempComNumber = Convert.ToInt16(ValidSondeiMet1U[radiosondeIndex].getPortName().Substring(3, 1 + testingName)); }
                }

                if (dataGrids[0].Rows.Count < ValidSondeiMet1U.Length)
                {

                }
                else
                {
                    //Loading data into the gridview
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[0].Value = (radiosondeIndex + 1).ToString();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Value = ValidSondeiMet1U[radiosondeIndex].CalData.SondeID;
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Value = ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber;
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Value = ValidSondeiMet1U[radiosondeIndex].CalData.ProbeID;
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[4].Value = ValidSondeiMet1U[radiosondeIndex].getPortName();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[5].Value = ValidSondeiMet1U[radiosondeIndex].CalData.PressureTemperature.ToString();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[6].Value = ValidSondeiMet1U[radiosondeIndex].CalData.Pressure.ToString();

                    //Displaying differances Also AT and Humidity
                    //Pressure Differance
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[7].Value = (ValidSondeiMet1U[radiosondeIndex].CalData.Pressure - currentPressure).ToString("0.000");

                    //AT
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[8].Value = ValidSondeiMet1U[radiosondeIndex].CalData.Temperature.ToString();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[9].Value = (ValidSondeiMet1U[radiosondeIndex].CalData.Temperature - currentTemp).ToString("0.000");

                    //Humidity
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[10].Value = ValidSondeiMet1U[radiosondeIndex].CalData.Humidity.ToString();
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[11].Value = (ValidSondeiMet1U[radiosondeIndex].CalData.Humidity - currentHumidity).ToString("0.000");

                    //Firmware
                    dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[12].Value = "Universal";

                    //Pass/Fail Coloring of GrideView
                    //Checking sonde id
                    if (checkForRepeat(ValidSondeiMet1U[radiosondeIndex].CalData.SondeID, "Serial Number") == false)
                    {
                        dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Style.BackColor = Color.White;

                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SondeID.Substring(0, 1).ToUpper() == "S")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                        }
                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SondeID.Substring(0, 1).ToUpper() == "E")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                        }

                        /*if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                        }
                         */
                    }
                    else
                    {
                        //dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = repeatSondeElements;
                    }

                    //Checking the tracking number
                    if (checkForRepeat(ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber, "Tracking Number") == false)
                    {
                        dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = Color.White;

                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber.Substring(0, 1).ToUpper() == "S")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                        }
                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber.Substring(0, 1).ToUpper() == "E")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                        }
                        if (ValidSondeiMet1U[radiosondeIndex].CalData.SerialNumber.Substring(0, 1).ToUpper() == "U")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[2].Style.BackColor = System.Drawing.Color.LightBlue;
                        }
                        /*
                        if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                        }
                         */
                    }
                    else
                    {
                        //dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[2].Style.BackColor = repeatSondeElements;
                    }

                    //Checking to make sure the PN starts with a "P"
                    if (checkForRepeat(ValidSondeiMet1U[radiosondeIndex].CalData.ProbeID, "Probe Number") == false)
                    {
                        dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = Color.White;

                        if (ValidSondeiMet1U[radiosondeIndex].CalData.ProbeID.Substring(0, 1).ToUpper() == "P")
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                        }
                        else
                        {
                            dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = ProgramSettings.colorFail;

                        }
                    }
                    else
                    {
                        //dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[3].Style.BackColor = repeatSondeElements;
                    }

                    //Pressure Differance
                    if (Math.Abs(currentPressure - ValidSondeiMet1U[radiosondeIndex].CalData.Pressure) > ProgramSettings.AcceptablePressureDiff)
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[7].Style.BackColor = ProgramSettings.colorFail; }
                    else
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[7].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

                    //Temp Differance
                    if (Math.Abs(currentTemp - ValidSondeiMet1U[radiosondeIndex].CalData.Temperature) > ProgramSettings.AcceptableTempDiff)
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[9].Style.BackColor = ProgramSettings.colorFail; }
                    else
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[9].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

                    //Humidity Differance
                    if (Math.Abs(currentHumidity - ValidSondeiMet1U[radiosondeIndex].CalData.Humidity) > ProgramSettings.AcceptableHumidityDiff)
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[11].Style.BackColor = ProgramSettings.colorFail; }
                    else
                    { dataGrids[rackToBeUpdated].Rows[radiosondeIndex].Cells[11].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

                }
            }
            catch
            {
            }
        }

        private void backgroundWorkerRadiosondeDataCollect_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UpdateMainStatusLabel("All data collection stopped and Radiosonde Com Ports Closed.");
        }

        #endregion

        #region Updates for Radiosonde Background worker.

        public void UpdateMainStatusLabel(string Message)
        {
            commandsRun.Add(Message);
            commandRunTime.Add(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff"));

            string lineTop = "";
            string lineMiddle = "";
            string lineCurrent = "";
            
            if (commandsRun.Count == 1)
            {
                lineTop = "";
                lineMiddle = "";
                lineCurrent = commandsRun[0];
            }
            if (commandsRun.Count == 2)
            {
                lineTop = "";
                lineMiddle = commandsRun[0];
                lineCurrent = commandsRun[1];
            }

            if (commandsRun.Count == 3)
            {
                lineTop = commandsRun[0];
                lineMiddle = commandsRun[1];
                lineCurrent = commandsRun[2];
            }

            if (commandsRun.Count >= 4)
            {
                lineTop = commandsRun[commandsRun.Count-3];
                lineMiddle = commandsRun[commandsRun.Count-2];
                lineCurrent = commandsRun[commandsRun.Count-1];
            }
            
            toolStripStatusLabelMainDisplay.Text = lineTop + "\n" + lineMiddle + "\n" + lineCurrent;
        }

        private void UpdateRackStatusLabels(int Rack, string Message)
        {
            //Rack 1 = Rack A, Rack 2 = Rack B, Rack 3 = Rack C, Rack 4 = Rack D
            if (Rack == 1)
            {
                toolStripStatusLabelRackA.Text = Message;
            }

            if (Rack == 2)
            {
                toolStripStatusLabelRackB.Text = Message;
            }

            if (Rack == 3)
            {
                toolStripStatusLabelRackC.Text = Message;
            }

            if (Rack == 4)
            {
                toolStripStatusLabelRackD.Text = Message;
            }

        }

        private void UpdateRackDataGrid(int counterRackA, int counterRackB, int counterRackC, int counterRackD)
        {
            if (counterRackA > 1)
            {
                dataGridViewRackA.Rows.Add(counterRackA - 1);
                dataGridViewRackA.MouseClick += new MouseEventHandler(dataGridViewRack_MouseClick);
            }

            if (counterRackB > 1)
            {
                dataGridViewRackB.Rows.Add(counterRackB - 1);

            }

            if (counterRackC > 1)
            {
                dataGridViewRackC.Rows.Add(counterRackC - 1);

            }

            if (counterRackD > 1)
            {
                dataGridViewRackD.Rows.Add(counterRackD - 1);

            }

            buttonControlSondeStop.Enabled = true; //Enabling the stop comms with sondes button.
            //Enabling the reconnect button
            buttonControlSondeReconnect.Enabled = true;

            //Enableing Coefficent loading
            groupBoxCoefficent.Enabled = true;

        }

        void dataGridViewRack_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                MessageBox.Show(e.Location.ToString());
            }

            
        }

        private void UpdateDataGridsDisplay()
        {
            int comNumber = 0;
            int counterRackA = 0;
            int counterRackB = 0;
            int counterRackC = 0;
            int counterRackD = 0;

            //Current Sensor Readings.
            double currentHumidity = Convert.ToDouble(labelSensorHumidity.Text);
            double currentTemp = Convert.ToDouble(labelSensorTemperature.Text);
            double currentPressure = Convert.ToDouble(labelSensorPressure.Text);

            //Colors
            Color repeatSondeElements = ProgramSettings.colorRepeat;

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":
                    #region Updating display for iMet-1 Legacy

                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {

                        if (ValidSondeArryiMet1[i].ComPort.PortName.Length == 4)
                        { comNumber = Convert.ToInt16(ValidSondeArryiMet1[i].ComPort.PortName.Substring(3, 1)); }

                        if (ValidSondeArryiMet1[i].ComPort.PortName.Length == 5)
                        { comNumber = Convert.ToInt16(ValidSondeArryiMet1[i].ComPort.PortName.Substring(3, 2)); }

                        if (ValidSondeArryiMet1[i].ComPort.PortName.Length == 6)
                        { comNumber = Convert.ToInt16(ValidSondeArryiMet1[i].ComPort.PortName.Substring(3, 3)); }

                        if (comNumber >= ProgramSettings.StartRackARadiosondeComms && comNumber < ProgramSettings.EndRackARadiosondeComms)
                        {
                            dataGridViewRackA.Rows[counterRackA].Cells[0].Value = (counterRackA + 1).ToString();
                            //Check to make sure the SN starts with and "S"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getSerialNum(), "Serial Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) == "S")
                                {
                                    dataGridViewRackA.Rows[counterRackA].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) == "E")
                                {
                                    dataGridViewRackA.Rows[counterRackA].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) != "S" && ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) != "E")
                                {
                                    dataGridViewRackA.Rows[counterRackA].Cells[1].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackA.Rows[counterRackA].Cells[1].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackA.Rows[counterRackA].Cells[1].Value = ValidSondeArryiMet1[i].getSerialNum();

                            //Check to make sure the TN starts with and "S"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getTrackingID(), "Tracking Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) == "S")
                                {
                                    dataGridViewRackA.Rows[counterRackA].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) == "E")
                                {
                                    dataGridViewRackA.Rows[counterRackA].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) != "S" && ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) != "E")
                                {
                                    dataGridViewRackA.Rows[counterRackA].Cells[2].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackA.Rows[counterRackA].Cells[2].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackA.Rows[counterRackA].Cells[2].Value = ValidSondeArryiMet1[i].getTrackingID();

                            //Check to make sure the PN starts with and "P"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getTUProbeID(), "Probe Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getTUProbeID().Substring(0, 1) == "P")
                                {
                                    dataGridViewRackA.Rows[counterRackA].Cells[3].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                }
                                else
                                {
                                    dataGridViewRackA.Rows[counterRackA].Cells[3].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackA.Rows[counterRackA].Cells[3].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackA.Rows[counterRackA].Cells[3].Value = ValidSondeArryiMet1[i].getTUProbeID();

                            dataGridViewRackA.Rows[counterRackA].Cells[4].Value = ValidSondeArryiMet1[i].ComPort.PortName;
                            dataGridViewRackA.Rows[counterRackA].Cells[5].Value = ValidSondeArryiMet1[i].getPressureTemp();
                            dataGridViewRackA.Rows[counterRackA].Cells[6].Value = ValidSondeArryiMet1[i].getPressure();

                            //Checking the pressure diff.
                            if (Math.Abs(currentPressure - Convert.ToDouble(ValidSondeArryiMet1[i].getPressure())) > ProgramSettings.AcceptablePressureDiff)
                            {
                                dataGridViewRackA.Rows[counterRackA].Cells[7].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackA.Rows[counterRackA].Cells[7].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - currentPressure);// Going to have to make this work
                            }
                            else
                            {
                                dataGridViewRackA.Rows[counterRackA].Cells[7].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackA.Rows[counterRackA].Cells[7].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - currentPressure);// Going to have to make this work
                            }

                            dataGridViewRackA.Rows[counterRackA].Cells[8].Value = ValidSondeArryiMet1[i].getAirTemp();

                            //Checking the temp diff
                            if (Math.Abs(currentTemp - Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp())) > ProgramSettings.AcceptableTempDiff)
                            {
                                dataGridViewRackA.Rows[counterRackA].Cells[9].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackA.Rows[counterRackA].Cells[9].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentTemp); //Temp diff here
                            }
                            else
                            {
                                dataGridViewRackA.Rows[counterRackA].Cells[9].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackA.Rows[counterRackA].Cells[9].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentTemp); //Temp diff here

                            }
                            dataGridViewRackA.Rows[counterRackA].Cells[10].Value = ValidSondeArryiMet1[i].getRH();

                            //Checking humidity diff
                            if (Math.Abs(currentHumidity - Convert.ToDouble(ValidSondeArryiMet1[i].getRH())) > ProgramSettings.AcceptableHumidityDiff)
                            {
                                dataGridViewRackA.Rows[counterRackA].Cells[11].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackA.Rows[counterRackA].Cells[11].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - currentHumidity); // U diff needed here.
                            }
                            else
                            {
                                dataGridViewRackA.Rows[counterRackA].Cells[11].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackA.Rows[counterRackA].Cells[11].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - currentHumidity); // U diff needed here.
                            }

                            //Firmware
                            dataGridViewRackA.Rows[counterRackA].Cells[12].Value = ValidSondeArryiMet1[i].getFirmwareVersion();

                            counterRackA++;
                        }

                        if (comNumber >= ProgramSettings.StartRackBRadiosondeComms && comNumber < ProgramSettings.EndRackBRadiosondeComms)
                        {
                            dataGridViewRackB.Rows[counterRackB].Cells[0].Value = (counterRackB + 1).ToString();
                            //Check to make sure the SN starts with and "S"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getSerialNum(), "Serial Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) == "S")
                                {
                                    dataGridViewRackB.Rows[counterRackB].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) == "E")
                                {
                                    dataGridViewRackB.Rows[counterRackB].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) != "S" && ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) != "E")
                                {
                                    dataGridViewRackB.Rows[counterRackB].Cells[1].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackB.Rows[counterRackB].Cells[1].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackB.Rows[counterRackB].Cells[1].Value = ValidSondeArryiMet1[i].getSerialNum();

                            //Check to make sure the TN starts with and "S"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getTrackingID(), "Tracking Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) == "S")
                                {
                                    dataGridViewRackB.Rows[counterRackB].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) == "E")
                                {
                                    dataGridViewRackB.Rows[counterRackB].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) != "S" && ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) != "E")
                                {
                                    dataGridViewRackB.Rows[counterRackB].Cells[2].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackB.Rows[counterRackB].Cells[2].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackB.Rows[counterRackB].Cells[2].Value = ValidSondeArryiMet1[i].getTrackingID();

                            //Check to make sure the PN starts with and "P"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getTUProbeID(), "Probe Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getTUProbeID().Substring(0, 1) == "P")
                                {
                                    dataGridViewRackB.Rows[counterRackB].Cells[3].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                }
                                else
                                {
                                    dataGridViewRackB.Rows[counterRackB].Cells[3].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackB.Rows[counterRackB].Cells[3].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackB.Rows[counterRackB].Cells[3].Value = ValidSondeArryiMet1[i].getTUProbeID();

                            dataGridViewRackB.Rows[counterRackB].Cells[4].Value = ValidSondeArryiMet1[i].ComPort.PortName;
                            dataGridViewRackB.Rows[counterRackB].Cells[5].Value = ValidSondeArryiMet1[i].getPressureTemp();
                            dataGridViewRackB.Rows[counterRackB].Cells[6].Value = ValidSondeArryiMet1[i].getPressure();

                            //Checking the pressure diff.
                            if (Math.Abs(currentPressure - Convert.ToDouble(ValidSondeArryiMet1[i].getPressure())) > ProgramSettings.AcceptablePressureDiff)
                            {
                                dataGridViewRackB.Rows[counterRackB].Cells[7].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackB.Rows[counterRackB].Cells[7].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - currentPressure);// Going to have to make this work
                            }
                            else
                            {
                                dataGridViewRackB.Rows[counterRackB].Cells[7].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackB.Rows[counterRackB].Cells[7].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - currentPressure);// Going to have to make this work
                            }

                            dataGridViewRackB.Rows[counterRackB].Cells[8].Value = ValidSondeArryiMet1[i].getAirTemp();

                            //Checking the temp diff
                            if (Math.Abs(currentTemp - Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp())) > ProgramSettings.AcceptableTempDiff)
                            {
                                dataGridViewRackB.Rows[counterRackB].Cells[9].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackB.Rows[counterRackB].Cells[9].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentTemp); //Temp diff here
                            }
                            else
                            {
                                dataGridViewRackB.Rows[counterRackB].Cells[9].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackB.Rows[counterRackB].Cells[9].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentTemp); //Temp diff here

                            }
                            dataGridViewRackB.Rows[counterRackB].Cells[10].Value = ValidSondeArryiMet1[i].getRH();

                            //Checking humidity diff
                            if (Math.Abs(currentHumidity - Convert.ToDouble(ValidSondeArryiMet1[i].getRH())) > ProgramSettings.AcceptableHumidityDiff)
                            {
                                dataGridViewRackB.Rows[counterRackB].Cells[11].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackB.Rows[counterRackB].Cells[11].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - currentHumidity); // U diff needed here.
                            }
                            else
                            {
                                dataGridViewRackB.Rows[counterRackB].Cells[11].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackB.Rows[counterRackB].Cells[11].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - currentHumidity); // U diff needed here.
                            }

                            //Firmware
                            dataGridViewRackB.Rows[counterRackB].Cells[12].Value = ValidSondeArryiMet1[i].getFirmwareVersion();

                            counterRackB++;
                        }

                        if (comNumber >= ProgramSettings.StartRackCRadiosondeComms && comNumber < ProgramSettings.EndRackCRadiosondeComms)
                        {
                            dataGridViewRackC.Rows[counterRackC].Cells[0].Value = (counterRackC + 1).ToString();
                            //Check to make sure the SN starts with and "S"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getSerialNum(), "Serial Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) == "S")
                                {
                                    dataGridViewRackC.Rows[counterRackC].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) == "E")
                                {
                                    dataGridViewRackC.Rows[counterRackC].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) != "S" && ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) != "E")
                                {
                                    dataGridViewRackC.Rows[counterRackC].Cells[1].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackC.Rows[counterRackC].Cells[1].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackC.Rows[counterRackC].Cells[1].Value = ValidSondeArryiMet1[i].getSerialNum();

                            //Check to make sure the TN starts with and "S"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getTrackingID(), "Tracking Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) == "S")
                                {
                                    dataGridViewRackC.Rows[counterRackC].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) == "E")
                                {
                                    dataGridViewRackC.Rows[counterRackC].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) != "S" && ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) != "E")
                                {
                                    dataGridViewRackC.Rows[counterRackC].Cells[2].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackC.Rows[counterRackC].Cells[2].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackC.Rows[counterRackC].Cells[2].Value = ValidSondeArryiMet1[i].getTrackingID();
                            //Check to make sure the PN starts with and "P"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getTUProbeID(), "Probe Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getTUProbeID().Substring(0, 1) == "P")
                                {
                                    dataGridViewRackC.Rows[counterRackC].Cells[3].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                }
                                else
                                {
                                    dataGridViewRackC.Rows[counterRackC].Cells[3].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackC.Rows[counterRackC].Cells[3].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackC.Rows[counterRackC].Cells[3].Value = ValidSondeArryiMet1[i].getTUProbeID();

                            dataGridViewRackC.Rows[counterRackC].Cells[4].Value = ValidSondeArryiMet1[i].ComPort.PortName;
                            dataGridViewRackC.Rows[counterRackC].Cells[5].Value = ValidSondeArryiMet1[i].getPressureTemp();
                            dataGridViewRackC.Rows[counterRackC].Cells[6].Value = ValidSondeArryiMet1[i].getPressure();

                            //Checking the pressure diff.
                            if (Math.Abs(currentPressure - Convert.ToDouble(ValidSondeArryiMet1[i].getPressure())) > ProgramSettings.AcceptablePressureDiff)
                            {
                                dataGridViewRackC.Rows[counterRackC].Cells[7].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackC.Rows[counterRackC].Cells[7].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - currentPressure);// Going to have to make this work
                            }
                            else
                            {
                                dataGridViewRackC.Rows[counterRackC].Cells[7].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackC.Rows[counterRackC].Cells[7].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - currentPressure);// Going to have to make this work
                            }

                            dataGridViewRackC.Rows[counterRackC].Cells[8].Value = ValidSondeArryiMet1[i].getAirTemp();

                            //Checking the temp diff
                            if (Math.Abs(currentTemp - Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp())) > ProgramSettings.AcceptableTempDiff)
                            {
                                dataGridViewRackC.Rows[counterRackC].Cells[9].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackC.Rows[counterRackC].Cells[9].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentTemp); //Temp diff here
                            }
                            else
                            {
                                dataGridViewRackC.Rows[counterRackC].Cells[9].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackC.Rows[counterRackC].Cells[9].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentTemp); //Temp diff here

                            }
                            dataGridViewRackC.Rows[counterRackC].Cells[10].Value = ValidSondeArryiMet1[i].getRH();

                            //Checking humidity diff
                            if (Math.Abs(currentHumidity - Convert.ToDouble(ValidSondeArryiMet1[i].getRH())) > ProgramSettings.AcceptableHumidityDiff)
                            {
                                dataGridViewRackC.Rows[counterRackC].Cells[11].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackC.Rows[counterRackC].Cells[11].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - currentHumidity); // U diff needed here.
                            }
                            else
                            {
                                dataGridViewRackC.Rows[counterRackC].Cells[11].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackC.Rows[counterRackC].Cells[11].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - currentHumidity); // U diff needed here.
                            }

                            //Firmware
                            dataGridViewRackC.Rows[counterRackC].Cells[12].Value = ValidSondeArryiMet1[i].getFirmwareVersion();

                            counterRackC++;
                        }

                        if (comNumber >= ProgramSettings.StartRackDRadiosondeComms && comNumber < ProgramSettings.EndRackDRadiosondeComms)
                        {
                            dataGridViewRackD.Rows[counterRackD].Cells[0].Value = (counterRackD + 1).ToString();
                            //Check to make sure the SN starts with and "S"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getSerialNum(), "Serial Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) == "S")
                                {
                                    dataGridViewRackD.Rows[counterRackD].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) == "E")
                                {
                                    dataGridViewRackD.Rows[counterRackD].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) != "S" && ValidSondeArryiMet1[i].getSerialNum().Substring(0, 1) != "E")
                                {
                                    dataGridViewRackD.Rows[counterRackD].Cells[1].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackD.Rows[counterRackD].Cells[1].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackD.Rows[counterRackD].Cells[1].Value = ValidSondeArryiMet1[i].getSerialNum();

                            //Check to make sure the TN starts with and "S"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getTrackingID(), "Tracking Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) == "S")
                                {
                                    dataGridViewRackD.Rows[counterRackD].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) == "E")
                                {
                                    dataGridViewRackD.Rows[counterRackD].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) != "S" && ValidSondeArryiMet1[i].getTrackingID().Substring(0, 1) != "E")
                                {
                                    dataGridViewRackD.Rows[counterRackD].Cells[2].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackD.Rows[counterRackD].Cells[2].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackD.Rows[counterRackD].Cells[2].Value = ValidSondeArryiMet1[i].getTrackingID();
                            //Check to make sure the PN starts with and "P"
                            if (checkForRepeat(ValidSondeArryiMet1[i].getTUProbeID(), "Probe Number") == false)
                            {
                                if (ValidSondeArryiMet1[i].getTUProbeID().Substring(0, 1) == "P")
                                {
                                    dataGridViewRackD.Rows[counterRackD].Cells[3].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                }
                                else
                                {
                                    dataGridViewRackD.Rows[counterRackD].Cells[3].Style.BackColor = ProgramSettings.colorFail;
                                }
                            }
                            else
                            {
                                dataGridViewRackD.Rows[counterRackD].Cells[3].Style.BackColor = repeatSondeElements;
                            }
                            dataGridViewRackD.Rows[counterRackD].Cells[3].Value = ValidSondeArryiMet1[i].getTUProbeID();

                            dataGridViewRackD.Rows[counterRackD].Cells[4].Value = ValidSondeArryiMet1[i].ComPort.PortName;
                            dataGridViewRackD.Rows[counterRackD].Cells[5].Value = ValidSondeArryiMet1[i].getPressureTemp();
                            dataGridViewRackD.Rows[counterRackD].Cells[6].Value = ValidSondeArryiMet1[i].getPressure();

                            //Checking the pressure diff.
                            if (Math.Abs(currentPressure - Convert.ToDouble(ValidSondeArryiMet1[i].getPressure())) > ProgramSettings.AcceptablePressureDiff)
                            {
                                dataGridViewRackD.Rows[counterRackD].Cells[7].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackD.Rows[counterRackD].Cells[7].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - currentPressure);// Going to have to make this work
                            }
                            else
                            {
                                dataGridViewRackD.Rows[counterRackD].Cells[7].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackD.Rows[counterRackD].Cells[7].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getPressure()) - currentPressure);// Going to have to make this work
                            }

                            dataGridViewRackD.Rows[counterRackD].Cells[8].Value = ValidSondeArryiMet1[i].getAirTemp();

                            //Checking the temp diff
                            if (Math.Abs(currentTemp - Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp())) > ProgramSettings.AcceptableTempDiff)
                            {
                                dataGridViewRackD.Rows[counterRackD].Cells[9].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackD.Rows[counterRackD].Cells[9].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentTemp); //Temp diff here
                            }
                            else
                            {
                                dataGridViewRackD.Rows[counterRackD].Cells[9].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackD.Rows[counterRackD].Cells[9].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getAirTemp()) - currentTemp); //Temp diff here

                            }
                            dataGridViewRackD.Rows[counterRackD].Cells[10].Value = ValidSondeArryiMet1[i].getRH();

                            //Checking humidity diff
                            if (Math.Abs(currentHumidity - Convert.ToDouble(ValidSondeArryiMet1[i].getRH())) > ProgramSettings.AcceptableHumidityDiff)
                            {
                                dataGridViewRackD.Rows[counterRackD].Cells[11].Style.BackColor = ProgramSettings.colorFail;
                                dataGridViewRackD.Rows[counterRackD].Cells[11].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - currentHumidity); // U diff needed here.
                            }
                            else
                            {
                                dataGridViewRackD.Rows[counterRackD].Cells[11].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                dataGridViewRackD.Rows[counterRackD].Cells[11].Value = String.Format("{0:0.000}", Convert.ToDouble(ValidSondeArryiMet1[i].getRH()) - currentHumidity); // U diff needed here.
                            }
                            //Firmware
                            dataGridViewRackD.Rows[counterRackD].Cells[12].Value = ValidSondeArryiMet1[i].getFirmwareVersion();

                            counterRackD++;
                        }



                        //Programming window updated if it is not null
                        if (programmingDisplay != null)
                        {
                            updateProgrammingWindow();
                        }
                    }
                    break;
                    #endregion

                case "iMet Un":
                    #region Updating display for iMet-1 Univercal cal mode.
                    //Counters for dataGide position.
                    int[] rackPosCounter = new int[4] {-1,-1,-1,-1};

                    //Data View Grids
                    System.Windows.Forms.DataGridView[] dataGrids = new DataGridView[4];
                    dataGrids[0] = dataGridViewRackA;
                    dataGrids[1] = dataGridViewRackB;
                    dataGrids[2] = dataGridViewRackC;
                    dataGrids[3] = dataGridViewRackD;

                    if (ValidSondeiMet1U == null)
                    {
                        return;
                    }

                    //foreach (iMet1U sonde in ValidSondeiMet1U)
                    for(int sonde = 0; sonde<ValidSondeiMet1U.Length; sonde++)
                    {

                        

                        int tempComNumber = 0;
                        int rackToBeUpdated = 0;
                        for (int testingName = 0; testingName < 2; testingName++)
                        {
                            if (ValidSondeiMet1U[sonde].getPortName().Length == (4 + testingName))
                            { tempComNumber = Convert.ToInt16(ValidSondeiMet1U[sonde].getPortName().Substring(3, 1 + testingName)); }
                        }

                        //Determining the rack that will be updated.
                        if (tempComNumber >= ProgramSettings.StartRackARadiosondeComms && tempComNumber < ProgramSettings.EndRackARadiosondeComms)
                        { rackToBeUpdated = 0; }
                        if (tempComNumber >= ProgramSettings.StartRackBRadiosondeComms && tempComNumber < ProgramSettings.EndRackBRadiosondeComms)
                        { rackToBeUpdated = 1; }
                        if (tempComNumber >= ProgramSettings.StartRackCRadiosondeComms && tempComNumber < ProgramSettings.EndRackCRadiosondeComms)
                        { rackToBeUpdated = 2; }
                        if (tempComNumber >= ProgramSettings.StartRackDRadiosondeComms && tempComNumber < ProgramSettings.EndRackDRadiosondeComms)
                        { rackToBeUpdated = 3; }

                        //Counter to keep track of what row in the rack to upgrade
                        rackPosCounter[rackToBeUpdated]++;

                        //Loading data into the gridview
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[0].Value = (rackPosCounter[rackToBeUpdated] + 1).ToString();
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Value = ValidSondeiMet1U[sonde].CalData.SondeID;
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[2].Value = ValidSondeiMet1U[sonde].CalData.SerialNumber;
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[3].Value = ValidSondeiMet1U[sonde].CalData.ProbeID;
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[4].Value = ValidSondeiMet1U[sonde].getPortName();
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[5].Value = ValidSondeiMet1U[sonde].CalData.PressureTemperature.ToString();
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[6].Value = ValidSondeiMet1U[sonde].CalData.Pressure.ToString();


                        //Displaying differances Also AT and Humidity
                        //Pressure Differance
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[7].Value = (ValidSondeiMet1U[sonde].CalData.Pressure - currentPressure).ToString("0.000");

                        //AT
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[8].Value = ValidSondeiMet1U[sonde].CalData.Temperature.ToString();
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[9].Value = (ValidSondeiMet1U[sonde].CalData.Temperature - currentTemp).ToString("0.000");

                        //Humidity
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[10].Value = ValidSondeiMet1U[sonde].CalData.Humidity.ToString();
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[11].Value = (ValidSondeiMet1U[sonde].CalData.Humidity - currentHumidity).ToString("0.000");

                        //Firmware
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[12].Value = ValidSondeiMet1U[sonde].CalData.FirmwareVersion;

                        //TXmode
                        string[] txDataElement = ValidSondeiMet1U[sonde].StatData.TXMode.Split('=');
                        dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[13].Value = txDataElement[1];

                        //Pass/Fail Coloring of GrideView
                        //Checking sonde id

                        if (checkForRepeat(ValidSondeiMet1U[sonde].CalData.SondeID, "Serial Number") == false)
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = Color.White;

                            if (ValidSondeiMet1U[sonde].CalData.SondeID != null)
                            {
                                if (ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 1).ToUpper() == "S")
                                {
                                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 1).ToUpper() == "E")
                                {
                                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }

                                /*if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                                {
                                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                 */
                            }
                        }
                        else
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = repeatSondeElements;
                        }

                        //Checking the tracking number
                        if (checkForRepeat(ValidSondeiMet1U[sonde].CalData.SerialNumber, "Tracking Number") == false)
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[2].Style.BackColor = Color.White;

                            if (ValidSondeiMet1U[sonde].CalData.SondeID != null)
                            {
                                if (ValidSondeiMet1U[sonde].CalData.SerialNumber.Substring(0, 1).ToUpper() == "S")
                                {
                                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeiMet1U[sonde].CalData.SerialNumber.Substring(0, 1).ToUpper() == "E")
                                {
                                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeiMet1U[sonde].CalData.SerialNumber.Substring(0, 1).ToUpper() == "U")
                                {
                                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[2].Style.BackColor = System.Drawing.Color.LightBlue;
                                }
                                /*
                                if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                                {
                                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                 */
                            }
                        }
                        else
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[2].Style.BackColor = repeatSondeElements;
                        }

                        //Checking to make sure the PN starts with a "P"
                        if (checkForRepeat(ValidSondeiMet1U[sonde].CalData.ProbeID, "Probe Number") == false)
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[3].Style.BackColor = Color.White;

                            if (ValidSondeiMet1U[sonde].CalData.SondeID != null)
                            {

                                if (ValidSondeiMet1U[sonde].CalData.ProbeID.Substring(0, 1).ToUpper() == "P" || Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.ProbeID.Substring(0, 1)) > 0 )
                                {
                                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[3].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                }
                                else
                                {
                                    dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[3].Style.BackColor = ProgramSettings.colorFail;

                                }
                            }
                        }
                        else
                        {
                            dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[3].Style.BackColor = repeatSondeElements;
                        }

                        //Pressure Differance
                        if (Math.Abs(currentPressure - ValidSondeiMet1U[sonde].CalData.Pressure) > ProgramSettings.AcceptablePressureDiff)
                        { dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[7].Style.BackColor = ProgramSettings.colorFail; }
                        else
                        { dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[7].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

                        //Temp Differance
                        if (Math.Abs(currentTemp - ValidSondeiMet1U[sonde].CalData.Temperature) > ProgramSettings.AcceptableTempDiff)
                        { dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[9].Style.BackColor = ProgramSettings.colorFail; }
                        else
                        { dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[9].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

                        //Humidity Differance
                        if (Math.Abs(currentHumidity - ValidSondeiMet1U[sonde].CalData.Humidity) > ProgramSettings.AcceptableHumidityDiff)
                        { dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[11].Style.BackColor = ProgramSettings.colorFail; }
                        else
                        { dataGrids[rackToBeUpdated].Rows[rackPosCounter[rackToBeUpdated]].Cells[11].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }
                    }
                   
                    break;
                    #endregion

                case "iMet 2.":
                    #region Updating display for old iMet-1 Un code.
                    //Counters for dataGide position.
                    int[] rackPosCounterOld = new int[4] { -1, -1, -1, -1 };

                    //Data View Grids
                    System.Windows.Forms.DataGridView[] dataGridsOld = new DataGridView[4];
                    dataGridsOld[0] = dataGridViewRackA;
                    dataGridsOld[1] = dataGridViewRackB;
                    dataGridsOld[2] = dataGridViewRackC;
                    dataGridsOld[3] = dataGridViewRackD;

                    if (ValidSondeiMet1U == null)
                    {
                        return;
                    }

                    //foreach (iMet1U sonde in ValidSondeiMet1U)
                    for (int sonde = 0; sonde < ValidSondeiMet1U.Length; sonde++)
                    {



                        int tempComNumber = 0;
                        int rackToBeUpdated = 0;
                        for (int testingName = 0; testingName < 2; testingName++)
                        {
                            if (ValidSondeiMet1U[sonde].getPortName().Length == (4 + testingName))
                            { tempComNumber = Convert.ToInt16(ValidSondeiMet1U[sonde].getPortName().Substring(3, 1 + testingName)); }
                        }

                        //Determining the rack that will be updated.
                        if (tempComNumber >= ProgramSettings.StartRackARadiosondeComms && tempComNumber < ProgramSettings.EndRackARadiosondeComms)
                        { rackToBeUpdated = 0; }
                        if (tempComNumber >= ProgramSettings.StartRackBRadiosondeComms && tempComNumber < ProgramSettings.EndRackBRadiosondeComms)
                        { rackToBeUpdated = 1; }
                        if (tempComNumber >= ProgramSettings.StartRackCRadiosondeComms && tempComNumber < ProgramSettings.EndRackCRadiosondeComms)
                        { rackToBeUpdated = 2; }
                        if (tempComNumber >= ProgramSettings.StartRackDRadiosondeComms && tempComNumber < ProgramSettings.EndRackDRadiosondeComms)
                        { rackToBeUpdated = 3; }

                        //Counter to keep track of what row in the rack to upgrade
                        rackPosCounterOld[rackToBeUpdated]++;

                        //Loading data into the gridview
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[0].Value = (rackPosCounterOld[rackToBeUpdated] + 1).ToString();
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[1].Value = ValidSondeiMet1U[sonde].CalData.SondeID;
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[2].Value = ValidSondeiMet1U[sonde].CalData.SerialNumber;
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[3].Value = ValidSondeiMet1U[sonde].CalData.ProbeID;
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[4].Value = ValidSondeiMet1U[sonde].getPortName();
                        //dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[5].Value = ValidSondeiMet1U[sonde].CalData.PressureTemperature.ToString();
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[6].Value = ValidSondeiMet1U[sonde].PTUData.Pressure;


                        //Displaying differances Also AT and Humidity
                        //Pressure Differance
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[7].Value = (ValidSondeiMet1U[sonde].PTUData.Pressure - currentPressure).ToString("0.000");

                        //AT
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[8].Value = ValidSondeiMet1U[sonde].PTUData.AirTemperature.ToString();
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[9].Value = (ValidSondeiMet1U[sonde].PTUData.AirTemperature - currentTemp).ToString("0.000");

                        //Humidity
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[10].Value = ValidSondeiMet1U[sonde].PTUData.RelativeHumidity.ToString();
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[11].Value = (ValidSondeiMet1U[sonde].PTUData.RelativeHumidity - currentHumidity).ToString("0.000");

                        //Firmware
                        dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[12].Value = ValidSondeiMet1U[sonde].CalData.FirmwareVersion;

                        //TXmode
                        //string[] txDataElement = ValidSondeiMet1U[sonde].StatData.TXMode.Split('=');
                        //dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[13].Value = txDataElement[1];

                        //Pass/Fail Coloring of GrideView
                        //Checking sonde id

                        /*

                        if (checkForRepeat(ValidSondeiMet1U[sonde].CalData.SondeID, "Serial Number") == false)
                        {
                            dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[1].Style.BackColor = Color.White;

                            if (ValidSondeiMet1U[sonde].CalData.SondeID != null)
                            {
                                if (ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 1).ToUpper() == "S")
                                {
                                    dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 1).ToUpper() == "E")
                                {
                                    dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }

                                /*if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                                {
                                    dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                 */
                        /*
                            }
                        }
                        else
                        {
                            dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[1].Style.BackColor = repeatSondeElements;
                        }

                        //Checking the tracking number
                        if (checkForRepeat(ValidSondeiMet1U[sonde].CalData.SerialNumber, "Tracking Number") == false)
                        {
                            dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[2].Style.BackColor = Color.White;

                            if (ValidSondeiMet1U[sonde].CalData.SondeID != null)
                            {
                                if (ValidSondeiMet1U[sonde].CalData.SerialNumber.Substring(0, 1).ToUpper() == "S")
                                {
                                    dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                if (ValidSondeiMet1U[sonde].CalData.SerialNumber.Substring(0, 1).ToUpper() == "E")
                                {
                                    dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[2].Style.BackColor = ProgramSettings.colorRadiosondeEnSci;
                                }
                                if (ValidSondeiMet1U[sonde].CalData.SerialNumber.Substring(0, 1).ToUpper() == "U")
                                {
                                    dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[2].Style.BackColor = System.Drawing.Color.LightBlue;
                                }
                                /*
                                if (Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) >= 0 && Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.SondeID.Substring(0, 2)) <= 99)
                                {
                                    dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[1].Style.BackColor = ProgramSettings.colorRadiosondeiMet1;
                                }
                                 */
                        /*
                            }
                        }
                        else
                        {
                            dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[2].Style.BackColor = repeatSondeElements;
                        }

                        //Checking to make sure the PN starts with a "P"
                        if (checkForRepeat(ValidSondeiMet1U[sonde].CalData.ProbeID, "Probe Number") == false)
                        {
                            dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[3].Style.BackColor = Color.White;

                            if (ValidSondeiMet1U[sonde].CalData.SondeID != null)
                            {

                                if (ValidSondeiMet1U[sonde].CalData.ProbeID.Substring(0, 1).ToUpper() == "P" || Convert.ToInt16(ValidSondeiMet1U[sonde].CalData.ProbeID.Substring(0, 1)) > 0)
                                {
                                    dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[3].Style.BackColor = ProgramSettings.colorRadiosondeElementPass;
                                }
                                else
                                {
                                    dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[3].Style.BackColor = ProgramSettings.colorFail;

                                }
                            }
                        }
                        else
                        {
                            dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[3].Style.BackColor = repeatSondeElements;
                        }
                         */

                        //Pressure Differance
                        if (Math.Abs(currentPressure - ValidSondeiMet1U[sonde].CalData.Pressure) > ProgramSettings.AcceptablePressureDiff)
                        { dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[7].Style.BackColor = ProgramSettings.colorFail; }
                        else
                        { dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[7].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

                        //Temp Differance
                        if (Math.Abs(currentTemp - ValidSondeiMet1U[sonde].CalData.Temperature) > ProgramSettings.AcceptableTempDiff)
                        { dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[9].Style.BackColor = ProgramSettings.colorFail; }
                        else
                        { dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[9].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }

                        //Humidity Differance
                        if (Math.Abs(currentHumidity - ValidSondeiMet1U[sonde].CalData.Humidity) > ProgramSettings.AcceptableHumidityDiff)
                        { dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[11].Style.BackColor = ProgramSettings.colorFail; }
                        else
                        { dataGridsOld[rackToBeUpdated].Rows[rackPosCounterOld[rackToBeUpdated]].Cells[11].Style.BackColor = ProgramSettings.colorRadiosondeElementPass; }
                    }
                        
                    break;
                    #endregion
            }
        }

        private void updateProgrammingWindow()
        {
            try
            {
                programmingDisplay.ValidSondeArryiMet1 = ValidSondeArryiMet1;
                programmingDisplay.updateToolStripStatus();
                programmingDisplay.updateInterface();
                
            }
            catch (Exception updateError)
            {
                UpdateMainStatusLabel(updateError.Message);
            }
        }
        public void writeToErrorLog(string Message)
        {
            // create a writer and open the file
            TextWriter errorLogging = File.AppendText(ProgramSettings.logErrorLocation);

            // write a line of text to the file
            errorLogging.WriteLine(DateTime.Now.ToString("HH:mm:ss.ff") + "," + ProgramSettings.CurrentUser + "," +Message);

            // close the stream
            errorLogging.Close();
        }

        #endregion

        #region Updating/Loading Serial Number, Tracking Number, and Probe ID
        private void dataGridViewRackA_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (RadiosondeStatus.activeRadiosondeData == true)
            {
                //If double clicked in the A Rack.
                decidingWhatRackToLoad(1, dataGridViewRackA.CurrentCell.ColumnIndex, dataGridViewRackA.CurrentCell.RowIndex, dataGridViewRackA.Rows[dataGridViewRackA.CurrentCell.RowIndex].Cells[4].Value.ToString());
            }
            else
            {
                MessageBox.Show("No current active radiosonde. Please search and try again.", "No Active Radiosonde", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                 
        }
        private void dataGridViewRackB_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (RadiosondeStatus.activeRadiosondeData == true)
            {
                //If double clicked in the B Rack.
                decidingWhatRackToLoad(2, dataGridViewRackB.CurrentCell.ColumnIndex, dataGridViewRackB.CurrentCell.RowIndex, dataGridViewRackB.Rows[dataGridViewRackB.CurrentCell.RowIndex].Cells[4].Value.ToString());
            }
            else
            {
                MessageBox.Show("No current active radiosonde. Please search and try again.", "No Active Radiosonde", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridViewRackC_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (RadiosondeStatus.activeRadiosondeData == true)
            {
                //If double clicked in the C Rack.
                decidingWhatRackToLoad(3, dataGridViewRackC.CurrentCell.ColumnIndex, dataGridViewRackC.CurrentCell.RowIndex, dataGridViewRackC.Rows[dataGridViewRackC.CurrentCell.RowIndex].Cells[4].Value.ToString());
            }
            else
            {
                MessageBox.Show("No current active radiosonde. Please search and try again.", "No Active Radiosonde", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridViewRackD_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (RadiosondeStatus.activeRadiosondeData == true)
            {
                //If double clicked in the D Rack.
                decidingWhatRackToLoad(4, dataGridViewRackD.CurrentCell.ColumnIndex, dataGridViewRackD.CurrentCell.RowIndex, dataGridViewRackD.Rows[dataGridViewRackD.CurrentCell.RowIndex].Cells[4].Value.ToString());
            }
            else
            {
                MessageBox.Show("No current active radiosonde. Please search and try again.", "No Active Radiosonde", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void decidingWhatRackToLoad(int Rack, int columnIndex, int rowIndex, string comPort)
        {
            if (RadiosondeStatus.activeRadiosondeData == true)
            {

                //If double clicked in the A Rack.
                if (Rack == 1)
                {
                    if (dataGridViewRackA.CurrentCell != null)
                    {
                        if (columnIndex == 1)
                        {
                            loadRadiosondeSerialNumber(1, dataGridViewRackA.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }

                        if (columnIndex == 2)
                        {
                            loadRadiosondeTrackingNumber(1, dataGridViewRackA.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }

                        if (columnIndex == 3)
                        {
                            loadRadiosondeProbeNumber(1, dataGridViewRackA.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }
                    }
                }
                //If double clicked in the B Rack.
                if (Rack == 2)
                {
                    if (dataGridViewRackB.CurrentCell != null)
                    {
                        if (columnIndex == 1)
                        {
                            loadRadiosondeSerialNumber(2, dataGridViewRackB.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }

                        if (columnIndex == 2)
                        {
                            loadRadiosondeTrackingNumber(2, dataGridViewRackB.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }

                        if (columnIndex == 3)
                        {
                            loadRadiosondeProbeNumber(2, dataGridViewRackB.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }
                    }
                }
                //If double clicked in the C Rack.
                if (Rack == 3)
                {
                    if (dataGridViewRackC.CurrentCell != null)
                    {
                        if (columnIndex == 1)
                        {
                            loadRadiosondeSerialNumber(3, dataGridViewRackC.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }

                        if (columnIndex == 2)
                        {
                            loadRadiosondeTrackingNumber(3, dataGridViewRackC.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }

                        if (columnIndex == 3)
                        {
                            loadRadiosondeProbeNumber(3, dataGridViewRackC.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }
                    }
                }
                //If double clicked in the D Rack.
                if (Rack == 4)
                {
                    if (dataGridViewRackD.CurrentCell != null)
                    {
                        if (columnIndex == 1)
                        {
                            loadRadiosondeSerialNumber(4, dataGridViewRackD.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }

                        if (columnIndex == 2)
                        {
                            loadRadiosondeTrackingNumber(4, dataGridViewRackD.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }

                        if (columnIndex == 3)
                        {
                            loadRadiosondeProbeNumber(4, dataGridViewRackD.Rows[rowIndex].Cells[4].Value.ToString());
                            return;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No current active radiosonde. Please search and try again.", "No Active Radiosonde", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void loadRadiosondeSerialNumber(int Rack, string comPort)
        {
            string value = "";
            string serialNumberFirstLetter = "";
            int serialNumberNumberPart = 0;

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":

                    if (InputBox("Serial Number", "Serial Number:", ref value) == DialogResult.OK)
                    {
                        try
                        {
                            serialNumberFirstLetter = value.Substring(0, 1).ToUpper();
                            serialNumberNumberPart = Convert.ToInt32(value.Substring(1, (value.Length - 1)));
                        }
                        catch
                        {
                            writeToErrorLog("Error with the serial number input by the user");
                            MessageBox.Show("Serial Number input is not formatted correctly.\nInput must be like: S123456", "Serial Number Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        //Loading the serial number into the radiosonde.
                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (ValidSondeArryiMet1[i].ComPort.PortName.ToString() == comPort)
                            {
                                ValidSondeArryiMet1[i].setSerialNum(serialNumberFirstLetter + serialNumberNumberPart.ToString());
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                    break;

                case "iMet Un":
                    if (InputBox("Serial Number", "Serial Number:", ref value) == DialogResult.OK)
                    {
                        //Loading the serial number into the radiosonde.
                        for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                        {
                            if (ValidSondeiMet1U[i].getPortName() == comPort)
                            {
                                ValidSondeiMet1U[i].setSondeID(value);
                            }
                        }
                    }
                    break;

            }
        }

        private void loadRadiosondeTrackingNumber(int Rack, string comPort)
        {
            string value = "";
            string trackingNumberFirstLetter = "";
            int trackingNumberNumberPart = 0;

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":
                    if (InputBox("Tracking Number", "Tracking Number:", ref value) == DialogResult.OK)
                    {
                        try
                        {
                            trackingNumberFirstLetter = value.Substring(0, 1).ToUpper();
                            trackingNumberNumberPart = Convert.ToInt32(value.Substring(1, (value.Length - 1)));
                        }
                        catch
                        {
                            writeToErrorLog("Error with the tracking number input by the user");
                            MessageBox.Show("Tracking Number input is not formatted correctly.\nInput must be like: S123456", "Tracking Number Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        //Loading the tracking number into the radiosonde.
                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (ValidSondeArryiMet1[i].ComPort.PortName.ToString() == comPort)
                            {
                                ValidSondeArryiMet1[i].setTrackingID(trackingNumberFirstLetter + trackingNumberNumberPart.ToString());
                                Thread.Sleep(1200);
                                ValidSondeArryiMet1[i].setSerialNum(trackingNumberFirstLetter + trackingNumberNumberPart.ToString());
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                    break;
                case "iMet Un":
                    if (InputBox("Tracking Number", "Tracking Number:", ref value) == DialogResult.OK)
                    {
                        //Loading the serial number into the radiosonde.
                        for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                        {
                            if (ValidSondeiMet1U[i].getPortName() == comPort)
                            {
                                ValidSondeiMet1U[i].setSerialNumber(value);
                                //ValidSondeiMet1U[i].setSondeID(value);
                            }
                        }
                    }
                    break;
            }
        }

        private void loadRadiosondeProbeNumber(int Rack, string comPort)
        {
            string value = "";
            string probeNumberFirstLetter = "";
            int probeNumberNumberPart = 0;

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":
                    if (InputBox("Probe Number", "Probe Number:", ref value) == DialogResult.OK)
                    {
                        try
                        {
                            probeNumberFirstLetter = value.Substring(0, 1).ToUpper();
                            probeNumberNumberPart = Convert.ToInt32(value.Substring(1, (value.Length - 1)));
                        }
                        catch
                        {
                            writeToErrorLog("Error with the probe number input by the user");
                            MessageBox.Show("Probe Number input is not formatted correctly.\nInput must be like: P123456", "Probe Number Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        //Loading the serial number into the radiosonde.
                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (ValidSondeArryiMet1[i].ComPort.PortName.ToString() == comPort)
                            {
                                ValidSondeArryiMet1[i].setTUProbeID(probeNumberFirstLetter + probeNumberNumberPart.ToString());
                            }
                        }
                    }
                    else
                    {
                        return;
                    }
                    break;
                case "iMet Un":
                    if (InputBox("Probe Number", "Probe Number:", ref value) == DialogResult.OK)
                    {
                        object[] radiosondeLoadData = new object[2];
                        radiosondeLoadData[0] = (object)comPort;
                        radiosondeLoadData[1] = (object)value;
                        backgroundLoadID.RunWorkerAsync(radiosondeLoadData);
                    }
                    break;
            }
        }

        void backgroundLoadID_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] sondeData = (object[])e.Argument;
            //Sending ID
            foreach (Universal_Radiosonde.iMet1U sonde in ValidSondeiMet1U)
            {
                if (sonde.getPortName() == (string)sondeData[0])
                {
                    UpdateMainStatusLabel("Sending Probe ID");
                    sonde.setProbeID((string)sondeData[1]);
                    UpdateMainStatusLabel("Sending Serial Number");
                    sonde.setSerialNumber((string)sondeData[1]);
                    UpdateMainStatusLabel("Sending Sonde ID");
                    sonde.setSondeID((string)sondeData[1]);
                }
            }
        }

        #endregion
        
        #region Loading coefficents

        #region Supprot methods for loading coefficents.

        public int howManyBackgroundWorkers(int amountOfRadiosonde)
        {
            int amountOfBackgroundWorkers = 0;

            //Squareing the amount of radiosonde and then converting to the nearest whole number
            int sqareRootofRadiosondeAmount = Convert.ToInt16(Math.Sqrt(Convert.ToDouble(amountOfRadiosonde)));


            if (amountOfRadiosonde > ProgramSettings.maxNumberOfBackgroundWorkers)
            {
                for (int i = ProgramSettings.maxNumberOfBackgroundWorkers; i > 0; i--)
                {
                    if(sqareRootofRadiosondeAmount%i == 0)
                    {
                        amountOfBackgroundWorkers = i+1;
                        break;
                    }
                }
                
            }
            else
            {
                amountOfBackgroundWorkers = amountOfRadiosonde;
            }

            /*
            if (amountOfRadiosonde >= ProgramSettings.maxNumberOfBackgroundWorkers)
            {
                amountOfBackgroundWorkers = ProgramSettings.maxNumberOfBackgroundWorkers;
            }
            else
            {
                amountOfBackgroundWorkers = amountOfRadiosonde;
            }
             */
 
            return amountOfBackgroundWorkers;
        }

        #endregion

        #region Controls for loading coefficents

        private void buttonLoadSingleCoefficentPressure_Click(object sender, EventArgs e)
        {
            string value = "";
            string trackingNumberFirstLetter = "";
            int trackingNumberNumberPart = 0;

            if (InputBox("Tracking Number", "Tracking Number:", ref value) == DialogResult.OK)
            {
                try
                {
                    switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
                    {
                        case "iMet-TX":
                            trackingNumberFirstLetter = value.Substring(0, 1).ToUpper();
                            break;
                        case "iMet Un":
                            trackingNumberFirstLetter = value.Substring(0, 1);
                            break;
                    }
                    trackingNumberNumberPart = Convert.ToInt32(value.Substring(1, (value.Length - 1)));
                }
                catch
                {
                    writeToErrorLog("Error finding " + trackingNumberFirstLetter + trackingNumberNumberPart.ToString() + " while trying to upload Pressure coefficents.");
                    MessageBox.Show("Tracking Number input is not formatted correctly.\nInput must be like: S123456", "Tracking Number Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //Serial Number(s)
                string[] serialNumberArray = new string[1];
                serialNumberArray[0] = trackingNumberFirstLetter + value.Substring(1, value.Length - 1);

                //Checking to make sure input tracking number exsist.
                Boolean exsistingTrackingNumber = false;

                //Checking to make sure the coefficent files exsist.
                string fileCheckReport = "";

                switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
                {
                    case "iMet-TX":
                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (serialNumberArray[0] == ValidSondeArryiMet1[i].getTrackingID())
                            {
                                if (File.Exists(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".p_cal") != true)
                                {
                                    fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                                }
                                exsistingTrackingNumber = true;
                            }
                        }
                        break;
                    case "iMet Un":
                        for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                        {
                            if (serialNumberArray[0] == ValidSondeiMet1U[i].CalData.SerialNumber)
                            {
                                if (File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeiMet1U[i].CalData.SerialNumber + ".p_cal") != true)
                                {
                                    fileCheckReport = fileCheckReport + ValidSondeiMet1U[i].CalData.SerialNumber + ",";
                                }
                                exsistingTrackingNumber = true;
                            }
                        }
                        break;
                }
                if (!exsistingTrackingNumber)
                {
                    MessageBox.Show("Unable to find input Tracking Number: " + serialNumberArray[0], "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }

                if (fileCheckReport != "")
                {
                    DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (testReport == DialogResult.No)
                    {
                        return;
                    }
                }

                //Coefficent type.
                int coefficentLoadType = 0;

                //Changing the color of the button to green.
                buttonLoadSingleCoefficentPressure.BackColor = System.Drawing.Color.Green;

                //Checking for repeat sonde sn's
                bool repeatSerialNumber = checkForRepeatSerialNumbers();

                if (repeatSerialNumber == true)
                {
                    UpdateMainStatusLabel("Coefficent Upload Stopped");
                    return;
                }

                //Disabling the coefficent load buttons
                disableCofficentButtons();

                //Stopping data collection.
                RadiosondeStatus.processRadiosondeHex = false;

                //Setting up the Background worker array
                int amountOfBackgroundWorkers = 1;
                setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


                //Starting the background worker array.
                startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);
            }
        }

        private void buttonLoadSingleCoefficentAirTemp_Click(object sender, EventArgs e)
        {
            string value = "";
            string trackingNumberFirstLetter = "";
            int trackingNumberNumberPart = 0;

            if (InputBox("Tracking Number", "Tracking Number:", ref value) == DialogResult.OK)
            {
                try
                {
                    switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
                    {
                        case "iMet-TX":
                            trackingNumberFirstLetter = value.Substring(0, 1).ToUpper();
                            break;
                        case "iMet Un":
                            trackingNumberFirstLetter = value.Substring(0, 1);
                            break;
                    }
                    
                    trackingNumberNumberPart = Convert.ToInt32(value.Substring(1, (value.Length - 1)));
                }
                catch
                {
                    writeToErrorLog("Error finding " + trackingNumberFirstLetter + trackingNumberNumberPart.ToString() + " while trying to upload Air Temp coefficents.");
                    MessageBox.Show("Tracking Number input is not formatted correctly.\nInput must be like: S123456", "Tracking Number Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //Serial Number(s)
                string[] serialNumberArray = new string[1];
                serialNumberArray[0] = trackingNumberFirstLetter + value.Substring(1,value.Length-1);

                //Checking to make sure input tracking number exsist.
                Boolean exsistingTrackingNumber = false;


                //Checking to make sure the coefficent files exsist.
                string fileCheckReport = "";
                switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
                {
                    case "iMet-TX":

                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (serialNumberArray[0] == ValidSondeArryiMet1[i].getTrackingID())
                            {
                                if (File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[i].getTUProbeID() + "_AT.tcal") != true)
                                {
                                    fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                                }
                                exsistingTrackingNumber = true;
                            }

                        }
                        break;

                    case "iMet Un":
                        for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                        {
                            if (serialNumberArray[0] == ValidSondeiMet1U[i].CalData.SerialNumber)
                            {
                                if (File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeiMet1U[i].CalData.ProbeID + "_AT.tcal") != true)
                                {
                                    fileCheckReport = fileCheckReport + ValidSondeiMet1U[i].CalData.SerialNumber + ",";
                                }
                                exsistingTrackingNumber = true;
                            }
                        }
                        break;
                }

                if (!exsistingTrackingNumber)
                {
                    MessageBox.Show("Unable to find input Tracking Number: " + serialNumberArray[0],"Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                    
                }


                if (fileCheckReport != "")
                {
                    DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (testReport == DialogResult.No)
                    {
                        return;
                    }
                }

                //Changing the color of the button to green.
                buttonLoadSingleCoefficentAirTemp.BackColor = System.Drawing.Color.Green;

                //Coefficents
                int coefficentLoadType = 1;

                //Checking for repeat sonde sn's
                bool repeatSerialNumber = checkForRepeatSerialNumbers();

                if (repeatSerialNumber == true)
                {
                    UpdateMainStatusLabel("Coefficent Upload Stopped");
                    return;
                }

                //Disabling the coefficent load buttons
                disableCofficentButtons();

                //Stopping data collection.
                RadiosondeStatus.processRadiosondeHex = false;

                //Setting up the Background worker array
                int amountOfBackgroundWorkers = 1;
                setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


                //Starting the background worker array.
                startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);
            }
        }

        private void buttonLoadSingleCoefficentHumidityTemp_Click(object sender, EventArgs e)
        {
            string value = "";
            string trackingNumberFirstLetter = "";
            int trackingNumberNumberPart = 0;

            if (InputBox("Tracking Number", "Tracking Number:", ref value) == DialogResult.OK)
            {
                try
                {
                    trackingNumberFirstLetter = value.Substring(0, 1).ToUpper();
                    trackingNumberNumberPart = Convert.ToInt32(value.Substring(1, (value.Length - 1)));
                }
                catch
                {
                    writeToErrorLog("Error finding " + trackingNumberFirstLetter + trackingNumberNumberPart.ToString() + " while trying to upload Humidity Temp coefficents.");
                    MessageBox.Show("Tracking Number input is not formatted correctly.\nInput must be like: S123456", "Tracking Number Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //Serial Number(s)
                string[] serialNumberArray = new string[1];
                serialNumberArray[0] = trackingNumberFirstLetter + trackingNumberNumberPart.ToString();

                //Checking to make sure input tracking number exsist.
                Boolean exsistingTrackingNumber = false;

                //Checking to make sure the coefficent files exsist.
                string fileCheckReport = "";
                for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                {
                    if (serialNumberArray[0] == ValidSondeArryiMet1[i].getTrackingID())
                    {
                        if (File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[i].getTUProbeID() + "_UT.tcal") != true)
                        {
                            fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                        }
                        exsistingTrackingNumber = true;
                    }
                }

                if (!exsistingTrackingNumber)
                {
                    MessageBox.Show("Unable to find input Tracking Number: " + serialNumberArray[0], "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }

                if (fileCheckReport != "")
                {
                    DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (testReport == DialogResult.No)
                    {
                        return;
                    }
                }

                //Changing the color of the button to green.
                buttonLoadSingleCoefficentHumidityTemp.BackColor = System.Drawing.Color.Green;

                //Coefficents
                int coefficentLoadType = 2;

                //Checking for repeat sonde sn's
                bool repeatSerialNumber = checkForRepeatSerialNumbers();

                if (repeatSerialNumber == true)
                {
                    UpdateMainStatusLabel("Coefficent Upload Stopped");
                    return;
                }

                //Disabling the coefficent load buttons
                disableCofficentButtons();

                //Stopping data collection.
                RadiosondeStatus.processRadiosondeHex = false;

                //Setting up the Background worker array
                int amountOfBackgroundWorkers = 1;
                setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


                //Starting the background worker array.
                startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);
            }
        }

        private void buttonLoadSingleCoefficentHumidity_Click(object sender, EventArgs e)
        {
            string value = "";
            string trackingNumberFirstLetter = "";
            int trackingNumberNumberPart = 0;

            if (InputBox("Tracking Number", "Tracking Number:", ref value) == DialogResult.OK)
            {
                try
                {
                    switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
                    {
                        case "iMet-TX":
                            trackingNumberFirstLetter = value.Substring(0, 1).ToUpper();
                            break;
                        case "iMet Un":
                            trackingNumberFirstLetter = value.Substring(0, 1);
                            break;
                    }
                    trackingNumberNumberPart = Convert.ToInt32(value.Substring(1, (value.Length - 1)));
                }
                catch
                {
                    writeToErrorLog("Error finding " + trackingNumberFirstLetter + trackingNumberNumberPart.ToString() + " while trying to upload Humidity coefficents.");
                    MessageBox.Show("Tracking Number input is not formatted correctly.\nInput must be like: S123456", "Tracking Number Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //Serial Number(s)
                string[] serialNumberArray = new string[1];
                serialNumberArray[0] = trackingNumberFirstLetter + value.Substring(1, value.Length-1);

                //Checking to make sure input tracking number exsist.
                Boolean exsistingTrackingNumber = false;

                //Checking to make sure the coefficent files exsist.
                string fileCheckReport = "";

                switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
                {
                    case "iMet-TX":

                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (serialNumberArray[0] == ValidSondeArryiMet1[i].getTrackingID())
                            {
                                if (File.Exists(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".u_cal") != true)
                                {
                                    fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                                }
                                exsistingTrackingNumber = true;
                            }
                        }
                        break;

                    case "iMet Un":
                        for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                        {
                            if (serialNumberArray[0] == ValidSondeiMet1U[i].CalData.SerialNumber)
                            {
                                if (File.Exists(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeiMet1U[i].CalData.SerialNumber + ".u_cal") != true)
                                {
                                    fileCheckReport = fileCheckReport + ValidSondeiMet1U[i].CalData.SerialNumber + ",";
                                }
                                exsistingTrackingNumber = true;
                            }
                        }
                        break;
                }

                if (!exsistingTrackingNumber)
                {
                    MessageBox.Show("Unable to find input Tracking Number: " + serialNumberArray[0], "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }

                if (fileCheckReport != "")
                {
                    DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (testReport == DialogResult.No)
                    {
                        return;
                    }
                }

                //Changing the color of the button to green.
                buttonLoadSingleCoefficentHumidity.BackColor = System.Drawing.Color.Green;

                //Coefficents
                int coefficentLoadType = 0;
                if (comboBoxSondeType.Text == "iMet-TX-HUM" && coefficentLoadType == 0)
                {
                    coefficentLoadType = 4;
                }
                if (comboBoxSondeType.Text == "iMet-TX-E+E" || comboBoxSondeType.Text == "iMet Universal" && coefficentLoadType == 0)
                {
                    coefficentLoadType = 3;
                }
                if(comboBoxSondeType.Text == "iMet-TX-HUM" && comboBoxSondeType.Text == "iMet-TX-E+E" && coefficentLoadType == 0)
                {
                    MessageBox.Show("Radiosonde type error has occured.\nPlease stop all sonde, select a sonde type from the list and try again.\nHumidity coefficent load canceled.", "Sonde Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    enableCoefficentButtons();
                    return;
                }

                //Checking to make sure that the SN's coefficent file contains the right amount of
                //coefficents to match the type of radiosonde selected.

                string[] checkingCoefficentFileLength = null;

                switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
                {
                    case "iMet-TX":

                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (serialNumberArray[0] == ValidSondeArryiMet1[i].getTrackingID())
                            {
                                checkingCoefficentFileLength = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".u_cal");


                                if (coefficentLoadType == 4 && checkingCoefficentFileLength.Length != 9)
                                {
                                    MessageBox.Show("Error in Radiosonde Coefficent file.\nCoefficent file doesn't contain enough coefficents for radiosonde type selected.\nHumidity coefficent load canceled.", "Sonde Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    enableCoefficentButtons();
                                    return;
                                }

                                if (coefficentLoadType == 3 && checkingCoefficentFileLength.Length != 6)
                                {
                                    MessageBox.Show("Error in Radiosonde Coefficent file.\nCoefficent file doesn't contain enough coefficents for radiosonde type selected.\nHumidity coefficent load canceled.", "Sonde Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    enableCoefficentButtons();
                                    return;
                                }
                            }

                        }
                        break;
                    case "iMet Un":
                        for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                        {
                            if (serialNumberArray[0] == ValidSondeiMet1U[i].CalData.SerialNumber)
                            {
                                checkingCoefficentFileLength = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeiMet1U[i].CalData.SerialNumber + ".u_cal");


                                if (coefficentLoadType == 4 && checkingCoefficentFileLength.Length != 9)
                                {
                                    MessageBox.Show("Error in Radiosonde Coefficent file.\nCoefficent file doesn't contain enough coefficents for radiosonde type selected.\nHumidity coefficent load canceled.", "Sonde Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    enableCoefficentButtons();
                                    return;
                                }

                                if (coefficentLoadType == 3 && checkingCoefficentFileLength.Length != 6)
                                {
                                    MessageBox.Show("Error in Radiosonde Coefficent file.\nCoefficent file doesn't contain enough coefficents for radiosonde type selected.\nHumidity coefficent load canceled.", "Sonde Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    enableCoefficentButtons();
                                    return;
                                }
                            }

                        }

                        break;
                }

                //Checking for repeat sonde sn's
                bool repeatSerialNumber = checkForRepeatSerialNumbers();

                if (repeatSerialNumber == true)
                {
                    UpdateMainStatusLabel("Coefficent Upload Stopped");
                    return;
                }

                //Disabling the coefficent load buttons
                disableCofficentButtons();

                //Stopping data collection.
                RadiosondeStatus.processRadiosondeHex = false;

                //Setting up the Background worker array
                int amountOfBackgroundWorkers = 1;
                setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


                //Starting the background worker array.
                startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);
            }
        }

        private void buttonLoadSingleCoefficentAll_Click(object sender, EventArgs e)
        {
            string value = "";
            string trackingNumberFirstLetter = "";
            int trackingNumberNumberPart = 0;

            if (InputBox("Tracking Number", "Tracking Number:", ref value) == DialogResult.OK)
            {
                try
                {
                    trackingNumberFirstLetter = value.Substring(0, 1).ToUpper();
                    trackingNumberNumberPart = Convert.ToInt32(value.Substring(1, (value.Length - 1)));
                }
                catch
                {
                    writeToErrorLog("Error finding " + trackingNumberFirstLetter + trackingNumberNumberPart.ToString() + " while trying to upload All coefficents.");
                    MessageBox.Show("Tracking Number input is not formatted correctly.\nInput must be like: S123456", "Tracking Number Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //Serial Number(s)
                string[] serialNumberArray = new string[1];
                serialNumberArray[0] = trackingNumberFirstLetter + trackingNumberNumberPart.ToString();
                
                //Checking to make sure input tracking number exsist.
                Boolean exsistingTrackingNumber = false;

                //Checking to make sure the coefficent files exsist.
                string fileCheckReport = "";
                for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                {
                    if (serialNumberArray[0] == ValidSondeArryiMet1[i].getTrackingID())
                    {
                        if (File.Exists(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".p_cal") != true ||
                            File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[i].getTUProbeID() + "_AT.tcal") != true ||
                            File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[i].getTUProbeID() + "_UT.tcal") != true ||
                            File.Exists(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".u_cal") != true)
                        {
                            fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                        }
                        exsistingTrackingNumber = true;
                    }
                }

                if (!exsistingTrackingNumber)
                {
                    MessageBox.Show("Unable to find input Tracking Number: " + serialNumberArray[0], "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }

                if (fileCheckReport != "")
                {
                    DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (testReport == DialogResult.No)
                    {
                        return;
                    }
                }

                //Changing the color of the button to green.
                buttonLoadSingleCoefficentAll.BackColor = System.Drawing.Color.Green;

                //Coefficents
                int coefficentLoadType = 5;

                //Checking for repeat sonde sn's
                bool repeatSerialNumber = checkForRepeatSerialNumbers();

                if (repeatSerialNumber == true)
                {
                    UpdateMainStatusLabel("Coefficent Upload Stopped");
                    return;
                }

                //Disabling the coefficent load buttons
                disableCofficentButtons();

                //Stopping data collection.
                RadiosondeStatus.processRadiosondeHex = false;

                //Setting up the Background worker array
                int amountOfBackgroundWorkers = 1;
                setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


                //Starting the background worker array.
                startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);
            }
        }

        private void buttonLoadAllCoefficentPressure_Click(object sender, EventArgs e)
        {
            loadAllCoefs(0);
            /*
            //Checking to make sure the coefficent files exsist.
            string fileCheckReport = "";
            for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
            {
                if (File.Exists(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".p_cal") != true)
                {
                    fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                }
            }

            if (fileCheckReport != "")
            {
                DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (testReport == DialogResult.No)
                {
                    return;
                }
            }

            //Checking for repeat sonde sn's
            bool repeatSerialNumber = checkForRepeatSerialNumbers();

            if (repeatSerialNumber == true)
            {
                UpdateMainStatusLabel("Coefficent Upload Stopped");
                return;
            }

            //Changing the color of the button to green.
            buttonLoadAllCoefficentPressure.BackColor = System.Drawing.Color.Green;

            //Disabling the coefficent load buttons
            disableCofficentButtons();

            //clearing past works from the data grids.
            clearPastTestReported();

            //Serial Number(s)
            string[] serialNumberArray = new string[ValidSondeArryiMet1.Length];
            for (int i = 0; i < serialNumberArray.Length; i++)
            {
                serialNumberArray[i] = ValidSondeArryiMet1[i].getTrackingID();
            }

            //Coefficents
            int coefficentLoadType = 0;

            //Stopping data collection.
            RadiosondeStatus.processRadiosondeHex = false;

            //Setting up the Background worker array
            int amountOfBackgroundWorkers = howManyBackgroundWorkers(ValidSondeArryiMet1.Length);
            
            setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


            //Starting the background worker array.
            startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);

            */
        }

        private void buttonLoadAllCoefficentAirTemp_Click(object sender, EventArgs e)
        {
            loadAllCoefs(1);
            /*
            //Checking to make sure the coefficent files exsist.
            string fileCheckReport = "";
            for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
            {
                if(File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[i].getTUProbeID() + "_AT.tcal") != true)
                {
                    fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                }
            }

            if (fileCheckReport != "")
            {
                DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (testReport == DialogResult.No)
                {
                    return;
                }
            }

            //Checking for repeat sonde sn's
            bool repeatSerialNumber = checkForRepeatSerialNumbers();

            if (repeatSerialNumber == true)
            {
                UpdateMainStatusLabel("Coefficent Upload Stopped");
                return;
            }

            //Changing the color of the button to green.
            buttonLoadAllCoefficentAirTemp.BackColor = System.Drawing.Color.Green;

            //Disabling the coefficent load buttons
            disableCofficentButtons();

            //clearing past works from the data grids.
            clearPastTestReported();

            //Serial Number(s)
            string[] serialNumberArray = new string[ValidSondeArryiMet1.Length];
            for (int i = 0; i < serialNumberArray.Length; i++)
            {
                serialNumberArray[i] = ValidSondeArryiMet1[i].getTrackingID();
            }            

            //Coefficents
            int coefficentLoadType = 1;

            //Stopping data collection.
            RadiosondeStatus.processRadiosondeHex = false;

            //Setting up the Background worker array
            int amountOfBackgroundWorkers = howManyBackgroundWorkers(ValidSondeArryiMet1.Length);

            setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


            //Starting the background worker array.
            startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);

            */
        }

        private void buttonLoadAllCoefficentHumidityTemp_Click(object sender, EventArgs e)
        {
            loadAllCoefs(2);
            /*
            //Checking to make sure the coefficent files exsist.
            string fileCheckReport = "";
            for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
            {
                if (File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[i].getTUProbeID() + "_UT.tcal") != true)
                {
                    fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                }
            }

            if (fileCheckReport != "")
            {
                DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (testReport == DialogResult.No)
                {
                    return;
                }
            }

            //Checking for repeat sonde sn's
            bool repeatSerialNumber = checkForRepeatSerialNumbers();

            if (repeatSerialNumber == true)
            {
                UpdateMainStatusLabel("Coefficent Upload Stopped");
                return;
            }

            //Changing the color of the button to green.
            buttonLoadAllCoefficentHumidityTemp.BackColor = System.Drawing.Color.Green;

            //Disabling the coefficent load buttons
            disableCofficentButtons();

            //clearing past works from the data grids.
            clearPastTestReported();

            //Serial Number(s)
            string[] serialNumberArray = new string[ValidSondeArryiMet1.Length];
            for (int i = 0; i < serialNumberArray.Length; i++)
            {
                serialNumberArray[i] = ValidSondeArryiMet1[i].getTrackingID();
            }

            //Coefficents
            int coefficentLoadType = 1;

            //Stopping data collection.
            RadiosondeStatus.processRadiosondeHex = false;

            //Setting up the Background worker array
            int amountOfBackgroundWorkers = howManyBackgroundWorkers(ValidSondeArryiMet1.Length);

            setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


            //Starting the background worker array.
            startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);

            */
        }

        private void buttonLoadAllCoefficentHumidity_Click(object sender, EventArgs e)
        {
            int coefficentLoadType = 0;
            if (comboBoxSondeType.Text == "iMet-TX-HUM" && coefficentLoadType == 0)
            {
                coefficentLoadType = 4;
            }
            else
            {
                coefficentLoadType = 3;
            }

            loadAllCoefs(coefficentLoadType);
            /*
             * 
             *  case 3://e+e humidity

                case 4://Hum humidity
             * 
            //Checking to make sure the coefficent files exsist.
            string fileCheckReport = "";
            for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
            {
                if (File.Exists(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".u_cal") != true)
                {
                    fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                }
            }

            if (fileCheckReport != "")
            {
                DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (testReport == DialogResult.No)
                {
                    return;
                }
            }

            //Checking for repeat sonde sn's
            bool repeatSerialNumber = checkForRepeatSerialNumbers();

            if (repeatSerialNumber == true)
            {
                UpdateMainStatusLabel("Coefficent Upload Stopped");
                return;
            }

            //Changing the color of the button to green.
            buttonLoadAllCoefficentHumidity.BackColor = System.Drawing.Color.Green;

            //Disabling the coefficent load buttons
            disableCofficentButtons();

            //clearing past works from the data grids.
            clearPastTestReported();

            //Serial Number(s)
            string[] serialNumberArray = new string[ValidSondeArryiMet1.Length];
            for (int i = 0; i < serialNumberArray.Length; i++)
            {
                serialNumberArray[i] = ValidSondeArryiMet1[i].getTrackingID();
            }

            //Coefficents
            int coefficentLoadType = 0;
            if (comboBoxSondeType.Text == "iMet-TX-HUM" && coefficentLoadType == 0)
            {
                coefficentLoadType = 4;
            }
            if (comboBoxSondeType.Text == "iMet-TX-E+E" && coefficentLoadType == 0)
            {
                coefficentLoadType = 3;
            }
            if (comboBoxSondeType.Text == "iMet-TX-HUM" && comboBoxSondeType.Text == "iMet-TX-E+E" && coefficentLoadType == 0)
            {
                MessageBox.Show("Radiosonde type error has occured.\nPlease stop all sonde, select a sonde type from the list and try again.\nHumidity coefficent load canceled.", "Sonde Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                enableCoefficentButtons();
                return;
            }

            //Checking to make sure that the SN's coefficent file contains the right amount of
            //coefficents to match the type of radiosonde selected.

            string[] checkingCoefficentFileLength = null;
            string checkingCoefficentFileLengthReport = "";
            for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
            {
                if (serialNumberArray[0] == ValidSondeArryiMet1[i].getTrackingID())
                {
                    checkingCoefficentFileLength = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".u_cal");
                }


                if (coefficentLoadType == 4 && checkingCoefficentFileLength.Length != 9)
                {
                    checkingCoefficentFileLengthReport = checkingCoefficentFileLengthReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                }

                if (coefficentLoadType == 3 && checkingCoefficentFileLength.Length != 6)
                {
                    checkingCoefficentFileLengthReport = checkingCoefficentFileLengthReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                }
            }

            if (checkingCoefficentFileLengthReport != "")
            {
                MessageBox.Show("The following Radiosondes coefficent files to not match the type selected.\n" + checkingCoefficentFileLengthReport + "\n" + "Please check your sondes and restart." + "\n" + "Humidity coefficent load canceled.", "Sonde Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                enableCoefficentButtons();
                return;
            }

            //Stopping data collection.
            RadiosondeStatus.processRadiosondeHex = false;

            //Setting up the Background worker array
            int amountOfBackgroundWorkers = howManyBackgroundWorkers(ValidSondeArryiMet1.Length);

            setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


            //Starting the background worker array.
            startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);
             */ 
        }

        private void buttonLoadAllCoefficentAll_Click(object sender, EventArgs e)
        {
            loadAllCoefs(5);
            /*
            //Checking to make sure the coefficent files exsist.
            string fileCheckReport = "";

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":
                    break;
            }

                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        if (File.Exists(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".p_cal") != true ||
                                    File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[i].getTUProbeID() + "_AT.tcal") != true ||
                                    File.Exists(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[i].getTUProbeID() + "_UT.tcal") != true ||
                                    File.Exists(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[i].getTrackingID() + ".u_cal") != true)
                        {
                            fileCheckReport = fileCheckReport + ValidSondeArryiMet1[i].getTrackingID() + ",";
                        }
                    }


            if (fileCheckReport != "")
            {
                DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent all files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (testReport == DialogResult.No)
                {
                    return;
                }
            }

            //Checking for repeat sonde sn's
            bool repeatSerialNumber = checkForRepeatSerialNumbers();

            if (repeatSerialNumber == true)
            {
                UpdateMainStatusLabel("Coefficent Upload Stopped");
                return;
            }

            //Changing the color of the button to green.
            buttonLoadAllCoefficentAll.BackColor = System.Drawing.Color.Green;

            //Disabling the coefficent load buttons
            disableCofficentButtons();

            //clearing past works from the data grids.
            clearPastTestReported();

            //Serial Number(s)

            string[] serialNumberArray = new string[ValidSondeArryiMet1.Length];
            for (int i = 0; i < serialNumberArray.Length; i++)
            {
                serialNumberArray[i] = ValidSondeArryiMet1[i].getTrackingID();
            }

            //Coefficents
            int coefficentLoadType = 5;

            //Stopping data collection.
            RadiosondeStatus.processRadiosondeHex = false;

            //Setting up the Background worker array
            int amountOfBackgroundWorkers = howManyBackgroundWorkers(ValidSondeArryiMet1.Length);

            setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


            //Starting the background worker array.
            startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, serialNumberArray, coefficentLoadType);
            */ 
        }

        public void loadAllCoefs(int coefType)
        {
            //Collecting the current radiosonde Tracking Numbers or Serial Numbers.
            string[] currentRadiosondes = null;
            string[] currentProbeNumber = null;
            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":
                    currentRadiosondes = new string[ValidSondeArryiMet1.Length];
                    currentProbeNumber = new string[ValidSondeArryiMet1.Length];
                    for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                    {
                        currentRadiosondes[i] = ValidSondeArryiMet1[i].getTrackingID();
                        currentProbeNumber[i] = ValidSondeArryiMet1[i].getTrackingID();
                    }
                    break;

                case "iMet Un":
                    currentRadiosondes = new string[ValidSondeiMet1U.Length];
                    currentProbeNumber = new string[ValidSondeiMet1U.Length];
                    for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                    {
                        currentRadiosondes[i] = ValidSondeiMet1U[i].CalData.SerialNumber;
                        currentProbeNumber[i] = ValidSondeiMet1U[i].CalData.ProbeID;
                    }
                    break;

            }

            //if nothing is added to the array then stopping before something goes really worng.
            if (currentRadiosondes == null || currentProbeNumber == null)
            {
                UpdateMainStatusLabel("No radiosondes added to have coef's loaded. Stopping all loading process.");
                return;
            }

            //Loading the type of file to look for.
            string[] fileType = null;
            switch (coefType)
            {
                case 0: //Pressue
                    fileType = new string[1]{ ".p_cal" };

                    //Updating the screen with what is happening
                    UpdateMainStatusLabel("Loading Pressure Coef's");

                    //Changing the color of the button to green.
                    buttonLoadAllCoefficentPressure.BackColor = System.Drawing.Color.Green;

                    break;
                case 1: //Air Temp
                    fileType = new string[1]{ "_AT.tcal" };

                    //Updating the screen with what is happening
                    UpdateMainStatusLabel("Loading Air Temp Coef's");

                    //Changing the color of the button to green.
                    buttonLoadAllCoefficentAirTemp.BackColor = System.Drawing.Color.Green;

                    break;
                case 2: //Humidity Temp not currently used, but still here
                    fileType = new string[1]{ "_UT.tcal" };

                    //Updating the screen with what is happening
                    UpdateMainStatusLabel("Loading Humidity Air Temp Coef's");

                    //Changing the color of the button to green.
                    buttonLoadAllCoefficentHumidityTemp.BackColor = System.Drawing.Color.Green;

                    break;
                case 3://e+e humidity
                    fileType = new string[1]{ ".u_cal" };

                    //Updating the screen with what is happening
                    UpdateMainStatusLabel("Loading E+E Humidity Coef's");

                    //Changing the color of the button to green.
                    buttonLoadAllCoefficentHumidity.BackColor = System.Drawing.Color.Green;

                    break;
                case 4://Hum humidity
                    fileType = new string[1]{ ".u_cal" };

                    //Updating the screen with what is happening
                    UpdateMainStatusLabel("Loading Humeral Humidity Coef's");

                    //Changing the color of the button to green.
                    buttonLoadAllCoefficentHumidity.BackColor = System.Drawing.Color.Green;

                    break;
                case 5:// All coefs.
                    fileType = new string[3] { ".p_cal", "_AT.tcal", ".u_cal" };

                    //Updating the screen with what is happening
                    UpdateMainStatusLabel("Loading All Coef's");

                    //Changing the color of the button to green.
                    buttonLoadAllCoefficentAll.BackColor = System.Drawing.Color.Green;

                    break;

            }

            if (fileType == null)
            {
                UpdateMainStatusLabel("Error in coef type to load.");
                return;
            }

            //Checking for repeat sonde sn's
            bool repeatSerialNumber = checkForRepeatSerialNumbers();

            if (repeatSerialNumber == true)
            {
                UpdateMainStatusLabel("Coefficent Upload Stopped Repeat Names Found.");
                return;
            }

            //Checking to make sure the coef files exsist.
            string fileCheckReport = "";
            for (int i = 0; i < currentRadiosondes.Length; i++)
            {
                for (int j = 0; j < fileType.Length; j++)
                {
                    //Formatting spaceing for the differance between at and the rest of the coef files.
                    string spaceing = "_";
                    string currentElement = currentRadiosondes[i];
                    if (fileType[j].Substring(0,1) == "_")
                    {
                        spaceing = "";
                        currentElement = currentProbeNumber[i];
                    }

                    //This will allow old radiosondes to load old coefs from the humeral days.
                    switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
                    {
                        case "iMet-TX":
                            if (fileType[j] == ".p_cal")
                            {
                                spaceing = "_";
                            }
                            break;
                        case "iMet Un":
                            if (fileType[j] == ".p_cal")
                            {
                                spaceing = "";
                            }
                            break;

                    }

                    //Checking the files.
                    if (File.Exists(ProgramSettings.coefficentStorageLocation + spaceing + currentElement + fileType[j]) != true)
                    {
                        fileCheckReport = fileCheckReport + currentRadiosondes[i] + ",";
                    }
                }
             }

            if (fileCheckReport != "")
            {
                DialogResult testReport = MessageBox.Show("The following Radiosondes do no have coefficent files.\n" + fileCheckReport + "\nDo you wish to continue?", "Coefficent File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (testReport == DialogResult.No)
                {
                    return;
                }
            }

            //Disabling the coefficent load buttons
            disableCofficentButtons();

            //clearing past works from the data grids.
            clearPastTestReported();

            //Stopping data collection.
            RadiosondeStatus.processRadiosondeHex = false;

            //Setting up the Background worker array
            int amountOfBackgroundWorkers = howManyBackgroundWorkers(currentRadiosondes.Length);

            setupLoadCoefficentBackgoundWorkers(amountOfBackgroundWorkers);


            //Starting the background worker array.
            startLoadCoefficentBackgroundWorkersArray(amountOfBackgroundWorkers, currentRadiosondes, coefType);

        }

        private void buttonClearReport_Click(object sender, EventArgs e)
        {
            //Clearing curent reported tests.
            clearPastTestReported();
        }

        #endregion

        #region Coefficent Enable/Disable buttons.
        public void enableCoefficentButtons()
        {
            Color normalBackground = buttonClearReport.BackColor;

            buttonClearReport.Enabled = true;

            buttonLoadAllCoefficentPressure.BackColor = normalBackground;
            buttonLoadAllCoefficentPressure.Enabled = true;

            buttonLoadAllCoefficentAirTemp.BackColor = normalBackground;
            buttonLoadAllCoefficentAirTemp.Enabled = true;

            buttonLoadAllCoefficentHumidityTemp.BackColor = normalBackground;
            //buttonLoadAllCoefficentHumidityTemp.Enabled = true;

            buttonLoadAllCoefficentHumidity.BackColor = normalBackground;
            buttonLoadAllCoefficentHumidity.Enabled = true;

            buttonLoadAllCoefficentAll.BackColor = normalBackground;
            buttonLoadAllCoefficentAll.Enabled = true;

            buttonLoadSingleCoefficentPressure.BackColor = normalBackground;
            buttonLoadSingleCoefficentPressure.Enabled = true;

            buttonLoadSingleCoefficentAirTemp.BackColor = normalBackground;
            buttonLoadSingleCoefficentAirTemp.Enabled = true;

            buttonLoadSingleCoefficentHumidityTemp.BackColor = normalBackground;
            //buttonLoadSingleCoefficentHumidityTemp.Enabled = true;

            buttonLoadSingleCoefficentHumidity.BackColor = normalBackground;
            buttonLoadSingleCoefficentHumidity.Enabled = true;

            buttonLoadSingleCoefficentAll.BackColor = normalBackground;
            buttonLoadSingleCoefficentAll.Enabled = true;

            buttonLoadCancel.Enabled = false;
        }

        public void disableCofficentButtons()
        {
            buttonClearReport.Enabled = false;
            buttonLoadAllCoefficentPressure.Enabled = false;
            buttonLoadAllCoefficentAirTemp.Enabled = false;
            buttonLoadAllCoefficentHumidityTemp.Enabled = false;
            buttonLoadAllCoefficentHumidity.Enabled = false;
            buttonLoadAllCoefficentAll.Enabled = false;
            buttonLoadSingleCoefficentPressure.Enabled = false;
            buttonLoadSingleCoefficentAirTemp.Enabled = false;
            buttonLoadSingleCoefficentHumidityTemp.Enabled = false;
            buttonLoadSingleCoefficentHumidity.Enabled = false;
            buttonLoadSingleCoefficentAll.Enabled = false;

            buttonLoadCancel.Enabled = true;
        }
        #endregion
        
        #region In this area I setup the backgroud worker array and start them and send them lists of serial numbers to process.

        private void setupLoadCoefficentBackgoundWorkers(int amountOfBackgroundWorkers)
        {
            loadRadiosondeCoefficent = new BackgroundWorker[amountOfBackgroundWorkers];
            for (int i = 0; i < amountOfBackgroundWorkers; i++)
            {
                loadRadiosondeCoefficent[i] = new BackgroundWorker();
                loadRadiosondeCoefficent[i].WorkerSupportsCancellation = true;
                loadRadiosondeCoefficent[i].DoWork += new DoWorkEventHandler(loadRadiosondeCoefficents_DoWork);
                loadRadiosondeCoefficent[i].RunWorkerCompleted += new RunWorkerCompletedEventHandler(loadRadiosondeCoefficents_RunWorkerCompleted);
            }
        }

        private void startLoadCoefficentBackgroundWorkersArray(int amountOfBackgroundWorkers, string[] serialNumberArray, int coefficentLoadType)
        {
            //Spliting the work up between the background workers.
            //As well as setting the array up that will be sent to the background worker
            int dataDividedUp = serialNumberArray.Length/amountOfBackgroundWorkers;
            int dataToBeSentOutCounter = 0;
            int arrayIndex = 0;
            string[,] dataToBeSentOut = new string[amountOfBackgroundWorkers,200];
            int counterSerialNumberArrayIndex = 0;

            //The following code breaks down the incoming array to a 2d array. It also includes the "mode" the 
            //provided list of serial number will be processed in. IE load AT, UT and so on.
            //index 0 of each array is that "mode" type.
            for(int i = 0; i<serialNumberArray.Length+amountOfBackgroundWorkers; i ++)
            {
                if (dataToBeSentOutCounter == 0 && arrayIndex < amountOfBackgroundWorkers)
                {
                    dataToBeSentOut[arrayIndex, 0] = coefficentLoadType.ToString();
                }

                if (dataToBeSentOutCounter > 0)
                {
                    dataToBeSentOut[arrayIndex, dataToBeSentOutCounter] = serialNumberArray[counterSerialNumberArrayIndex];
                    counterSerialNumberArrayIndex++;
                }

                if (arrayIndex == amountOfBackgroundWorkers - 1 && dataToBeSentOutCounter >= dataDividedUp  && amountOfBackgroundWorkers > 1)
                {
                    for (int m = 1; m < (serialNumberArray.Length - counterSerialNumberArrayIndex) + 1; m++)
                    {
                        dataToBeSentOut[arrayIndex, m + dataToBeSentOutCounter] = serialNumberArray[(m + counterSerialNumberArrayIndex - 1)];
                    }
                    break;
                }

                if (dataToBeSentOutCounter == dataDividedUp)
                {
                    dataToBeSentOutCounter = 0;
                    arrayIndex++;
                }
                else
                {
                    dataToBeSentOutCounter++;
                }
            }
            
            //Starting the Background Works and send them there commands and serial number to handle.
            for (int i = 0; i < amountOfBackgroundWorkers; i++)
            {
                //breaking down the 2d array into a singal array that can be sent to the background worker.
                string[] dataToBackgroundWorker = new string[dataToBeSentOut.Length/amountOfBackgroundWorkers];
                for (int m = 0; m < dataToBeSentOut.Length/amountOfBackgroundWorkers; m++)
                {
                    dataToBackgroundWorker[m] = dataToBeSentOut[i, m];
                }

                //starting the backgound worker and sending it its work load.
                if (!loadRadiosondeCoefficent[i].IsBusy)
                {
                    loadRadiosondeCoefficent[i].RunWorkerAsync(dataToBackgroundWorker);
                }
                dataToBackgroundWorker = null;
            }
        }

        #endregion

        #region This is the Background Worker Do Work and RunWorker Complete.
        private void loadRadiosondeCoefficents_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            //breaking recived event data into a string array.
            string[] serialNumbers = e.Argument as string[];

            //What type of coefficents are being loaded.
            string loadType = "";
            if (serialNumbers[0] == "0")
            { loadType = "Pressure"; }
            if (serialNumbers[0] == "1")
            { loadType = "Air Temperature"; }
            if (serialNumbers[0] == "2")
            { loadType = "Humidity Temperature"; }
            if (serialNumbers[0] == "3")
            { loadType = "e+e Humidity"; }
            if (serialNumbers[0] == "4")
            { loadType = "Hum Humidity"; }
            if (serialNumbers[0] == "5")
            { loadType = "All"; }

            //Reporting that coefficents are being loaded.
            UpdateMainStatusLabel("Loading " + loadType + " coefficents to radiosonde(s).");
            
            //Assigning arrays that will be sent to radisonde class.
            int[] coefficentIndexNumber = null;
            string[] coefficentValue = null;

            //Determining what coefficents need to be loaded.
            int counterIndex = 0;
            switch (Convert.ToInt16(serialNumbers[0]))
            {
                case 0: //Write Pressure coefficents
                    coefficentIndexNumber = new int[25];
                    for (int i = 0; i < coefficentIndexNumber.Length; i++)
                    {
                        coefficentIndexNumber[i] = i; // Loading the coefficent index.
                    }
                    break;
                case 1: //Writing Air Temp coefficents
                    coefficentIndexNumber = new int[4];
                    counterIndex = 25;
                    for (int i = 0; i < coefficentIndexNumber.Length; i++)
                    {
                        coefficentIndexNumber[i] = counterIndex;
                        counterIndex++;
                    }
                    break;
                case 2: //Writing Humidity Temp coefficents
                    coefficentIndexNumber = new int[4];
                    counterIndex = 29;
                    for (int i = 0; i < coefficentIndexNumber.Length; i++)
                    {
                        coefficentIndexNumber[i] = counterIndex;
                        counterIndex++;
                    }
                    break;
                case 3: //Writing e+e Humidity coefficents
                    coefficentIndexNumber = new int[5];
                    counterIndex = 36;
                    for (int i = 0; i < coefficentIndexNumber.Length; i++)
                    {
                        coefficentIndexNumber[i] = counterIndex;
                        counterIndex++;
                    }
                    break;
                case 4: //Writing Humeral Humidity coefficents
                    coefficentIndexNumber = new int[8];
                    counterIndex = 36;
                    coefficentIndexNumber[0] = 43;
                    for (int i = 1; i < coefficentIndexNumber.Length; i++)
                    {
                        coefficentIndexNumber[i] = counterIndex;
                        counterIndex++;
                    }
                    break;
                case 5: //Writing all coefficents
                    coefficentIndexNumber = new int[44];
                    for (int i = 0; i < coefficentIndexNumber.Length; i++)
                    {
                        coefficentIndexNumber[i] = i;
                    }
                    break;
                default:
                    return;

            }
            
            //Loop for loading coeffients for current serial number, assembling coefficent array, and updating userdisplay.

            string[] tempCoefficentValuesFromFile = null; //Temp file.

            string[] tempCoefficentValuesFromPressureFile = null;
            string[] tempCoefficentValuesFromAirTempFile =  null;
            string[] tempCoefficentValuesFromHumidityTempFile = null;
            string[] tempCoefficentValuesFromHumidityFile = null;

            int currentSondeInArray = 0; //Current sonde being worked on.
            string[] loadCoefficentResults = null;
            int radiosondeRackLOC = 0; //What rack is the radiosonde on.
            int radiosondeRackPOSLOC = 0; //What position in that rack is the radiosonde.

            //Delcares needed for compacting the data down in a all coefficent write.
            int counterCoefficentCompact = 0;
            int[] tempCoefficentNumberIndex = null;
            string[] tempCoefficentValue = null;
            int counterGoodData = 0;
            //Counters used during all coefficent writing.
            int counterAT = 1;
            int counterUT = 1;
            int counterHumidity = 1;


            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":
                    #region For loading coef to iMet-1 legacy radiosondes

                    for (int i = 1; i < serialNumbers.Length; i++)
                    {
                        if (serialNumbers[i] == null) //Breaking out if there is no more serial number to process.
                        {
                            break;
                        }
                        if ((worker.CancellationPending == true))
                        {
                            e.Cancel = true;
                            break;
                        }
                        else
                        {
                            //Looping to find the valid radiosonde that matches the input serial number.
                            for (int m = 0; m < ValidSondeArryiMet1.Length && serialNumbers[i] != null; m++)
                            {
                                if ((worker.CancellationPending == true))
                                {
                                    e.Cancel = true;
                                    break;
                                }
                                else
                                {
                                    if (ValidSondeArryiMet1[m].getTrackingID() == serialNumbers[i])
                                    {
                                        currentSondeInArray = m;
                                    }
                                }
                            }

                            switch (Convert.ToInt16(serialNumbers[0]))//Setting up array and loading coefficents form file.
                            {
                                case 0: //Pressure
                                    coefficentValue = new string[25];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[currentSondeInArray].getTrackingID() + ".p_cal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileError;
                                    }
                                    for (int r = 0; r < coefficentValue.Length; r++)
                                    {
                                        coefficentValue[r] = tempCoefficentValuesFromFile[r + 1];
                                    }
                                    break;
                                case 1: //Air Temp
                                    coefficentValue = new string[4];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[currentSondeInArray].getTUProbeID() + "_AT.tcal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileError;
                                    }
                                    coefficentValue[0] = tempCoefficentValuesFromFile[1];
                                    coefficentValue[1] = tempCoefficentValuesFromFile[2];
                                    coefficentValue[2] = "0.00000e+00";
                                    coefficentValue[3] = tempCoefficentValuesFromFile[3];
                                    break;
                                case 2: //Humidity Temp
                                    coefficentValue = new string[4];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[currentSondeInArray].getTUProbeID() + "_UT.tcal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileError;
                                    }
                                    coefficentValue[0] = tempCoefficentValuesFromFile[1];
                                    coefficentValue[1] = tempCoefficentValuesFromFile[2];
                                    coefficentValue[2] = "0.00000e+00";
                                    coefficentValue[3] = tempCoefficentValuesFromFile[3];
                                    break;
                                case 3: //e+e humidity coefficents
                                    coefficentValue = new string[5];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[currentSondeInArray].getTrackingID() + ".u_cal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileError;
                                    }
                                    for (int r = 0; r < coefficentValue.Length; r++)
                                    {
                                        coefficentValue[r] = tempCoefficentValuesFromFile[r + 1];
                                    }
                                    //formatting coefficent file string to be accepted by the radiosonde class.
                                    coefficentValue[2] = (Convert.ToDouble(coefficentValue[2]) / 10).ToString() + "E+01";
                                    coefficentValue[4] = (Convert.ToDouble(coefficentValue[4]) / 10).ToString() + "E+01";
                                    break;
                                case 4: //humirel coefficents
                                    coefficentValue = new string[8];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[currentSondeInArray].getTrackingID() + ".u_cal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileError;
                                    }
                                    for (int r = 0; r < coefficentValue.Length; r++)
                                    {
                                        coefficentValue[r] = tempCoefficentValuesFromFile[r + 1];
                                    }
                                    break;
                                case 5:
                                    coefficentValue = new string[44];
                                    try
                                    {
                                        tempCoefficentValuesFromPressureFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[currentSondeInArray].getTrackingID() + ".p_cal");
                                        tempCoefficentValuesFromAirTempFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[currentSondeInArray].getTUProbeID() + "_AT.tcal");
                                        tempCoefficentValuesFromHumidityTempFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeArryiMet1[currentSondeInArray].getTUProbeID() + "_UT.tcal");
                                        tempCoefficentValuesFromHumidityFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeArryiMet1[currentSondeInArray].getTrackingID() + ".u_cal");
                                        tempCoefficentValuesFromFile = new string[1];
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileError;
                                    }

                                    if (coefficentIndexNumber.Length != 44)
                                    {
                                        coefficentIndexNumber = null;
                                        coefficentIndexNumber = new int[44];
                                        for (int t = 0; t < coefficentIndexNumber.Length; t++)
                                        {
                                            coefficentIndexNumber[t] = t;
                                        }
                                    }

                                    if (tempCoefficentValuesFromHumidityFile.Length == 6)//If the coefficent file is for e+e.
                                    {

                                        counterAT = 1;
                                        counterUT = 1;
                                        counterHumidity = 1;
                                        for (int r = 0; r < coefficentValue.Length; r++)
                                        {
                                            if (r >= 0 && r <= 24)
                                            {
                                                coefficentValue[r] = tempCoefficentValuesFromPressureFile[r + 1];
                                            }
                                            if (r >= 25 && r <= 28)
                                            {
                                                if (r == 27)
                                                { coefficentValue[r] = "0.00000e+00"; }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromAirTempFile[counterAT];
                                                    counterAT++;
                                                }
                                            }
                                            if (r >= 29 && r <= 32)
                                            {
                                                if (r == 31)
                                                { coefficentValue[r] = "0.00000e+00"; }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromHumidityTempFile[counterUT];
                                                    counterUT++;
                                                }
                                            }
                                            if (r >= 36 && r <= 40)
                                            {
                                                //formatting coefficent file string to be accepted by the radiosonde class.
                                                if (counterHumidity == 3) { tempCoefficentValuesFromHumidityFile[3] = (Convert.ToDouble(tempCoefficentValuesFromHumidityFile[3]) / 10).ToString() + "E+01"; }
                                                if (counterHumidity == 5) { tempCoefficentValuesFromHumidityFile[5] = (Convert.ToDouble(tempCoefficentValuesFromHumidityFile[5]) / 10).ToString() + "E+01"; }

                                                coefficentValue[r] = tempCoefficentValuesFromHumidityFile[counterHumidity];
                                                counterHumidity++;
                                            }
                                        }
                                    }

                                    if (tempCoefficentValuesFromHumidityFile.Length == 9)//If the coefficent file is for humeral sensors.
                                    {
                                        counterAT = 1;
                                        counterUT = 1;
                                        counterHumidity = 1;
                                        for (int r = 0; r < coefficentValue.Length; r++)
                                        {
                                            if (r >= 0 && r <= 24)//pressure
                                            {
                                                coefficentValue[r] = tempCoefficentValuesFromPressureFile[r + 1];
                                            }
                                            if (r >= 25 && r <= 28) //air temp
                                            {
                                                if (r == 27)
                                                { coefficentValue[r] = "0.00000e+00"; }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromAirTempFile[counterAT];
                                                    counterAT++;
                                                }
                                            }
                                            if (r >= 29 && r <= 32)//humidity temp
                                            {
                                                if (r == 31)
                                                { coefficentValue[r] = "0.00000e+00"; }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromHumidityTempFile[counterUT];
                                                    counterUT++;
                                                }
                                            }
                                            if (r >= 35 && r <= 42)
                                            {
                                                if (r == 35)
                                                {
                                                    coefficentValue[43] = tempCoefficentValuesFromHumidityFile[counterHumidity];
                                                    counterHumidity++;
                                                }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromHumidityFile[counterHumidity];
                                                    counterHumidity++;
                                                }
                                            }
                                        }

                                    }

                                    if (tempCoefficentValuesFromHumidityFile.Length != 6 || tempCoefficentValuesFromHumidityFile.Length != 9)
                                    {

                                    }

                                    //Removing blank spaces and compacting down the coefficents.
                                    for (int r = 0; r < coefficentValue.Length; r++)
                                    {
                                        if (coefficentValue[r] != null)
                                        {
                                            counterCoefficentCompact++;
                                        }
                                    }
                                    tempCoefficentNumberIndex = new int[counterCoefficentCompact];
                                    tempCoefficentValue = new string[counterCoefficentCompact];
                                    counterGoodData = 0;
                                    for (int p = 0; p < coefficentValue.Length; p++)
                                    {
                                        if (coefficentValue[p] != null)
                                        {
                                            tempCoefficentNumberIndex[counterGoodData] = coefficentIndexNumber[p];
                                            tempCoefficentValue[counterGoodData] = coefficentValue[p];
                                            counterGoodData++;
                                        }
                                    }
                                    coefficentIndexNumber = null;
                                    coefficentValue = null;
                                    coefficentIndexNumber = tempCoefficentNumberIndex;
                                    coefficentValue = tempCoefficentValue;
                                    counterCoefficentCompact = 0;
                                    break;
                                default:
                                    return;

                            }
                        NoFileError:
                            //Updating the screen to let the user know what is going on.
                            //Determining the rack and pos of the sonde currently being worked on.
                            determinSondeRackPos(ref radiosondeRackLOC, ref radiosondeRackPOSLOC, currentSondeInArray);
                            /*
                            for (int m = 0; m < (dataGridViewRackA.Rows.Count + dataGridViewRackB.Rows.Count + dataGridViewRackC.Rows.Count + dataGridViewRackD.Rows.Count); m++)
                            {
                                if (dataGridViewRackA.Rows[0].Cells[1].Value != null && m < dataGridViewRackA.Rows.Count)
                                {
                                    if (dataGridViewRackA.Rows[m].Cells[1].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                                    {
                                        radiosondeRackLOC = 1;
                                        radiosondeRackPOSLOC = m;
                                    }
                                }
                                if (dataGridViewRackB.Rows[0].Cells[1].Value != null && m < dataGridViewRackB.Rows.Count)
                                {
                                    if (dataGridViewRackB.Rows[m].Cells[1].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                                    {
                                        radiosondeRackLOC = 2;
                                        radiosondeRackPOSLOC = m;
                                    }
                                }

                                if (dataGridViewRackC.Rows[0].Cells[1].Value != null && m < dataGridViewRackC.Rows.Count)
                                {
                                    if (dataGridViewRackC.Rows[m].Cells[1].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                                    {
                                        radiosondeRackLOC = 3;
                                        radiosondeRackPOSLOC = m;
                                    }
                                }

                                if (dataGridViewRackD.Rows[0].Cells[1].Value != null && m < dataGridViewRackD.Rows.Count)
                                {
                                    if (dataGridViewRackD.Rows[m].Cells[1].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                                    {
                                        radiosondeRackLOC = 4;
                                        radiosondeRackPOSLOC = m;
                                    }
                                }
                            }
                            */

                            colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Testing"); //Changing the color to indicate working on this radiosonde.
                            UpdateRackStatusLabels(radiosondeRackLOC, "Sending coefficents to: " + ValidSondeArryiMet1[currentSondeInArray].getTrackingID());
                            UpdateMainStatusLabel("Sending coefficents to: " + ValidSondeArryiMet1[currentSondeInArray].getTrackingID());

                            //Sending out coefficent data to the radiosonde to be written.
                            if (tempCoefficentValuesFromFile != null)
                            {
                                if (serialNumbers[0].Length > 1)
                                {
                                    loadCoefficentResults = ValidSondeArryiMet1[currentSondeInArray].setCoefficentArray2(coefficentIndexNumber, coefficentValue, false);
                                }
                                else
                                {
                                    loadCoefficentResults = ValidSondeArryiMet1[currentSondeInArray].setCoefficentArray2(coefficentIndexNumber, coefficentValue, true);

                                }
                            }

                            if (loadCoefficentResults[0] == "Error")
                            {
                                colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Fail");
                                UpdateRackStatusLabels(radiosondeRackLOC, "Writing coefficents failed on Radiosonde: " + ValidSondeArryiMet1[currentSondeInArray].getTrackingID());
                                UpdateMainStatusLabel("Writing coefficents failed on Radiosonde: " + ValidSondeArryiMet1[currentSondeInArray].getTrackingID());
                            }
                            else
                            {
                                colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Pass");
                                UpdateRackStatusLabels(radiosondeRackLOC, "Writing coefficents passed on Radiosonde: " + ValidSondeArryiMet1[currentSondeInArray].getTrackingID());
                                UpdateMainStatusLabel("Writing coefficents passed on Radiosonde: " + ValidSondeArryiMet1[currentSondeInArray].getTrackingID());
                            }

                            //Resetting veriable to inital settings for next loop.
                            tempCoefficentValuesFromFile = null;
                            currentSondeInArray = 0;
                            loadCoefficentResults = null;
                            radiosondeRackLOC = 0;
                            radiosondeRackPOSLOC = 0;
                        }
                    }
                    break;
                    #endregion

                case "iMet Un":

                    for (int i = 1; i < serialNumbers.Length; i++)
                    {
                        if (serialNumbers[i] == null) //Breaking out if there is no more serial number to process.
                        {
                            break;
                        }
                        if ((worker.CancellationPending == true))
                        {
                            e.Cancel = true;
                            break;
                        }
                        else
                        {
                            //Looking to find the correct radiosonde
                            for (int m = 0; m < ValidSondeiMet1U.Length && serialNumbers[i] != null; m++)
                            {
                                if ((worker.CancellationPending == true))
                                {
                                    e.Cancel = true;
                                    break;
                                }
                                else
                                {
                                    if (ValidSondeiMet1U[m].CalData.SerialNumber == serialNumbers[i])
                                    {
                                        currentSondeInArray = m;
                                    }
                                }
                            }

                            switch (Convert.ToInt16(serialNumbers[0]))//Setting up array and loading coefficents form file.
                            {
                                case 0: //Pressure
                                    coefficentValue = new string[25];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber + ".p_cal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileErrorU;
                                    }
                                    for (int r = 0; r < coefficentValue.Length; r++)
                                    {
                                        coefficentValue[r] = tempCoefficentValuesFromFile[r + 1];
                                    }
                                    break;
                                case 1: //Air Temp
                                    coefficentValue = new string[4];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeiMet1U[currentSondeInArray].CalData.ProbeID + "_AT.tcal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileErrorU;
                                    }
                                    coefficentValue[0] = tempCoefficentValuesFromFile[1];
                                    coefficentValue[1] = tempCoefficentValuesFromFile[2];
                                    coefficentValue[2] = "0.00000e+00";
                                    coefficentValue[3] = tempCoefficentValuesFromFile[3];
                                    break;
                                case 2: //Humidity Temp
                                    coefficentValue = new string[4];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeiMet1U[currentSondeInArray].CalData.ProbeID + "_UT.tcal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileErrorU;
                                    }
                                    coefficentValue[0] = tempCoefficentValuesFromFile[1];
                                    coefficentValue[1] = tempCoefficentValuesFromFile[2];
                                    coefficentValue[2] = "0.00000e+00";
                                    coefficentValue[3] = tempCoefficentValuesFromFile[3];
                                    break;
                                case 3: //e+e humidity coefficents
                                    coefficentValue = new string[5];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber + ".u_cal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileErrorU;
                                    }
                                    for (int r = 0; r < coefficentValue.Length; r++)
                                    {
                                        coefficentValue[r] = tempCoefficentValuesFromFile[r + 1];
                                    }
                                    //formatting coefficent file string to be accepted by the radiosonde class.
                                    coefficentValue[2] = (Convert.ToDouble(coefficentValue[2]) / 10).ToString() + "E+01";
                                    coefficentValue[4] = (Convert.ToDouble(coefficentValue[4]) / 10).ToString() + "E+01";
                                    break;
                                case 4: //humirel coefficents
                                    coefficentValue = new string[8];
                                    try
                                    {
                                        tempCoefficentValuesFromFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber + ".u_cal");
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileErrorU;
                                    }
                                    for (int r = 0; r < coefficentValue.Length; r++)
                                    {
                                        coefficentValue[r] = tempCoefficentValuesFromFile[r + 1];
                                    }
                                    break;
                                case 5:
                                    coefficentValue = new string[44];
                                    try
                                    {
                                        tempCoefficentValuesFromPressureFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber + ".p_cal");
                                        tempCoefficentValuesFromAirTempFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeiMet1U[currentSondeInArray].CalData.ProbeID + "_AT.tcal");
                                        //tempCoefficentValuesFromHumidityTempFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + ValidSondeiMet1U[currentSondeInArray].CalData.ProbeID + "_UT.tcal");
                                        tempCoefficentValuesFromHumidityFile = File.ReadAllLines(ProgramSettings.coefficentStorageLocation + "_" + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber + ".u_cal");
                                        tempCoefficentValuesFromFile = new string[1];
                                    }
                                    catch
                                    {
                                        loadCoefficentResults = new string[1];
                                        loadCoefficentResults[0] = "Error";
                                        goto NoFileErrorU;
                                    }

                                    if (coefficentIndexNumber.Length != 44)
                                    {
                                        coefficentIndexNumber = null;
                                        coefficentIndexNumber = new int[44];
                                        for (int t = 0; t < coefficentIndexNumber.Length; t++)
                                        {
                                            coefficentIndexNumber[t] = t;
                                        }
                                    }

                                    if (tempCoefficentValuesFromHumidityFile.Length == 6)//If the coefficent file is for e+e.
                                    {

                                        counterAT = 1;
                                        counterUT = 1;
                                        counterHumidity = 1;
                                        for (int r = 0; r < coefficentValue.Length; r++)
                                        {
                                            if (r >= 0 && r <= 24)
                                            {
                                                coefficentValue[r] = tempCoefficentValuesFromPressureFile[r + 1];
                                            }
                                            if (r >= 25 && r <= 28)
                                            {
                                                if (r == 27)
                                                { coefficentValue[r] = "0.00000e+00"; }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromAirTempFile[counterAT];
                                                    counterAT++;
                                                }
                                            }
                                            /*
                                            if (r >= 29 && r <= 32)
                                            {
                                                if (r == 31)
                                                { coefficentValue[r] = "0.00000e+00"; }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromHumidityTempFile[counterUT];
                                                    counterUT++;
                                                }
                                            }
                                            */ 
                                            if (r >= 36 && r <= 40)
                                            {
                                                //formatting coefficent file string to be accepted by the radiosonde class.
                                                if (counterHumidity == 3) { tempCoefficentValuesFromHumidityFile[3] = (Convert.ToDouble(tempCoefficentValuesFromHumidityFile[3]) / 10).ToString() + "E+01"; }
                                                if (counterHumidity == 5) { tempCoefficentValuesFromHumidityFile[5] = (Convert.ToDouble(tempCoefficentValuesFromHumidityFile[5]) / 10).ToString() + "E+01"; }

                                                coefficentValue[r] = tempCoefficentValuesFromHumidityFile[counterHumidity];
                                                counterHumidity++;
                                            }
                                        }
                                    }

                                    if (tempCoefficentValuesFromHumidityFile.Length == 9)//If the coefficent file is for humeral sensors.
                                    {
                                        counterAT = 1;
                                        counterUT = 1;
                                        counterHumidity = 1;
                                        for (int r = 0; r < coefficentValue.Length; r++)
                                        {
                                            if (r >= 0 && r <= 24)//pressure
                                            {
                                                coefficentValue[r] = tempCoefficentValuesFromPressureFile[r + 1];
                                            }
                                            if (r >= 25 && r <= 28) //air temp
                                            {
                                                if (r == 27)
                                                { coefficentValue[r] = "0.00000e+00"; }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromAirTempFile[counterAT];
                                                    counterAT++;
                                                }
                                            }
                                            if (r >= 29 && r <= 32)//humidity temp
                                            {
                                                if (r == 31)
                                                { coefficentValue[r] = "0.00000e+00"; }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromHumidityTempFile[counterUT];
                                                    counterUT++;
                                                }
                                            }
                                            if (r >= 35 && r <= 42)
                                            {
                                                if (r == 35)
                                                {
                                                    coefficentValue[43] = tempCoefficentValuesFromHumidityFile[counterHumidity];
                                                    counterHumidity++;
                                                }
                                                else
                                                {
                                                    coefficentValue[r] = tempCoefficentValuesFromHumidityFile[counterHumidity];
                                                    counterHumidity++;
                                                }
                                            }
                                        }

                                    }

                                    if (tempCoefficentValuesFromHumidityFile.Length != 6 || tempCoefficentValuesFromHumidityFile.Length != 9)
                                    {

                                    }

                                    //Removing blank spaces and compacting down the coefficents.
                                    for (int r = 0; r < coefficentValue.Length; r++)
                                    {
                                        if (coefficentValue[r] != null)
                                        {
                                            counterCoefficentCompact++;
                                        }
                                    }
                                    tempCoefficentNumberIndex = new int[counterCoefficentCompact];
                                    tempCoefficentValue = new string[counterCoefficentCompact];
                                    counterGoodData = 0;
                                    for (int p = 0; p < coefficentValue.Length; p++)
                                    {
                                        if (coefficentValue[p] != null)
                                        {
                                            tempCoefficentNumberIndex[counterGoodData] = coefficentIndexNumber[p];
                                            tempCoefficentValue[counterGoodData] = coefficentValue[p];
                                            counterGoodData++;
                                        }
                                    }
                                    coefficentIndexNumber = null;
                                    coefficentValue = null;
                                    coefficentIndexNumber = tempCoefficentNumberIndex;
                                    coefficentValue = tempCoefficentValue;
                                    counterCoefficentCompact = 0;
                                    break;
                                default:
                                    return;

                            }
                        NoFileErrorU:
                            //Updating the screen to let the user know what is going on.
                            //Determining the rack and pos of the sonde currently being worked on.
                            determinSondeRackPos(ref radiosondeRackLOC, ref radiosondeRackPOSLOC, currentSondeInArray);
                            /*
                            for (int m = 0; m < (dataGridViewRackA.Rows.Count + dataGridViewRackB.Rows.Count + dataGridViewRackC.Rows.Count + dataGridViewRackD.Rows.Count); m++)
                            {
                                if (dataGridViewRackA.Rows[0].Cells[1].Value != null && m < dataGridViewRackA.Rows.Count)
                                {
                                    if (dataGridViewRackA.Rows[m].Cells[1].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                                    {
                                        radiosondeRackLOC = 1;
                                        radiosondeRackPOSLOC = m;
                                    }
                                }
                                if (dataGridViewRackB.Rows[0].Cells[1].Value != null && m < dataGridViewRackB.Rows.Count)
                                {
                                    if (dataGridViewRackB.Rows[m].Cells[1].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                                    {
                                        radiosondeRackLOC = 2;
                                        radiosondeRackPOSLOC = m;
                                    }
                                }

                                if (dataGridViewRackC.Rows[0].Cells[1].Value != null && m < dataGridViewRackC.Rows.Count)
                                {
                                    if (dataGridViewRackC.Rows[m].Cells[1].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                                    {
                                        radiosondeRackLOC = 3;
                                        radiosondeRackPOSLOC = m;
                                    }
                                }

                                if (dataGridViewRackD.Rows[0].Cells[1].Value != null && m < dataGridViewRackD.Rows.Count)
                                {
                                    if (dataGridViewRackD.Rows[m].Cells[1].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                                    {
                                        radiosondeRackLOC = 4;
                                        radiosondeRackPOSLOC = m;
                                    }
                                }
                            }
                            */

                            colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Testing"); //Changing the color to indicate working on this radiosonde.
                            UpdateRackStatusLabels(radiosondeRackLOC, "Sending coefficents to: " + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber);
                            UpdateMainStatusLabel("Sending coefficents to: " + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber);

                            //Sending out coefficent data to the radiosonde to be written.
                            if (tempCoefficentValuesFromFile != null)
                            {

                                try
                                {
                                    for (int cof = 0; cof < coefficentIndexNumber.Length; cof++)
                                    {
                                        int currentCoef = coefficentIndexNumber[cof];
                                        double currentData = Convert.ToDouble(coefficentValue[cof]);
                                        ValidSondeiMet1U[currentSondeInArray].setCoefficient(currentCoef, currentData);
                                    }

                                    colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Pass");
                                    UpdateRackStatusLabels(radiosondeRackLOC, "Writing coefficents passed on Radiosonde: " + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber);
                                    UpdateMainStatusLabel("Writing coefficents passed on Radiosonde: " + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber);
                                }
                                catch
                                {
                                    colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Fail");
                                    UpdateRackStatusLabels(radiosondeRackLOC, "Writing coefficents failed on Radiosonde: " + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber);
                                    UpdateMainStatusLabel("Writing coefficents failed on Radiosonde: " + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber);

                                }

                            }
                            else
                            {
                                colorPOSCell(radiosondeRackLOC, radiosondeRackPOSLOC, "Fail");
                                UpdateRackStatusLabels(radiosondeRackLOC, "Writing coefficents failed on Radiosonde: " + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber);
                                UpdateMainStatusLabel("Writing coefficents failed on Radiosonde: " + ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber);
                            }

                            
                          

                            //Resetting veriable to inital settings for next loop.
                            tempCoefficentValuesFromFile = null;
                            currentSondeInArray = 0;
                            loadCoefficentResults = null;
                            radiosondeRackLOC = 0;
                            radiosondeRackPOSLOC = 0;
                        }
                        
                    }

                    break;

            }

        }

        private void loadRadiosondeCoefficents_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Resetting global coefficent background worker array.
            bool[] allBackgroundWorkersComplete = new bool[loadRadiosondeCoefficent.Length];
            int completeWorkersCounter = 0;
            for (int i = 0; i < loadRadiosondeCoefficent.Length; i++)
            {
                if (loadRadiosondeCoefficent[i].IsBusy == false)
                {
                    allBackgroundWorkersComplete[i] = true;
                }
            }

            for (int i = 0; i < allBackgroundWorkersComplete.Length; i++)
            {
                completeWorkersCounter = completeWorkersCounter + Convert.ToInt16(allBackgroundWorkersComplete[i]);
            }

            if (completeWorkersCounter == loadRadiosondeCoefficent.Length)
            {
                loadRadiosondeCoefficent = null; //Resetting background worker array.
                UpdateMainStatusLabel("Clearing all radiosonde buffers. Please stand by.");

                //Clearing all radiosonde buffers.
                switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
                {
                    case "iMet-TX":
                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            ValidSondeArryiMet1[i].ComPort.DiscardInBuffer();
                        }
                        break;
                }

                RadiosondeStatus.processRadiosondeHex = true; //Restarting data collection.
                UpdateMainStatusLabel("All coefficent processes completed");

                //Enabling Load coefficent buttons.
                enableCoefficentButtons();
            }


        }
        #endregion

        private bool checkForRepeatSerialNumbers()
        {
            List<string> repeatSerialNumers = new List<string>(); //List of repeat serial numbers
            string[] radiosondeSerialNumers = null;

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":
                    radiosondeSerialNumers = new string[ValidSondeArryiMet1.Length];
                    
                for (int i = 0; i < radiosondeSerialNumers.Length; i++)
                {
                    radiosondeSerialNumers[i] = ValidSondeArryiMet1[i].getTrackingID();
                }

                for (int i = 0; i < radiosondeSerialNumers.Length; i++)
                {
                    for (int j = 0; j < radiosondeSerialNumers.Length; j++)
                    {
                        if (radiosondeSerialNumers[i] == radiosondeSerialNumers[j] && i != j)
                        {
                            repeatSerialNumers.Add(ValidSondeArryiMet1[j].getTrackingID());
                        }
                    }
                }

                break;

                case "iMet Un":
                radiosondeSerialNumers = new string[ValidSondeiMet1U.Length];

                for (int i = 0; i < radiosondeSerialNumers.Length; i++)
                {
                    radiosondeSerialNumers[i] =ValidSondeiMet1U[i].CalData.SerialNumber;
                }

                for (int i = 0; i < radiosondeSerialNumers.Length; i++)
                {
                    for (int j = 0; j < radiosondeSerialNumers.Length; j++)
                    {
                        if (radiosondeSerialNumers[i] == radiosondeSerialNumers[j] && i != j)
                        {
                            repeatSerialNumers.Add(ValidSondeiMet1U[i].CalData.SerialNumber);
                        }
                    }
                }

                break;
            }

            repeatSerialNumers.Sort();
            string repeatSNReport = string.Join(",", repeatSerialNumers.ToArray());
            if (repeatSerialNumers.Count > 0)
            {
                MessageBox.Show("Repeat Radiosondes Found.\n" + repeatSNReport,"Repeat Radiosonde",MessageBoxButtons.OK,MessageBoxIcon.Error);
                repeatSerialNumers.Clear();
                return true;
            }
            return false;
                    

        }

        private bool checkForRepeat(string currentRadiosondeElementID, string SondeElement)
        {

            List<string> repeatSondeElement = new List<string>(); //List of repeat sonde elements.

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0,7))
            {
                case "iMet-TX":
                    #region Repeat elements for iMet-1 Legacy
                    if (SondeElement == "Serial Number")
                    {
                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (currentRadiosondeElementID == ValidSondeArryiMet1[i].getSerialNum())
                            {
                                repeatSondeElement.Add(ValidSondeArryiMet1[i].getSerialNum());
                            }
                        }
                    }

                    if (SondeElement == "Tracking Number")
                    {
                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (currentRadiosondeElementID == ValidSondeArryiMet1[i].getTrackingID())
                            {
                                repeatSondeElement.Add(ValidSondeArryiMet1[i].getTrackingID());
                            }
                        }
                    }

                    if (SondeElement == "Probe Number")
                    {
                        for (int i = 0; i < ValidSondeArryiMet1.Length; i++)
                        {
                            if (currentRadiosondeElementID == ValidSondeArryiMet1[i].getTUProbeID())
                            {
                                repeatSondeElement.Add(ValidSondeArryiMet1[i].getTUProbeID());
                            }
                        }
                    }

                    if (repeatSondeElement.Count > 1)
                    {
                        return true;
                    }

                    return false;

                    #endregion

                case "iMet Un":
                    #region Repeat elements for iMet-1 Uni
                    if (SondeElement == "Serial Number")
                    {
                        for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                        {
                            if (currentRadiosondeElementID == ValidSondeiMet1U[i].CalData.SondeID)
                            {
                                repeatSondeElement.Add(ValidSondeiMet1U[i].CalData.SondeID);
                            }
                        }
                    }

                    if (SondeElement == "Tracking Number")
                    {
                        for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                        {
                            if (currentRadiosondeElementID == ValidSondeiMet1U[i].CalData.SerialNumber)
                            {
                                repeatSondeElement.Add(ValidSondeiMet1U[i].CalData.SerialNumber);
                            }
                        }
                    }

                    if (SondeElement == "Probe Number")
                    {
                        for (int i = 0; i < ValidSondeiMet1U.Length; i++)
                        {
                            if (currentRadiosondeElementID == ValidSondeiMet1U[i].CalData.ProbeID)
                            {
                                repeatSondeElement.Add(ValidSondeiMet1U[i].CalData.ProbeID);
                            }
                        }
                    }

                    if (repeatSondeElement.Count > 1)
                    {
                        return true;
                    }

                    return false;
                    #endregion
            }

            //Catch all
            return false;

        }

        private void colorFillLineBackground(int Rack, int rowNumber)
        {
            if (Rack == 1)
            {
                for (int colorThem = 0; colorThem < dataGridViewRackA.Rows[rowNumber].Cells.Count; colorThem++)
                {
                    dataGridViewRackA.Rows[rowNumber].Cells[colorThem].Style.BackColor = System.Drawing.Color.Yellow;

                }
            }
        }

        public void colorClearLineBackground(int Rack, int rowNumber)
        {
            if (Rack == 1)
            {
                for (int colorThem = 0; colorThem < dataGridViewRackA.Rows[rowNumber].Cells.Count; colorThem++)
                {
                    dataGridViewRackA.Rows[rowNumber].Cells[colorThem].Style.BackColor = System.Drawing.Color.White;

                }
            }
        }

        public void determinSondeRackPos(ref int radiosondeRackLOC, ref int radiosondeRackPOSLOC, int currentSondeInArray)
        {
            //Determining the rack and pos of the sonde currently being worked on.

            switch (RadiosondeStatus.currentRadiosondeType.Substring(0, 7))
            {
                case "iMet-TX":
                    for (int m = 0; m < (dataGridViewRackA.Rows.Count + dataGridViewRackB.Rows.Count + dataGridViewRackC.Rows.Count + dataGridViewRackD.Rows.Count); m++)
                    {
                        if (dataGridViewRackA.Rows[0].Cells[1].Value != null && m < dataGridViewRackA.Rows.Count)
                        {
                            if (dataGridViewRackA.Rows[m].Cells[2].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                            {
                                radiosondeRackLOC = 1;
                                radiosondeRackPOSLOC = m;
                            }
                        }
                        if (dataGridViewRackB.Rows[0].Cells[1].Value != null && m < dataGridViewRackB.Rows.Count)
                        {
                            if (dataGridViewRackB.Rows[m].Cells[2].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                            {
                                radiosondeRackLOC = 2;
                                radiosondeRackPOSLOC = m;
                            }
                        }

                        if (dataGridViewRackC.Rows[0].Cells[1].Value != null && m < dataGridViewRackC.Rows.Count)
                        {
                            if (dataGridViewRackC.Rows[m].Cells[2].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                            {
                                radiosondeRackLOC = 3;
                                radiosondeRackPOSLOC = m;
                            }
                        }

                        if (dataGridViewRackD.Rows[0].Cells[1].Value != null && m < dataGridViewRackD.Rows.Count)
                        {
                            if (dataGridViewRackD.Rows[m].Cells[2].Value.ToString() == ValidSondeArryiMet1[currentSondeInArray].getTrackingID())
                            {
                                radiosondeRackLOC = 4;
                                radiosondeRackPOSLOC = m;
                            }
                        }
                    }
                    break;
                case "iMet Un":
                    for (int m = 0; m < (dataGridViewRackA.Rows.Count + dataGridViewRackB.Rows.Count + dataGridViewRackC.Rows.Count + dataGridViewRackD.Rows.Count); m++)
                    {
                        if (dataGridViewRackA.Rows[0].Cells[1].Value != null && m < dataGridViewRackA.Rows.Count)
                        {
                            if (dataGridViewRackA.Rows[m].Cells[2].Value.ToString() == ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber)
                            {
                                radiosondeRackLOC = 1;
                                radiosondeRackPOSLOC = m;
                            }
                        }
                        if (dataGridViewRackB.Rows[0].Cells[1].Value != null && m < dataGridViewRackB.Rows.Count)
                        {
                            if (dataGridViewRackB.Rows[m].Cells[2].Value.ToString() == ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber)
                            {
                                radiosondeRackLOC = 2;
                                radiosondeRackPOSLOC = m;
                            }
                        }

                        if (dataGridViewRackC.Rows[0].Cells[1].Value != null && m < dataGridViewRackC.Rows.Count)
                        {
                            if (dataGridViewRackC.Rows[m].Cells[2].Value.ToString() == ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber)
                            {
                                radiosondeRackLOC = 3;
                                radiosondeRackPOSLOC = m;
                            }
                        }

                        if (dataGridViewRackD.Rows[0].Cells[1].Value != null && m < dataGridViewRackD.Rows.Count)
                        {
                            if (dataGridViewRackD.Rows[m].Cells[2].Value.ToString() == ValidSondeiMet1U[currentSondeInArray].CalData.SerialNumber)
                            {
                                radiosondeRackLOC = 4;
                                radiosondeRackPOSLOC = m;
                            }
                        }
                    }

                    break;
            }
        }

        public void colorPOSCell(int Rack, int rowNumber, string condition)
        {
            Color conditonColor = System.Drawing.Color.White;
            DataGridView rackDataGrid;
            
            //Selecting rack.
            switch (Rack)
            {
                case 1:
                    rackDataGrid = dataGridViewRackA;
                    break;
                case 2:
                    rackDataGrid = dataGridViewRackB;
                    break;
                case 3:
                    rackDataGrid = dataGridViewRackC;
                    break;
                case 4:
                    rackDataGrid = dataGridViewRackD;
                    break;
                default:
                    return;
            }

            //Assigning color to the condition.
            if (condition == "Testing")
            { conditonColor = System.Drawing.Color.Yellow; }
            if (condition == "Fail")
            { conditonColor = ProgramSettings.colorFail; }
            if (condition == "Pass")
            { conditonColor = ProgramSettings.colorPass; }
            if (condition == "Clear")
            { conditonColor = System.Drawing.Color.White; }

            //Appling the color to the data gride.
            rackDataGrid.Rows[rowNumber].Cells[0].Style.BackColor = conditonColor ;

        }

        private void clearPastTestReported()
        {
            
            if (dataGridViewRackA.Rows[0].Cells[1].Value != null)
            {
                for (int i = 0; i < dataGridViewRackA.Rows.Count; i++)
                {
                    colorPOSCell(1, i, "Clear");
                }
            }
            if (dataGridViewRackB.Rows[0].Cells[1].Value != null)
            {
                for (int i = 0; i < dataGridViewRackB.Rows.Count; i++)
                {
                    colorPOSCell(2, i, "Clear");
                }
            }
            if (dataGridViewRackC.Rows[0].Cells[1].Value != null)
            {
                for (int i = 0; i < dataGridViewRackC.Rows.Count; i++)
                {
                    colorPOSCell(3, i, "Clear");
                }
            }
            if (dataGridViewRackD.Rows[0].Cells[1].Value != null)
            {
                for (int i = 0; i < dataGridViewRackD.Rows.Count; i++)
                {
                    colorPOSCell(4, i, "Clear");
                }
            }
            
            
        }

        #endregion       

        private void changeUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string value = "";
            if (InputBox("User Name", "Your Name:", ref value) == DialogResult.OK)
            {
                ProgramSettings.CurrentUser = value;
                UpdateMainStatusLabel("User changed to: " + value);
            }
        }

        private void buttonLoadCancel_Click(object sender, EventArgs e)
        {
            UpdateMainStatusLabel("Stopping all coeffiecent being loaded. Current coefficent being loaded to radiosonde will continue.");

            for (int i = 0; i < loadRadiosondeCoefficent.Length; i++)
            {
                loadRadiosondeCoefficent[i].CancelAsync();
            }

        }

        private void comboBoxSondeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadiosondeStatus.currentRadiosondeType = comboBoxSondeType.SelectedItem.ToString();
        }

        private void sondeProgrammingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creating new instance of the programming display
            programmingDisplay = new frmProgrammingDisplay();

            //Sending valid radiosondes to the programming page.
            programmingDisplay.ValidSondeArryiMet1 = ValidSondeArryiMet1;

            //Displaying the form
            programmingDisplay.Show(this);

            //Disableing the menu options so that only one window can be open at a time.
            sondeProgrammingToolStripMenuItem.Enabled = false;

            UpdateMainStatusLabel("Radiosonde Programmer Open.");
        }

        private void sondeAuxSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Creating new instance of the programming display
            auxSettings = new frmUniSettings();

            //Displaying the form
            auxSettings.Show(this);

            //Disableing the menu options so that only one window can be open at a time.
            sondeAuxSettingsToolStripMenuItem.Enabled = false;

            UpdateMainStatusLabel("Radiosonde TX Settings Open.");
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            throw new Exception("This is a test error");

            //ValidSondeiMet1U[0].setTXMode("IMS");
            //ValidSondeiMet1U[0].setTXMode("Bel202");
            

        }

        private void saveSnapshotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Checking to make sure there are active radiosondes.
            if (ValidSondeArryiMet1 == null)
            {
                if (ValidSondeiMet1U == null)
                {
                    MessageBox.Show("No Active Radiosondes.", "Snapshot Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (ValidSondeiMet1U == null)
            {
                if (ValidSondeArryiMet1 == null)
                {
                    MessageBox.Show("No Active Radiosondes.", "Snapshot Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            //Ask user to define save loacation and filename.
            SaveFileDialog saveSnapshot = new SaveFileDialog();
            saveSnapshot.Filter = "CSV File (*.csv)|*.csv";
            saveSnapshot.FileName = DateTime.Now.ToString("yyyyMMdd-HHmmss");
            DialogResult results = saveSnapshot.ShowDialog();

            string snapshotFileName = "";

            if (results == DialogResult.OK)
            {
                snapshotFileName = saveSnapshot.FileName;
            }
            else
            {
                return;
            }

            //Compliling data gride into one objects.
            object[] grids = new object[4];
            grids[0] = dataGridViewRackA;
            grids[1] = dataGridViewRackB;
            grids[2] = dataGridViewRackC;
            grids[3] = dataGridViewRackD;

            //Populate array with radiosondes ID's, ptu data, and diff data from data grids
            List<string> snapshotData = new List<string>();

            //Starting file data
            snapshotData.Add("Multisonde Snapshot Report - " + DateTime.Now.ToString("yyyy/MM/dd - HH:mm:ss"));
            snapshotData.Add("");
            snapshotData.Add("POS,SondeSN,SondeTN,SondePN,Com,P_Temp,Pressure,P_Diff,Temp,T_Diff,Humidity,U_Diff,Firmware,TXMode");

            foreach (DataGridView grid in grids)
            {
                if (grid.RowCount > 0 && grid.Rows[0].Cells[4].Value != null)
                {
                    
                    for(int j = 0; j< grid.RowCount; j++)
                    {
                        string totalMessage = "";
                        for (int y = 0; y < 14; y++)
                        {
                            totalMessage = totalMessage + grid.Rows[j].Cells[y].Value + ","; 
                        }
                        snapshotData.Add(totalMessage);
                    }
                }
            }

            //EXE path
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(frmMainDisplay)).CodeBase);

            //Write data to file.
            string[] fileType = saveSnapshot.FileName.Split('.');

            switch (fileType[fileType.Length - 1])
            {
                case "csv": //comma seperated data
                    System.IO.File.WriteAllLines(snapshotFileName, snapshotData.ToArray());
                    break;

                    /*
                     * This just never worked. When I have more time a revisit might be good.
                     * 
                case "xls": //Excel complent file
                    object misValue = System.Reflection.Missing.Value;

                    //Setting up connection to excel
                    Excel.Application excelApp = new Excel.Application();
                    excelApp.Workbooks.Open(path + "\\templet.xls", misValue, misValue, misValue, misValue, misValue, misValue, misValue, misValue, misValue, misValue, misValue, misValue, misValue, misValue);
                    
                    //Data input to workbook.
                    //Starter
                    excelApp.Cells[2, 5] = "Multisonde Snapshot Report";
                    excelApp.Cells[3, 5] = DateTime.Now.ToString("yyyy/MM/dd - HH:mm:ss");

                    //Data starting at 7,1
                    int offset = 5;

                    string[] localdata = snapshotData.ToArray();
                    for (int f = 2; f < localdata.Length; f++)
                    {
                        string[] brakeDownData = localdata[f].Split(',');

                        for (int u = 0; u < brakeDownData.Length; u++)
                        {
                            excelApp.Cells[f + offset, u + 1] = brakeDownData[u];
                        }
                        
                        //If data is bad turn entire line bold
                        if (f > 2)
                        {
                            if (Math.Abs(Convert.ToDouble(brakeDownData[7])) > ProgramSettings.AcceptablePressureDiff ||
                                Math.Abs(Convert.ToDouble(brakeDownData[9])) > ProgramSettings.AcceptableTempDiff ||
                                Math.Abs(Convert.ToDouble(brakeDownData[11])) > ProgramSettings.AcceptableHumidityDiff)
                            {
                                Excel.Range currentRow = (Excel.Range)excelApp.Cells[f + offset, 1];
                                currentRow.EntireRow.Font.Bold = true;
                            }
                            else
                            {
                                Excel.Range currentRow = (Excel.Range)excelApp.Cells[f + offset, 1];
                                currentRow.EntireRow.Font.Bold = false;
                            }
                        }
                        
                        

                    }

                    

                    //Saving the data to desired location.
                    excelApp.SaveWorkspace(saveSnapshot.FileName);
                    
                    //Closing connection to excel
                    excelApp.Workbooks.Close();
                    break;
                    */

            }

            //Updating status to what happen.
            UpdateMainStatusLabel("Snapshot data sent to: " + snapshotFileName);

            System.Diagnostics.Process.Start(snapshotFileName);

        }


        #region Methods for resizing the program
        private void frmMainDisplay_Resize(object sender, EventArgs e)
        {
            int newFormWidth = this.Size.Width;
            int newFromHeight = this.Size.Height;
            //Locking the form width min to 857 and height to 550
            if (newFormWidth < 857) { newFormWidth = 857; }
            if (newFromHeight < 560) { newFromHeight = 560; }

            this.Size = new Size(newFormWidth, newFromHeight);
            tabControlRackDisplay.Size = new Size(this.Size.Width - 217, this.Size.Height - 110);

            //Fixing data grids
            dataGridViewRackA.Size = new Size(tabControlRackDisplay.Width, tabControlRackDisplay.Height - 60);
            dataGridViewRackB.Size = new Size(tabControlRackDisplay.Width, tabControlRackDisplay.Height - 60);
            dataGridViewRackC.Size = new Size(tabControlRackDisplay.Width, tabControlRackDisplay.Height - 60);
            dataGridViewRackD.Size = new Size(tabControlRackDisplay.Width, tabControlRackDisplay.Height - 60);

            //Fixing tab controls
            groupBoxStatus.Location = new Point(this.Size.Width - 211, groupBoxStatus.Location.Y);
            groupBoxCoefficent.Location = new Point(this.Size.Width - 211, this.Size.Height - groupBoxCoefficent.Size.Height - 81);

            //Fixing the bottom labal bar.
        }

        #endregion

        private void sondeRelaySettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sondeRelaySettingsToolStripMenuItem.Enabled = false;
            programRelay = new frmRelay();
            UpdateMainStatusLabel("Relay Program Menu Open");
            programRelay.Show(this);

            
        }

        private void pressureManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.showPressureManager();  //Opening pressure manager
        }



        #region Methods for updating and changing displays



        #endregion


    }
}
