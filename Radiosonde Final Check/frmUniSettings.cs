﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Radiosonde_Final_Check
{
    //Delegate for updating display
    delegate void UpdateDisplay();

    public partial class frmUniSettings : Form
    {

        public frmMainDisplay parent;
        public string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
        public BackgroundWorker backgroundWorkerSetSettings = new BackgroundWorker();

        public frmUniSettings()
        {
            InitializeComponent();
        }

        private void frmUniSettings_Load(object sender, EventArgs e)
        {
            parent = (frmMainDisplay)this.Owner;

            //Setting starting switch picture.
            updateSwitchImage(0);

            //Loading normal settings
            loadNormalSettings();

            //if no valid universal radiosonde are connected then disable all controls.
            if (parent.ValidSondeiMet1U == null)
            {
                disableControl();
            }

            //Background worker
            backgroundWorkerSetSettings.WorkerSupportsCancellation = true;
            backgroundWorkerSetSettings.DoWork += new DoWorkEventHandler(backgroundWorkerSetSettings_DoWork);
            backgroundWorkerSetSettings.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorkerSetSettings_RunWorkerCompleted);

        }

        private void loadNormalSettings()
        {
            //I think this should come from a file, but for right now this will have to do.
            comboBoxSP1.Text = "402.00";
            comboBoxSP2.Text = "403.00";
            comboBoxSP3.Text = "404.00";
            comboBoxSP4.Text = "405.00";
        }

        private void frmUniSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Enableing the menu item to allow this control to be reopened.
            parent.sondeAuxSettingsToolStripMenuItem.Enabled = true;
        }

        #region Methods for updating the display

        public void updateSwitchImage(int curPos)
        {
            panelSwitchDetail.BackgroundImage = Image.FromFile(path.Substring(6, path.Length - 6) + "\\Images\\SP" + curPos.ToString() + ".png");
        }

        /// <summary>
        /// When control is access the combobox is filled with data and graphic is trigger to update.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxSP_Enter(object sender, EventArgs e)
        {
            System.Windows.Forms.ComboBox incoming = (System.Windows.Forms.ComboBox)sender;
            System.Windows.Forms.Control controlObject = (System.Windows.Forms.Control)sender;

            updateSwitchImage(Convert.ToInt16(controlObject.Name.Substring(controlObject.Name.Length - 1, 1)));

            incoming.Items.Clear();
            incoming.Items.Add("402.00");
            incoming.Items.Add("403.00");
            incoming.Items.Add("404.00");
            incoming.Items.Add("404.50");
            incoming.Items.Add("405.00");
            incoming.Items.Add("405.50");
            incoming.Items.Add("406.00");
            incoming.Items.Add("1676.00");
            incoming.Items.Add("1678.00");
            incoming.Items.Add("1680.00");
            incoming.Items.Add("1682.00");
        }


        #endregion

        #region Methods dealing with mouse events in the combo boxes.

        private void comboBoxSP1_MouseEnter(object sender, EventArgs e)
        {
            updateSwitchImage(1);
        }

        private void comboBoxSP2_MouseEnter(object sender, EventArgs e)
        {
            updateSwitchImage(2);
        }


        private void comboBoxSP3_MouseEnter(object sender, EventArgs e)
        {
            updateSwitchImage(3);
        }

        private void comboBoxSP4_MouseEnter(object sender, EventArgs e)
        {
            updateSwitchImage(4);
        }

        #endregion

        private void buttonSet_Click(object sender, EventArgs e)
        {
            //Perventing writing to nothing.
            if (parent.ValidSondeiMet1U == null)
            {
                MessageBox.Show("Unable to set setting.", "Set Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                parent.writeToErrorLog("Unable to set Aux Settings. No Valid Uni Radiosondes.");
                return;
            }

            //Clearing any old things run or loaded.
            for(int i = 0; i<parent.ValidSondeiMet1U.Length; i++)
            {
                parent.colorClearLineBackground(1, i);
            }

            //Disableing controls.
            disableControl();

            //Compliling the freq settings.
            double[] freqs = new double[4];
            freqs[0] = Convert.ToDouble(comboBoxSP1.Text);
            freqs[1] = Convert.ToDouble(comboBoxSP2.Text);
            freqs[2] = Convert.ToDouble(comboBoxSP3.Text);
            freqs[3] = Convert.ToDouble(comboBoxSP4.Text);

            //Starting background worker that will set all the settings.
            backgroundWorkerSetSettings.RunWorkerAsync(freqs);


        }

        private void disableControl()
        {
            buttonSet.Enabled = false;
            groupBoxFreqSettings.Enabled = false;
            groupBoxTXMode.Enabled = false;
            parent.disableCofficentButtons();
        }

        public void controls_On()
        {
            BeginInvoke(new UpdateDisplay(enableControl));
        }

        public void controls_Off()
        {
            BeginInvoke(new UpdateDisplay(disableControl));
        }

        private void enableControl()
        {
            buttonSet.Enabled = true;
            groupBoxFreqSettings.Enabled = true;
            groupBoxTXMode.Enabled = true;
            parent.enableCoefficentButtons();
        }

        void backgroundWorkerSetSettings_DoWork(object sender, DoWorkEventArgs e)
        {
            double[] setFreq = (double[])e.Argument;

            //The radiosonde index counter.
            int counter = 0;




            //Collecting the informationt need to set things.
            string mode = "";
            if (radioButtonIMSMode.Checked)
            {
                mode = "IMS";
            }
            if (radioButtonBell202Mode.Checked)
            {
                mode = "bel202";
            }


            foreach (Universal_Radiosonde.iMet1U sonde in parent.ValidSondeiMet1U)
            {
                if (!backgroundWorkerSetSettings.CancellationPending) //Something to stop the looping if needed.
                {
                    //Indicating that programming is happening
                    parent.colorPOSCell(1, counter, "Testing");

                    //Stopping the sonde from outputting caldata
                    //sonde.setManufacturingMode(false);
                    System.Threading.Thread.Sleep(200); //A little pause to allow the command to take effect.

                    

                    //Setting the freq's
                    parent.UpdateMainStatusLabel("Setting " + sonde.CalData.SerialNumber.ToString() + " to Freq's: " + 
                        setFreq[0].ToString("0.00") + "," + setFreq[1].ToString("0.00") + "," + setFreq[2].ToString("0.00") + "," + setFreq[3].ToString("0.00"));


                    //Setting the mode.
                    parent.UpdateMainStatusLabel("Setting " + sonde.CalData.SerialNumber.ToString() + " to " + mode);
                    bool refResponce = sonde.setTXMode(mode);
                    parent.UpdateMainStatusLabel("Setting Complete");

                    System.Threading.Thread.Sleep(1000);

                    //Set the sonde to the desired freqency.
                    sonde.setTXFreq(setFreq);
                    parent.UpdateMainStatusLabel("Setting Complete");

                    //Indicating that the programing is done.
                    if (refResponce)
                    {
                        parent.colorPOSCell(1, counter, "Pass");
                    }
                    else
                    {
                        parent.colorPOSCell(1, counter, "Fail");
                    }

                    //Restarting calmode data
                    sonde.setManufacturingMode(true);
                    
                    System.Threading.Thread.Sleep(500);

                    counter++;
                }
            }
        }

        void backgroundWorkerSetSettings_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Reenable everything.
            BeginInvoke(new UpdateDisplay(this.enableControl));
        }


    }
}
