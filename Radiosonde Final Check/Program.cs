﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
 

namespace Radiosonde_Final_Check
{
    static class Program
    {
        //Public elements
        public static TestEquipment.Paroscientific sensorPressure;  //Referance Pressure Sensor
        public static frmPressureControl frmPrManager;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            try
            {
                Application.Run(new frmMainDisplay());
            }
            
            catch(Exception e)
            {
                //Writing an error message for debug reasons.
                string logLoc = @"errorMulti.text";
                System.IO.TextWriter loggingError = System.IO.File.AppendText(logLoc);
                DateTime errorTime = DateTime.Now;

                loggingError.WriteLine(errorTime.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                loggingError.WriteLine(e.Source);
                loggingError.WriteLine(e.Message);
                loggingError.WriteLine(e.StackTrace);
                //Another layer if needed.
                if (e.InnerException != null)
                {
                    loggingError.WriteLine(e.InnerException.Source);
                    loggingError.WriteLine(e.InnerException.Message);
                    loggingError.WriteLine(e.InnerException.StackTrace);
                }
                loggingError.WriteLine("");

                loggingError.Close();

                //Email error to Admin
                Alerts.alertsEmail emailAdmin = new Alerts.alertsEmail();

                //Loading Admin and server config.
                string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                

                string[] fileData = System.IO.File.ReadAllLines(path.Substring(6, path.Length - 6) + "\\Admin.cfg");

                /*
                emailAdmin.alertsSendEmail(fileData[6], "Error in Multisonde " + errorTime.ToString("yyyy-MM-dd HH:mm:ss"),
                    "Error at: " + errorTime.ToString("yyyy-MM-dd HH:mm:ss") + " in " + e.Source + "\n" + e.Message +"\nSee log file at: " + logLoc +
                    " for more detail.",
                        fileData[0], fileData[1], fileData[2], fileData[3], fileData[4], Convert.ToBoolean(fileData[5]),false);
                */

                //Telling the user what has happen.
                MessageBox.Show("An Error has occurred. A report has been generated for:\n\n" + e.Message + "\n\n See: " + System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" + logLoc + "\n\n For more information.",
                    "Program Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                
            }
            
        }

        public static void showPressureManager()
        {
            frmPrManager = new frmPressureControl();    //Setting up the control
            frmPrManager.ShowDialog();                  //Displaying form
        }
    }
}
