﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Radiosonde_Final_Check
{
    public partial class frmSettings : Form
    {
        public frmSettings()
        {
            InitializeComponent();
        }

        public static class ProgramSettings  
        {
            public static string CurrentUser = "";

            //Rack "A" com ports
            public static int StartRackARadiosondeComms = 90; //24;
            public static int EndRackARadiosondeComms = 92; //72;

            //Rack "B" com ports
            public static int StartRackBRadiosondeComms = 0;
            public static int EndRackBRadiosondeComms = 0;
            //Rack "C" com ports
            public static int StartRackCRadiosondeComms = 0;
            public static int EndRackCRadiosondeComms = 0;

            //Rack "D" com ports
            public static int StartRackDRadiosondeComms = 0;
            public static int EndRackDRadiosondeComms = 0;

            //Acceptable differances
            public static double AcceptableTempDiff = 0.5;
            public static double AcceptablePressureDiff = 1.8;
            public static double AcceptableHumidityDiff = 8;


            //Referance Sensors Settings.
            public static string SensorComPortGE = "COM3";
            public static string SensorComPortHMP234 = "COM8";
            public static string SensorParoComPort = "COM11";
            public static string SensorThermotronComPort = "COM13";
            public static string SensorComPortEE31 = "COM14";

            //Current sensors in use.
            public static string CurrentTUSensor = "";
            public static string CurrentPressureSensor = "";

            //Error log location
            public static string logErrorLocation = DateTime.Now.ToString("yyyyMMdd") + "-ErrorLog.csv";

            //Coefficent storage loaction
            public static string coefficentStorageLocation = "";
            public static string xCalCoefficentStorageLocation = "";

            //Max amount of load coefficent background workers
            public static int maxNumberOfBackgroundWorkers = 8;

            //Colors for pass, fails, radiosonde types, and other things like that.
            public static Color colorPass = System.Drawing.Color.Green;
            public static Color colorFail = System.Drawing.Color.Red;
            public static Color colorRepeat = System.Drawing.Color.LightSeaGreen;
            public static Color colorRadiosondeiMet1 = System.Drawing.Color.White;
            public static Color colorRadiosondeEnSci = System.Drawing.Color.LightGray;
            public static Color colorRadiosondeElementPass = System.Drawing.Color.White;

            public static bool logOn = false;

        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            loadComPortsIntoComboBox(); //Loading combo boxs with the serial ports.
            loadSettingFromFile(); //Loading in current settings from file.
            updateScreenWithSettings(); //Update the screen with the current loaded settings.
        }

        private void loadSettingFromFile()
        {
            string path = System.IO.Path.GetDirectoryName(
               System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            string configLine = "";

            TextReader ProgramSettingsReader = new StreamReader(path.Substring(6, path.Length - 6) + "\\Settings.cfg");
            // read lines of text

            configLine = ProgramSettingsReader.ReadToEnd();
            configLine = configLine.Replace("\r", "");
            string[] configElement = configLine.Split('\n', '=');

            // close the stream
            ProgramSettingsReader.Close();

            //Setting up the global stuff.
            //rack com ports.


            if (configElement[1].Length == 4)
            { ProgramSettings.StartRackARadiosondeComms = Convert.ToInt16(configElement[1].Substring(3, 1)); }
            if (configElement[1].Length == 5)
            { ProgramSettings.StartRackARadiosondeComms = Convert.ToInt16(configElement[1].Substring(3, 2)); }
            if (configElement[1].Length == 6)
            { ProgramSettings.StartRackARadiosondeComms = Convert.ToInt16(configElement[1].Substring(3, 3)); }

            if (configElement[3].Length == 4)
            { ProgramSettings.EndRackARadiosondeComms = Convert.ToInt16(configElement[3].Substring(3, 1)); }
            if (configElement[3].Length == 5)
            { ProgramSettings.EndRackARadiosondeComms = Convert.ToInt16(configElement[3].Substring(3, 2)); }
            if (configElement[3].Length == 6)
            { ProgramSettings.EndRackARadiosondeComms = Convert.ToInt16(configElement[3].Substring(3, 3)); }

            if (configElement[5].Length == 4)
            { ProgramSettings.StartRackBRadiosondeComms = Convert.ToInt16(configElement[5].Substring(3, 1)); }
            if (configElement[5].Length == 5)
            { ProgramSettings.StartRackBRadiosondeComms = Convert.ToInt16(configElement[5].Substring(3, 2)); }
            if (configElement[5].Length == 6)
            { ProgramSettings.StartRackBRadiosondeComms = Convert.ToInt16(configElement[5].Substring(3, 3)); }

            if (configElement[7].Length == 4)
            { ProgramSettings.EndRackBRadiosondeComms = Convert.ToInt16(configElement[7].Substring(3, 1)); }
            if (configElement[7].Length == 5)
            { ProgramSettings.EndRackBRadiosondeComms = Convert.ToInt16(configElement[7].Substring(3, 2)); }
            if (configElement[7].Length == 6)
            { ProgramSettings.EndRackBRadiosondeComms = Convert.ToInt16(configElement[7].Substring(3, 3)); }

            if (configElement[9].Length == 4)
            { ProgramSettings.StartRackCRadiosondeComms = Convert.ToInt16(configElement[9].Substring(3, 1)); }
            if (configElement[9].Length == 5)
            { ProgramSettings.StartRackCRadiosondeComms = Convert.ToInt16(configElement[9].Substring(3, 2)); }
            if (configElement[9].Length == 6)
            { ProgramSettings.StartRackCRadiosondeComms = Convert.ToInt16(configElement[9].Substring(3, 3)); }

            if (configElement[11].Length == 4)
            { ProgramSettings.EndRackCRadiosondeComms = Convert.ToInt16(configElement[11].Substring(3, 1)); }
            if (configElement[11].Length == 5)
            { ProgramSettings.EndRackCRadiosondeComms = Convert.ToInt16(configElement[11].Substring(3, 2)); }
            if (configElement[11].Length == 6)
            { ProgramSettings.EndRackCRadiosondeComms = Convert.ToInt16(configElement[11].Substring(3, 3)); }

            if (configElement[13].Length == 4)
            { ProgramSettings.StartRackDRadiosondeComms = Convert.ToInt16(configElement[13].Substring(3, 1)); }
            if (configElement[13].Length == 5)
            { ProgramSettings.StartRackDRadiosondeComms = Convert.ToInt16(configElement[13].Substring(3, 2)); }
            if (configElement[13].Length == 6)
            { ProgramSettings.StartRackDRadiosondeComms = Convert.ToInt16(configElement[13].Substring(3, 3)); }

            if (configElement[15].Length == 4)
            { ProgramSettings.EndRackDRadiosondeComms = Convert.ToInt16(configElement[15].Substring(3, 1)); }
            if (configElement[15].Length == 5)
            { ProgramSettings.EndRackDRadiosondeComms = Convert.ToInt16(configElement[15].Substring(3, 2)); }
            if (configElement[15].Length == 6)
            { ProgramSettings.EndRackDRadiosondeComms = Convert.ToInt16(configElement[15].Substring(3, 3)); }

            //Setting sensor coms.
            ProgramSettings.SensorComPortGE = configElement[17];

            ProgramSettings.SensorComPortHMP234 = configElement[19];
            ProgramSettings.SensorParoComPort = configElement[21];
            ProgramSettings.SensorThermotronComPort = configElement[23];

            //Setting acceptable diffs.
            ProgramSettings.AcceptablePressureDiff = Convert.ToDouble(configElement[25]);
            ProgramSettings.AcceptableTempDiff = Convert.ToDouble(configElement[27]);
            ProgramSettings.AcceptableHumidityDiff = Convert.ToDouble(configElement[29]);

            //Setting coefficent storage location
            ProgramSettings.coefficentStorageLocation = configElement[31];

            //Loading colors used in the program
            ProgramSettings.colorPass = Color.FromArgb(Convert.ToInt32(configElement[35]));
            ProgramSettings.colorFail = Color.FromArgb(Convert.ToInt32(configElement[37]));
            ProgramSettings.colorRepeat = Color.FromArgb(Convert.ToInt32(configElement[39]));
            ProgramSettings.colorRadiosondeiMet1 = Color.FromArgb(Convert.ToInt32(configElement[41]));
            ProgramSettings.colorRadiosondeEnSci = Color.FromArgb(Convert.ToInt32(configElement[43]));
            ProgramSettings.colorRadiosondeElementPass = Color.FromArgb(Convert.ToInt32(configElement[45]));

            //Loading the xCal storage location
            ProgramSettings.xCalCoefficentStorageLocation = configElement[47];

            //Loading EE31 sensor Com Port settings.
            ProgramSettings.SensorComPortEE31 = configElement[49];

            //Loading logOn Settings
            ProgramSettings.logOn = Convert.ToBoolean(configElement[51]);
        }

        private void loadComPortsIntoComboBox() //This loads all the aviable com ports into the combo boxes
        {
            string[] allComPorts = System.IO.Ports.SerialPort.GetPortNames();

            //Adding Disable to each com port combo box
            comboBoxRackAStartingCom.Items.Add("Disabled");
            comboBoxRackAEndingCom.Items.Add("Disabled");
            comboBoxRackBStartingCom.Items.Add("Disabled");
            comboBoxRackBEndingCom.Items.Add("Disabled");
            comboBoxRackCStartingCom.Items.Add("Disabled");
            comboBoxRackCEndingCom.Items.Add("Disabled");
            comboBoxRackDStartingCom.Items.Add("Disabled");
            comboBoxRackDEndingCom.Items.Add("Disabled");
            comboBoxChamberComPort.Items.Add("Disabled");
            comboBoxGEHygroComPort.Items.Add("Disabled");
            comboBoxEE31ComPort.Items.Add("Disabled");
            comboBoxGPSComPort.Items.Add("Disabled");
            comboBoxHMP234ComPort.Items.Add("Disabled");
            //comboBoxParoComPort.Items.Add("Disabled");
            comboBoxSpectrumComPort.Items.Add("Disabled");

            foreach (string comPorts in allComPorts)
            {
                comboBoxRackAStartingCom.Items.Add(comPorts);
                comboBoxRackAEndingCom.Items.Add(comPorts);

                comboBoxRackBStartingCom.Items.Add(comPorts);
                comboBoxRackBEndingCom.Items.Add(comPorts);

                comboBoxRackCStartingCom.Items.Add(comPorts);
                comboBoxRackCEndingCom.Items.Add(comPorts);

                comboBoxRackDStartingCom.Items.Add(comPorts);
                comboBoxRackDEndingCom.Items.Add(comPorts);

                comboBoxChamberComPort.Items.Add(comPorts);
                comboBoxGEHygroComPort.Items.Add(comPorts);
                comboBoxEE31ComPort.Items.Add(comPorts);
                comboBoxGPSComPort.Items.Add(comPorts);
                comboBoxHMP234ComPort.Items.Add(comPorts);
                //comboBoxParoComPort.Items.Add(comPorts);
                comboBoxSpectrumComPort.Items.Add(comPorts);
            }

        }

        private void updateScreenWithSettings()
        {
            //Rack comports.
            if(ProgramSettings.StartRackARadiosondeComms != 0)
            {
                comboBoxRackAStartingCom.Text = "COM" + ProgramSettings.StartRackARadiosondeComms.ToString();
                comboBoxRackAEndingCom.Text = "COM" + (ProgramSettings.EndRackARadiosondeComms).ToString();
            }
            else
            {
                comboBoxRackAStartingCom.Text = "Disabled";
                comboBoxRackAEndingCom.Text = "Disabled";
            }

            if(ProgramSettings.StartRackBRadiosondeComms != 0)
            {
                comboBoxRackBStartingCom.Text = "COM" + ProgramSettings.StartRackBRadiosondeComms.ToString();
                comboBoxRackBEndingCom.Text = "COM" + (ProgramSettings.EndRackBRadiosondeComms).ToString();
            }
            else
            {
                comboBoxRackBStartingCom.Text = "Disabled";
                comboBoxRackBEndingCom.Text = "Disabled";
            }

            if (ProgramSettings.StartRackCRadiosondeComms != 0)
            {
                comboBoxRackCStartingCom.Text = "COM" + ProgramSettings.StartRackCRadiosondeComms.ToString();
                comboBoxRackCEndingCom.Text = "COM" + (ProgramSettings.EndRackCRadiosondeComms).ToString();
            }
            else
            {
                comboBoxRackCStartingCom.Text = "Disabled";
                comboBoxRackCEndingCom.Text = "Disabled";
            }

            if (ProgramSettings.StartRackDRadiosondeComms != 0)
            {
                comboBoxRackDStartingCom.Text = "COM" + ProgramSettings.StartRackDRadiosondeComms.ToString();
                comboBoxRackDEndingCom.Text = "COM" + (ProgramSettings.EndRackDRadiosondeComms).ToString();
            }
            else
            {
                comboBoxRackDStartingCom.Text = "Disabled";
                comboBoxRackDEndingCom.Text = "Disabled";
            }
            
            //External sensors.
            comboBoxGEHygroComPort.Text = ProgramSettings.SensorComPortGE.ToString();
            comboBoxHMP234ComPort.Text = ProgramSettings.SensorComPortHMP234.ToString();
            textBoxParoConnection.Text = ProgramSettings.SensorParoComPort.ToString();
            comboBoxChamberComPort.Text = ProgramSettings.SensorThermotronComPort.ToString();
            comboBoxEE31ComPort.Text = ProgramSettings.SensorComPortEE31.ToString();

            //Acceptable Offsets
            textBoxAcceptablePressureDiff.Text = ProgramSettings.AcceptablePressureDiff.ToString("0.000");
            textBoxAcceptableTempDiff.Text = ProgramSettings.AcceptableTempDiff.ToString("0.000");
            textBoxAcceptableHumidityDiff.Text = ProgramSettings.AcceptableHumidityDiff.ToString("0.000");

            //Coefficent Load Location
            textBoxCoefficentStorageLoc.Text = ProgramSettings.coefficentStorageLocation;

            //xCal Coefficent Load Location
            textBoxxCalCoefficentStorageLoc.Text = ProgramSettings.xCalCoefficentStorageLocation;

            //Colors
            updateSettingsColors();


        }

        private void updateSettingsColors()
        {
            //Colors
            panelColorPass.BackColor = ProgramSettings.colorPass;
            panelColorFail.BackColor = ProgramSettings.colorFail;
            panelColoriMet1SN.BackColor = ProgramSettings.colorRadiosondeiMet1;
            panelColorEnSciSN.BackColor = ProgramSettings.colorRadiosondeEnSci;
            panelColorRepeat.BackColor = ProgramSettings.colorRepeat;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            frmSettings.ActiveForm.Dispose();
        }

        #region Code pertaining to selecting a color for an operation.

        private string selectColor()
        {
            ColorDialog colorDlg = new ColorDialog();
            string colorSelected = "";

            if (colorDlg.ShowDialog() == DialogResult.OK)
            {
                colorSelected = colorDlg.Color.ToArgb().ToString();
                return colorSelected;
            }

            return "Error";
        }

        private void panelColorPass_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorPass = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingsColors();
            }
        }

        private void panelColorFail_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorFail = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingsColors();
            }
        }

        private void panelColoriMet1SN_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorRadiosondeiMet1 = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingsColors();
            }
        }

        private void panelColorEnSciSN_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorRadiosondeEnSci = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingsColors();
            }
        }

        private void panelColorRepeat_DoubleClick(object sender, EventArgs e)
        {
            string changeColorTo = "";
            changeColorTo = selectColor();
            if (changeColorTo == "Error")
            {
                return;
            }
            else
            {
                ProgramSettings.colorRepeat = Color.FromArgb(Convert.ToInt32(changeColorTo));
                updateSettingsColors();
            }
        }

        #endregion

        private void buttonOk_Click(object sender, EventArgs e)
        {
            //Error checking
            //Rack com ports.
            ComboBox[] comboBoxRackSettings = new ComboBox[8];
            string comboBoxErrorReport = "";

            //loading combobox array
            comboBoxRackSettings[0] = comboBoxRackAStartingCom;
            comboBoxRackSettings[1] = comboBoxRackAEndingCom;
            comboBoxRackSettings[2] = comboBoxRackBStartingCom;
            comboBoxRackSettings[3] = comboBoxRackBEndingCom;
            comboBoxRackSettings[4] = comboBoxRackCStartingCom;
            comboBoxRackSettings[5] = comboBoxRackCEndingCom;
            comboBoxRackSettings[6] = comboBoxRackDStartingCom;
            comboBoxRackSettings[7] = comboBoxRackDEndingCom;

            for (int r = 0; r < comboBoxRackSettings.Length; r++)
            {
                if (comboBoxRackSettings[r].Text.Length == 0)
                {
                    comboBoxErrorReport = comboBoxErrorReport + comboBoxRackSettings[r].Name.ToString() + " is blank.\n";
                }
                if (comboBoxRackSettings[r].Text.Length >= 3)
                {
                    if (comboBoxRackSettings[r].Text.Substring(0, 3) != "COM" && comboBoxRackSettings[r].Text != "Disabled")
                    {
                        comboBoxErrorReport = comboBoxErrorReport + comboBoxRackSettings[r].Name.ToString() + " is formated incorrectly.\n";
                    }
                }
            }

            if (comboBoxErrorReport != "")
            {
                MessageBox.Show("Rack com port selection error\n" + comboBoxErrorReport + "Please check your setting and try again", "Rack Com's error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return;
            }

            //Checking for disabled racking and setting the globals.
            if (comboBoxRackAStartingCom.Text == "Disabled")
            { ProgramSettings.StartRackARadiosondeComms = 0; }
            else
            {
                if (comboBoxRackAStartingCom.Text.Length == 4)
                { ProgramSettings.StartRackARadiosondeComms = Convert.ToInt16(comboBoxRackAStartingCom.Text.Substring(3, 1)); }
                if (comboBoxRackAStartingCom.Text.Length == 5)
                { ProgramSettings.StartRackARadiosondeComms = Convert.ToInt16(comboBoxRackAStartingCom.Text.Substring(3, 2)); }
                if (comboBoxRackAStartingCom.Text.Length == 6)
                { ProgramSettings.StartRackARadiosondeComms = Convert.ToInt16(comboBoxRackAStartingCom.Text.Substring(3, 3)); }
            }
            if (comboBoxRackAEndingCom.Text == "Disabled")
            { ProgramSettings.EndRackARadiosondeComms = 0; }
            else
            {
                if (comboBoxRackAEndingCom.Text.Length == 4)
                { ProgramSettings.EndRackARadiosondeComms = Convert.ToInt16(comboBoxRackAEndingCom.Text.Substring(3, 1)); }
                if (comboBoxRackAEndingCom.Text.Length == 5)
                { ProgramSettings.EndRackARadiosondeComms = Convert.ToInt16(comboBoxRackAEndingCom.Text.Substring(3, 2)); }
                if (comboBoxRackAEndingCom.Text.Length == 6)
                { ProgramSettings.EndRackARadiosondeComms = Convert.ToInt16(comboBoxRackAEndingCom.Text.Substring(3, 3)); }
            }

            if (comboBoxRackBStartingCom.Text == "Disabled")
            { ProgramSettings.StartRackBRadiosondeComms = 0; }
            else
            {
                if (comboBoxRackBStartingCom.Text.Length == 4)
                { ProgramSettings.StartRackBRadiosondeComms = Convert.ToInt16(comboBoxRackBStartingCom.Text.Substring(3, 1)); }
                if (comboBoxRackBStartingCom.Text.Length == 5)
                { ProgramSettings.StartRackBRadiosondeComms = Convert.ToInt16(comboBoxRackBStartingCom.Text.Substring(3, 2)); }
                if (comboBoxRackBStartingCom.Text.Length == 6)
                { ProgramSettings.StartRackBRadiosondeComms = Convert.ToInt16(comboBoxRackBStartingCom.Text.Substring(3, 3)); }
            }
            if (comboBoxRackBEndingCom.Text == "Disabled")
            { ProgramSettings.EndRackBRadiosondeComms = 0; }
            else
            {
                if (comboBoxRackBEndingCom.Text.Length == 4)
                { ProgramSettings.EndRackBRadiosondeComms = Convert.ToInt16(comboBoxRackBEndingCom.Text.Substring(3, 1)); }
                if (comboBoxRackBEndingCom.Text.Length == 5)
                { ProgramSettings.EndRackBRadiosondeComms = Convert.ToInt16(comboBoxRackBEndingCom.Text.Substring(3, 2)); }
                if (comboBoxRackBEndingCom.Text.Length == 6)
                { ProgramSettings.EndRackBRadiosondeComms = Convert.ToInt16(comboBoxRackBEndingCom.Text.Substring(3, 3)); }
            }

            if (comboBoxRackCStartingCom.Text == "Disabled")
            { ProgramSettings.StartRackCRadiosondeComms = 0; }
            else
            {
                if (comboBoxRackCStartingCom.Text.Length == 4)
                { ProgramSettings.StartRackCRadiosondeComms = Convert.ToInt16(comboBoxRackCStartingCom.Text.Substring(3, 1)); }
                if (comboBoxRackCStartingCom.Text.Length == 5)
                { ProgramSettings.StartRackCRadiosondeComms = Convert.ToInt16(comboBoxRackCStartingCom.Text.Substring(3, 2)); }
                if (comboBoxRackCStartingCom.Text.Length == 6)
                { ProgramSettings.StartRackCRadiosondeComms = Convert.ToInt16(comboBoxRackCStartingCom.Text.Substring(3, 3)); }
            }
            if (comboBoxRackCEndingCom.Text == "Disabled")
            { ProgramSettings.EndRackCRadiosondeComms = 0; }
            else
            {
                if (comboBoxRackCEndingCom.Text.Length == 4)
                { ProgramSettings.EndRackCRadiosondeComms = Convert.ToInt16(comboBoxRackCEndingCom.Text.Substring(3, 1)); }
                if (comboBoxRackCEndingCom.Text.Length == 5)
                { ProgramSettings.EndRackCRadiosondeComms = Convert.ToInt16(comboBoxRackCEndingCom.Text.Substring(3, 2)); }
                if (comboBoxRackCEndingCom.Text.Length == 6)
                { ProgramSettings.EndRackCRadiosondeComms = Convert.ToInt16(comboBoxRackCEndingCom.Text.Substring(3, 3)); }
            }

            if (comboBoxRackDStartingCom.Text == "Disabled")
            { ProgramSettings.StartRackDRadiosondeComms = 0; }
            else
            {
                if (comboBoxRackDStartingCom.Text.Length == 4)
                { ProgramSettings.StartRackDRadiosondeComms = Convert.ToInt16(comboBoxRackDStartingCom.Text.Substring(3, 1)); }
                if (comboBoxRackDStartingCom.Text.Length == 5)
                { ProgramSettings.StartRackDRadiosondeComms = Convert.ToInt16(comboBoxRackDStartingCom.Text.Substring(3, 2)); }
                if (comboBoxRackDStartingCom.Text.Length == 6)
                { ProgramSettings.StartRackDRadiosondeComms = Convert.ToInt16(comboBoxRackDStartingCom.Text.Substring(3, 3)); }
            }
            if (comboBoxRackDEndingCom.Text == "Disabled")
            { ProgramSettings.EndRackDRadiosondeComms = 0; }
            else
            {
                if (comboBoxRackDEndingCom.Text.Length == 4)
                { ProgramSettings.EndRackDRadiosondeComms = Convert.ToInt16(comboBoxRackDEndingCom.Text.Substring(3, 1)); }
                if (comboBoxRackDEndingCom.Text.Length == 5)
                { ProgramSettings.EndRackDRadiosondeComms = Convert.ToInt16(comboBoxRackDEndingCom.Text.Substring(3, 2)); }
                if (comboBoxRackDEndingCom.Text.Length == 6)
                { ProgramSettings.EndRackDRadiosondeComms = Convert.ToInt16(comboBoxRackDEndingCom.Text.Substring(3, 3)); }
            }


            // create a writer and open the file
            FileInfo t = new FileInfo("Settings.cfg");
            StreamWriter programSettingFile =t.CreateText();


            // write a line of text to the file
            programSettingFile.WriteLine("RackAStart=COM"+ ProgramSettings.StartRackARadiosondeComms.ToString());
            programSettingFile.WriteLine("RackAEnd=COM" + ProgramSettings.EndRackARadiosondeComms.ToString());
            programSettingFile.WriteLine("RackBStart=COM" + ProgramSettings.StartRackBRadiosondeComms.ToString());
            programSettingFile.WriteLine("RackBEnd=COM" + ProgramSettings.EndRackBRadiosondeComms.ToString());
            programSettingFile.WriteLine("RackCStart=COM" + ProgramSettings.StartRackCRadiosondeComms.ToString());
            programSettingFile.WriteLine("RackCEnd=COM" + ProgramSettings.EndRackCRadiosondeComms.ToString());
            programSettingFile.WriteLine("RackDStart=COM" + ProgramSettings.StartRackDRadiosondeComms.ToString());
            programSettingFile.WriteLine("RackDEnd=COM" + ProgramSettings.EndRackDRadiosondeComms.ToString());
            programSettingFile.WriteLine("GEHydroComPort=" + comboBoxGEHygroComPort.Text);
            programSettingFile.WriteLine("HMP234COMPORT=" + comboBoxHMP234ComPort.Text);
            programSettingFile.WriteLine("PAROCOMPORT=" + textBoxParoConnection.Text);
            programSettingFile.WriteLine("THERMOTRONCOMPORT=" + comboBoxChamberComPort.Text);
            programSettingFile.WriteLine("AcceptPressure=" + Convert.ToDouble(textBoxAcceptablePressureDiff.Text).ToString("0.000"));
            programSettingFile.WriteLine("AcceptTempature=" + Convert.ToDouble(textBoxAcceptableTempDiff.Text).ToString("0.000"));
            programSettingFile.WriteLine("AcceptHumidity=" + Convert.ToDouble(textBoxAcceptableHumidityDiff.Text).ToString("0.000"));
            programSettingFile.WriteLine("coefficentStorageLocation=" + textBoxCoefficentStorageLoc.Text);
            programSettingFile.WriteLine("//The Following are the colors used in the program.=");
            programSettingFile.WriteLine("Pass=" + ProgramSettings.colorPass.ToArgb().ToString());
            programSettingFile.WriteLine("Fail=" + ProgramSettings.colorFail.ToArgb().ToString());
            programSettingFile.WriteLine("RepeatElement=" + ProgramSettings.colorRepeat.ToArgb().ToString());
            programSettingFile.WriteLine("RadiosondeiMet-1=" + ProgramSettings.colorRadiosondeiMet1.ToArgb().ToString());
            programSettingFile.WriteLine("RadiosondeEnsci=" + ProgramSettings.colorRadiosondeEnSci.ToArgb().ToString());
            programSettingFile.WriteLine("RadiosondeElementPass=" + ProgramSettings.colorRadiosondeElementPass.ToArgb().ToString());
            programSettingFile.WriteLine("xCalXoefficentStorageLocation=" + textBoxxCalCoefficentStorageLoc.Text);
            programSettingFile.WriteLine("EE31COMPORT=" + comboBoxEE31ComPort.Text);
            programSettingFile.WriteLine("logOn=false");

            // close the stream
            programSettingFile.Close();

            frmMainDisplay parent = (frmMainDisplay)this.Owner;
            parent.configUpdate(ProgramSettings.SensorThermotronComPort);


            frmSettings.ActiveForm.Dispose(); //Closing the form after all the processing.
        }

        #region Methods for loading/finding/selecting a folder location
        private void buttonSelectCoefficentLOC_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog selectCoefficentLOC = new FolderBrowserDialog();

            if(selectCoefficentLOC.ShowDialog() == DialogResult.OK)
            {
                this.textBoxCoefficentStorageLoc.Text = selectCoefficentLOC.SelectedPath;
            }
        }

        private void buttonSelectxCoefficentLOC_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog selectxCoefficentLOC = new FolderBrowserDialog();

            if (selectxCoefficentLOC.ShowDialog() == DialogResult.OK)
            {
                this.textBoxxCalCoefficentStorageLoc.Text = selectxCoefficentLOC.SelectedPath;
            }
        }

        #endregion

    }
}
