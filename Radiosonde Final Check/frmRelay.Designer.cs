﻿namespace Radiosonde_Final_Check
{
    partial class frmRelay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labelSondeSN = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxCR1MIN = new System.Windows.Forms.ComboBox();
            this.comboBoxCR1LEN = new System.Windows.Forms.ComboBox();
            this.comboBoxCR2LEN = new System.Windows.Forms.ComboBox();
            this.comboBoxCR2MIN = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxCR3LEN = new System.Windows.Forms.ComboBox();
            this.comboBoxCR3MIN = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxCR4LEN = new System.Windows.Forms.ComboBox();
            this.comboBoxCR4MIN = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.buttonProgram = new System.Windows.Forms.Button();
            this.buttonTest = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.labelCurrentGPSTime = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBoxEventProgramming = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBoxTest = new System.Windows.Forms.GroupBox();
            this.groupBoxEventProgramming.SuspendLayout();
            this.groupBoxTest.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sonde SN:";
            // 
            // labelSondeSN
            // 
            this.labelSondeSN.BackColor = System.Drawing.SystemColors.Window;
            this.labelSondeSN.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelSondeSN.Location = new System.Drawing.Point(72, 26);
            this.labelSondeSN.Name = "labelSondeSN";
            this.labelSondeSN.Size = new System.Drawing.Size(63, 15);
            this.labelSondeSN.TabIndex = 1;
            this.labelSondeSN.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "CR1:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(174, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Length";
            // 
            // comboBoxCR1MIN
            // 
            this.comboBoxCR1MIN.FormattingEnabled = true;
            this.comboBoxCR1MIN.Location = new System.Drawing.Point(118, 79);
            this.comboBoxCR1MIN.Name = "comboBoxCR1MIN";
            this.comboBoxCR1MIN.Size = new System.Drawing.Size(40, 21);
            this.comboBoxCR1MIN.TabIndex = 6;
            this.comboBoxCR1MIN.Text = "XX";
            this.comboBoxCR1MIN.SelectedIndexChanged += new System.EventHandler(this.comboBoxCR1MIN_SelectedIndexChanged);
            // 
            // comboBoxCR1LEN
            // 
            this.comboBoxCR1LEN.FormattingEnabled = true;
            this.comboBoxCR1LEN.Location = new System.Drawing.Point(177, 79);
            this.comboBoxCR1LEN.Name = "comboBoxCR1LEN";
            this.comboBoxCR1LEN.Size = new System.Drawing.Size(34, 21);
            this.comboBoxCR1LEN.TabIndex = 7;
            this.comboBoxCR1LEN.Text = "X";
            // 
            // comboBoxCR2LEN
            // 
            this.comboBoxCR2LEN.FormattingEnabled = true;
            this.comboBoxCR2LEN.Location = new System.Drawing.Point(177, 111);
            this.comboBoxCR2LEN.Name = "comboBoxCR2LEN";
            this.comboBoxCR2LEN.Size = new System.Drawing.Size(34, 21);
            this.comboBoxCR2LEN.TabIndex = 13;
            this.comboBoxCR2LEN.Text = "X";
            // 
            // comboBoxCR2MIN
            // 
            this.comboBoxCR2MIN.FormattingEnabled = true;
            this.comboBoxCR2MIN.Location = new System.Drawing.Point(118, 111);
            this.comboBoxCR2MIN.Name = "comboBoxCR2MIN";
            this.comboBoxCR2MIN.Size = new System.Drawing.Size(40, 21);
            this.comboBoxCR2MIN.TabIndex = 12;
            this.comboBoxCR2MIN.Text = "XX";
            this.comboBoxCR2MIN.SelectedIndexChanged += new System.EventHandler(this.comboBoxCR2MIN_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(73, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "CR2:";
            // 
            // comboBoxCR3LEN
            // 
            this.comboBoxCR3LEN.FormattingEnabled = true;
            this.comboBoxCR3LEN.Location = new System.Drawing.Point(177, 144);
            this.comboBoxCR3LEN.Name = "comboBoxCR3LEN";
            this.comboBoxCR3LEN.Size = new System.Drawing.Size(34, 21);
            this.comboBoxCR3LEN.TabIndex = 19;
            this.comboBoxCR3LEN.Text = "X";
            // 
            // comboBoxCR3MIN
            // 
            this.comboBoxCR3MIN.FormattingEnabled = true;
            this.comboBoxCR3MIN.Location = new System.Drawing.Point(118, 144);
            this.comboBoxCR3MIN.Name = "comboBoxCR3MIN";
            this.comboBoxCR3MIN.Size = new System.Drawing.Size(40, 21);
            this.comboBoxCR3MIN.TabIndex = 18;
            this.comboBoxCR3MIN.Text = "XX";
            this.comboBoxCR3MIN.SelectedIndexChanged += new System.EventHandler(this.comboBoxCR3MIN_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(73, 147);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "CR3:";
            // 
            // comboBoxCR4LEN
            // 
            this.comboBoxCR4LEN.FormattingEnabled = true;
            this.comboBoxCR4LEN.Location = new System.Drawing.Point(177, 175);
            this.comboBoxCR4LEN.Name = "comboBoxCR4LEN";
            this.comboBoxCR4LEN.Size = new System.Drawing.Size(34, 21);
            this.comboBoxCR4LEN.TabIndex = 25;
            this.comboBoxCR4LEN.Text = "X";
            // 
            // comboBoxCR4MIN
            // 
            this.comboBoxCR4MIN.FormattingEnabled = true;
            this.comboBoxCR4MIN.Location = new System.Drawing.Point(118, 175);
            this.comboBoxCR4MIN.Name = "comboBoxCR4MIN";
            this.comboBoxCR4MIN.Size = new System.Drawing.Size(40, 21);
            this.comboBoxCR4MIN.TabIndex = 24;
            this.comboBoxCR4MIN.Text = "XX";
            this.comboBoxCR4MIN.SelectedIndexChanged += new System.EventHandler(this.comboBoxCR4MIN_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(73, 178);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "CR4:";
            // 
            // buttonProgram
            // 
            this.buttonProgram.Location = new System.Drawing.Point(102, 209);
            this.buttonProgram.Name = "buttonProgram";
            this.buttonProgram.Size = new System.Drawing.Size(75, 23);
            this.buttonProgram.TabIndex = 26;
            this.buttonProgram.Text = "Program";
            this.buttonProgram.UseVisualStyleBackColor = true;
            this.buttonProgram.Click += new System.EventHandler(this.buttonProgram_Click);
            // 
            // buttonTest
            // 
            this.buttonTest.Location = new System.Drawing.Point(109, 80);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(75, 23);
            this.buttonTest.TabIndex = 27;
            this.buttonTest.Text = "Test";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(149, 27);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "GPS Time:";
            // 
            // labelCurrentGPSTime
            // 
            this.labelCurrentGPSTime.BackColor = System.Drawing.SystemColors.Window;
            this.labelCurrentGPSTime.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelCurrentGPSTime.Location = new System.Drawing.Point(209, 26);
            this.labelCurrentGPSTime.Name = "labelCurrentGPSTime";
            this.labelCurrentGPSTime.Size = new System.Drawing.Size(63, 15);
            this.labelCurrentGPSTime.TabIndex = 29;
            this.labelCurrentGPSTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(7, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(273, 61);
            this.label15.TabIndex = 30;
            this.label15.Text = "Testing will erase any event programmed. Please make sure to  program after testi" +
    "ng.";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxEventProgramming
            // 
            this.groupBoxEventProgramming.Controls.Add(this.label16);
            this.groupBoxEventProgramming.Controls.Add(this.label1);
            this.groupBoxEventProgramming.Controls.Add(this.labelSondeSN);
            this.groupBoxEventProgramming.Controls.Add(this.labelCurrentGPSTime);
            this.groupBoxEventProgramming.Controls.Add(this.label3);
            this.groupBoxEventProgramming.Controls.Add(this.label14);
            this.groupBoxEventProgramming.Controls.Add(this.buttonProgram);
            this.groupBoxEventProgramming.Controls.Add(this.label4);
            this.groupBoxEventProgramming.Controls.Add(this.comboBoxCR4LEN);
            this.groupBoxEventProgramming.Controls.Add(this.comboBoxCR1MIN);
            this.groupBoxEventProgramming.Controls.Add(this.comboBoxCR4MIN);
            this.groupBoxEventProgramming.Controls.Add(this.comboBoxCR1LEN);
            this.groupBoxEventProgramming.Controls.Add(this.label7);
            this.groupBoxEventProgramming.Controls.Add(this.label13);
            this.groupBoxEventProgramming.Controls.Add(this.comboBoxCR3LEN);
            this.groupBoxEventProgramming.Controls.Add(this.comboBoxCR2MIN);
            this.groupBoxEventProgramming.Controls.Add(this.comboBoxCR3MIN);
            this.groupBoxEventProgramming.Controls.Add(this.comboBoxCR2LEN);
            this.groupBoxEventProgramming.Controls.Add(this.label10);
            this.groupBoxEventProgramming.Location = new System.Drawing.Point(12, 12);
            this.groupBoxEventProgramming.Name = "groupBoxEventProgramming";
            this.groupBoxEventProgramming.Size = new System.Drawing.Size(286, 249);
            this.groupBoxEventProgramming.TabIndex = 31;
            this.groupBoxEventProgramming.TabStop = false;
            this.groupBoxEventProgramming.Text = "Event Programming";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(124, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "Min";
            // 
            // groupBoxTest
            // 
            this.groupBoxTest.Controls.Add(this.label15);
            this.groupBoxTest.Controls.Add(this.buttonTest);
            this.groupBoxTest.Location = new System.Drawing.Point(12, 282);
            this.groupBoxTest.Name = "groupBoxTest";
            this.groupBoxTest.Size = new System.Drawing.Size(286, 114);
            this.groupBoxTest.TabIndex = 32;
            this.groupBoxTest.TabStop = false;
            this.groupBoxTest.Text = "Testing";
            // 
            // frmRelay
            // 
            this.AcceptButton = this.buttonProgram;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 437);
            this.Controls.Add(this.groupBoxTest);
            this.Controls.Add(this.groupBoxEventProgramming);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRelay";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Relay Program";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmRelay_FormClosed);
            this.Load += new System.EventHandler(this.frmRelay_Load);
            this.groupBoxEventProgramming.ResumeLayout(false);
            this.groupBoxEventProgramming.PerformLayout();
            this.groupBoxTest.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelSondeSN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxCR1MIN;
        private System.Windows.Forms.ComboBox comboBoxCR1LEN;
        private System.Windows.Forms.ComboBox comboBoxCR2LEN;
        private System.Windows.Forms.ComboBox comboBoxCR2MIN;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxCR3LEN;
        private System.Windows.Forms.ComboBox comboBoxCR3MIN;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxCR4LEN;
        private System.Windows.Forms.ComboBox comboBoxCR4MIN;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buttonProgram;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelCurrentGPSTime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBoxEventProgramming;
        private System.Windows.Forms.GroupBox groupBoxTest;
        private System.Windows.Forms.Label label16;
    }
}